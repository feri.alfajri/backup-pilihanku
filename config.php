<?php
// HTTP
define('HTTP_SERVER', 'http://www.pilihanku.net/');
define('HTTPS_SERVER', 'https://www.pilihanku.net/');
define('LISENSI', 'bumitekno.com');
define('URL_DOMAIN', 'http://localhost/pilihanku/');
define('URL_UTAMA', 'http://localhost/pilihanku/');
define('SUBDOMAIN', '');

// DIR
define('DIR_ROUTES', 'app/routes/');
define('DIR_CONTROLLER', 'app/controller/');
define('DIR_MODEL', 'app/model/');
define('DIR_LIBRARY', 'app/library/');
define('DIR_VIEW', 'app/view/');
define('DIR_THEME', 'app/view/theme/');
define('DIR_MODULE', 'app/view/module/');

// DIR FILE
define('DIR_UPLOAD', 'app/upload/');
define('DIR_IMAGE', 'app/upload/image/');
define('DIR_PICTURE', 'app/upload/picture/');
define('DIR_FILE', 'app/upload/file/');
define('DIR_FLYER', 'app/upload/flyer/');
define('DIR_FLYER_UPLOAD', 'app/upload/flyer/upload/');

// URL
define('URL_UPLOAD', 'app/upload/');
define('URL_IMAGE', 'app/upload/image/');
define('URL_PICTURE', 'app/upload/picture/');
define('URL_FILE', 'app/upload/file/');
define('URL_FLYER', 'app/upload/flyer/');
define('URL_THEME', 'app/view/theme/');

// THEME
define('THEME','mitech');
define('THEME_MEMBER', 'sbadmin2');
define('THEME_ADMIN', 'sbadmin2');
define('THEME_SUPER', 'sbadminpro');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'pilihanku');
define('DB_ERROR', '<center><h2>MOHON MAAF, KAMI KELEBIHAN BEBAN<br>MOHON KUNJUNGI KAMI BEBERAPA SAAT LAGI...!</h2></center>');
define('DB_PORT', '3306');

// SMTP SETTING
define('SMTP_KONEKSI', 'ssl');
define('SMTP_PORT', 465);
define('SMTP_HOST', 'smtp.gmail.com');
define('SMTP_USERNAME', 'smtpmypro@gmail.com'); 
define('SMTP_PASSWORD', '31031997');
define('ASAL_EMAIL', 'info@pilihanku.net');

//SETTING WHATSAPP API
define('URLWA_MEDIA', 'https://wa-notif.bumidev.com/send-media');
define('URLWA_POST', 'https://tekno.bumidev.com/app/api/send-message');
define('URLWA_JADWAL', 'https://tekno.bumidev.com/api/send_jadwal.php');
define('KEY_WA', 'O76zfHRHIgG4WfTz7xBl7Pbs6BQM2XJO');
define('SENDER_WA', '6281235067234');
define('IMAGE_WA', 'https://www.desago.id/cms_profile/upload/image/logo.png');

//SETTING METATAG GOOGLE
$nameweb = 'PilihanKu.Net';
$titleweb = 'PilihanKu.Net';
$metadesc = 'PilihanKu.Net';	
$metakey = 'PilihanKu.Net';	
$logo = 'logo.png';
$favicon = 'favicon.png';	
$footer = '© 2022 copyright. All rights reserved.';	

//ADDRESS
$address = '<b>PILIHANKU.NET</b><br/>Kebumen, Kec. Pringsurat ';
$city = 'Kab. Temanggung';
$phone = '081390957111';
$mobile = '081390957111';
$fax = '';
$email = 'info@pilihanku.id';
$facebook = '';
$twitter = '';
$instagram = '';
$youtube = '';
$tiktok = '';
$linkedin = '';

?>