<?php
session_start();

$host = $_SERVER['HTTP_HOST'];
$host_obj = explode(".", $host);
if($host_obj[0] == 'www'){  $subdomain = $host_obj[1]; } 
else {  $subdomain = $host_obj[0]; }

//include 'ssl.php';
include 'config.php';
include DIR_MODEL.'model.php';


define('SUBDOMAIN', $subdomain);
define('URL_DOMAIN', 'http://'.$subdomain.'.pilihanku.net/');
define('URL_MEMBER', 'https://www.pilihanku.net/member/');
define('URL_UTAMA', 'https://www.pilihanku.net/');

$model=new model();
$model->koneksi();	
$model->get_variable();	

include DIR_MODEL.'web_setting.php';
include DIR_MODEL.'url_variable.php';

define('THEME',$theme);

if ($jumsetting==0) {	
	echo ERROR_NOWEB; 
} 
else {	
	if ($menu=='logout') {	
		unset($_SESSION['name']); 
		unset($_SESSION['uname']); 
		unset($_SESSION['pword']); 
		unset($_SESSION['cat']);
		unset($_SESSION['pict']);
		header("location:".URL_DOMAIN); 
		// remove all session variables
		session_unset();

		// destroy the session
		session_destroy();
	} 
	elseif ($menu=='adm') {	 
		include DIR_ROUTES.'admin.php'; 	
	} 
	else {
	    include DIR_ROUTES.'subdomain.php';
		
	}
}	
	
$model->putus();
?>
