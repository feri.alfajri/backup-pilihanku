<?php
// HTTP
error_reporting(0);

define('HTTP_SERVER', 'http://www.pilihanku.net/');
define('HTTPS_SERVER', 'https://www.pilihanku.net/');
define('LISENSI', 'bumitekno.com');


// DIR
define('DIR_ROUTES', '/home/pilihanku/public_html/app/routes/');
define('DIR_CONTROLLER', '/home/pilihanku/public_html/app/controller/');
define('DIR_MODEL', '/home/pilihanku/public_html/app/model/');
define('DIR_LIBRARY', '/home/pilihanku/public_html/app/library/');
define('DIR_VIEW', '/home/pilihanku/public_html/app/view/');
define('DIR_THEME', '/home/pilihanku/public_html/app/view/theme/');
define('DIR_MODULE', '/home/pilihanku/public_html/app/view/module/');
define('DIR_AJAX', '/home/pilihanku/public_html/app/ajax/');

// DIR FILE
define('DIR_UPLOAD', '/home/pilihanku/public_html/app/upload/');
define('DIR_IMAGE', '/home/pilihanku/public_html/app/upload/image/');
define('DIR_PICTURE', '/home/pilihanku/public_html/app/upload/picture/');
define('DIR_FILE', '/home/pilihanku/public_html/app/upload/file/');
define('DIR_FLYER', '/home/pilihanku/public_html/app/upload/flyer/');

// URL
define('URL_UPLOAD', 'https://www.pilihanku.net/app/upload/');
define('URL_IMAGE', 'https://www.pilihanku.net/app/upload/image/');
define('URL_PICTURE', 'https://www.pilihanku.net/app/upload/picture/');
define('URL_FILE', 'https://www.pilihanku.net/app/upload/file/');
define('URL_FLYER', 'https://www.pilihanku.net/app/upload/flyer/');
define('URL_THEME', 'https://www.pilihanku.net/app/view/theme/');

// THEME
define('THEME_MEMBER', 'sbadmin2');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'pilihanku_root');
define('DB_PASSWORD', 'u@&m)#a*^m-cpanel');
define('DB_DATABASE', 'pilihanku_dbs');
define('DB_ERROR', '<center><h2>MOHON MAAF, KAMI KELEBIHAN BEBAN<br>MOHON KUNJUNGI KAMI BEBERAPA SAAT LAGI...!</h2></center>');
define('DB_PORT', '3306');

// SMTP SETTING
define('SMTP_KONEKSI', 'ssl');
define('SMTP_PORT', 465);
define('SMTP_HOST', 'smtp.gmail.com');
define('SMTP_USERNAME', 'smtpmypro@gmail.com'); 
define('SMTP_PASSWORD', '31031997');
define('ASAL_EMAIL', 'budimark@gmail.com');

define('ERROR_NOWEB', '<center><h2>MOHON MAAF, WEBSITE YANG ANDA CARI TIDAK DITEMUKAN...!</h2></center>');
?>