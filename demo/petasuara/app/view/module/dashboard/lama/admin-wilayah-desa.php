<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_ADMIN.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_ADMIN.'/app/sidebar.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_ADMIN.'/app/topbar.php'; ?>
				<?php
				if (empty($_GET['no'])) { $no_desa = 1; } else { $no_desa = $_GET['no']; }
				$qdesa = "SELECT * FROM desa WHERE no = '$no_desa'";
				$sqldesa = mysqli_query($model->koneksi, $qdesa);
				$ddesa= mysqli_fetch_array($sqldesa);
				$name_desa = $ddesa['name'];
				$jumlah_tps = $ddesa['jumlah_tps'];
				//SUARA
				$qsuara = "SELECT 
					SUM(tps_1) as tps_1, SUM(tps_2) as tps_2, SUM(tps_3) as tps_3, SUM(tps_4) as tps_4, SUM(tps_5) as tps_5,
					SUM(tps_6) as tps_6, SUM(tps_7) as tps_7, SUM(tps_8) as tps_8, SUM(tps_9) as tps_9, SUM(tps_10) as tps_10,
					SUM(tps_11) as tps_11, SUM(tps_12) as tps_12, SUM(tps_13) as tps_13, SUM(tps_14) as tps_14, SUM(tps_15) as tps_15,
					SUM(tps_16) as tps_16, SUM(tps_17) as tps_17, SUM(tps_18) as tps_18, SUM(tps_19) as tps_19, SUM(tps_20) as tps_20,
					SUM(tps_21) as tps_21, SUM(tps_22) as tps_22, SUM(tps_23) as tps_23, SUM(tps_24) as tps_24, SUM(tps_25) as tps_25					
					FROM suara WHERE no_desa = '$no_desa' ";
				$sqlsuara = mysqli_query ($model->koneksi, $qsuara);
				$dsuara = mysqli_fetch_array ($sqlsuara);
				$tps_1 = $dsuara['tps_1']; $tps_2 = $dsuara['tps_2']; $tps_3 = $dsuara['tps_3']; $tps_4 = $dsuara['tps_4']; $tps_5 = $dsuara['tps_5'];
				$tps_6 = $dsuara['tps_6']; $tps_7 = $dsuara['tps_7']; $tps_8 = $dsuara['tps_8']; $tps_9 = $dsuara['tps_9']; $tps_10 = $dsuara['tps_10'];
				$tps_11 = $dsuara['tps_11']; $tps_12 = $dsuara['tps_12']; $tps_13 = $dsuara['tps_13']; $tps_14 = $dsuara['tps_14']; $tps_15 = $dsuara['tps_15'];
				$tps_16 = $dsuara['tps_16']; $tps_17 = $dsuara['tps_17']; $tps_18 = $dsuara['tps_18']; $tps_19 = $dsuara['tps_19']; $tps_20 = $dsuara['tps_20'];	
				$tps_21 = $dsuara['tps_21']; $tps_22 = $dsuara['tps_22']; $tps_23 = $dsuara['tps_23']; $tps_24 = $dsuara['tps_24']; $tps_25 = $dsuara['tps_25'];
				$total_suara = $tps_1+$tps_2+$tps_3+$tps_4+$tps_5+$tps_6+$tps_7+$tps_8+$tps_9+$tps_10+$tps_11+$tps_12+$tps_13+$tps_14+$tps_15+$tps_16+$tps_17+$tps_18+$tps_19+$tps_20+$tps_21+$tps_22+$tps_23+$tps_24+$tps_25; 
				
				//SUARA PKS
				$no_partai = 8;
				$qsuara_pks = "SELECT 
					SUM(tps_1) as tps_1, SUM(tps_2) as tps_2, SUM(tps_3) as tps_3, SUM(tps_4) as tps_4, SUM(tps_5) as tps_5,
					SUM(tps_6) as tps_6, SUM(tps_7) as tps_7, SUM(tps_8) as tps_8, SUM(tps_9) as tps_9, SUM(tps_10) as tps_10,
					SUM(tps_11) as tps_11, SUM(tps_12) as tps_12, SUM(tps_13) as tps_13, SUM(tps_14) as tps_14, SUM(tps_15) as tps_15,
					SUM(tps_16) as tps_16, SUM(tps_17) as tps_17, SUM(tps_18) as tps_18, SUM(tps_19) as tps_19, SUM(tps_20) as tps_20,
					SUM(tps_21) as tps_21, SUM(tps_22) as tps_22, SUM(tps_23) as tps_23, SUM(tps_24) as tps_24, SUM(tps_25) as tps_25	
					FROM suara WHERE no_desa = '$no_desa' AND no_partai = '8' ";
				$sqlsuara_pks = mysqli_query ($model->koneksi, $qsuara_pks);
				$dsuara_pks = mysqli_fetch_array ($sqlsuara_pks);
				$tps_1 = $dsuara_pks['tps_1']; $tps_2 = $dsuara_pks['tps_2']; $tps_3 = $dsuara_pks['tps_3']; $tps_4 = $dsuara_pks['tps_4']; $tps_5 = $dsuara_pks['tps_5'];
				$tps_6 = $dsuara_pks['tps_6']; $tps_7 = $dsuara_pks['tps_7']; $tps_8 = $dsuara_pks['tps_8']; $tps_9 = $dsuara_pks['tps_9']; $tps_10 = $dsuara_pks['tps_10'];
				$tps_11 = $dsuara_pks['tps_11']; $tps_12 = $dsuara_pks['tps_12']; $tps_13 = $dsuara_pks['tps_13']; $tps_14 = $dsuara_pks['tps_14']; $tps_15 = $dsuara_pks['tps_15'];
				$tps_16 = $dsuara_pks['tps_16']; $tps_17 = $dsuara_pks['tps_17']; $tps_18 = $dsuara_pks['tps_18']; $tps_19 = $dsuara_pks['tps_19']; $tps_20 = $dsuara_pks['tps_20'];	
				$tps_21 = $dsuara_pks['tps_21']; $tps_22 = $dsuara_pks['tps_22']; $tps_23 = $dsuara_pks['tps_23']; $tps_24 = $dsuara_pks['tps_24']; $tps_25 = $dsuara_pks['tps_25'];
				$total_suara_pks = $tps_1+$tps_2+$tps_3+$tps_4+$tps_5+$tps_6+$tps_7+$tps_8+$tps_9+$tps_10+$tps_11+$tps_12+$tps_13+$tps_14+$tps_15+$tps_16+$tps_17+$tps_18+$tps_19+$tps_20+$tps_21+$tps_22+$tps_23+$tps_24+$tps_25; 
				//JUMLAH TPS
				$qdesa = "SELECT jumlah_tps FROM desa WHERE no='$no_desa'";
				$sqldesa = mysqli_query($model->koneksi, $qdesa);
				$ddesa = mysqli_fetch_array($sqldesa);
				$jumlah_tps = $ddesa['jumlah_tps'];
				//TPS BASIS
				if ($tps_1>30) { $basis_1 = 1; } else { $basis_1 = 0; } 			if ($tps_11>30) { $basis_11 = 1; } else { $basis_11 = 0; }  			if ($tps_21>30) { $basis_21 = 1; } else { $basis_21 = 0; }  
				if ($tps_2>30) { $basis_2 = 1; } else { $basis_2 = 0; } 			if ($tps_12>30) { $basis_12 = 1; } else { $basis_12 = 0; } 				if ($tps_22>30) { $basis_22 = 1; } else { $basis_22 = 0; } 
				if ($tps_3>30) { $basis_3 = 1; } else { $basis_3 = 0; } 			if ($tps_13>30) { $basis_13 = 1; } else { $basis_13 = 0; } 				if ($tps_23>30) { $basis_23 = 1; } else { $basis_23 = 0; } 
				if ($tps_4>30) { $basis_4 = 1; } else { $basis_4 = 0; } 			if ($tps_14>30) { $basis_14 = 1; } else { $basis_14 = 0; } 				if ($tps_24>30) { $basis_24 = 1; } else { $basis_24 = 0; } 
				if ($tps_5>30) { $basis_5 = 1; } else { $basis_5 = 0; } 			if ($tps_15>30) { $basis_15 = 1; } else { $basis_15 = 0; } 				if ($tps_25>30) { $basis_25 = 1; } else { $basis_25 = 0; } 
				if ($tps_6>30) { $basis_6 = 1; } else { $basis_6 = 0; } 			if ($tps_16>30) { $basis_16 = 1; } else { $basis_16 = 0; } 
				if ($tps_7>30) { $basis_7 = 1; } else { $basis_7 = 0; } 			if ($tps_17>30) { $basis_17 = 1; } else { $basis_17 = 0; } 
				if ($tps_8>30) { $basis_8 = 1; } else { $basis_8 = 0; } 			if ($tps_18>30) { $basis_18 = 1; } else { $basis_18 = 0; } 
				if ($tps_9>30) { $basis_9 = 1; } else { $basis_9 = 0; } 			if ($tps_19>30) { $basis_19 = 1; } else { $basis_19 = 0; } 
				if ($tps_10>30) { $basis_10 = 1; } else { $basis_10 = 0; } 			if ($tps_20>30) { $basis_20 = 1; } else { $basis_20 = 0; } 				
				$total_basis_pks = $basis_1+$basis_2+$basis_3+$basis_4+$basis_5+$basis_6+$basis_7+$basis_8+$basis_9+$basis_10+$basis_11+$basis_12+$basis_13+$basis_14+$basis_15+$basis_16+$basis_17+$basis_18+$basis_19+$basis_20+$basis_21+$basis_22+$basis_23+$basis_24+$basis_25; 
				$url_default = URL_DOMAIN.$menu.'/'.$link;
				?>
				<div class="container-fluid">
				    <h2>Dashboard Desa <?php echo $name_desa;?></h2>
					<div class="row">
						<div class="col-md-3">
							<div class="card bg-danger">
								<div class="card-body">
									<h6>Total Suara</h6>
									<h1 class="text-right"><?php echo number_format($total_suara,0,',','.'); ?></h1>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="card bg-warning">
								<div class="card-body">
									<h6>Suara PKS</h6>
									<h1 class="text-right"><?php echo number_format($total_suara_pks,0,',','.'); ?></h1>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="card bg-success">
								<div class="card-body">
									<h6>Jumlah TPS</h6>
									<h1 class="text-right"><?php echo $jumlah_tps;?></h1>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="card bg-primary">
								<div class="card-body">
									<h6>TPS Basis (Suara>30)</h6>
									<h1 class="text-right"><?php echo $total_basis_pks;?></h1>
								</div>
							</div>
						</div>
					</div>
					<br/>
					
					
					<div class="row">
						<div class="col-md-4">
							<div class="card">
								<div class="card-header h6 bg-secondary">Top Suara Partai</div>
								<div class="card-body">
									<table width="100%" class="table table-striped table-bordered table-hover">
										<?php
										$qpartai = "SELECT  no, name,( SELECT SUM(total) FROM suara WHERE no_desa='$no_desa' AND suara.no_partai = partai.no ) as jumlahsuara
											FROM partai ORDER BY jumlahsuara DESC LIMIT 10";
										$sqlpartai = mysqli_query ($model->koneksi, $qpartai);
										while ($dpartai = mysqli_fetch_array ($sqlpartai)) {
											$no_partai = $dpartai['no'];
											$name_partai = $dpartai['name'];
											$suara_partai = $dpartai['jumlahsuara'];
											$url_dashboard_partai = $url_default.'/'.$no_partai.'/partai';
											?>
											<tr>
												<td><a href="<?php echo $url_dashboard_partai;?>" title="Dashboard Partai <?php echo $name_partai;?>"><?php echo $name_partai;?></a></td>
												<td class="text-right" width="80"><?php echo number_format($suara_partai,0,',','.');?></td>
											</tr>
											<?php
										}
										?>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="card">
								<div class="card-header h6 bg-secondary">Top Suara Caleg</div>
								<div class="card-body">
									<table width="100%" class="table table-striped table-bordered table-hover">
										<?php
										$qcaleg = "SELECT  no, name,( SELECT SUM(total) FROM suara WHERE no_desa='$no_desa' AND suara.no_caleg = caleg.no) as jumlahsuara
											FROM caleg WHERE nomor_urut !='0' ORDER BY jumlahsuara DESC LIMIT 10";
										$sqlcaleg = mysqli_query ($model->koneksi, $qcaleg);
										while ($dcaleg = mysqli_fetch_array ($sqlcaleg)) {
											$no_caleg = $dcaleg['no'];
											$name_caleg = $dcaleg['name'];
											$suara_caleg = $dcaleg['jumlahsuara'];
											$url_dashboard_caleg = $url_default.'/'.$no_caleg.'/caleg';
											?>
											<tr>
												<td><a href="<?php echo $url_dashboard_caleg;?>" title="Dashboard Partai <?php echo $name_caleg;?>"><?php echo $name_caleg;?></a></td>
												<td class="text-right" width="80"><?php echo number_format($suara_caleg,0,',','.');?></td>
											</tr>
											<?php
										}
										?>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="card">
								<div class="card-header h6  bg-secondary">Top Suara Caleg PKS</div>
								<div class="card-body">
									<table width="100%" class="table table-striped table-bordered table-hover">
										<?php
										$qcaleg = "SELECT  no, name,( SELECT SUM(total) FROM suara WHERE no_desa='$no_desa' AND suara.no_caleg = caleg.no) as jumlahsuara
											FROM caleg WHERE no_partai ='8' ORDER BY jumlahsuara DESC LIMIT 10";
										$sqlcaleg = mysqli_query ($model->koneksi, $qcaleg);
										while ($dcaleg = mysqli_fetch_array ($sqlcaleg)) {
											$no_caleg = $dcaleg['no'];
											$name_caleg = $dcaleg['name'];
											$suara_caleg = $dcaleg['jumlahsuara'];
											$url_dashboard_caleg = $url_default.'/'.$no_caleg.'/caleg';
											?>
											<tr>
												<td><a href="<?php echo $url_dashboard_caleg;?>" title="Dashboard Partai <?php echo $name_caleg;?>"><?php echo $name_caleg;?></a></td>
												<td class="text-right" width="80"><?php echo number_format($suara_caleg,0,',','.');?></td>
											</tr>
											<?php
										}
										?>
									</table>
								</div>
							</div>
						</div>
					</div>
					<br/>
					
					
					<div class="card">
						<div class="card-header h6 bg-info">Suara Per TPS</div>
						<div class="card-body">
							<table width="100%" class="table table-striped table-bordered table-hover">
								<tr class="text-center">
									<th width="5%">TPS</th>
									<th class="text-left">Dusun</th>
									<?php
									$qpartai = "SELECT no,name FROM partai WHERE big = '1' ORDER BY no ASC";
									$sqlpartai = mysqli_query($model->koneksi, $qpartai);
									while ($dpartai = mysqli_fetch_array($sqlpartai)) {
										$no_partai = $dpartai['no'];
										$name_partai = $dpartai['name'];
										if ($no_partai==8) { $bgtable = '#FFCC88'; } else { $bgtable = 'none'; }?>
										<th width="8%" style="background:<?php echo $bgtable;?>"><?php echo $name_partai; ?></th>
										<?php
									}
									?>
								</tr>
								<?php
								$url_default = URL_DOMAIN.$menu.'/'.$link.'/'.$no_desa.'/desa/page';
								$nomor = 1;
								for ($i = 1; $i<=$jumlah_tps; $i++) {
									$qtps = "SELECT no_dusun FROM tps WHERE no_desa = '$no_desa' AND tps = '$i'";
									$sqltps = mysqli_query($model->koneksi, $qtps);
									$jtps = mysqli_num_rows($sqltps);
									if ($jtps == 0) {
										$name_dusun = 'Nama Dusun';
									}
									else {
										$dtps = mysqli_fetch_array($sqltps);
										$no_dusun = $dtps['no_dusun'];
										$qdusun = "SELECT name FROM dusun WHERE no = '$no_dusun'";
										$sqldusun = mysqli_query($model->koneksi, $qdusun);
										$ddusun = mysqli_fetch_array($sqldusun);
										$name_dusun = $ddusun['name'];
									}
									$url_dashboard_tps = $url_default.'/'.$i;
									?>
									<tr class="text-center">
										<td><?php echo $i;?></td>
										<td class="text-left"><a href="<?php echo $url_dashboard_tps;?>" title="Dashboard TPS"><?php echo $name_dusun;?></a></td>
										<?php
										$qpartai = "SELECT no FROM partai WHERE big = '1' ORDER BY no ASC";
										$sqlpartai = mysqli_query($model->koneksi, $qpartai);
										while ($dpartai = mysqli_fetch_array($sqlpartai)) {
											$no_partai = $dpartai['no'];
											if ($no_partai==8) { $bgtable = '#FFCC88'; } else { $bgtable = 'none'; }
											$label_tps = 'tps_'.$i;
											$qsuara = "SELECT SUM($label_tps) as jumlahsuara FROM suara WHERE no_desa = '$no_desa' AND no_partai = '$no_partai'";
											$sqlsuara = mysqli_query ($model->koneksi, $qsuara);
											$dsuara = mysqli_fetch_array ($sqlsuara);
											$suara_partai = $dsuara['jumlahsuara'];
											?>
											<td style="background:<?php echo $bgtable;?>"><?php echo number_format($suara_partai,0,',','.'); ?></td>
											<?php
										}
										?>
									</tr>
									<?php		
								}
								?>
							</table>
						</div>
					</div>
				</div>
			
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_ADMIN.'/app/asset-body.php'; ?>
	
</body>
 
</html>