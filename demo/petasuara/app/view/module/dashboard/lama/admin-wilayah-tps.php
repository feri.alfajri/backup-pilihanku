<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_ADMIN.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_ADMIN.'/app/sidebar.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_ADMIN.'/app/topbar.php'; ?>
				<?php
				if (empty($_GET['no'])) { $no_desa = 1; } else { $no_desa = $_GET['no']; }
				if (empty($_GET['page'])) { $tps = 1; } else { $tps = $_GET['page']; }
				$qdesa = "SELECT * FROM desa WHERE no = '$no_desa'";
				$sqldesa = mysqli_query($model->koneksi, $qdesa);
				$ddesa= mysqli_fetch_array($sqldesa);
				$name_desa = $ddesa['name'];
				$no_kecamatan = $ddesa['no_kecamatan'];
				$no_kabupaten = $ddesa['no_kabupaten'];
				$labeltps = 'tps_'.$tps;
				$qtps = "SELECT no_dusun FROM tps WHERE no_desa = '$no_desa' AND tps = '$tps'";
				$sqltps = mysqli_query($model->koneksi, $qtps);
				$jtps = mysqli_num_rows($sqltps);
				if ($jtps == 0) {
					$name_dusun = 'Nama Dusun';
					$no_dusun = 0;
				}
				else {
					$dtps = mysqli_fetch_array($sqltps);
					$no_dusun = $dtps['no_dusun'];
					$qdusun = "SELECT name FROM dusun WHERE no = '$no_dusun'";
					$sqldusun = mysqli_query($model->koneksi, $qdusun);
					$ddusun = mysqli_fetch_array($sqldusun);
					$name_dusun = $ddusun['name'];
				}
				//SUARA
				$qsuara = "SELECT  SUM($labeltps) as $labeltps FROM suara WHERE no_desa = '$no_desa' ";
				$sqlsuara = mysqli_query ($model->koneksi, $qsuara);
				$dsuara = mysqli_fetch_array ($sqlsuara);
				$total_suara = $dsuara[$labeltps];
				
				//SUARA PKS
				$no_partai = 8;
				$qsuara_pks = "SELECT  SUM($labeltps) as $labeltps FROM suara WHERE no_desa = '$no_desa' AND no_partai = '8' ";
				$sqlsuara_pks = mysqli_query ($model->koneksi, $qsuara_pks);
				$dsuara_pks = mysqli_fetch_array ($sqlsuara_pks);
				$total_suara_pks = $dsuara_pks[$labeltps];
				$url_default = URL_DOMAIN.$menu.'/'.$link;
				
				//ZONASI
				$qzonasi = "SELECT * FROM zonasi WHERE tps = '$tps' AND no_desa='$no_desa'";
				$sqlzonasi = mysqli_query($model->koneksi, $qzonasi);
				$jzonasi = mysqli_num_rows($sqlzonasi);
				if ($jzonasi>0) {
					$dzonasi = mysqli_fetch_array($sqlzonasi);
					$zona = $dzonasi['zona'];
				}
				else {
					$zona = '';
				}
				if (empty ($_POST['process'])) {
				}
				elseif ($_POST['process'] == 'save')  {		
					$zona = $_POST['zona'];
					if ($jzonasi>0) {
						$qinput = " UPDATE zonasi SET zona = '$zona' WHERE tps = '$tps' AND no_desa='$no_desa' ";
					}
					else {
						$qtopcaleg = "SELECT  no, name,( SELECT SUM($labeltps) FROM suara WHERE no_desa='$no_desa' AND suara.no_caleg = caleg.no) as jumlahsuara
						FROM caleg WHERE nomor_urut !='0' ORDER BY jumlahsuara DESC LIMIT 1";
						$sqltopcaleg = mysqli_query ($model->koneksi, $qtopcaleg);
						$dtopcaleg = mysqli_fetch_array ($sqltopcaleg);
						$topcaleg = $dtopcaleg['name'];
						$topcalegsuara = $dtopcaleg['jumlahsuara'];
						$qtoppartai = "SELECT  no, name,( SELECT SUM($labeltps) FROM suara WHERE no_desa='$no_desa' AND suara.no_partai = partai.no ) as jumlahsuara
							FROM partai ORDER BY jumlahsuara DESC LIMIT 1";
						$sqltoppartai = mysqli_query ($model->koneksi, $qtoppartai);
						$dtoppartai = mysqli_fetch_array ($sqltoppartai);
						$toppartai = $dtoppartai['name'];
						$toppartaisuara = $dtoppartai['jumlahsuara'];
						$qtoppks = "SELECT SUM($labeltps) as jumlahsuara FROM suara WHERE no_desa='$no_desa' AND no_partai ='8' ";
						$sqltoppks = mysqli_query ($model->koneksi, $qtoppks);
						$dtoppks = mysqli_fetch_array ($sqltoppks);
						$toppkssuara = $dtoppks['jumlahsuara'];						
						$qinput = "INSERT INTO zonasi SET 
							zona = '$zona',
							tps = '$tps',
							no_dusun = '$no_dusun',
							no_desa = '$no_desa',
							no_kecamatan = '$no_kecamatan',
							no_kabupaten = '$no_kabupaten',
							topcaleg = '$topcaleg', 
							toppartai = '$toppartai', 
							suara_topcaleg = '$topcalegsuara', 
							suara_toppartai = '$toppartaisuara', 
							suara_pks = '$toppkssuara'
							";
					}
					mysqli_query ($model->koneksi, $qinput);
					?>
					<!--<div class="container-fluid"><div class="bg-success p-2">Zona Berhasil Diupdate</div></div>-->
					<?php
				}
				?>
				<div class="container-fluid">					
				    <h2>Dashboard Desa <?php echo $name_desa;?> TPS <?php echo $tps;?> / <?php echo $name_dusun;?></h2>
					<div class="row">
						<div class="col-md-4">
							<div class="card bg-danger">
								<div class="card-body">
									<h6>Total Suara</h6>
									<h1 class="text-right"><?php echo number_format($total_suara,0,',','.'); ?></h1>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="card bg-warning">
								<div class="card-body">
									<h6>Suara PKS</h6>
									<h1 class="text-right"><?php echo number_format($total_suara_pks,0,',','.'); ?></h1>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="card bg-success">
								<div class="card-body">
									<h6>Masukkan Ke Zona :</h6>
									<form action="" method="post">
										<input type="hidden" name="process" value="save">
										<div class="row">
											<div class="col-md-9">
												<select name="zona" class="form-control h5">
													<?php
													if ($zona=='hijau')  { $labelzona='Ambil Alih'; }
													elseif ($zona=='kuning') { $labelzona='Tarung'; }
													elseif ($zona=='orange') { $labelzona='Pertahankan'; }
													elseif ($zona=='merah') { $labelzona='Hindari'; }
													else { $labelzona='Pilih Zona'; }
													?>
													<option value="<?php echo $zona;?>"><?php echo $labelzona;?></option>
													<option value="hijau">Ambil Alih</option>
													<option value="kuning">Tarung</option>
													<option value="orange">Pertahankan</option>
													<option value="merah">Hindari</option>
												</select>
											</div>
											<div class="col-md-3 text-right">
											<input type="submit" name="submit" value="OK" class="h5 btn btn-primary"/>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<br/>
					
					
					<div class="row">
						<div class="col-md-4">
							<div class="card">
								<div class="card-header h6 bg-secondary">Top Suara Partai</div>
								<div class="card-body">
									<table width="100%" class="table table-striped table-bordered table-hover">
										<?php
										$qpartai = "SELECT  no, name,( SELECT SUM($labeltps) FROM suara WHERE no_desa='$no_desa' AND suara.no_partai = partai.no ) as jumlahsuara
											FROM partai ORDER BY jumlahsuara DESC LIMIT 10";
										$sqlpartai = mysqli_query ($model->koneksi, $qpartai);
										while ($dpartai = mysqli_fetch_array ($sqlpartai)) {
											$no_partai = $dpartai['no'];
											$name_partai = $dpartai['name'];
											$suara_partai = $dpartai['jumlahsuara'];
											$url_dashboard_partai = $url_default.'/'.$no_partai.'/partai';
											?>
											<tr>
												<td><a href="<?php echo $url_dashboard_partai;?>" title="Dashboard Partai <?php echo $name_partai;?>"><?php echo $name_partai;?></a></td>
												<td class="text-right" width="80"><?php echo number_format($suara_partai,0,',','.');?></td>
											</tr>
											<?php
										}
										?>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="card">
								<div class="card-header h6 bg-secondary">Top Suara Caleg</div>
								<div class="card-body">
									<table width="100%" class="table table-striped table-bordered table-hover">
										<?php
										$qcaleg = "SELECT  no, name,( SELECT SUM($labeltps) FROM suara WHERE no_desa='$no_desa' AND suara.no_caleg = caleg.no) as jumlahsuara
											FROM caleg WHERE nomor_urut !='0' ORDER BY jumlahsuara DESC LIMIT 10";
										$sqlcaleg = mysqli_query ($model->koneksi, $qcaleg);
										while ($dcaleg = mysqli_fetch_array ($sqlcaleg)) {
											$no_caleg = $dcaleg['no'];
											$name_caleg = $dcaleg['name'];
											$suara_caleg = $dcaleg['jumlahsuara'];
											$url_dashboard_caleg = $url_default.'/'.$no_caleg.'/caleg';
											?>
											<tr>
												<td><a href="<?php echo $url_dashboard_caleg;?>" title="Dashboard Partai <?php echo $name_caleg;?>"><?php echo $name_caleg;?></a></td>
												<td class="text-right" width="80"><?php echo number_format($suara_caleg,0,',','.');?></td>
											</tr>
											<?php
										}
										?>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="card">
								<div class="card-header h6  bg-secondary">Top Suara Caleg PKS</div>
								<div class="card-body">
									<table width="100%" class="table table-striped table-bordered table-hover">
										<?php
										$qcaleg = "SELECT  no, name,( SELECT SUM($labeltps) FROM suara WHERE no_desa='$no_desa' AND suara.no_caleg = caleg.no) as jumlahsuara
											FROM caleg WHERE no_partai ='8' ORDER BY jumlahsuara DESC LIMIT 10";
										$sqlcaleg = mysqli_query ($model->koneksi, $qcaleg);
										while ($dcaleg = mysqli_fetch_array ($sqlcaleg)) {
											$no_caleg = $dcaleg['no'];
											$name_caleg = $dcaleg['name'];
											$suara_caleg = $dcaleg['jumlahsuara'];
											$url_dashboard_caleg = $url_default.'/'.$no_caleg.'/caleg';
											?>
											<tr>
												<td><a href="<?php echo $url_dashboard_caleg;?>" title="Dashboard Partai <?php echo $name_caleg;?>"><?php echo $name_caleg;?></a></td>
												<td class="text-right" width="80"><?php echo number_format($suara_caleg,0,',','.');?></td>
											</tr>
											<?php
										}
										?>
									</table>
								</div>
							</div>
						</div>
					</div>
					<br/>
					
				</div>
			
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_ADMIN.'/app/asset-body.php'; ?>
	
</body>
 
</html>