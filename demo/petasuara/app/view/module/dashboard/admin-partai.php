<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_ADMIN.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_ADMIN.'/app/sidebar.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_ADMIN.'/app/topbar.php'; ?>
				<?php
				$uname=$_SESSION['uname'];
				if ($uname=='dapil1') { $no_dapil = 1; } elseif ($uname=='dapil2') { $no_dapil = 2; } elseif ($uname=='dapil3') { $no_dapil = 3; }
				elseif ($uname=='dapil4') { $no_dapil = 4; } elseif ($uname=='dapil5') { $no_dapil = 5; } elseif ($uname=='dapil6') { $no_dapil = 6; }
				else { $no_dapil = 6; }
				if (empty($_GET['no'])) { $no_partai = 1; } else { $no_partai = $_GET['no']; }
				$qpartai = "SELECT * FROM partai WHERE no = '$no_partai'";
				$sqlpartai = mysqli_query($model->koneksi, $qpartai);
				$dpartai = mysqli_fetch_array($sqlpartai);
				$name_partai = $dpartai['name'];
				//SUARA
				$qsuara = "SELECT 
					SUM(tps_1) as tps_1, SUM(tps_2) as tps_2, SUM(tps_3) as tps_3, SUM(tps_4) as tps_4, SUM(tps_5) as tps_5,
					SUM(tps_6) as tps_6, SUM(tps_7) as tps_7, SUM(tps_8) as tps_8, SUM(tps_9) as tps_9, SUM(tps_10) as tps_10,
					SUM(tps_11) as tps_11, SUM(tps_12) as tps_12, SUM(tps_13) as tps_13, SUM(tps_14) as tps_14, SUM(tps_15) as tps_15,
					SUM(tps_16) as tps_16, SUM(tps_17) as tps_17, SUM(tps_18) as tps_18, SUM(tps_19) as tps_19, SUM(tps_20) as tps_20,
					SUM(tps_21) as tps_21, SUM(tps_22) as tps_22, SUM(tps_23) as tps_23, SUM(tps_24) as tps_24, SUM(tps_25) as tps_25					
					FROM suara WHERE no_dapil = '$no_dapil' ";
				$sqlsuara = mysqli_query ($model->koneksi, $qsuara);
				$dsuara = mysqli_fetch_array ($sqlsuara);
				$tps_1 = $dsuara['tps_1']; $tps_2 = $dsuara['tps_2']; $tps_3 = $dsuara['tps_3']; $tps_4 = $dsuara['tps_4']; $tps_5 = $dsuara['tps_5'];
				$tps_6 = $dsuara['tps_6']; $tps_7 = $dsuara['tps_7']; $tps_8 = $dsuara['tps_8']; $tps_9 = $dsuara['tps_9']; $tps_10 = $dsuara['tps_10'];
				$tps_11 = $dsuara['tps_11']; $tps_12 = $dsuara['tps_12']; $tps_13 = $dsuara['tps_13']; $tps_14 = $dsuara['tps_14']; $tps_15 = $dsuara['tps_15'];
				$tps_16 = $dsuara['tps_16']; $tps_17 = $dsuara['tps_17']; $tps_18 = $dsuara['tps_18']; $tps_19 = $dsuara['tps_19']; $tps_20 = $dsuara['tps_20'];	
				$tps_21 = $dsuara['tps_21']; $tps_22 = $dsuara['tps_22']; $tps_23 = $dsuara['tps_23']; $tps_24 = $dsuara['tps_24']; $tps_25 = $dsuara['tps_25'];
				$total_suara = $tps_1+$tps_2+$tps_3+$tps_4+$tps_5+$tps_6+$tps_7+$tps_8+$tps_9+$tps_10+$tps_11+$tps_12+$tps_13+$tps_14+$tps_15+$tps_16+$tps_17+$tps_18+$tps_19+$tps_20+$tps_21+$tps_22+$tps_23+$tps_24+$tps_25; 
				
				//SUARA PARTAI
				$qsuara_partai = "SELECT 
					SUM(tps_1) as tps_1, SUM(tps_2) as tps_2, SUM(tps_3) as tps_3, SUM(tps_4) as tps_4, SUM(tps_5) as tps_5,
					SUM(tps_6) as tps_6, SUM(tps_7) as tps_7, SUM(tps_8) as tps_8, SUM(tps_9) as tps_9, SUM(tps_10) as tps_10,
					SUM(tps_11) as tps_11, SUM(tps_12) as tps_12, SUM(tps_13) as tps_13, SUM(tps_14) as tps_14, SUM(tps_15) as tps_15,
					SUM(tps_16) as tps_16, SUM(tps_17) as tps_17, SUM(tps_18) as tps_18, SUM(tps_19) as tps_19, SUM(tps_20) as tps_20,
					SUM(tps_21) as tps_21, SUM(tps_22) as tps_22, SUM(tps_23) as tps_23, SUM(tps_24) as tps_24, SUM(tps_25) as tps_25	
					FROM suara WHERE no_dapil = '$no_dapil' AND no_partai = '$no_partai' ";
				$sqlsuara_partai = mysqli_query ($model->koneksi, $qsuara_partai);
				$dsuara_partai = mysqli_fetch_array ($sqlsuara_partai);
				$tps_1 = $dsuara_partai['tps_1']; $tps_2 = $dsuara_partai['tps_2']; $tps_3 = $dsuara_partai['tps_3']; $tps_4 = $dsuara_partai['tps_4']; $tps_5 = $dsuara_partai['tps_5'];
				$tps_6 = $dsuara_partai['tps_6']; $tps_7 = $dsuara_partai['tps_7']; $tps_8 = $dsuara_partai['tps_8']; $tps_9 = $dsuara_partai['tps_9']; $tps_10 = $dsuara_partai['tps_10'];
				$tps_11 = $dsuara_partai['tps_11']; $tps_12 = $dsuara_partai['tps_12']; $tps_13 = $dsuara_partai['tps_13']; $tps_14 = $dsuara_partai['tps_14']; $tps_15 = $dsuara_partai['tps_15'];
				$tps_16 = $dsuara_partai['tps_16']; $tps_17 = $dsuara_partai['tps_17']; $tps_18 = $dsuara_partai['tps_18']; $tps_19 = $dsuara_partai['tps_19']; $tps_20 = $dsuara_partai['tps_20'];	
				$tps_21 = $dsuara_partai['tps_21']; $tps_22 = $dsuara_partai['tps_22']; $tps_23 = $dsuara_partai['tps_23']; $tps_24 = $dsuara_partai['tps_24']; $tps_25 = $dsuara_partai['tps_25'];
				$total_suara_partai = $tps_1+$tps_2+$tps_3+$tps_4+$tps_5+$tps_6+$tps_7+$tps_8+$tps_9+$tps_10+$tps_11+$tps_12+$tps_13+$tps_14+$tps_15+$tps_16+$tps_17+$tps_18+$tps_19+$tps_20+$tps_21+$tps_22+$tps_23+$tps_24+$tps_25; 
				//JUMLAH TPS
				$qdesa = "SELECT SUM(jumlah_tps) as jumlah_tps FROM desa WHERE no_dapil = '$no_dapil'";
				$sqldesa = mysqli_query($model->koneksi, $qdesa);
				$ddesa = mysqli_fetch_array($sqldesa);
				$jumlah_tps = $ddesa['jumlah_tps'];
				//DESA BASIS
				if ($tps_1>30) { $basis_1 = 1; } else { $basis_1 = 0; } 			if ($tps_11>30) { $basis_11 = 1; } else { $basis_11 = 0; }  			if ($tps_21>30) { $basis_21 = 1; } else { $basis_21 = 0; }  
				if ($tps_2>30) { $basis_2 = 1; } else { $basis_2 = 0; } 			if ($tps_12>30) { $basis_12 = 1; } else { $basis_12 = 0; } 				if ($tps_22>30) { $basis_22 = 1; } else { $basis_22 = 0; } 
				if ($tps_3>30) { $basis_3 = 1; } else { $basis_3 = 0; } 			if ($tps_13>30) { $basis_13 = 1; } else { $basis_13 = 0; } 				if ($tps_23>30) { $basis_23 = 1; } else { $basis_23 = 0; } 
				if ($tps_4>30) { $basis_4 = 1; } else { $basis_4 = 0; } 			if ($tps_14>30) { $basis_14 = 1; } else { $basis_14 = 0; } 				if ($tps_24>30) { $basis_24 = 1; } else { $basis_24 = 0; } 
				if ($tps_5>30) { $basis_5 = 1; } else { $basis_5 = 0; } 			if ($tps_15>30) { $basis_15 = 1; } else { $basis_15 = 0; } 				if ($tps_25>30) { $basis_25 = 1; } else { $basis_25 = 0; } 
				if ($tps_6>30) { $basis_6 = 1; } else { $basis_6 = 0; } 			if ($tps_16>30) { $basis_16 = 1; } else { $basis_16 = 0; } 
				if ($tps_7>30) { $basis_7 = 1; } else { $basis_7 = 0; } 			if ($tps_17>30) { $basis_17 = 1; } else { $basis_17 = 0; } 
				if ($tps_8>30) { $basis_8 = 1; } else { $basis_8 = 0; } 			if ($tps_18>30) { $basis_18 = 1; } else { $basis_18 = 0; } 
				if ($tps_9>30) { $basis_9 = 1; } else { $basis_9 = 0; } 			if ($tps_19>30) { $basis_19 = 1; } else { $basis_19 = 0; } 
				if ($tps_10>30) { $basis_10 = 1; } else { $basis_10 = 0; } 			if ($tps_20>30) { $basis_20 = 1; } else { $basis_20 = 0; } 				
				$total_basis_pks = $basis_1+$basis_2+$basis_3+$basis_4+$basis_5+$basis_6+$basis_7+$basis_8+$basis_9+$basis_10+$basis_11+$basis_12+$basis_13+$basis_14+$basis_15+$basis_16+$basis_17+$basis_18+$basis_19+$basis_20+$basis_21+$basis_22+$basis_23+$basis_24+$basis_25; 
				?>
				<div class="container-fluid">
				    <h2>Dashboard Partai <?php echo $name_partai;?></h2>
					<div class="row">
						<div class="col-md-3">
							<div class="card bg-danger">
								<div class="card-body">
									<h6>Total Suara Kecamatan</h6>
									<h1 class="text-right"><?php echo number_format($total_suara,0,',','.'); ?></h1>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="card bg-warning">
								<div class="card-body">
									<h6>Total Suara <?php echo $name_partai;?></h6>
									<h1 class="text-right"><?php echo number_format($total_suara_partai,0,',','.'); ?></h1>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="card bg-success">
								<div class="card-body">
									<h6>Jumlah TPS</h6>
									<h1 class="text-right"><?php echo $jumlah_tps;?></h1>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="card bg-primary">
								<div class="card-body">
									<h6>TPS Basis (Suara>30)</h6>
									<h1 class="text-right"><?php echo $total_basis_pks;?></h1>
								</div>
							</div>
						</div>
					</div>
					<br/>
					

					<div class="row">
						<div class="col-md-6">
							<div class="card">
								<div class="card-header h6 bg-secondary">Top Suara Desa</div>
								<div class="card-body">
									<table width="100%" class="table table-striped table-bordered table-hover">
										<?php
										$url_default = URL_DOMAIN.$menu.'/'.$link;
										$qdesa = "SELECT  no, name,( SELECT SUM(total) FROM suara WHERE no_partai = '$no_partai' AND suara.no_desa = desa.no ) as jumlahsuara
											FROM desa WHERE no_dapil = '$no_dapil' ORDER BY jumlahsuara DESC LIMIT 10";
										$sqldesa = mysqli_query ($model->koneksi, $qdesa);
										while ($ddesa = mysqli_fetch_array ($sqldesa)) {
											$desaNo = $ddesa['no'];
											$desaName = $ddesa['name'];
											$desaSuara = $ddesa['jumlahsuara'];
											$url_dashboard_desa = $url_default.'/'.$desaNo.'/desa';
											?>
											<tr>
												<td><a href="<?php echo $url_dashboard_desa;?>" title="Dashboard Desa <?php echo $desaName;?>"><?php echo $desaName;?></td>
												<td class="text-right" width="80"><?php echo number_format($desaSuara,0,',','.');?></td>
											</tr>
											<?php
										}
										?>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="card">
								<div class="card-header h6  bg-secondary">Top Suara Caleg <?php echo $name_partai;?></div>
								<div class="card-body">
									<table width="100%" class="table table-striped table-bordered table-hover">
										<?php
										$qcaleg = "SELECT  no, name,( SELECT SUM(total) FROM suara WHERE no_dapil = '$no_dapil' AND suara.no_caleg = caleg.no) as jumlahsuara
											FROM caleg WHERE no_partai ='$no_partai' AND no_dapil = '$no_dapil' ORDER BY jumlahsuara DESC LIMIT 10";
										$sqlcaleg = mysqli_query ($model->koneksi, $qcaleg);
										while ($dcaleg = mysqli_fetch_array ($sqlcaleg)) {
											$no_caleg = $dcaleg['no'];
											$name_caleg = $dcaleg['name'];
											$suara_caleg = $dcaleg['jumlahsuara'];
											$url_dashboard_caleg = $url_default.'/'.$no_caleg.'/caleg';
											?>
											<tr>
												<td><a href="<?php echo $url_dashboard_caleg;?>" title="Dashboard Desa <?php echo $name_caleg;?>"><?php echo $name_caleg;?></a></td>
												<td class="text-right" width="80"><?php echo number_format($suara_caleg,0,',','.');?></td>
											</tr>
											<?php
										}
										?>
									</table>
								</div>
							</div>
						</div>
					</div>
					<br/>

					<div class="card">
						<div class="card-header h6 bg-info">Suara Per Desa</div>
						<div class="card-body">
						    <?php
							$url_default = URL_DOMAIN.$menu.'/'.$link;
							$qkec = "SELECT no,name FROM kecamatan WHERE no_dapil = '$no_dapil' ORDER BY no ASC";
							$sqlkec = mysqli_query($model->koneksi, $qkec);
							while ($dkec = mysqli_fetch_array($sqlkec)) {
								$no_kecamatan = $dkec['no'];
								$name_kecamatan = $dkec['name'];
								$url_dashboard_kecamatan = $url_default.'/'.$no_kecamatan.'/kecamatan';
								?>
								<h4><a href="<?php echo $url_dashboard_kecamatan;?>" title="Dashboard Kecamatan <?php echo $name_kecamatan;?>"><?php echo $name_kecamatan;?></a></h4>
								
    							<table width="100%" class="table table-striped table-bordered table-hover">
    								<tr class="text-center">
    									<th width="1%">No</th>
    									<th width="15%" class="text-left">Desa</th>
    									<?php
    									$qcaleg = "SELECT no,name FROM caleg WHERE no_partai = '$no_partai' ORDER BY nomor_urut ASC";
    									$sqlcaleg = mysqli_query($model->koneksi, $qcaleg);
    									while ($dcaleg = mysqli_fetch_array($sqlcaleg)) {
    										$no_caleg = $dcaleg['no'];
    										$name_caleg = $dcaleg['name'];
    										if ($no_caleg==8) { $bgtable = '#FFCC88'; } else { $bgtable = 'none'; }?>
    										<th width="11%" style="background:<?php echo $bgtable;?>"><?php echo $name_caleg; ?></th>
    										<?php
    									}
    									?>
    								</tr>
    								<?php
    								
    								$url_default = URL_DOMAIN.$menu.'/'.$link;
    								$nomor = 1;
    								$qdesa = "SELECT no,name FROM desa WHERE no_kecamatan = '$no_kecamatan' ORDER BY name ASC";
    								$sqldesa = mysqli_query($model->koneksi, $qdesa);
    								while ($ddesa = mysqli_fetch_array($sqldesa)) {
    									$no_desa = $ddesa['no'];
    									$name_desa = $ddesa['name'];
    									$url_dashboard_desa = $url_default.'/'.$no_desa.'/desa';
    									?>
    									<tr class="text-center">
    										<td><?php echo $nomor;?></td>
    										<td class="text-left"><a href="<?php echo $url_dashboard_desa;?>" title="Dashboard Desa <?php echo $name_desa;?>"><?php echo $name_desa; ?></a></td>
    										<?php
    										
    										$qcaleg = "SELECT no,name FROM caleg WHERE no_partai = '$no_partai' ORDER BY nomor_urut ASC";
    										$sqlcaleg = mysqli_query($model->koneksi, $qcaleg);
    										while ($dcaleg = mysqli_fetch_array($sqlcaleg)) {
    											$no_caleg = $dcaleg['no'];
    											$qsuara = "SELECT 
    												SUM(tps_1) as tps_1, SUM(tps_2) as tps_2, SUM(tps_3) as tps_3, SUM(tps_4) as tps_4, SUM(tps_5) as tps_5,
    												SUM(tps_6) as tps_6, SUM(tps_7) as tps_7, SUM(tps_8) as tps_8, SUM(tps_9) as tps_9, SUM(tps_10) as tps_10,
    												SUM(tps_11) as tps_11, SUM(tps_12) as tps_12, SUM(tps_13) as tps_13, SUM(tps_14) as tps_14, SUM(tps_15) as tps_15,
    												SUM(tps_16) as tps_16, SUM(tps_17) as tps_17, SUM(tps_18) as tps_18, SUM(tps_19) as tps_19, SUM(tps_20) as tps_20,					
    												SUM(tps_21) as tps_21, SUM(tps_22) as tps_22, SUM(tps_23) as tps_23, SUM(tps_24) as tps_24, SUM(tps_25) as tps_25	
    												FROM suara WHERE no_kecamatan = '$no_kecamatan' AND no_desa ='$no_desa' AND no_partai = '$no_partai' AND  no_caleg = '$no_caleg'";
    											$sqlsuara = mysqli_query ($model->koneksi, $qsuara);
    											$dsuara = mysqli_fetch_array ($sqlsuara);
    											$tps_1 = $dsuara['tps_1']; $tps_2 = $dsuara['tps_2']; $tps_3 = $dsuara['tps_3']; $tps_4 = $dsuara['tps_4']; $tps_5 = $dsuara['tps_5'];
    											$tps_6 = $dsuara['tps_6']; $tps_7 = $dsuara['tps_7']; $tps_8 = $dsuara['tps_8']; $tps_9 = $dsuara['tps_9']; $tps_10 = $dsuara['tps_10'];
    											$tps_11 = $dsuara['tps_11']; $tps_12 = $dsuara['tps_12']; $tps_13 = $dsuara['tps_13']; $tps_14 = $dsuara['tps_14']; $tps_15 = $dsuara['tps_15'];
    											$tps_16 = $dsuara['tps_16']; $tps_17 = $dsuara['tps_17']; $tps_18 = $dsuara['tps_18']; $tps_19 = $dsuara['tps_19']; $tps_20 = $dsuara['tps_20'];				
    											$tps_21 = $dsuara['tps_21']; $tps_22 = $dsuara['tps_22']; $tps_23 = $dsuara['tps_23']; $tps_24 = $dsuara['tps_24']; $tps_25 = $dsuara['tps_25'];
    											$total_suara = $tps_1+$tps_2+$tps_3+$tps_4+$tps_5+$tps_6+$tps_7+$tps_8+$tps_9+$tps_10+$tps_11+$tps_12+$tps_13+$tps_14+$tps_15+$tps_16+$tps_17+$tps_18+$tps_19+$tps_20+$tps_21+$tps_22+$tps_23+$tps_24+$tps_25;  
    											
    											?>
    											<td><?php echo number_format($total_suara,0,',','.'); ?></td>
    											<?php
    										}
    										
    										?>
    									</tr>
    									<?php
    									$nomor++;
    								}
    								?>
    							</table>
    							<?php
							}
							?>
						</div>
					</div>
				</div>
			
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_ADMIN.'/app/asset-body.php'; ?>
	
</body>
 
</html>