<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_ADMIN.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_ADMIN.'/app/sidebar.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_ADMIN.'/app/topbar.php'; ?>
				
				<div class="container-fluid">				
					<?php
					
					$uname = $_SESSION['uname'];
					$table_name = 'user';							
					$update_type = '';
					$update_label = 'nama,alamat,kota,provinsi,telepon,email';
					$update_form = 'name,address,city,province,phone,email';
					$update_condition = "WHERE username='$uname'";							
					
					$admin->title('Ubah '.$titlepage);
					$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					?>
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_ADMIN.'/app/asset-body.php'; ?>
	
</body>
 
</html>