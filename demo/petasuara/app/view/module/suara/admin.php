<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_ADMIN.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_ADMIN.'/app/sidebar.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_ADMIN.'/app/topbar.php'; ?>
				<?php
				$uname = $_SESSION['uname']; 
				if ($uname=='dapil1') { $no_dapil = 1; $no_desa_awal = 74; } 
				elseif ($uname=='dapil2') { $no_dapil = 2; $no_desa_awal = 1; } 
				elseif ($uname=='dapil3') { $no_dapil = 3; $no_desa_awal = 204; }
				elseif ($uname=='dapil4') { $no_dapil = 4; $no_desa_awal = 1; } 
				elseif ($uname=='dapil5') { $no_dapil = 5; $no_desa_awal = 1; } 
				elseif ($uname=='dapil6') { $no_dapil = 6; $no_desa_awal = 1; }
				else { $no_dapil = 6; $no_desa_awal = 1; }
				?>
				<div class="container-fluid">
				    <div class="row">
						<div class="col-md-9"></div>
						<div class="col-md-3">
							<form action="" method="post"> 
								<input type="hidden" name="process" value="desa"/>
								<div class="row">
									<div class="col-md-8">
										<select name="no_desa" class="form-control">
											<option value="0">Pilih Desa</option><?php
											$qkec = "SELECT * FROM kecamatan WHERE no_dapil = '$no_dapil' ORDER BY no ASC";
											$sqlkec = mysqli_query ($model->koneksi, $qkec);
											while ($dkec = mysqli_fetch_array ($sqlkec)) {
												$no_kecamatan = $dkec['no'];
												$name_kecamatan = $dkec['name'];
												$qdesa = "SELECT * FROM desa WHERE no_kecamatan = '$no_kecamatan' ORDER BY name ASC";
												$sqldesa = mysqli_query ($model->koneksi, $qdesa);
												while ($ddesa = mysqli_fetch_array ($sqldesa)) {
													?>
													<option value="<?php echo $ddesa['no'];?>"><?php echo $name_kecamatan.' - '.$ddesa['name'];?></option>
													<?php
												}
											}
											?>
										</select>
									</div>
									<div class="col-md-4">
									<input type="submit" name="submit" value="LANJUT" class="btn btn-primary"/>
									</div>
								</div>
							</form>
						</div>
					</div>
					<?php
					if (empty($_POST['process'])) { $no_desa = $no_desa_awal; }
					else { $no_desa = $_POST['no_desa']; }					
					$qdesa = "SELECT * FROM desa WHERE no = '$no_desa'";
					$sqldesa = mysqli_query($model->koneksi, $qdesa);
					$ddesa = mysqli_fetch_array($sqldesa);
					$name_desa = $ddesa['name'];
					$jumlah_tps = $ddesa['jumlah_tps'];
					$no_kecamatan = $ddesa['no_kecamatan'];
					$qkec = "SELECT * FROM kecamatan WHERE no = '$no_kecamatan'";
					$sqlkec = mysqli_query($model->koneksi, $qkec);
					$dkec = mysqli_fetch_array($sqlkec);
					$no_dapil = $dkec['no_dapil'];
					$name_kecamatan = $dkec['name'];
					?>
					<h3>Perolehan Suara<br/>Desa <?php echo $name_desa;?> Kec. <?php echo $name_kecamatan;?></h3>
					<table width="100%" class="table table-striped table-bordered table-hover">
						<thead>
							<tr class="text-center">
								<th class="text-left" rowspan="2">PEROLEHAN SUARA <br/>PARTAI DAN CALON</th>
								<th colspan="<?php echo $jumlah_tps;?>">TPS</th>
								<th width="60" rowspan="2">JUMLAH<br/>AKHIR</th>
							</tr>
							<tr class="text-center">
								<?php
								for ($i=1; $i<=$jumlah_tps; $i++) {            
									?><th width="50"><?php echo $i;?></th><?php
								}
								?>
								
							</tr>
						</thead>
						<tbody>
							<?php
							$qpartai = "SELECT no,name FROM partai WHERE publish = '1' ORDER BY no ASC";
							$sqlpartai = mysqli_query($model->koneksi, $qpartai);
							while ($dpartai = mysqli_fetch_array($sqlpartai)) {
								$partaiNo = $dpartai['no'];
								$partaiName = $dpartai['name'];
								?>
								<tr class="text-center">
									<td class="text-uppercase text-left" style="background:#ccc;"><?php echo $partaiNo.'. '.$partaiName;?></td>
									<?php
									$jumsuarapartai = 0;
									for ($i=1; $i<=$jumlah_tps; $i++) {  
										$notps = 'tps_'.$i;
										$qsuara = "SELECT * FROM suara WHERE no_desa = $no_desa AND no_partai = '$partaiNo' AND nomor_urut = '0' ";
										$sqlsuara = mysqli_query ($model->koneksi, $qsuara);
										$jsuara = mysqli_num_rows ($sqlsuara);
										if ($jsuara==0) { $suara = 0; }
										else { $dsuara = mysqli_fetch_array($sqlsuara); $suara = $dsuara[$notps];  } 
										$jumsuarapartai = $jumsuarapartai + $suara;
										?>
										<td><?php echo $suara;?></td>
										<?php
									}
									?>
									<td><?php echo $jumsuarapartai;?></td>
								</tr>
								<?php
								$qcaleg = "SELECT * FROM caleg WHERE no_partai = '$partaiNo' AND no_dapil = '$no_dapil' AND nomor_urut != '0' ORDER BY nomor_urut ASC";
								$sqlcaleg = mysqli_query ($model->koneksi, $qcaleg);
								while ($dcaleg = mysqli_fetch_array ($sqlcaleg)) {
									$calegNo = $dcaleg['no'];
									$calegName = $dcaleg['name'];
									$calegNoUrut = $dcaleg['nomor_urut'];
									$calegIncumbent = $dcaleg['incumbent'];
									?>
									<tr class="text-center">
										<td class="text-left"><?php echo $calegNoUrut.'. '.$calegName;?></td>
										<?php
										$jumsuaracaleg = 0;
										for ($i=1; $i<=$jumlah_tps; $i++) {       
											$notps = 'tps_'.$i;
											$qsuara = "SELECT * FROM suara WHERE no_desa = $no_desa AND no_partai = '$partaiNo' AND nomor_urut = '$calegNoUrut' ";
											$sqlsuara = mysqli_query ($model->koneksi, $qsuara);
											$jsuara = mysqli_num_rows ($sqlsuara);
											if ($jsuara==0) { $suara = 0; }
											else { $dsuara = mysqli_fetch_array ($sqlsuara); $suara = $dsuara[$notps];  } 
											$jumsuaracaleg = $jumsuaracaleg + $suara;
											if ($suara>=30) { 
												if ($calegIncumbent==1) { $bgnote='#FF6666'; } 
												else { 
													if ($partaiNo==8) { $bgnote='#FFAA22';  } else { $bgnote='#66FF66'; }
												} 
											} 
											else { $bgnote='none'; 
											} 
											?>
											<td style="background:<?php echo $bgnote;?>"><?php echo $suara;?></td>
											<?php
										}
										?>
										<td><?php echo $jumsuaracaleg;?></td>
									</tr>
									<?php
								}
								?>
								<tr class="text-center">
									<th class="text-left">Jumlah Partai & Caleg</th>
									<?php
									$totalsuara = 0;
									for ($i=1; $i<=$jumlah_tps; $i++) {   
										$notps = 'tps_'.$i;
										$qsuara = "SELECT SUM($notps) FROM suara WHERE no_desa = $no_desa AND no_partai = '$partaiNo'";
										$sqlsuara = mysqli_query ($model->koneksi, $qsuara);
										$dsuara = mysqli_fetch_array ($sqlsuara); 
										$jumsuara = $dsuara[0];
										?>
										<th><?php echo $jumsuara;?></th>
										<?php
										$totalsuara = $totalsuara + $jumsuara;;
									}
									?>
									<th><?php echo $totalsuara;?></th>
								</tr>
								<tr><td></td></tr>
								<?php
							}
							?>
						</tbody>
					</table>
                    
				</div>
			
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_ADMIN.'/app/asset-body.php'; ?>
	
</body>
 
</html>