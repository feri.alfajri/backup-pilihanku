<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_ADMIN.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_ADMIN.'/app/sidebar.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_ADMIN.'/app/topbar.php'; ?>
				<?php 
					$uname = $_SESSION['uname']; 
				if ($uname=='dapil1') { $no_dapil = 1; } elseif ($uname=='dapil2') { $no_dapil = 2; } elseif ($uname=='dapil3') { $no_dapil = 3; }
				elseif ($uname=='dapil4') { $no_dapil = 4; } elseif ($uname=='dapil5') { $no_dapil = 5; } elseif ($uname=='dapil6') { $no_dapil = 6; }
				else { $no_dapil = 6; }
				//SUARA TOP PARTAI
				$qzonasi = "SELECT SUM(suara_topcaleg) FROM zonasi WHERE no_dapil = '$no_dapil' AND zona = 'orange'";
				$sqlzonasi = mysqli_query($model->koneksi, $qzonasi);
				$dzonasi = mysqli_fetch_array($sqlzonasi);
				$suara_top = $dzonasi[0];
				//SUARA PKS
				$qzonasi = "SELECT SUM(suara_pks) FROM zonasi WHERE no_dapil = '$no_dapil' AND zona = 'orange'";
				$sqlzonasi = mysqli_query($model->koneksi, $qzonasi);
				$dzonasi = mysqli_fetch_array($sqlzonasi);
				$suara_pks = $dzonasi[0];
				//JUMLAH TPS
				$qzonasi = "SELECT no FROM zonasi WHERE no_dapil = '$no_dapil' AND zona = 'orange'";
				$sqlzonasi = mysqli_query($model->koneksi, $qzonasi);
				$jzonasi = mysqli_num_rows($sqlzonasi);
				?>
				<div class="container-fluid">
				    <h2>Zona Hindari</h2>
					
					
					<div class="card">
						<div class="card-header h6 bg-danger">Berdasarkan Jumlah Suara Dominan (Suara > 70)</div>
						<div class="card-body">
							<?php
							function table_row ($tps,$no_desa,$no_kecamatan) {
								$admin=new admin();
								$admin->get_variable();
								$admin->koneksi();
								$qtps = "SELECT * FROM tps WHERE no_desa = '$no_desa' AND tps = '$tps'";
								$sqltps = mysqli_query($admin->koneksi, $qtps);
								$jtps = mysqli_num_rows($sqltps);											
								if ($jtps == 0) { $nama_dusun = 'Nama Dusun'; $address=''; } 
								else {
									$dtps = mysqli_fetch_array($sqltps);
									$no_dusun = $dtps['no_dusun'];
									$address = $dtps['address'];
									$qdusun = "SELECT name FROM dusun WHERE no_desa = '$no_desa' AND no = '$no_dusun'";
									$sqldusun = mysqli_query($admin->koneksi, $qdusun);
									$ddusun = mysqli_fetch_array($sqldusun);
									$nama_dusun = $ddusun['name'];
								}
								$labeltps = 'tps_'.$tps;
								$qtoppartai = "SELECT  no, name,( SELECT SUM($labeltps) FROM suara WHERE no_desa='$no_desa' AND suara.no_partai = partai.no ) as jumlahsuara
									FROM partai ORDER BY jumlahsuara DESC LIMIT 1";
								$sqltoppartai = mysqli_query ($admin->koneksi, $qtoppartai);
								$dtoppartai = mysqli_fetch_array ($sqltoppartai);
								$toppartai = $dtoppartai['name'];
								$toppartaisuara = $dtoppartai['jumlahsuara'];
								
								
								$qsuara_pks = "SELECT  SUM($labeltps) as $labeltps FROM suara WHERE no_desa = '$no_desa' AND no_partai = '8'";
								$sqlsuara_pks = mysqli_query ($admin->koneksi, $qsuara_pks);
								$dsuara_pks = mysqli_fetch_array ($sqlsuara_pks);
								$total_suara_pks = $dsuara_pks[$labeltps];
								
								?>
								<tr class="text-center">
									<td><?php echo $tps;?></td>
									<td class="text-left"><?php echo $nama_dusun;?></td>
									<td class="text-left"><?php echo $address;?></td>
									<td><?php echo $toppartai.' ('.$toppartaisuara.')';?></td>
									<td><?php echo $total_suara_pks;?></td>
								</tr>
								<?php
							}
									
									
									
							$min = 100;
							$qdesa = "SELECT no,name FROM desa WHERE no_dapil = '$no_dapil' ORDER BY no ASC";
							$sqldesa = mysqli_query($model->koneksi, $qdesa);
							while ($ddesa = mysqli_fetch_array($sqldesa)) {
								$no_desa = $ddesa['no'];
								$name_desa = $ddesa['name'];
							 	$qsuara = "SELECT 
									SUM(tps_1) as tps_1, SUM(tps_2) as tps_2, SUM(tps_3) as tps_3, SUM(tps_4) as tps_4, SUM(tps_5) as tps_5,
									SUM(tps_6) as tps_6, SUM(tps_7) as tps_7, SUM(tps_8) as tps_8, SUM(tps_9) as tps_9, SUM(tps_10) as tps_10,
									SUM(tps_11) as tps_11, SUM(tps_12) as tps_12, SUM(tps_13) as tps_13, SUM(tps_14) as tps_14, SUM(tps_15) as tps_15,
									SUM(tps_16) as tps_16, SUM(tps_17) as tps_17, SUM(tps_18) as tps_18, SUM(tps_19) as tps_19, SUM(tps_20) as tps_20,
									SUM(tps_21) as tps_21, SUM(tps_22) as tps_22, SUM(tps_23) as tps_23, SUM(tps_24) as tps_24, SUM(tps_25) as tps_25					
									FROM suara WHERE no_desa = '$no_desa' AND 
									(
										(no_partai = '1' AND nomor_urut='1') OR 
										(no_partai = '2' AND nomor_urut='1') OR 
										(no_partai = '3' AND nomor_urut='1') OR 
										(no_partai = '3' AND nomor_urut='2') OR 
										(no_partai = '4' AND nomor_urut='1') OR
										(no_partai = '10' AND nomor_urut='1')
									)

									";
								$sqlsuara = mysqli_query ($model->koneksi, $qsuara);
								$dsuara = mysqli_fetch_array ($sqlsuara);
								$tps_1 = $dsuara['tps_1']; $tps_2 = $dsuara['tps_2']; $tps_3 = $dsuara['tps_3']; $tps_4 = $dsuara['tps_4']; $tps_5 = $dsuara['tps_5'];
								$tps_6 = $dsuara['tps_6']; $tps_7 = $dsuara['tps_7']; $tps_8 = $dsuara['tps_8']; $tps_9 = $dsuara['tps_9']; $tps_10 = $dsuara['tps_10'];
								$tps_11 = $dsuara['tps_11']; $tps_12 = $dsuara['tps_12']; $tps_13 = $dsuara['tps_13']; $tps_14 = $dsuara['tps_14']; $tps_15 = $dsuara['tps_15'];
								$tps_16 = $dsuara['tps_16']; $tps_17 = $dsuara['tps_17']; $tps_18 = $dsuara['tps_18']; $tps_19 = $dsuara['tps_19']; $tps_20 = $dsuara['tps_20'];	
								$tps_21 = $dsuara['tps_21']; $tps_22 = $dsuara['tps_22']; $tps_23 = $dsuara['tps_23']; $tps_24 = $dsuara['tps_24']; $tps_25 = $dsuara['tps_25'];
								if ($tps_1>=$min or $tps_2>=$min or $tps_3>=$min or $tps_4>=$min or $tps_5>=$min or $tps_6>=$min or $tps_7>=$min or $tps_8>=$min or $tps_9>=$min or $tps_10>=$min or
									$tps_11>=$min or $tps_12>=$min or $tps_13>=$min or $tps_14>=$min or $tps_15>=$min or $tps_16>=$min or $tps_17>=$min or $tps_18>=$min or $tps_19>=$min or $tps_20>=$min or
									$tps_21>=$min or $tps_22>=$min or $tps_23>=$min or $tps_24>=$min or $tps_25>=$min ) { $muncul=1; } else { $muncul=0; }
								if ($muncul == 1) {
									?>
									<h4><?php echo $name_desa; ?></h4>
									<table width="100%" class="table table-striped table-bordered table-hover">
										<tr class="text-center">
											<th width="5%" class="text-left">TPS</th>
											<th width="17%" class="text-left">Dusun</th>
											<th class="text-left">Alamat</th>
											<th width="12%">Top Partai</th>
											<th width="10%">Suara PKS</th>
										</tr>
										<?php
										if ($tps_1>=$min) { table_row(1,$no_desa,$no_kecamatan); }
										if ($tps_2>=$min) { table_row(2,$no_desa,$no_kecamatan); }
										if ($tps_3>=$min) { table_row(3,$no_desa,$no_kecamatan); }
										if ($tps_4>=$min) { table_row(4,$no_desa,$no_kecamatan); }
										if ($tps_5>=$min) { table_row(5,$no_desa,$no_kecamatan); }
										if ($tps_6>=$min) { table_row(6,$no_desa,$no_kecamatan); }
										if ($tps_7>=$min) { table_row(7,$no_desa,$no_kecamatan); }
										if ($tps_8>=$min) { table_row(8,$no_desa,$no_kecamatan); }
										if ($tps_9>=$min) { table_row(9,$no_desa,$no_kecamatan); }
										if ($tps_10>=$min) { table_row(10,$no_desa,$no_kecamatan); }
										if ($tps_11>=$min) { table_row(11,$no_desa,$no_kecamatan); }
										if ($tps_12>=$min) { table_row(12,$no_desa,$no_kecamatan); }
										if ($tps_13>=$min) { table_row(13,$no_desa,$no_kecamatan); }
										if ($tps_14>=$min) { table_row(14,$no_desa,$no_kecamatan); }
										if ($tps_15>=$min) { table_row(15,$no_desa,$no_kecamatan); }
										if ($tps_16>=$min) { table_row(16,$no_desa,$no_kecamatan); }
										if ($tps_17>=$min) { table_row(17,$no_desa,$no_kecamatan); }
										if ($tps_18>=$min) { table_row(18,$no_desa,$no_kecamatan); }
										if ($tps_19>=$min) { table_row(19,$no_desa,$no_kecamatan); }
										if ($tps_10>=$min) { table_row(20,$no_desa,$no_kecamatan); }
										if ($tps_21>=$min) { table_row(21,$no_desa,$no_kecamatan); }
										if ($tps_22>=$min) { table_row(22,$no_desa,$no_kecamatan); }
										if ($tps_23>=$min) { table_row(23,$no_desa,$no_kecamatan); }
										if ($tps_24>=$min) { table_row(24,$no_desa,$no_kecamatan); }
										if ($tps_25>=$min) { table_row(25,$no_desa,$no_kecamatan); }
										?>
									</table>
									<?php
								}
							}
							?>							
						</div>
					</div>
					<br/>
					
							
							
				</div>
			
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_ADMIN.'/app/asset-body.php'; ?>
	
</body>
 
</html>