<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_ADMIN.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_ADMIN.'/app/sidebar.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_ADMIN.'/app/topbar.php'; ?>
				<?php 
				$uname = $_SESSION['uname']; 
				if ($uname=='dapil1') { $no_dapil = 1; } elseif ($uname=='dapil2') { $no_dapil = 2; } elseif ($uname=='dapil3') { $no_dapil = 3; }
				elseif ($uname=='dapil4') { $no_dapil = 4; } elseif ($uname=='dapil5') { $no_dapil = 5; } elseif ($uname=='dapil6') { $no_dapil = 6; }
				else { $no_dapil = 6; }
				//SUARA TOP PARTAI
				$qzonasi = "SELECT SUM(suara_toppartai) FROM zonasi WHERE no_dapil = '$no_dapil' AND zona = 'hijau'";
				$sqlzonasi = mysqli_query($model->koneksi, $qzonasi);
				$dzonasi = mysqli_fetch_array($sqlzonasi);
				$suara_top = $dzonasi[0];
				//SUARA PKS
				$qzonasi = "SELECT SUM(suara_pks) FROM zonasi WHERE no_dapil = '$no_dapil' AND zona = 'hijau'";
				$sqlzonasi = mysqli_query($model->koneksi, $qzonasi);
				$dzonasi = mysqli_fetch_array($sqlzonasi);
				$suara_pks = $dzonasi[0];
				//JUMLAH TPS
				$qzonasi = "SELECT no FROM zonasi WHERE no_dapil = '$no_dapil' AND zona = 'hijau'";
				$sqlzonasi = mysqli_query($model->koneksi, $qzonasi);
				$jzonasi = mysqli_num_rows($sqlzonasi);
				?>
				<div class="container-fluid">
				    <h2>Zona Ambil Alih</h2>
					<div class="row">
						<div class="col-md-4">
							<div class="card bg-success">
								<div class="card-body">
									<h6>Potensi Suara</h6>
									<h1 class="text-right"><?php echo $suara_pks+$suara_top;?></h1>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="card bg-success">
								<div class="card-body">
									<h6>Suara PKS Sebelumnya</h6>
									<h1 class="text-right"><?php echo $suara_pks;?></h1>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="card bg-success">
								<div class="card-body">
									<h6>Jumlah TPS</h6>
									<h1 class="text-right"><?php echo $jzonasi;?></h1>
								</div>
							</div>
						</div>
					</div>
					<br/>
					
					<div class="card">
						<div class="card-header h6 bg-secondary">Daftar TPS Yang Harus Diambil Alih</div>
						<div class="card-body">
							
							<?php
							$url_default = URL_DOMAIN.$menu.'/dashboard-wilayah';
							$qdesa = "SELECT no,name FROM desa WHERE no_dapil = '$no_dapil' ORDER BY name ASC";
							$sqldesa = mysqli_query($model->koneksi, $qdesa);
							while ($ddesa = mysqli_fetch_array($sqldesa)) {
								$no_desa = $ddesa['no'];
								$name_desa = $ddesa['name'];
								$qzonasi = "SELECT * FROM zonasi WHERE no_dapil = '$no_dapil' AND no_desa = '$no_desa' AND zona = 'hijau' ORDER BY tps ASC";
								$sqlzonasi = mysqli_query($model->koneksi, $qzonasi);
								$jzonasi = mysqli_num_rows($sqlzonasi);
								if ($jzonasi!=0) {									
									?>
									<h4><?php echo $name_desa; ?></h4>
									<table width="100%" class="table table-striped table-bordered table-hover">
										<tr class="text-center">
											<th width="5%" class="text-left">TPS</th>
											<th width="17%" class="text-left">Dusun</th>
											<th class="text-left">Alamat</th>
											<th width="20%" class="text-left">Top Caleg</th>
											<th width="12%">Top Partai</th>
											<th width="10%">Suara PKS</th>
										</tr>
										<?php
										while ($dzonasi = mysqli_fetch_array($sqlzonasi)) {
											$tps = $dzonasi['tps'];
											$no_dusun = $dzonasi['no_dusun'];
											$qtps = "SELECT * FROM tps WHERE no_desa = '$no_desa' AND tps = '$tps'";
											$sqltps = mysqli_query($model->koneksi, $qtps);
											$jtps = mysqli_num_rows($sqltps);											
											if ($jtps == 0) { $nama_dusun = 'Nama Dusun'; $address=''; } 
											else {
												$dtps = mysqli_fetch_array($sqltps);
												$no_dusun = $dtps['no_dusun'];
												$address = $dtps['address'];
												$qdusun = "SELECT name FROM dusun WHERE no_desa = '$no_desa' AND no = '$no_dusun'";
												$sqldusun = mysqli_query($model->koneksi, $qdusun);
												$ddusun = mysqli_fetch_array($sqldusun);
												$nama_dusun = $ddusun['name'];
											}
											$topcaleg = $dzonasi['topcaleg'];
											$topcalegsuara = $dzonasi['suara_topcaleg'];
											$toppartai = $dzonasi['toppartai'];
											$toppartaisuara = $dzonasi['suara_toppartai'];
											$toppkssuara = $dzonasi['suara_pks'];
											$url_dashboard_tps = $url_default.'/'.$no_desa.'/desa/page/'.$tps;
											?>
											<tr class="text-center">
												<td><?php echo $tps;?></td>
												<td class="text-left"><a href="<?php echo $url_dashboard_tps;?>" title="Dashboard TPS <?php echo $nama_dusun;?>"><?php echo $nama_dusun;?></a></td>
												<td class="text-left"><?php echo $address;?></td>
												<td><?php echo $topcaleg.' ('.$topcalegsuara.')';?></td>
												<td><?php echo $toppartai.' ('.$toppartaisuara.')';?></td>
												<td><?php echo $toppkssuara;?></td>
												
											</tr>
											<?php
										}
										?>
									</table>
									<?php
								}
							}
							?>
							
						</div>
					</div>
							
							
				</div>
			
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_ADMIN.'/app/asset-body.php'; ?>
	
</body>
 
</html>