<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_ADMIN.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_ADMIN.'/app/sidebar.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_ADMIN.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<?php
					$no_kabupaten = 4;
                    $uname = $_SESSION['uname']; 
    				if ($uname=='dapil1') { $no_dapil = 1; } elseif ($uname=='dapil2') { $no_dapil = 2; } elseif ($uname=='dapil3') { $no_dapil = 3; }
    				elseif ($uname=='dapil4') { $no_dapil = 4; } elseif ($uname=='dapil5') { $no_dapil = 5; } elseif ($uname=='dapil6') { $no_dapil = 6; }
    				else { $no_dapil = 6; }
					
					?>
					<h4>Formulir Input Data </h4>
                    <?php
                    if (empty ($_POST['process'])) {
						?>
						<div class="card">
							<div class="card-body">
								<form action="" method="post">                        
									<input type="hidden" name="process" value="form"/>
									<div id="view">
										<div class="view_label">Pilih Desa</div>
										<div class="view_dot">:</div>
										<div class="view_content">
											<select name="no_desa" class="form-control">
												<?php
												$qkec = "SELECT no,name FROM kecamatan WHERE no_dapil = '$no_dapil'";
                            					$sqlkec = mysqli_query($model->koneksi, $qkec);
                            					while ($dkec = mysqli_fetch_array($sqlkec)) {
                                					$no_kecamatan = $dkec['no'];
                                					$namekec = $dkec['name'];
    												$qdesa = "SELECT no,name FROM desa WHERE no_kecamatan = '$no_kecamatan' AND publish='1' ORDER BY name ASC";
    												$sqldesa = mysqli_query ($model->koneksi, $qdesa);
    												while ($ddesa = mysqli_fetch_array ($sqldesa)) {
    													$no_desa = $ddesa['no'];
    													$name_desa = $ddesa['name'];
    													?>
    													<option value="<?php echo $no_desa;?>"><?php echo $namekec.' - '.$name_desa;?></option>
    													<?php
    												}
                            					}
												?>
											</select>
										</div>
									</div>
									<div id="view">
										<div class="view_label">Pilih Partai</div>
										<div class="view_dot">:</div>
										<div class="view_content">
											<select name="no_partai" class="form-control">
												<?php
												$qpartai = "SELECT * FROM partai ORDER BY no ASC";
												$sqlpartai = mysqli_query ($model->koneksi, $qpartai);
												while ($dpartai = mysqli_fetch_array ($sqlpartai)) {
													?>
													<option value="<?php echo $dpartai['no'];?>"><?php echo $dpartai['no'].'. '.$dpartai['name'];?></option>
													<?php
												}
												?>
											</select>
										</div>
									</div>
									<input type="submit" name="submit" value="LANJUT" class="btn btn-primary" style="margin:5px 0px;"/>
								</form>
							</div>
						</div>
						<?php
					}
					elseif ($_POST['process']=='form') {
						$no_desa = $_POST['no_desa'];
						$no_partai = $_POST['no_partai'];
						$qceksuara = "SELECT * FROM suara WHERE no_desa = '$no_desa' AND no_partai = '$no_partai' ";
        				$sqlceksuara = mysqli_query($model->koneksi, $qceksuara);
        				$jceksuara = mysqli_num_rows($sqlceksuara);
        				if ($jceksuara>0) {
        				    ?>
        				    <h3>Partai Ini Sudah Diinput</h3>
        				    Mohon maaf, partai dan desa yang Anda pilih sudah diinput.<br/>
        				    <a href="<?php echo URL_DOMAIN.$menu.'/'.$link;?>">Kembali</a>
        				    <?php
        				}
        				else {
    						$qdesa = "SELECT * FROM desa WHERE no = '$no_desa'";
    						$sqldesa = mysqli_query($model->koneksi, $qdesa);
    						$ddesa = mysqli_fetch_array($sqldesa);
    						$namedesa = $ddesa['name'];
    						$jumlah_tps = $ddesa['jumlah_tps'];
    						$qpartai = "SELECT * FROM partai WHERE no = '$no_partai'";
    						$sqlpartai = mysqli_query($model->koneksi, $qpartai);
    						$dpartai = mysqli_fetch_array($sqlpartai);
    						$namepartai = $dpartai['name'];
                            ?>
    						<h2><?php echo $namepartai;?></h2>
    						<h3> <?php echo 'Desa '.$namedesa.', Kecamatan '.$namekec;?></h3>
                            <form action="" method="post">                        
                                <input type="hidden" name="no_desa" value="<?php echo $no_desa;?>"/>
                                <input type="hidden" name="no_partai" value="<?php echo $no_partai;?>"/>
                                <input type="hidden" name="process" value="save"/>
                                <table width="100%" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr class="text-center">
                                            <th width="200" class="text-left">PARTAI DAN CALON</th>
                                            <?php
                                            for ($i=1; $i<=$jumlah_tps; $i++) {            
                                                ?><th>TPS <?php echo $i;?></th><?php
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>                            
                                        <?php
                                        $qcaleg = "SELECT * FROM caleg WHERE no_partai = '$no_partai' AND no_dapil = '$no_dapil' ORDER BY nomor_urut ASC";
                                        $sqlcaleg = mysqli_query ($model->koneksi, $qcaleg);
                                        while ($dcaleg = mysqli_fetch_array ($sqlcaleg)) {
                                            $calegNo = $dcaleg['no'];
                                            $calegName = $dcaleg['name'];
                                            $calegNoUrut = $dcaleg['nomor_urut'];
                                            ?>
                                            <tr class="text-center">
                                                <td class="text-left"><?php echo $calegNoUrut.'. '.$calegName;?></td>
                                                <?php
                                                for ($i=1; $i<=$jumlah_tps; $i++) {       
                                                    $nameinput = $calegNo."_tps_".$i;
                                                    ?>
                                                    <td>
                                                        <input type="number"  class="form-control p-0 text-center" style="padding:0px;" name="<?php echo $nameinput;?>"/>
                                                    </td>
                                                    <?php
                                                }
                                                ?>
                                            </tr>
                                            <?php
                                        }                            
                                        ?>
                                    </tbody>
                                </table>
                                <input type="submit" class="btn btn-primary btn-lg" onclick="return confirm('Yakin Dengan Data Yang Diinput ?');" value="SIMPAN"/>
                            </form>
                            <br/><br/>
                            <?php
                            $randcode=rand(111111,999999); 
                            $_SESSION['code']=$randcode;
        				}
                    }
                    elseif ($_POST['process']=='save')  {
                        if (empty ($_SESSION['code'])) {  
                            echo '<h3>Data Berhasil Simpan</h3>Terima Kasih, Data Yang Anda Input Berhasil Masuk Database.';
                        }
                        else { 
							$no_desa = $_POST['no_desa'];
							$no_partai = $_POST['no_partai'];
							$qdesa = "SELECT * FROM desa WHERE no = '$no_desa'";
							$sqldesa = mysqli_query($model->koneksi, $qdesa);
							$ddesa = mysqli_fetch_array($sqldesa);
							$jumlah_tps = $ddesa['jumlah_tps'];
							$no_kecamatan = $ddesa['no_kecamatan'];
                            if (empty($_SESSION['code'])) { } else { unset($_SESSION['code']); }
                            $qcaleg = "SELECT * FROM caleg WHERE no_partai = '$no_partai' AND no_dapil = '$no_dapil' ORDER BY nomor_urut ASC";
                            $sqlcaleg = mysqli_query ($model->koneksi, $qcaleg);
                            while ($dcaleg = mysqli_fetch_array ($sqlcaleg)) {
                                $calegNo = $dcaleg['no'];
                                $calegNoUrut = $dcaleg['nomor_urut'];
                                $qlocation = "INSERT INTO suara SET 
									no_kabupaten = '$no_kabupaten',
									no_dapil = '$no_dapil',
									no_kecamatan = '$no_kecamatan',
                                    no_desa = '$no_desa',
                                    no_partai = '$no_partai',
                                    nomor_urut = '$calegNoUrut',
                                    no_caleg = '$calegNo', ";
                                $total = 0;
                                $qadd = " ";
                                for ($i=1; $i<=$jumlah_tps; $i++) {   
                                    $namecolumn = "tps_".$i ;   
                                    $nameinput = $calegNo."_tps_".$i;
                                    $valuepost = $_POST[$nameinput];
									if ($valuepost=='') {  } else { $total = $total + $valuepost; }
                                    $qadd = $qadd." $namecolumn = '$valuepost',  ";
									
                                }
                                $qinput = $qlocation.$qadd." total = '$total', date = sysdate() ";
                                mysqli_query ($model->koneksi, $qinput);                            
                            }
                            echo '<h3>Data Berhasil Simpan</h3>Terima Kasih, Data Yang Anda Input Berhasil Masuk Database.';
                        }
                    }
                    ?>
					
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_ADMIN.'/app/asset-body.php'; ?>
	
</body>
 
</html>