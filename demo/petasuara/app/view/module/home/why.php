<div class="feature-icon-wrapper section-space--ptb_70 bg-white"> 
	<div class="container">
		<div class="section-title-wrap text-center section-space--mb_20">
			<h3 class="heading">Mengapa Memilih Kami</h3>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="app-block wow bounceInUp  text-center" data-wow-delay=".1s">
					<img src="<?php echo URL_PICTURE;?>izin.png" alt="biro haji umroh resmi"/>
					<h5>Biro Haji Umroh Resmi</h5>
					<p>Kami telah mengantongi ijin Penyelengara Umroh Resmi dari Kementerian Agama dengan Surat No. PPIU : U.556 tahun 2020 dan PIHK : 716 tahun 2021.</p>
				</div>            
			</div>
			<div class="col-sm-4">
				<div class="app-block wow bounceInUp  text-center" data-wow-delay=".2s">
					<img src="<?php echo URL_PICTURE;?>sertifikat.png" alt="sertifikat halal mui"/>
					<h5>Sertifikat Halal MUI</h5>
					<p>Kami memiliki Sertifikat Halal dari Dewan Syariah Nasional Majelis Ulama Indonesia. Sebagai jaminan dan pedoman kami dalam memberikan layanan Umroh sesuai dengan syariah.</p>
				</div>            
			</div>
			<div class="col-sm-4">
				<div class="app-block wow bounceInUp  text-center" data-wow-delay=".3s">
					<img src="<?php echo URL_PICTURE;?>operator.png" alt="operator umroh terbaik"/>
					<h5>Operator Umroh Terbaik</h5>
					<p>Salah satu prestasi kami adalah mendapatkan predikat Operator Haji dan Umroh Terbaik dalam kompetisi pariwisata Halal Nasional 2016 dari Kementerian Pariwisata RI.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="app-block wow bounceInUp text-center" data-wow-delay=".4s">
					<img src="<?php echo URL_PICTURE;?>pembimbing.png" alt="sekolah manasik umroh"/>
					<h5>Sekolah Manasik Hasanah</h5>
					<p>Sekolah untuk persiapan ibadah Umroh dan haji dengan kurikulum yang lengkap, dan pembimbing yang berpengalaman, menjadikan ibadah Umroh Anda semakin bermakna.</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="app-block wow bounceInUp text-center" data-wow-delay=".5s">
					<img src="<?php echo URL_PICTURE;?>handling.png" alt="tim handling profesional"/>
					<h5>Tim Handling Profesional</h5>
					<p>Kami memiliki petugas perwakilan di Makkah dan Madinah yang akan menyiapkan seluruh akomodasi jamaah mulai dari hotel, bus, catering dan segala persoalan jamaah.</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="app-block wow bounceInUp text-center" data-wow-delay=".6s">
					<img src="<?php echo URL_PICTURE;?>pasti.png" alt="pasti berangkat"/>
					<h5>Pasti Berangkat</h5>
					<p>Kami pastikan keberangkatan Anda tepat waktu berapapun jumlahnya dan kami jamin Anda pasti berangkat umroh sesuai dengan tanggal keberangkatan yang Anda pilih.</p>
				</div>            
			</div>
		</div>
	</div>
</div>