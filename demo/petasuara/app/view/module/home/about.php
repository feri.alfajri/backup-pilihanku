<div class="section-space--ptb_70" style="background:#FF8800;color:#FFFFFF;"> 
	<div class="container">
		<h3 class="heading mb-20 text-center text-white">Tentang Hasanah Tour & Travel</h3>
		
		<div class="row">
			<div class="col-sm-3 text-center" data-wow-delay=".5s">
				<img class="img-responsive" src="<?php echo URL_PICTURE;?>/logo-only.png" width="100%" style="margin:5px 0px" alt="Hasanah Tours and Travel">
			</div>
			<div class="col-sm-9 text-white">
				<p>Hasanah Tours & Travel adalah sebuah perushaaan travel umroh dibawah naungan PT. ASYESA. Kami mampu menjawab semua kebutuhan perjalanan ibadah anda ke tanah suci. 
				dengan jaminan memberikan pelayanan sepenuh hati kepada Anda.</p>
				<h5 class=" text-white">IJIN RESMI</h5>
				<p>
				1. Penyelenggara Perjalanan Ibadah Umroh (PPIU) : NOMOR U.556 TAHUN 2020 <br>
				2. Penyelenggara Ibadah Haji Khusus (PIHK) : NOMOR 716 TAHUN 2021
				</p>
				<h5 class=" text-white">PRINSIP KAMI</h5>
				<p>
				1. Kami menjadikan keJUJURan, AMANAH dan PROFESIONALISME sebagai budaya perusahaan.<br>
				2. Kami berorientasi kepada Keridhoan Allah SWT dan KEPUASAN PELANGGAN ( costumer satisfaction).<br>
				3. Kami mengikuti perkembangan TEKNOLOGI dan informasi (IT) dalam operasionalisasinya.<br>
				4. Kami menjadikan KEBERKAHAN RIZKI sebagai tujuan asasi perusahaan.
				</p>
			</div>
		</div>
		
	</div>
</div>