<div class="processing-hero processing-hero-bg">
	<div class="container">
		<div class="row align-items-center" style="padding:100px 0"> 
			<div class="col-lg-7 col-md-7">
				<div class="processing-hero-text wow move-up">
					<h2 class="font-weight--reguler mb-15" style="color:#FFFFFF">Ingin ScaleUp Bisnis dengan cepat dan Omset melesat, tanpa biaya yang berat ?</h2>
					<p>
						Jangan biarkan Anda mengeluarkan biaya besar-besaran untuk meningkatkan marketing. Apalagi jika dieksekusi oleh tim yang belum berpengalaman dan sistem yang belum teruji.
						Serahkan urusan digital marketing Anda kepada kami yang telah memiliki banyak portofolio dan teruji hasilnya.
						Hubungi Kami untuk mendapatkan rekomendasi terbaik tentang solusi digital marketing Anda.
					</p>
					<div class="hero-button mt-20">
						<a href="<?php echo URL_DOMAIN;?>contact" class="btn btn--secondary" title="Hubungi Kami">Hubungi Kami</a>
					</div>
				</div>
			</div>
			<div class="col-lg-5 col-md-5">
				<div class="processing-hero-images-wrap wow move-up">
					<div class="video-popup infotech-video-box">
						<img src="<?php echo URL_PICTURE;?>seo-banner.png" alt="banner-website" width="100%">

					</div>
				</div>
			</div>
		</div>
	</div>
</div>