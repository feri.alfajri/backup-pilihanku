
<li class="nav-item mt-3"><a class="nav-link"  style="padding-bottom:0px"><b>DASHBOARD</b></a></li>
<hr class="sidebar-divider my-0">
<li class="nav-item <?php if ($link=='dashboard') { echo 'active'; } ?>">
    <a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/dashboard">
		<i class="fas fa-fw fa-desktop"></i><span>Dashboard</span>
	</a>
</li>
<hr class="sidebar-divider my-0">
<li class="nav-item <?php if ($link=='zona-pertahankan' or $link=='zona-ambil-alih' or $link=='zona-tarung' or $link=='zona-hindari') { echo 'active'; } ?>">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#zonasi" aria-expanded="true" aria-controls="zonasi">
        <i class="fas fa-fw fa-edit"></i><span>Zonasi</span>
    </a>
    <div id="zonasi" class="collapse <?php if ($link=='zona-pertahankan' or $link=='zona-ambil-alih' or $link=='zona-tarung' or $link=='zona-hindari') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a href="<?php echo URL_DOMAIN.$menu;?>/zona-pertahankan" class="collapse-item <?php if ($link=='zona-pertahankan') { echo 'active'; } ?>" title="Zona Pertahankan">Zona Pertahankan</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/zona-ambil-alih" class="collapse-item <?php if ($link=='zona-ambil-alih') { echo 'active'; } ?>" title="Zona Ambil Alih">Zona Ambil Alih</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/zona-tarung" class="collapse-item <?php if ($link=='zona-tarung') { echo 'active'; } ?>" title="Zona Tarung">Zona Tarung</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/zona-hindari" class="collapse-item <?php if ($link=='zona-hindari') { echo 'active'; } ?>" title="Zona Hindari">Zona Hindari</a>
        </div>
    </div>
</li>
<hr class="sidebar-divider my-0">


<li class="nav-item mt-3"><a class="nav-link"  style="padding-bottom:0px"><b>SUARA</b></a></li>
<hr class="sidebar-divider my-0">
<li class="nav-item <?php if ($link=='suara') { echo 'active'; } ?>">
    <a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/suara">
		<i class="fas fa-fw fa-user"></i><span>Suara</span>
	</a>
</li>
<hr class="sidebar-divider my-0">


<li class="nav-item mt-3"><a class="nav-link"  style="padding-bottom:0px"><b>UPDATE</b></a></li>
<hr class="sidebar-divider my-0">
<li class="nav-item <?php if ($link=='input') { echo 'active'; } ?>">
    <a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/input">
		<i class="fas fa-fw fa-edit"></i><span>Input</span>
	</a>
</li>
<hr class="sidebar-divider my-0">

<li class="nav-item <?php if ($link=='tps' or $link=='dusun' or $link=='desa' or $link=='caleg' or $link=='kabupaten') { echo 'active'; } ?>">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#master" aria-expanded="true" aria-controls="master">
        <i class="fas fa-fw fa-edit"></i><span>Master</span>
    </a>
    <div id="master" class="collapse <?php if ($link=='tps' or $link=='dusun' or $link=='desa' or $link=='kecamatan' or $link=='kabupaten') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a href="<?php echo URL_DOMAIN.$menu;?>/caleg" class="collapse-item <?php if ($link=='caleg') { echo 'active'; } ?>" title="Caleg">Caleg</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/desa" class="collapse-item <?php if ($link=='desa') { echo 'active'; } ?>" title="Desa">Desa</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/dusun" class="collapse-item <?php if ($link=='dusun') { echo 'active'; } ?>" title="Dusun">Dusun</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/tps" class="collapse-item <?php if ($link=='tps') { echo 'active'; } ?>" title="TPS">TPS</a>
        </div>
    </div>
</li>
<hr class="sidebar-divider my-0">


<div class="text-center d-none d-md-inline mt-3">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>