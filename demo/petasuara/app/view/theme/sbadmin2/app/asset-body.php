<a class="scroll-to-top rounded" href="#page-top"><i class="fas fa-angle-up"></i></a>
<script>
	if ($(document).width() < 643) { 
		$("body").toggleClass("sidebar-toggled");
		$(".sidebar").toggleClass("toggled");
	} 
</script> 

<script src="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="<?php echo URL_THEME.THEME_MEMBER;?>/js/sb-admin-2.min.js"></script>

<script src="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo URL_THEME.THEME_MEMBER;?>/js/demo/datatables-demo.js"></script>