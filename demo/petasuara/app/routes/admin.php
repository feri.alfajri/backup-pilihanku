<?php
if (empty ($_SESSION['uname']) or empty ($_SESSION['pword'])) {
    
    include DIR_MODULE.'account/admin-login.php';
    
}
else { 
	
	$access = 'admin';
	
	switch ($link) {
		
		case '': 
			$titlepage = 'Dashboard';
			include DIR_CONTROLLER.'AccountController.php';  
			break;
		
		case 'profile': 
			$titlepage = 'Profil'; 
			include DIR_CONTROLLER.'AccountController.php';  
			break;
			
		case 'password': 
			$titlepage = 'Password'; 
			include DIR_CONTROLLER.'AccountController.php';  
			break;
		
		case 'input': 
			$titlepage = 'Input Data'; 
			include DIR_CONTROLLER.'InputController.php';  
			break;

			
		case 'photo': 
			$titlepage = 'Foto'; 
			include DIR_CONTROLLER.'AccountController.php';  
			break;
		
		case 'tps': 
			$titlepage = 'TPS'; 
			include DIR_CONTROLLER.'MasterController.php';  
			break;
		
		case 'caleg': 
			$titlepage = 'Caleg'; 
			include DIR_CONTROLLER.'MasterController.php';  
			break;
			
		case 'dusun': 
			$titlepage = 'Dusun'; 
			include DIR_CONTROLLER.'MasterController.php';  
			break;
			
		case 'desa': 
			$titlepage = 'Desa'; 
			include DIR_CONTROLLER.'MasterController.php';  
			break;
			
		case 'kecamatan': 
			$titlepage = 'Kecamatan'; 
			include DIR_CONTROLLER.'MasterController.php';  
			break;
			
		case 'kabupaten': 
			$titlepage = 'Kabupaten'; 
			include DIR_CONTROLLER.'MasterController.php';  
			break;
			
		case 'user': 
			$titlepage = 'User / Admin'; 
			include DIR_CONTROLLER.'UserController.php';  
			break;
			
		case 'dashboard': 
			$titlepage = 'Dashboard Wilayah'; 
			include DIR_CONTROLLER.'DashboardController.php';  
			break;
			
		case 'dashboard-partai': 
			$titlepage = 'Dashboard Partai'; 
			include DIR_CONTROLLER.'DashboardController.php';  
			break;
			
		case 'zona-pertahankan': 
			$titlepage = 'Zona Pertahankan'; 
			include DIR_CONTROLLER.'ZonasiController.php';  
			break;
			
		case 'zona-ambil-alih': 
			$titlepage = 'Zona Rebut'; 
			include DIR_CONTROLLER.'ZonasiController.php';  
			break;
			
		case 'zona-tarung': 
			$titlepage = 'Zona Tarung'; 
			include DIR_CONTROLLER.'ZonasiController.php';  
			break;
			
		case 'zona-hindari': 
			$titlepage = 'Zona Hindari'; 
			include DIR_CONTROLLER.'ZonasiController.php';  
			break;
			
		case 'suara': 
			$titlepage = 'Suara'; 
			include DIR_CONTROLLER.'SuaraController.php';  
			break;
						
		default:
			$titlepage = 'Error';
			include DIR_CONTROLLER.'TextController.php';
			
	}

}
?>