<?php
if ($access == 'admin') {  

    include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
    
    if ($link == 'zona-pertahankan') {	
	
        include DIR_MODULE.'zonasi/admin-pertahankan.php';	
		
    }
	elseif ($link == 'zona-ambil-alih') {		
	
        include DIR_MODULE.'zonasi/admin-ambil-alih.php';	
		
    }
	elseif ($link == 'zona-tarung') {		
	
        include DIR_MODULE.'zonasi/admin-tarung.php';	
		
    }
	elseif ($link == 'zona-hindari') {		
	
        include DIR_MODULE.'zonasi/admin-hindari.php';	
		
    }
    else {		
	
        include DIR_MODULE.'zonasi/admin-pertahankan.php';	
		
    }	

} 
else {
	
    
}
?>