<?php
if ($access == 'admin') {  

    include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
    
    if ($link == 'dashboard') {	
	
		if ($act == '') {
			
			include DIR_MODULE.'dashboard/admin-dapil.php';	
			
		}
		elseif ($act == 'kecamatan') {
			
			include DIR_MODULE.'dashboard/admin-kecamatan.php';	
			
		}
		elseif ($act == 'desa') {
			
			if ($page == '') {
				include DIR_MODULE.'dashboard/admin-desa.php';	
			}
			else {
				include DIR_MODULE.'dashboard/admin-tps.php';	
			}
			
		}
		elseif ($act == 'partai') {
			
			include DIR_MODULE.'dashboard/admin-partai.php';				
			
		}
		elseif ($act == 'caleg') {
			
			include DIR_MODULE.'dashboard/admin-caleg.php';				
			
		}
		else {
			
			include DIR_MODULE.'dashboard/admin-dapil.php';	
			
		}
		
    }
    else {		
	
        include DIR_MODULE.'dashboard/admin-dapil.php';	
		
    }	

} 
else {
	
    
}
?>