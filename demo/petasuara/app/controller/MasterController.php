<?php
if ($access == 'admin') {  

    include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
    
    if ($link == 'tps') {	
	
        include DIR_MODULE.'master/admin-tps.php';	
		
    }
	elseif ($link == 'dusun') {		
	
        include DIR_MODULE.'master/admin-dusun.php';	
		
    }
	elseif ($link == 'desa') {		
	
        include DIR_MODULE.'master/admin-desa.php';	
		
    }
	elseif ($link == 'kecamatan') {		
	
        include DIR_MODULE.'master/admin-kecamatan.php';	
		
    }
	elseif ($link == 'kabupaten') {		
	
        include DIR_MODULE.'master/admin-kabupaten.php';	
		
    }
    elseif ($link == 'caleg') {		
	
        include DIR_MODULE.'master/admin-caleg.php';	
		
    }
    else {		
	
        include DIR_MODULE.'master/admin-tps.php';	
		
    }

} 
else {
	
    
}
?>