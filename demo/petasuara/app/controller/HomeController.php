<?php
if ($access=='admin') { 

	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
    
    if ($link == 'slider') {		
	
        include DIR_MODULE.'home/admin-slider.php';		
		
    }
    else {		
	
        include DIR_MODULE.'home/admin-home.php';	
		
    }
	
}
else {

    include DIR_MODULE.'home/public.php';

}
?>