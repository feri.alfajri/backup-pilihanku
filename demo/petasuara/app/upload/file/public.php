<?php
$access='public';

switch ($menu) {
    
    case '': 
        include DIR_CONTROLLER.'HomeController.php'; 
        break;
    
    case 'about': 
        include DIR_CONTROLLER.'AboutController.php'; 
        break; 
        
    case 'blog': 
        include DIR_CONTROLLER.'BlogController.php'; 
        break;
    
    case 'portofolio': 
        include DIR_CONTROLLER.'PortofolioController.php'; 
        break;
    
    case 'contact': 
        include DIR_CONTROLLER.'ContactController.php'; 
        break;    
        
    default:
        include DIR_CONTROLLER.'TextController.php';
        
}

?>