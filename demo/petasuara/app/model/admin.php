<?php  
class admin extends model {   
	
	function form($label,$form,$action,$value,$tampilan) {
		$spasi=explode("_",$form);  
		$jumspasi=count($spasi);
		if ($jumspasi<=1) {  $formcontent=$form; $tabelcontent=$form; } else { list ($formcontent,$tabelcontent)=explode("_",$form); $tabelcontent = str_replace($formcontent . '_', '', $form);  }
		if ($formcontent=="form") { $tipe="text"; }
		elseif ($formcontent=="author") { $tipe="author"; }
		elseif ($formcontent=="content") { $tipe="texteditor"; }
		elseif ($formcontent=="penjelasan") { $tipe="texteditor"; }
		elseif ($formcontent=="description") { $tipe="texteditor"; }
		elseif ($formcontent=="problem") { $tipe="texteditor"; }
		elseif ($formcontent=="gain") { $tipe="texteditor"; }
		elseif ($formcontent=="solution") { $tipe="texteditor"; }
		elseif ($formcontent=="benefit") { $tipe="texteditor"; }
		elseif ($formcontent=="feature") { $tipe="texteditor"; }
		elseif ($formcontent=="instructor") { $tipe="texteditor"; }
		elseif ($formcontent=="testimonial") { $tipe="texteditor"; }
		elseif ($formcontent=="bonus") { $tipe="texteditor"; }
		elseif ($formcontent=="cta") { $tipe="texteditor"; }
		
		elseif ($formcontent=="address") { $tipe="textarea"; }
		elseif ($formcontent=="comment") { $tipe="textarea"; }
		elseif ($formcontent=="testimoni") { $tipe="textarea"; }
		elseif ($formcontent=="meta") { $tipe="textarea"; }
		elseif ($formcontent=="note") { $tipe="textarea"; }	
		elseif ($formcontent=="offer") { $tipe="textarea"; }	
		elseif ($formcontent=="detail") { $tipe="textarea"; }
		elseif ($formcontent=="spesification") { $tipe="textarea"; }
		elseif ($formcontent=="answer") { $tipe="textarea"; }
		
		elseif ($formcontent=="picture") { $tipe="picture"; }
		elseif ($formcontent=="thumbnail") { $tipe="picture"; }
		elseif ($formcontent=="picture1") { $tipe="picture"; }
		elseif ($formcontent=="picture2") { $tipe="picture"; }
		elseif ($formcontent=="picture3") { $tipe="picture"; }
		elseif ($formcontent=="picture4") { $tipe="picture"; }
		elseif ($formcontent=="picture5") { $tipe="picture"; }
		elseif ($formcontent=="picture6") { $tipe="picture"; }
		elseif ($formcontent=="picture7") { $tipe="picture"; }
		elseif ($formcontent=="picture8") { $tipe="picture"; }
		elseif ($formcontent=="picture9") { $tipe="picture"; }
		elseif ($formcontent=="logo") { $tipe="picture"; }
		elseif ($formcontent=="cover") { $tipe="picture"; }
		elseif ($formcontent=="favicon") { $tipe="picture"; }
		
		elseif ($formcontent=="file") { $tipe="file"; }
		elseif ($formcontent=="id") { $tipe="id"; }
		elseif ($formcontent=="up") { $tipe="up"; }
		elseif ($formcontent=="no") { $tipe="no"; }
		elseif ($formcontent=="password") { $tipe="password"; }
		elseif ($formcontent=="date") { $tipe="date"; }
		elseif ($formcontent=="show") { $tipe="optiontampil"; }
		elseif ($formcontent=="publish") { $tipe="publish"; }
		elseif ($formcontent=="target") { $tipe="optiontarget"; }
		elseif ($formcontent=="newsletter") { $tipe="newsletter"; }
		elseif ($formcontent=="offers") { $tipe="offers"; }	
        elseif ($formcontent=="toaster") { $tipe="optionadatidak"; }	
		elseif ($formcontent=="pool") { $tipe="optionadatidak"; }
		elseif ($formcontent=="parking") { $tipe="optionadatidak"; }
		elseif ($formcontent=="cabletv") { $tipe="optionadatidak"; }
		elseif ($formcontent=="bedding") { $tipe="optionadatidak"; }
		elseif ($formcontent=="dishwater") { $tipe="optionadatidak"; }
		elseif ($formcontent=="lift") { $tipe="optionadatidak"; }
		elseif ($formcontent=="balcony") { $tipe="optionadatidak"; }
		elseif ($formcontent=="ac") { $tipe="optionadatidak"; }
		elseif ($formcontent=="sub") { $tipe="no"; }
		else { $tipe="text"; }
		if ($form=="no_gallery_category") { $tabelcontent="gallery_category"; } 
		elseif ($form=="no_service_category") { $tabelcontent="service_category"; } 
		elseif ($form=="no_course_bab") { $tabelcontent="course_bab"; } 
		elseif ($form=="no_faq_category") { $tabelcontent="faq_category"; } 
		elseif ($form=="id_customer_kategori") { $tabelcontent="customer_kategori"; }
		elseif ($form=="sub_location") { $tabelcontent="sub_location"; } 
		elseif ($form=="no_location") { $tabelcontent="location"; } 
		elseif ($form=="id_properties") { $tabelcontent="properties"; } 
		elseif ($form=="no_properties_category") { $tabelcontent="properties_category"; } 
		elseif ($form=="no_sertifikat_properties") { $tabelcontent="sertifikat_properties"; } 
		else {  $tabelcontent= $tabelcontent; }
		
		if ($tipe=="text") {  ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content"><input type="text" name="<?php echo $form;?>" id="<?php echo $form;?>" class="form-control" style="width:100%;" value="<?php echo $value;?>"/></div>
			</div><?php
		}
		elseif ($tipe=="author") {  
			if ($value=="") { $valueauthor=$_SESSION['uname']; } else { $valueauthor=$value; } ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content"><input type="text" name="<?php echo $form;?>" id="<?php echo $form;?>" value="<?php echo $valueauthor;?>" class="form-control" style="width:100%;"/></div>
			</div><?php
		}
		elseif ($tipe=="password") {  ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content"><input type="password" name="<?php echo $form;?>" id="<?php echo $form;?>"  class="form-control" style="width:100%;"/></div>
			</div><?php
		}
		elseif ($tipe=="texteditor") {  ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content"><textarea name="<?php echo $form;?>" id="editor" class="form-control" style="width:100%; height:300px;background:#FFFFFF;"><?php echo $value;?></textarea></div>
			</div><?php
		}
		elseif ($tipe=="textarea") { ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content"><textarea name="<?php echo $form;?>" id="<?php echo $form;?>" class="form-control" style="width:100%; height:80px; text-transform:none;"><?php echo $value;?></textarea></div>
			</div><?php
		}
		elseif ($tipe=="picture") { ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content"><?php 
					if ($value=="") { ?>
						<input name="picturelama" type="hidden" value="<?php echo $value;?>"/>
						<input type="file" name="<?php echo $form;?>" id="<?php echo $form;?>" class="form-control" style="width:100%; padding:0px;"/><?php 
					} 
					else { ?>
						<img src="<?php echo URL_UPLOAD;?>image.php/<?php echo $value;?>?width=90&nocache&quality=100&image=<?php echo URL_PICTURE.$value;?>" width="90" height="64" align="left" alt="<?php echo $value;?>" style="margin:0px 10px 5px 0px;"/>
						<input name="picturelama" type="hidden" value="<?php echo $value;?>"/>
						<input type="file" name="<?php echo $form;?>" id="<?php echo $form;?>" class="form-control" style="width:100%; margin-top:5px; padding:0px;"/>
						<small>Kosongkon fom ini jika tidak ada perubahan gambar</small><?php 
					} ?>
				</div>
			</div><?php
		}
		elseif ($tipe=="file") {  ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content">
					<input type="file" name="file" id="file" style="width:100%; padding:0px;"/><?php 
					if ($value=="") { ?><input name="filelama" type="hidden"  class="form-control" value="<?php echo $value;?>"/><?php } 
					else { ?>
						<input name="filelama" type="hidden" class="form-control" value="<?php echo $value;?>" />
						<small>Kosongkon fom ini jika tidak ada perubahan file</small><?php 
					} ?>
				</div>
			</div><?php
		}
		elseif ($tipe=="no" or $tipe=="id") { 
			if ($tipe=="id") { $kolom="no"; $formdata="title"; } else { $kolom="no"; $formdata="name"; }?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content">
					<select name="<?php echo $form;?>" class="form-control" style="width:100%;"><?php
						$tabelcat=$tabelcontent;
						if ($value=="") { ?>
							<option value="0">- Tidak Dipilih -</option><?php
							$qid=mysqli_query($this->koneksi,"SELECT * FROM $tabelcat ORDER BY $formdata");
							while($did=mysqli_fetch_array($qid)){ ?><option value="<?php echo $did[$kolom];?>"><?php echo $did[$formdata];?></option><?php }
						} 
						else {
							if ($value==0) { ?>
								<option value="0">- Tidak Dipilih -</option><?php
							}
							else {
								$qidid=mysqli_query($this->koneksi,"SELECT * FROM $tabelcat WHERE $kolom='$value' LIMIT 0,1");
								$didid=mysqli_fetch_array($qidid); ?>
								<option value="<?php echo $didid[$kolom];?>"><?php echo $didid[$formdata];?></option><?php
							}
							$qid=mysqli_query($this->koneksi,"SELECT * FROM $tabelcat ORDER BY $formdata");
							while($did=mysqli_fetch_array($qid)){ ?><option value="<?php echo $did[$kolom];?>"><?php echo $did[$formdata];?></option><?php } ?>
							<option value="0">- Tidak Dipilih -</option><?php
						} ?>
					</select>
				</div>
			</div><?php
		}
		elseif ($tipe=="optiontarget") {  ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content">
					<select name="target" class="form-control" style="width:100%;"><?php
						if ($value=="") { ?><option value="_parent">Buka Di Halaman Yang Sama</option><option value="_blank">Buka Di Tab Yang Baru</option><?php }
						elseif ($value=="_parent"){ ?><option value="_parent">Buka Di Halaman Yang Sama</option><option value="_blank">Buka Di Tab Yang Baru</option><?php } 
						else { ?><option value="_blank">Buka Di Tab Yang Baru</option><option value="_parent">Buka Di Halaman Yang Sama</option><?php } ?>
					</select>
				</div>
			</div><?php
		}
		elseif ($tipe=="optiontampil") {?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content">
					<select name="<?php echo $form;?>" class="form-control" style="width:100%;"><?php 
						if ($value==""){ ?><option value="1">Tampilkan</option><option value="0">Sembunyikan</option><?php } 
						elseif ($value==1) { ?><option value="1">Tampilkan</option><option value="0">Sembunyikan</option><?php } 
						elseif ($value==0) { ?><option value="0">Sembunyikan</option><option value="1">Tampilkan</option><?php } ?>
					</select>
				</div>
			</div><?php
		}
		elseif ($tipe=="optionadatidak") {?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content">
					<select name="<?php echo $form;?>" class="form-control" style="width:100%;"><?php 
						if ($value==""){ ?><option value="1">Ada</option><option value="0">Tidak</option><?php } 
						elseif ($value==1) { ?><option value="1">Ada</option><option value="0">Tidak</option><?php } 
						elseif ($value==0) { ?><option value="0">Tidak</option><option value="1">Ada</option><?php } ?>
					</select>
				</div>
			</div><?php
		}
		elseif ($tipe=="publish") {?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content">
					<select name="<?php echo $form;?>" class="form-control" style="width:100%;"><?php 
						if ($value==""){ ?><option value="1">Tampilkan</option><option value="0">Sembunyikan</option><?php } 
						elseif ($value==1) { ?><option value="1">Tampilkan</option><option value="0">Sembunyikan</option><?php } 
						elseif ($value==0) { ?><option value="0">Sembunyikan</option><option value="1">Tampilkan</option><?php } ?>
					</select>
				</div>
			</div><?php
		}
		elseif ($tipe=="newsletter" or $tipe="offers") {?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content">
					<select name="<?php echo $form;?>" class="form-control" style="width:100%;"><?php 
						if ($value==""){ ?><option value="1">Ya</option><option value="0">Tidak</option><?php } 
						elseif ($value==1) { ?><option value="1">Ya</option><option value="0">Tidak</option><?php } 
						elseif ($value==0) { ?><option value="0">Tidak</option><option value="1">Ya</option><?php } ?>
					</select>
				</div>
			</div><?php
		}
		elseif ($tipe=="date") { ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content"><input type="text" name="<?php echo $form;?>" id="<?php echo $form;?>" class="form-control" style="width:100%;" value="<?php echo $value; ?>"/></div>
			</div><?php
		}
		else { ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content"><input type="text" class="form-control" name="<?php echo $form;?>" id="<?php echo $form; ?>" style="width:100%;" value="<?php echo $value; ?>"/></div>				
			</div><?php
		}
	}
	
	
	function notify ($tipe) {
		$link=$this->link;
		$menu=$this->menu;
		$linkmod=URL_DOMAIN.$menu."/".$link;
		if($tipe=="empty"){ ?>
			<h3>Data Tidak Ada</h3>Maaf, Data Yang Anda Cari Tidak Ada<?php
		}
		elseif($tipe=="img_empty"){ ?>
			<h3>Masukkan picture</h3>Silahkan Masukkan picture<?php
		}
		elseif($tipe=="img_format"){ ?>
			<h3>Format picture Salah</h3>Format picture Yang Diijinkan adalah (JPG, PNG, GIF)<?php
		}
		elseif($tipe=="image_dimention"){ ?>
			<h3>Resolusi picture Terlalu Besar</h3>Resolusi picture Yang Diijinkan maksimal 1000 X 1000 px<?php
		}
		elseif($tipe=="image_size"){ ?>
			<h3>Ukuran picture Terlalu Besar</h3>Ukuran picture Yang Diijinkan maksimal 1 MB<?php
		}
		elseif($tipe=="file_empty"){ ?>
			<h3>Masukkan File Anda</h3>Silahkan Masukkan File<?php
		}
		elseif($tipe=="file_size"){ ?>
			<h3>Ukuran File Terlalu Besar</h3>Ukuran File Yang Diijinkan maksimal 10 MB<?php
		}
		elseif($tipe=="nosession"){ ?>
			<h3>Session Telah Habis</h3>Maaf, Session Telah Habis. Silahkan Ulangi Lagi<?php
		}
		elseif($tipe=="save_ok"){ ?>
			<h3>Data Berhasil Diisi</h3>Selamat, Data Berhasil Diisi<br/><a href="<?php echo $linkmod;?>/" title="Kembali">Kembali</a><?php
		}
		elseif($tipe=="password_ok"){ ?>
			<h3>Password Berhasil Diisi</h3>Selamat, Password Berhasil Diisi<br/><a href="<?php echo $linkmod;?>/" title="Kembali">Kembali</a><?php
		}
		elseif($tipe=="save_fail"){ ?>
			<h3>Data Gagal Diisi</h3>Maaf, Data Gagal Diisi<br/><a href="<?php echo $linkmod;?>/" title="Kembali">Kembali</a><?php
		}
		elseif($tipe=="edit_ok"){ ?>
			<h3>Data Berhasil Diisi</h3>Selamat, Data Berhasil Diisi<br/><a href="<?php echo $linkmod;?>/" title="Kembali">Kembali</a><?php
		}
		elseif($tipe=="delete_ok"){ ?>
			<h3>Data Berhasil Dihapus</h3>Selamat, Data Berhasil Dihapus<br/><a href="<?php echo $linkmod;?>/" title="Kembali">Kembali</a><?php
		}
		elseif($tipe=="delete_pictok"){ ?>
			<h3>picture Berhasil Dihapus</h3>Selamat, picture Berhasil Diisi<br/><a href="<?php echo $linkmod;?>/" title="Kembali">Kembali</a><?php
		}
		elseif($tipe=="email_notvalid"){ ?>
			<h3>Email Anda Salah</h3>Maaf, Email Anda Tidak Valid<br/><a href="<?php echo $linkmod;?>/" title="Kembali">Kembali</a><?php
		}
		else {
		}
	}	
	
	
	function bulan($bulan) { 
		if ($bulan=="1") { $this->bulannama="Januari"; } elseif ($bulan=="2") { $this->bulannama="Februari"; }
		elseif ($bulan=="3") { $this->bulannama="Maret"; } elseif ($bulan=="4") { $this->bulannama="April"; }
		elseif ($bulan=="5") { $this->bulannama="Mei"; } elseif ($bulan=="6") { $this->bulannama="Juni"; }
		elseif ($bulan=="7") { $this->bulannama="Juli"; } elseif ($bulan=="8") { $this->bulannama="Agustus"; }
		elseif ($bulan=="9") { $this->bulannama="September"; } elseif ($bulan=="10") { $this->bulannama="Oktober"; }
		elseif ($bulan=="11") { $this->bulannama="November"; } elseif ($bulan=="12") { $this->bulannama="Desember"; }
		else { $this->bulannama=""; } 
	}
	
	
}
?>