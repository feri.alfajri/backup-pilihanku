<?php 
class model { 
	public function __construct() { 
		$this->koneksi();
		$this->get_variable();
	}
	 
	function koneksi() {		
		$this->koneksi=$koneksi=mysqli_connect(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
	}
	
		
	function putus() {		 
		mysqli_close($this->koneksi);
	}
	
	
	function get_variable() { 
	    if(empty($_GET['subdomain'])) {
    		if (empty($_GET['menu'])) { $this->menu=""; } else { $this->menu=strtok($_GET['menu'],"'"); }
    		if (empty($_GET['link'])) { $this->link=""; } else { $this->link=strtok($_GET['link'],"'"); }			
    		if (empty($_GET['no'])) { $this->no=""; } else { $this->no=strtok($_GET['no'],"'"); }
    		if (empty($_GET['act'])) { $this->act=""; } else { $this->act=strtok($_GET['act'],"'"); }	
    		if (empty($_GET['orderby'])) { $this->orderby=""; } else { $this->orderby=strtok($_GET['orderby'],"'"); }	
    		if (empty($_GET['by'])) { $this->by=""; } else { $this->by=strtok($_GET['by'],"'"); }	
    		if (empty($_GET['page'])) { $this->page=""; } else { $this->page=strtok($_GET['page'],"'"); }
	    }
	    else {
	        $url=substr($_SERVER['REQUEST_URI'],1);
    		$url=explode('/',$url);	
    		if (empty($url[1])) {		$this->menu=$url[0]; 	$this->link=""; 		$this->no=""; 		$this->act=""; 		$this->page=""; 		$this->orderby="";      $this->by="";       }
    		elseif (empty($url[2])) { 	$this->menu=$url[0]; 	$this->link=$url[1];    $this->no=""; 		$this->act=""; 		$this->page="";			$this->orderby="";      $this->by="";    }
    		elseif (empty($url[3])) { 	
    			if ($url[1]=="page") { 	$this->menu=$url[0]; 	$this->link="";  		$this->no=""; 		$this->act=""; 		$this->page=$url[2];	$this->orderby="";      $this->by="";       }
    			else { 					$this->menu=$url[0]; 	$this->link=""; 		$this->no=$url[1]; 	$this->act=$url[2]; $this->page="";			$this->orderby="";      $this->by="";       } 
    		}
    		elseif (empty($url[4])) { 
    			if ($url[2]=="page") { 	$this->menu=$url[0]; 	$this->link=$url[1]; 	$this->no=""; 		$this->act=""; 		$this->page=$url[3]; 	$this->orderby="";      $this->by="";       }
    			else { 					$this->menu=$url[0]; 	$this->link=$url[1]; 	$this->no=$url[2]; 	$this->act=$url[3]; $this->page="";			$this->orderby="";      $this->by="";       }
    		} 
    		elseif (empty($url[6])) { 	
    			if ($url[4]=="page") { 	$this->menu=$url[0]; 	$this->link=$url[1]; 	$this->no=$url[2]; 	$this->act=$url[3]; $this->page=$url[5]; 	$this->orderby="";      $this->by="";       }
    			else { 					$this->menu=$url[0]; 	$this->link=$url[1]; 	$this->no=$url[2];  $this->act=$url[3]; $this->page=""; 	    $this->orderby=$url[4]; $this->by=$url[5];  }
    		} 
    		elseif (empty($url[8])) { 
    			                        $this->menu=$url[0]; 	$this->link=$url[1]; 	$this->no=$url[2]; 	$this->act=$url[3]; $this->page=$url[7]; 	$this->orderby=$url[4]; $this->by=$url[5]; 
    		} 

	    }
	}
	
	function edit($judulweb,$tabel,$formulir,$syarat) {
		$menu=$this->menu;
		$form=explode(",",$formulir);
		$jumform=count($form)-1;
		$ubah="";
		for($i=0;$i<=$jumform;$i++){
			if ($i==$jumform) { $koma=""; } else { $koma=", "; }
			if ($form[$i]=="password") { $_POST[$form[$i]]; $isikolom=md5($_POST[$form[$i]]); }
			else { $isikolom=strip_tags($_POST[$form[$i]]); $isikolom=str_replace("'","",$isikolom); $isikolom=str_replace('"','',$isikolom); }
			$ubah=$ubah.$form[$i]."='".$isikolom."'".$koma;
		}
		mysqli_query($this->koneksi,"UPDATE $tabel SET $ubah $syarat"); ?>
		<p>Terima Kasih, Update Data telah berhasil dilakukan.<br/><a href="<?php echo URL_DOMAIN.$menu;?>/" title="Kembali">Kembali</a></p><?php	
	}
	
	
	function paging($jumhal,$linkpage,$page) {
		if ($page>1) { $prev=$page-1; ?><li><a href="<?php echo $linkpage;?>/" title="First">&laquo;</a></li><li><a href="<?php echo $linkpage."/page/".$prev;?>/" title="Prev">&lsaquo;</a></li><?php } 
		else { ?><li><a href="#" title="First">&laquo;</a></li><li><a href="#" title="Prev">&lsaquo;</a></li><?php } 
		$sebakhir=$jumhal-1;
		if ($jumhal<=3) { $awal=1; $akhir=$jumhal; }
		else {
			if ($page=="") { $awal=1; $akhir=$page+3; }
			elseif ($page==1) { $awal=1; $akhir=$page+2; }
			elseif ($page==2) { $awal=1; $akhir=$page+2; }
			elseif ($page==$jumhal) { $awal=$page-2; $akhir=$jumhal; }
			elseif ($page==$sebakhir) { $awal=$page-2; $akhir=$jumhal; }
			else { $awal=$page-2; $akhir=$page+2; }
		}
		for ($i=$awal; $i<=$akhir; $i++) {
			if ($i!=$page) { ?><li><a href="<?php echo $linkpage."/page/".$i;?>/" title="<?php echo $i;?>"><?php echo $i;?></a></li><?php }  
			else { ?><li class="active"><a href="#"><?php echo $i;?></a></li><?php } 
		}
		if ($page<$jumhal){ $next=$page+1; ?><li><a href="<?php echo $linkpage."/page/".$next;?>/" title="Next">&rsaquo;</a></li><li><a href="<?php echo $linkpage."/page/".$jumhal;?>/" title="Last">&raquo;</a></li><?php } 
		else { ?><li><a href="#" title="Next">&rsaquo;</a></li><li><a href="#" title="Last">&raquo;</a></li><?php } 
	}
	
}
?>