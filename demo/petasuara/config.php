<?php
// HTTP
define('HTTP_SERVER', 'http://www.pilihanku.net/demo/petasuara/');
define('HTTPS_SERVER', 'https://www.pilihanku.net/demo/petasuara/');
define('LISENSI', 'pilihanku.net');
define('URL_DOMAIN', 'https://www.pilihanku.net/demo/petasuara/');

// DIR
define('DIR_ROUTES', '/home/pilihanku/public_html/demo/petasuara/app/routes/');
define('DIR_CONTROLLER', '/home/pilihanku/public_html/demo/petasuara/app/controller/');
define('DIR_MODEL', '/home/pilihanku/public_html/demo/petasuara/app/model/');
define('DIR_LIBRARY', '/home/pilihanku/public_html/demo/petasuara/app/library/');
define('DIR_VIEW', '/home/pilihanku/public_html/demo/petasuara/app/view/');
define('DIR_THEME', '/home/pilihanku/public_html/demo/petasuara/app/view/theme/');
define('DIR_MODULE', '/home/pilihanku/public_html/demo/petasuara/app/view/module/');

// DIR FILE
define('DIR_UPLOAD', '/home/pilihanku/public_html/demo/petasuara/app/upload/');
define('DIR_IMAGE', '/home/pilihanku/public_html/demo/petasuara/app/upload/image/');
define('DIR_PICTURE', '/home/pilihanku/public_html/demo/petasuara/app/upload/picture/');
define('DIR_FILE', '/home/pilihanku/public_html/demo/petasuara/app/upload/file/');

// URL
define('URL_UPLOAD', 'https://www.pilihanku.net/demo/petasuara/app/upload/');
define('URL_IMAGE', 'https://www.pilihanku.net/demo/petasuara/app/upload/image/');
define('URL_PICTURE', 'https://www.pilihanku.net/demo/petasuara/app/upload/picture/');
define('URL_FILE', 'https://www.pilihanku.net/demo//petasuara/app/upload/file/');
define('URL_THEME', 'https://www.pilihanku.net/demo/petasuara/app/view/theme/');

// THEME
define('THEME','sbadmin2');
define('THEME_MEMBER', 'sbadmin2');
define('THEME_ADMIN', 'sbadmin2');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'pilihanku_root');
define('DB_PASSWORD', 'u@&m)#a*^m-cpanel');
define('DB_DATABASE', 'pilihanku_relawandb');
define('DB_ERROR', '<center><h2>MOHON MAAF, KAMI KELEBIHAN BEBAN<br>MOHON KUNJUNGI KAMI BEBERAPA SAAT LAGI...!</h2></center>');
define('DB_PORT', '3306');

// SMTP SETTING
define('SMTP_KONEKSI', 'ssl');
define('SMTP_PORT', 465);
define('SMTP_HOST', 'smtp.gmail.com');
define('SMTP_USERNAME', 'smtpmypro@gmail.com'); 
define('SMTP_PASSWORD', '31031997');
define('ASAL_EMAIL', 'budimark@gmail.com');

//SETTING METATAG GOOGLE
$nameweb = 'Peta Suara 2019';
$titleweb = 'Peta Suara 2019';
$metadesc = 'Peta Suara 2019';	
$metakey = 'Peta Suara 2019';	
$logo = 'logo.png';
$favicon = 'favicon.png';	
$footer = '© 2022 copyright. All rights reserved.';	

//ADDRESS
$address = 'KOMPLEK RUKO PURI GADING<br/>Jl. Puri Gading Raya Blok I.02 No.5 Jatimelati Kec. Pondok Melati ';
$city = 'Kota Bekasi';
$phone = '622184310362';
$mobile = '6282210329429';
$fax = '021 - 8431 0362';
$email = 'info@hasanahtours.com';
$facebook = 'https://www.facebook.com/hasanahtoursandtravel';
$twitter = '';
$instagram = 'https://www.instagram.com/hasanah.tours.travel';
$youtube = 'https://www.youtube.com/channel/UCvmKcTHt44GM7PPed-HuDBg';
$tiktok = 'https://www.tiktok.com/@hasanahtour_umrohhaji';
$linkedin = '';

?>