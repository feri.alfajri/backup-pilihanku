<?php

session_start();

include 'ssl.php';
include 'config.php';
include DIR_MODEL.'model.php';

$model=new model();
$model->koneksi();	
$model->get_variable();	

$menu=$model->menu; 
$link=$model->link;
$no=$model->no;
$act=$model->act;
$orderby=$model->orderby;
$by=$model->by;
$page=$model->page;

if ($menu=='logout') {	
	unset($_SESSION['name']); 
    unset($_SESSION['uname']); 
    unset($_SESSION['pword']); 
    unset($_SESSION['cat']);
    unset($_SESSION['pict']);
    header("location:".URL_DOMAIN); 
} 
elseif ($menu=='admstr') {	 
	include DIR_ROUTES.'admin.php'; 	
} 
elseif ($menu=='member') {	 
	include DIR_ROUTES.'member.php'; 	
}
else {
	include DIR_ROUTES.'public.php'; 	
}

$model->putus();
?>
