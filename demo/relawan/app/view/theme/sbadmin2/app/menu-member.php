<li class="nav-item <?php if ($link=='') { echo 'active'; } ?>">
    <a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>">
		<i class="fas fa-fw fa-desktop"></i><span>Dashboard</span>
	</a>
</li>
<hr class="sidebar-divider my-0">
 
<li class="nav-item <?php if ($link=='jamaah_register' or $link=='jamaah_prospect' or $link=='jamaah' or $link=='jamaah_alumni') { echo 'active'; } ?>">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#jamaah" aria-expanded="true" aria-controls="jamaah">
        <i class="fas fa-fw fa-users"></i><span>Jamaah</span>
    </a>
    <div id="jamaah" class="collapse <?php if ($link=='jamaah_register' or $link=='jamaah_prospect' or $link=='jamaah' or $link=='jamaah_alumni') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a href="<?php echo URL_DOMAIN.$menu;?>/jamaah_register" class="collapse-item <?php if ($link=='jamaah_register') { echo 'active'; } ?>" title="Pendaftaran">Pendaftaran</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/jamaah_prospect" class="collapse-item <?php if ($link=='jamaah_prospect') { echo 'active'; } ?>" title="Prospek">Prospek</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/jamaah" class="collapse-item <?php if ($link=='jamaah') { echo 'active'; } ?>" title="Jamaah">Jamaah</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/jamaah_alumni" class="collapse-item <?php if ($link=='jamaah_alumni') { echo 'active'; } ?>" title="Alumni">Alumni</a>
        </div>
    </div>
</li>
<hr class="sidebar-divider my-0">

<li class="nav-item">
    <a class="nav-link collapsed" href="<?php echo URL_DOMAIN;?>logout" title="Logout" onclick="return confirm('Yakin Keluar ?');">
        <i class="fas fa-fw fa-sign-out-alt"></i><span>Logout</span>
    </a>
</li>

<div class="text-center d-none d-md-inline mt-3">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>