<li class="nav-item <?php if ($link=='') { echo 'active'; } ?>">
    <a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>">
		<i class="fas fa-fw fa-desktop"></i><span>Dashboard</span>
	</a>
</li>
<hr class="sidebar-divider my-0">

<li class="nav-item <?php if ($link=='relawan' or $link=='dashboard-relawan' or $link=='kordes' or $link=='korcam') { echo 'active'; } ?>">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#relawan" aria-expanded="true" aria-controls="relawan">
        <i class="fas fa-fw fa-user"></i><span>Relawan</span>
    </a>
    <div id="relawan" class="collapse <?php if ($link=='relawan' or $link=='dashboard-relawan' or $link=='kordes' or $link=='korcam') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a href="<?php echo URL_DOMAIN.$menu;?>/dashboard-relawan" class="collapse-item <?php if ($link=='dashboard-relawan') { echo 'active'; } ?>" title="Dashboard">Dashboard</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/relawan" class="collapse-item <?php if ($link=='relawan') { echo 'active'; } ?>" title="Relawan">Relawan</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/kordes" class="collapse-item <?php if ($link=='kordes') { echo 'active'; } ?>" title="Kordes">Kordes</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/korcam" class="collapse-item <?php if ($link=='korcam') { echo 'active'; } ?>" title="Korcam">Korcam</a>
        </div>
    </div>
</li>
<hr class="sidebar-divider my-0">

<li class="nav-item <?php if ($link=='pendukung' or $link=='dashboard-pendukung') { echo 'active'; } ?>">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pendukung" aria-expanded="true" aria-controls="pendukung">
        <i class="fas fa-fw fa-users"></i><span>Pendukung</span>
    </a>
    <div id="pendukung" class="collapse <?php if ($link=='pendukung' or $link=='dashboard-pendukung') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a href="<?php echo URL_DOMAIN.$menu;?>/dashboard-pendukung" class="collapse-item <?php if ($link=='dashboard-pendukung') { echo 'active'; } ?>" title="Dashboard">Dashboard</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/pendukung" class="collapse-item <?php if ($link=='pendukung') { echo 'active'; } ?>" title="Pendukung">Pendukung</a>
        </div>
    </div>
</li>
<hr class="sidebar-divider my-0">

<li class="nav-item <?php if ($link=='send' or $link=='message') { echo 'active'; } ?>">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#message" aria-expanded="true" aria-controls="message">
        <i class="fas fa-fw fa-envelope"></i><span>Pesan</span>
    </a>
    <div id="message" class="collapse <?php if ($link=='send' or $link=='message') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a href="<?php echo URL_DOMAIN.$menu;?>/send" class="collapse-item <?php if ($link=='send') { echo 'active'; } ?>" title="Kirim Pesan">Kirim Pesan</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/message" class="collapse-item <?php if ($link=='message') { echo 'active'; } ?>" title="Riwayat Pesan">Riwayat Pesan</a>
        </div>
    </div>
</li>
<hr class="sidebar-divider my-0">

<li class="nav-item <?php if ($link=='event-add' or $link=='event-next' or $link=='event-history') { echo 'active'; } ?>">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#event" aria-expanded="true" aria-controls="event">
        <i class="fas fa-fw fa-calendar"></i><span>Kegiatan</span>
    </a>
    <div id="event" class="collapse <?php if ($link=='event-add' or $link=='event-next' or $link=='event-history') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a href="<?php echo URL_DOMAIN.$menu;?>/event-next" class="collapse-item <?php if ($link=='event-next') { echo 'active'; } ?>" title="Kegiatan Terdekat">Kegiatan Terdekat</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/event-history" class="collapse-item <?php if ($link=='event-history') { echo 'active'; } ?>" title="Riwayat Kegiatan">Riwayat Kegiatan</a>
        </div>
    </div>
</li>
<hr class="sidebar-divider my-0">

<li class="nav-item <?php if ($link=='image' or $link=='video') { echo 'active'; } ?>">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#media" aria-expanded="true" aria-controls="media">
        <i class="fas fa-fw fa-bullhorn"></i><span>Media</span>
    </a>
    <div id="media" class="collapse <?php if ($link=='image' or $link=='video') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a href="<?php echo URL_DOMAIN.$menu;?>/image" class="collapse-item <?php if ($link=='image') { echo 'active'; } ?>" title="Gambar">Gambar</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/video" class="collapse-item <?php if ($link=='video') { echo 'active'; } ?>" title="Video">Video</a>
        </div>
    </div>
</li>
<hr class="sidebar-divider my-0">

<li class="nav-item <?php if ($link=='tps' or $link=='dusun' or $link=='desa' or $link=='kecamatan' or $link=='kabupaten') { echo 'active'; } ?>">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#master" aria-expanded="true" aria-controls="master">
        <i class="fas fa-fw fa-edit"></i><span>Master</span>
    </a>
    <div id="master" class="collapse <?php if ($link=='tps' or $link=='dusun' or $link=='desa' or $link=='kecamatan' or $link=='kabupaten') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <a href="<?php echo URL_DOMAIN.$menu;?>/tps" class="collapse-item <?php if ($link=='tps') { echo 'active'; } ?>" title="TPS">TPS</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/dusun" class="collapse-item <?php if ($link=='dusun') { echo 'active'; } ?>" title="Dusun">Dusun</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/desa" class="collapse-item <?php if ($link=='desa') { echo 'active'; } ?>" title="Desa">Desa</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/kecamatan" class="collapse-item <?php if ($link=='kecamatan') { echo 'active'; } ?>" title="Kecamatan">Kecamatan</a>
            <a href="<?php echo URL_DOMAIN.$menu;?>/kabupaten" class="collapse-item <?php if ($link=='kabupaten') { echo 'active'; } ?>" title="Kabupaten">Kabupaten</a>
        </div>
    </div>
</li>
<hr class="sidebar-divider my-0">

<li class="nav-item <?php if ($link=='user') { echo 'active'; } ?>">
    <a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/user">
		<i class="fas fa-fw fa-user"></i><span>User / Admin</span>
	</a>
</li>
<hr class="sidebar-divider my-0">

<li class="nav-item">
    <a class="nav-link collapsed" href="<?php echo URL_DOMAIN;?>logout" title="Logout" onclick="return confirm('Yakin Keluar ?');">
        <i class="fas fa-fw fa-sign-out-alt"></i><span>Logout</span>
    </a>
</li>

<div class="text-center d-none d-md-inline mt-3">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>