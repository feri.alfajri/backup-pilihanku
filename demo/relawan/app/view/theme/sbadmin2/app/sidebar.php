<ul class="navbar-nav bg-danger sidebar sidebar-dark accordion" id="accordionSidebar">
    
	<a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo URL_DOMAIN.$menu;?>">
	    <div class="sidebar-brand-icon" style="padding-top:10px">
          <img src="<?php echo URL_IMAGE;?>logo.png"  width="100%" /> 
        </div>
	</a>
	<?php
	if ($access == 'admin') {
		include 'menu-admin.php';
	}
	else {
		if ($_SESSION['pict']=='') { $pictuser = 'member.png'; }  else { $pictuser = $_SESSION['pict']; } 
		?>
		<div class="p-2 text-center text-white">
			<img class="img-profile rounded-circle" src="<?php echo URL_PICTURE.$pictuser;?>" width="90%" style="max-width:100px">
			<div>
				<b class="text-uppercase"><?php echo $_SESSION['name'];?></b>
				<br/>
				ID. <?php echo $_SESSION['uname'];?>
				<br/>
				<a class="text-warning" href="<?php echo URL_DOMAIN?>logout" onclick="return confirm('Yakin Keluar ?');">
					<i class="fas fa-sign-out-alt text-warning"></i> <span>Logout</span>
				</a>
			</div>
		 </div>
		<hr class="sidebar-divider my-0">		
		<?php 
		include 'menu-member.php';
	}
	?>
</ul>