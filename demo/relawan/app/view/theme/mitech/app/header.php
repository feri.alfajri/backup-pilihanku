<?php include 'loader.php';?>

<div class="header-bottom-wrap header-sticky bg-white"> 
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="header position-relative">

                    <div class="header__logo">
                        <a href="<?php echo URL_DOMAIN;?>" title="<?php echo $nameweb;?>">
                                <img src="<?php echo URL_IMAGE;?>logo.png" class="img-fluid" alt="Logo">
                            </a>
                    </div>

                    <div class="header-right">

                        <div class="header__navigation menu-style-three d-none d-xl-block">
                            <nav class="navigation-menu">
                                <ul>
                                    <li>
                                        <a href="<?php echo URL_DOMAIN;?>" title="Home"><span>Home</span></a>
                                    </li>
									<li>
                                        <a href="<?php echo URL_DOMAIN;?>profil" title="Profil"><span>Profil</span></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo URL_DOMAIN;?>kontak" title="Kontak"><span>Kontak</span></a>
                                    </li>
                                </ul>
                                
                            </nav>
                        </div>

                        <div class="mobile-navigation-icon d-block d-xl-none" id="mobile-menu-trigger">
                            <i></i>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    


