<script src="<?php echo URL_THEME.THEME;?>/assets/js/vendor/modernizr-2.8.3.min.js"></script>
<script src="<?php echo URL_THEME.THEME;?>/assets/js/vendor/jquery-3.5.1.min.js"></script>
<script src="<?php echo URL_THEME.THEME;?>/assets/js/vendor/jquery-migrate-3.3.0.min.js"></script>
<script src="<?php echo URL_THEME.THEME;?>/assets/js/vendor/bootstrap.min.js"></script>
<script src="<?php echo URL_THEME.THEME;?>/assets/js/plugins/wow.min.js"></script>
<script src="<?php echo URL_THEME.THEME;?>/assets/js/plugins/swiper.min.js"></script>
<script src="<?php echo URL_THEME.THEME;?>/assets/js/plugins/lightgallery.min.js"></script>
<script src="<?php echo URL_THEME.THEME;?>/assets/js/plugins/waypoints.min.js"></script>
<script src="<?php echo URL_THEME.THEME;?>/assets/js/plugins/countdown.min.js"></script>
<script src="<?php echo URL_THEME.THEME;?>/assets/js/plugins/isotope.min.js"></script>
<script src="<?php echo URL_THEME.THEME;?>/assets/js/plugins/masonry.min.js"></script>
<script src="<?php echo URL_THEME.THEME;?>/assets/js/plugins/images-loaded.min.js"></script>
<script src="<?php echo URL_THEME.THEME;?>/assets/js/plugins/wavify.js"></script>
<script src="<?php echo URL_THEME.THEME;?>/assets/js/plugins/jquery.wavify.js"></script>
<script src="<?php echo URL_THEME.THEME;?>/assets/js/plugins/circle-progress.min.js"></script>
<script src="<?php echo URL_THEME.THEME;?>/assets/js/plugins/counterup.min.js"></script>
<script src="<?php echo URL_THEME.THEME;?>/assets/js/plugins/animation-text.min.js"></script>
<script src="<?php echo URL_THEME.THEME;?>/assets/js/plugins/vivus.min.js"></script>

<script src="<?php echo URL_THEME.THEME;?>/assets/js/plugins/some-plugins.js"></script>
<!--
<script src="<?php echo URL_THEME.THEME;?>/assets/js/plugins/plugins.min.js"></script>
-->
<script src="<?php echo URL_THEME.THEME;?>/assets/js/main.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-47480275-1"></script> 
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-47480275-1');
</script>

