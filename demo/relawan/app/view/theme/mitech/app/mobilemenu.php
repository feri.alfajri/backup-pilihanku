<div class="mobile-menu-overlay" id="mobile-menu-overlay">
    <div class="mobile-menu-overlay__inner">
        
        <div class="mobile-menu-overlay__header">
            <div class="container-fluid">
                <div class="row align-items-center"> 
                    <div class="col-md-6 col-8">
                        <div class="logo">
                            <a href="<?php echo URL_DOMAIN;?>" title="<?php echo $nameweb;?>">
                                <img src="<?php echo URL_IMAGE;?>logo.png" class="img-fluid" alt="Logo Bumitekno">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 col-4">
                        <div class="mobile-menu-content text-right">
                            <span class="mobile-navigation-close-icon" id="mobile-menu-close-trigger"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="mobile-menu-overlay__body">
            <nav class="offcanvas-navigation offcanvas-navigation--onepage">
                <ul>
                    
                    <li><a href="<?php echo URL_DOMAIN;?>" title="<?php echo $nameweb;?>">Home</a></li>
                    <li><a href="<?php echo URL_DOMAIN;?>profil" title="Profil"><span>Profil</span></a></li>
                    <li><a href="<?php echo URL_DOMAIN;?>kontak" title="Kontak">Kontak</a></li>
                </ul>
            </nav>
        </div>
        
    </div>
</div>