<meta http-equiv="content-type" content="text/html; charset=utf-8" /> 
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet" href="<?php echo URL_THEME.THEME;?>/assets/css/vendor/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo URL_THEME.THEME;?>/assets/css/vendor/cerebrisans.css">
<link rel="stylesheet" href="<?php echo URL_THEME.THEME;?>/assets/css/vendor/fontawesome-all.min.css">
<link rel="stylesheet" href="<?php echo URL_THEME.THEME;?>/assets/css/plugins/swiper.min.css">
<link rel="stylesheet" href="<?php echo URL_THEME.THEME;?>/assets/css/plugins/animate-text.css">
<link rel="stylesheet" href="<?php echo URL_THEME.THEME;?>/assets/css/plugins/animate.min.css">
<link rel="stylesheet" href="<?php echo URL_THEME.THEME;?>/assets/css/plugins/lightgallery.min.css">
<link rel="stylesheet" href="<?php echo URL_THEME.THEME;?>/assets/css/style.css">
<meta name="google-site-verification" content="EgEhPppfAJXPA7u7hbQsZht2AWBRHbiGtxCHMVc8ef8" />
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-19QMNV6D1V"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-19QMNV6D1V');
</script>
