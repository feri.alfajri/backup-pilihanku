<div class="footer-area-wrapper bg-gray">
    <div class="footer-area section-space--ptb_80">
        <div class="container">
            <div class="row footer-widget-wrapper">
                <div class="col-lg-6 col-md-6 col-sm-6 footer-widget"> 
                    <div class="footer-widget__logo mb-20"> 
                        <img src="<?php echo URL_IMAGE;?>logo.png" class="img-fluid" alt="logo bumitekno" align="left" style="margin-right:20px" width="200">
                        <p>Hasanah Tours & Travel adalah sebuah perushaaan travel umroh dibawah naungan PT. ASYESA. Kami mampu menjawab semua kebutuhan perjalanan ibadah anda ke tanah suci. 
						dengan jaminan memberikan pelayanan sepenuh hati kepada Anda.</p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 footer-widget">
                    <h6 class="footer-widget__title mb-20">Menu</h6>
                    <ul class="footer-widget__list">
                        <li><a href="<?php echo URL_DOMAIN;?>profil" title="Profil"><span>Profil</span></a></li>
                    	<li><a href="<?php echo URL_DOMAIN;?>kontak" title="Kontak"><span>Kontak</span></a></li>
                    </ul>
                </div>
				<div class="col-lg-4 col-md-4 col-sm-6 footer-widget">
                    <h6 class="footer-widget__title mb-20">Contact</h6>
                    <ul class="footer-widget__list">
                    	<li>Telepon : <?php echo $phone;?></li>
                    	<li>WA : <?php echo $mobile;?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <div class="footer-copyright-area section-space--pb_30">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 text-center text-md-left">
                    <span class="copyright-text">&copy; 2022 <a href="https://www.budimark.com/" title="budimark.com">BudiMark.com</a> - All Rights Reserved.</span>
                </div>
                <div class="col-md-6 text-center text-md-right">
                    <ul class="list ht-social-networks solid-rounded-icon">
                        <li class="item">
                            <a href="<?php echo $facebook;?>" target="_blank" aria-label="Facebook" class="social-link hint--bounce hint--top hint--primary">
                                <i class="fab fa-facebook-f link-icon"></i>
                            </a>
                        </li>
                        <li class="item">
                            <a href="<?php echo $instagram;?>" target="_blank" aria-label="Instagram" class="social-link hint--bounce hint--top hint--primary">
                                <i class="fab fa-instagram link-icon"></i>
                            </a>
                        </li>
                        <li class="item">
                            <a href="<?php echo $youtube;?>" target="_blank" aria-label="Youtube" class="social-link hint--bounce hint--top hint--primary">
                                <i class="fab fa-youtube link-icon"></i>
                            </a>
                        </li>
                        <li class="item">
                            <a href="<?php echo $tiktok;?>" target="_blank" aria-label="Tiktok" class="social-link hint--bounce hint--top hint--primary">
                                <i class="fab fa-tiktok link-icon"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
</div>

<a href="#" class="scroll-top" id="scroll-top">
    <i class="arrow-top fal fa-long-arrow-up"></i>
    <i class="arrow-bottom fal fa-long-arrow-up"></i>
</a>

<?php include 'mobilemenu.php';?>