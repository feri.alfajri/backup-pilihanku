<div class="breadcrumb-area" style="padding:50px 0px;">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_box text-center">
                    <h1 class="breadcrumb-title"><?php echo $titleweb;?></h1>
                    <ul class="breadcrumb-list"> 
                        <li class="breadcrumb-item"><a href="<?php echo URL_DOMAIN;?>">Home</a></li>
                        <li class="breadcrumb-item active"><?php echo $titleweb;?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div> 
