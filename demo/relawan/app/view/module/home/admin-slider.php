<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_ADMIN.'/app/asset-head.php';?>
</head>

<body class="nav-fixed">
   
	<?php include DIR_THEME.THEME_ADMIN.'/app/topbar.php'; ?>
	
	<div id="layoutSidenav">
	
        <div id="layoutSidenav_nav">
			<?php include DIR_THEME.THEME_ADMIN.'/app/sidebar.php'; ?>			
		</div>
		
		<div id="layoutSidenav_content">
			<main>
				
				<?php include DIR_THEME.THEME_ADMIN.'/app/header.php'; ?>

				<div class="container-xl px-4 mt-4">
					<div class="card mb-3">
						<div class="card-body">
							
							<?php
							$title = 'HomePage';  
							
							$menu_label = 'Tambah';
							$menu_action = 'create';
							
							$table_name = 'home_slider';
							$table_label = 'gambar,nama,urutan';
							$table_column = 'picture,name,number';
							$table_width = '10,50,10'; 
							$table_button = 'update,read,delete';  
							$table_condition = '';
							
							$create_type = 'picture';
							$create_label = 'nama,urutan,deskripsi,url,text url,gambar';
							$create_form = 'name,number,description,url,url_text,picture';
							$create_condition = '';
							
							$read_label = 'nama,urutan,deskripsi,url,text url,gambar';
							$read_content = 'name,number,description,url,url_text,picture';
							$read_condition = "WHERE no='$no'";
							
							$update_type = 'picture';
							$update_label = 'nama,urutan,deskripsi,url,text url,gambar';
							$update_form = 'name,number,description,url,url_text,picture';
							$update_condition = "WHERE no='$no'";
							
							$delete_condition = "WHERE no='$no'";
							
							if (empty ($act)) {
							
								$admin->menu($menu_label,$menu_action);
								$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
								
							} 
							elseif ($act=='create') {
			
								$admin->title('Tambah '.$title);
								$admin->create($table_name,$create_type,$create_label,$create_form,$create_condition);
								
							} 
							elseif ($act=='read') {
								
								$admin->title('Detail '.$title);
								$admin->read($table_name,$read_label,$read_content,$read_condition); 
								
							} 
							elseif ($act=='update') {
								
								$admin->title('Ubah '.$title);
								$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
								
							} 
							elseif ($act=='delete') {
			
								$admin->menu($menu_label,$menu_action);
								//$admin->delete($table_name,$delete_condition);		
								echo '<div class="alert alert-danger" role="alert">Demi Keamanan, Fitur Hapus Dinonaktifkan</div>';
								$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
								
							} 
							else {
			
								$admin->menu($menu_label,$menu_action);
								$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
								
							}
							?>

						</div>
					</div>
				</div>
				
			</main>
		</div>
		
	</div>	
	
	<?php include DIR_THEME.THEME_ADMIN.'/app/asset-body.php'; ?>
	
</body>
 
</html>