            <div class="feature-icon-wrapper section-space--ptb_70 bg-white">
                <div class="container"> 
                    <div class="section-title-wrap text-center section-space--mb_20">
                        <h3 class="heading">Mengapa Harus Segera Go Online</h3>
                    </div>
                    <div class="feature-list__three">
                        <div class="row">
                            
                            <div class="col-lg-4">
                                <div class="grid-item animate">
                                    <div class="ht-box-icon style-03">
                                        <div class="icon-box-wrap">
                                            <div class="content-header">
                                                <div class="icon"><i class="fal fa-map-marked-alt"></i></div>
                                                <h5 class="heading">Jangkauan Luas</h5>
                                            </div>
                                            <div class="content">
                                                <div class="text">Jangkauan internet sangat luas dibandingkan dengan media-media lainnya. Dengan internet, Anda dapat terhubung dengan konsumen Anda dari seluruh dunia.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="grid-item animate">
                                    <div class="ht-box-icon style-03">
                                        <div class="icon-box-wrap">
                                            <div class="content-header">
                                                <div class="icon"><i class="fal fa-hourglass-half"></i></div>
                                                <h5 class="heading">Waktu Tak Terbatas</h5>
                                            </div>
                                            <div class="content">
                                                <div class="text">Anda tetap dapat menerima konsumen meskipun kantor Anda sedang ditutup / libur. Digital marketing dapat berjalan selama 24 jam nonstop setiap harinya.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="grid-item animate">
                                    <div class="ht-box-icon style-03">
                                        <div class="icon-box-wrap">
                                            <div class="content-header">
                                                <div class="icon"><i class="fal fa-users"></i></div>
                                                <h5 class="heading">Pengguna Bertambah</h5>
                                            </div>
                                            <div class="content">
                                                <div class="text">Pengguna internet di Indonesia telah mencapai 212,35 juta. Angka ini terus bertumbuh seiring dengan semakin majunya dan semakin terjangkaunya biaya internet.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="grid-item animate">
                                    <div class="ht-box-icon style-03">
                                        <div class="icon-box-wrap">
                                            <div class="content-header">
                                                <div class="icon"><i class="fal fa-compress-arrows-alt"></i></div>
                                                <h5 class="heading">Lebih Tertarget</h5>
                                            </div>
                                            <div class="content">
                                                <div class="text">Dalam program Digital Marketing, Anda bisa menentukan siapa target pasar dari bisnis Anda. Budget yang Anda keluarkan tidak akan sia-sia karena tepat sasaran.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="grid-item animate">
                                    <div class="ht-box-icon style-03">
                                        <div class="icon-box-wrap">
                                            <div class="content-header">
                                                <div class="icon"><i class="fal fa-wallet"></i></div>
                                                <h5 class="heading">Budget Hemat dan Fleksibel</h5>
                                            </div>
                                            <div class="content">
                                                <div class="text">Anda dapat mengatur  budget marketing Anda. Dan sudah terbukti biaya marketing online jauh lebih hemat dibandingan dengan biaya marketing offline / tradisional.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="grid-item animate">
                                    <div class="ht-box-icon style-03">
                                        <div class="icon-box-wrap">
                                            <div class="content-header">
                                                <div class="icon"><i class="fal fa-hand-holding-usd"></i></div>
                                                <h5 class="heading">Omset Lebih Baik</h5>
                                            </div>
                                            <div class="content">
                                                <div class="text">Berdasarkan survey Google menyebutkan bahwa perusahaan yang menerapkan strategi digital marketing memiliki perkembangan omset 2,8 kali lipat lebih besar.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
					<br/>
					<div class="text-center">
						<h3 class="mb-2">Tapi Sayangnya ?</h3>					
						<h5>
							Untuk Go Online dan menjalankan Strategi Digital Marketing bukanlah hal mudah. <br/>
							Ada banyak sekali hal yang harus dipelajari dan harus dikuasai seperti :<br/>
							Search Engine Optimation (SEO), Search Engine Marketing (SEM), Social Media Marketing, <br/>
							Dan tentunya dengan segala macam varibel dan parameter dalam menentukan strategi serta mengukur keberhasilan program Digital Marketing bisnis Anda.
						</h5>
					</div>
                </div>
            </div>