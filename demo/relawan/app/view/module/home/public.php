<!DOCTYPE html> 
<html class="no-js" lang="id">
     
<head><?php
    include DIR_THEME.THEME.'/app/asset-head.php';
    include DIR_THEME.THEME.'/app/metatag.php';
    include DIR_THEME.THEME.'/app/opengraph.php';
    include DIR_THEME.THEME.'/app/favicon.php';
    include DIR_THEME.THEME.'/app/title.php';
    ?>
</head>

<body>
	<div id="main-wrapper">
	    <div class="site-wrapper-reveal">
            <?php
        	include (DIR_THEME.THEME.'/app/header.php');	
        	
        	include 'headline.php'; 
        	include 'why.php';
			
        	include (DIR_THEME.THEME.'/app/footer.php');
            ?>
	    </div>
	<div>
	<?php
	include (DIR_THEME.THEME.'/app/asset-body.php');
	?>
</body>

</html>