<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_ADMIN.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_ADMIN.'/app/sidebar.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_ADMIN.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<?php 
					function dashboard_caleg() {
						$admin=new admin();
						$admin->get_variable();
						$admin->koneksi();
						$link=$admin->link;
						$menu=$admin->menu;
						$linkmod=URL_DOMAIN.$menu."/".$link; 
						$qcalon=mysqli_query($admin->koneksi,"SELECT name,address,phone,target_kordes,target_korcam,target_suara,target_relawan,picture FROM setting LIMIT 0,1");
						$dcalon=$dcalon=mysqli_fetch_array($qcalon); 
						$namecalon=$dcalon['name'];
						$addresscalon=$dcalon['address'];
						$phonecalon=$dcalon['phone'];
						if ($dcalon['picture']=="") {  $pictcalon="member.png"; } else { $pictcalon=$dcalon['picture'];  } 	
						$target_suara=$dcalon['target_suara'];
						$target_relawan=$dcalon['target_relawan'];
						$target_kordes=$dcalon['target_kordes'];
						$target_korcam=$dcalon['target_korcam'];
						$qrelawan=mysqli_query($admin->koneksi,"SELECT no FROM relawan");
						$jrelawan=mysqli_num_rows($qrelawan);
						$qpendukung=mysqli_query($admin->koneksi,"SELECT no FROM pendukung");
						$jpendukung=mysqli_num_rows($qpendukung);
						if ($target_relawan==0) { $prosenrelawan=0; } else { $prosenrelawan=round($jrelawan/$target_relawan*100,0);  } ?>
						<h2>Dashboard Relawan</h2>
						<div class="row">
							<div class="col-md-6">
								<div class="card bg-danger mb-4 text-white">
									<div class="card-body" style="height:150px;">
										<div style="width:25%;float:left;">
											<img src="<?php echo URL_PICTURE.$pictcalon;?>" class="img-responsive" width="80%" style="background:#EAEAEA;"/>
										</div>
										<div style="width:75%;float:right;">
											<h4>PROGRESS RELAWAN</h4>
											<table width="100%" class="panel-table">
												<tbody>
													<td width="100" class="text-center">
														<div  class="h3 bg-warning"><span class="h1"><?php echo $prosenrelawan;?></span>%</div>
													</td>
													<td style="padding-left:15px;">
														<table width="100%" style="font-size:16px">
															<tr style="border-bottom:1px solid #fff;"><td width="100">Target</td><td width="10">:</td><td class="text-right"><?php echo number_format($target_relawan);?></td></tr>
															<tr><td>Tercapai</td><td>:</td><td class="text-right"><?php echo number_format($jrelawan);?></td></tr>
														</table>
													</td>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card bg-primary mb-4 text-white">
									<div class="card-body" style="height:150px;">
										<table width="100%" class="panel-table">
											<tbody><?php
												$qkab=mysqli_query($admin->koneksi,"SELECT no,name FROM kabupaten ORDER BY urutan ASC");
												while($dkab=mysqli_fetch_array($qkab)){ 
													$nokab=$dkab['no'];
													$namekab=$dkab['name'];
													$qpendukung=mysqli_query($admin->koneksi,"SELECT no FROM relawan WHERE no_kabupaten='$nokab'");
													$jpendukung=mysqli_num_rows($qpendukung);
													?>
													<tr style="border-bottom:1px solid #ffffff;"><td width="150"><?php echo $namekab;?></td><td width="10">:</td><td class="text-right"><?php echo $jpendukung;?></td></tr><?php
												} ?>
											</tbody>
										</table>					
									</div>
								</div>
							</div>
						</div>
						<!--
						<div class="row">
							<div class="col-md-6">
								<div class="card mb-4">
									<div class="card-header bg-warning "><span style="font-size:16px;font-weight:bold;">Kecamatan Paling Banyak Relawan</span></div>
									<div class="card-body">
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th width="40" class="text-center">No</th>
													<th>Nama Kecamatan</th>
													<th width="120" class="text-center">Jum Relawan</th>
												</tr>
											</thead>
											<tbody><?php
												$nokec=1;
												$qkec=mysqli_query($admin->koneksi,"SELECT no,name FROM kecamatan LIMIT 8");
												while($dkec=mysqli_fetch_array($qkec)){ 
													$nokecamatan=$dkec['no'];
													$namekec=$dkec['name'];
													$qpendukung=mysqli_query($admin->koneksi,"SELECT no FROM relawan WHERE no_kecamatan='$nokecamatan'");
													$jpendukung=mysqli_num_rows($qpendukung);
													?>
													<tr>
														<td><?php echo $nokec;?></td>
														<td><?php echo $namekec;?></td>
														<td class="text-right"><?php echo $jpendukung;?></td>
													</tr>
													<?php
													$nokec++;
												} ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card mb-">
									<div class="card-header bg-info "><span style="font-size:16px;font-weight:bold;">Desa Paling Banyak Relawan</span></div>
									<div class="card-body">
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th width="40" class="text-center">No</th>
													<th>Nama Desa</th>
													<th width="120" class="text-center">Jum Relawan</th>
												</tr>
											</thead>
											<tbody><?php
												$nodesa=1;
												$qdesa=mysqli_query($admin->koneksi,"SELECT no,name FROM desa LIMIT 8");
												while($ddesa=mysqli_fetch_array($qdesa)){ 
													$nodesa=$ddesa['no'];
													$namedesa=$ddesa['name'];
													$qpendukung=mysqli_query($admin->koneksi,"SELECT no FROM relawan WHERE no_desa='$nodesa'");
													$jpendukung=mysqli_num_rows($qpendukung);
													?>
													<tr>
														<td><?php echo $nodesa;?></td>
														<td><?php echo $namedesa;?></td>
														<td class="text-right"><?php echo $jpendukung;?></td>
													</tr>
													<?php
													$nodesa++;
												} ?>
											</tbody>
										</table>					
									</div>
								</div>
							</div>
						</div>
						-->
						<div class="row">
						<?php	
						$qkab=mysqli_query($admin->koneksi,"SELECT no,name FROM kabupaten ORDER BY urutan ASC");
						while($dkab=mysqli_fetch_array($qkab)){ 
							$nokab=$dkab['no'];
							$namekab=$dkab['name'];  ?>	
							<div class="col-md-6">
								<div class="card mb-4">
									<div class="card-header"><span style="font-size:16px;font-weight:bold;"><?php echo $namekab;?></span></div>
									<div class="card-body">
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th width="40" class="text-center">No</th>
													<th>Nama Kecamatan</th>
													<th width="100" class="text-center">Target</th>
													<th width="100" class="text-center">Tercapai</th>
													<th width="80" class="text-center">Prosen</th>
												</tr>
											</thead>
											<tbody><?php
											$nomor=1;
											$total_relawan=0;
											$qkecamatan=mysqli_query($admin->koneksi,"SELECT no,name,target_relawan FROM kecamatan WHERE no_kabupaten='$nokab'");
											while($dkecamatan=mysqli_fetch_array($qkecamatan)){
												$nokecamatan=$dkecamatan['no'];
												$namekecamatan=$dkecamatan['name'];
												$target_relawan=$dkecamatan['target_relawan'];
												$qrelawan=mysqli_query($admin->koneksi,"SELECT no FROM relawan WHERE no_kecamatan='$nokecamatan'");
												$jrelawan=mysqli_num_rows($qrelawan);
												$prosen = ceil($jrelawan/$target_relawan*100);
												$total_relawan=$total_relawan+$jrelawan;
												
												?>
												<tr>
													<td class="text-center"><?php echo $nomor;?></td>
													<td><a href="<?php echo $linkmod."/".$nokecamatan;?>/kecamatan/" title="<?php echo $namekecamatan;?>"><?php echo $namekecamatan;?></a></td>
													<td class="text-center"><?php echo $target_relawan;?></td>
													<td class="text-center"><?php if ($jrelawan!=0) { echo $jrelawan; } ?></td>
													<td class="text-center"><?php if ($jrelawan!=0) { echo $prosen.'%'; } ?> </td>
												</tr><?php
												$nomor++;
											}?>
											<tr>
												<th class="text-center"></th>
												<th>TOTAL</th>
												<th class="text-center"><?php echo $total_relawan;?></th>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div><?php
						} 	
						?>
						</div>
						<?php
					}



					function dashboard_calegkecamatan () {
						$admin=new admin();
						$admin->get_variable();
						$admin->koneksi();
						$link=$admin->link;
						$menu=$admin->menu;
						$nokecamatan=$admin->no;
						$linkmod=URL_DOMAIN.$menu; 	
						$qkecamatan=mysqli_query($admin->koneksi,"SELECT no,name FROM kecamatan WHERE no='$nokecamatan' ORDER BY no ASC");
						$dkecamatan=mysqli_fetch_array($qkecamatan);
						$namekecamatan=$dkecamatan['name']; ?>
						<h2>Relawan Kec. <?php echo $namekecamatan;?></h2>
						<div class="card mb-4">
							<div class="card-body">
								<?php
								$qdesa=mysqli_query($admin->koneksi,"SELECT no,name FROM desa WHERE no_kecamatan='$nokecamatan'");
								while($ddesa=mysqli_fetch_array($qdesa)){
									$nodesa=$ddesa['no'];
									$namedesa=$ddesa['name'];
									$qrelawan=mysqli_query($admin->koneksi,"SELECT no,name,nik,address,phone FROM relawan WHERE no_desa='$nodesa'");
									$jrelawan = mysqli_num_rows($qrelawan);
									if ($jrelawan!=0) {										  ?>
										<b style="text-transform:uppercase;">Desa <?php echo $namedesa;?></b>
										<table class="table table-bordered table-hover">
										<thead>
											<tr>
												<th width="40" class="text-center">No</th>
												<th width="250">Nama</th>
												<th width="150" class="text-center">NIK</th>
												<th>Alamat</th>
												<th width="150" class="text-center">Telepon</th>
											</tr>
										</thead>
										<tbody>
										<?php
										$nomor=1;
										while($drelawan=mysqli_fetch_array($qrelawan)) {
											$norelawan=$drelawan['no'];
											$namerelawan=$drelawan['name'];
											$addressrelawan=$drelawan['address'];
											$nikrelawan=$drelawan['nik'];
											$phonerelawan=$drelawan['phone'];?>
											<tr>
												<td class="text-center"><?php echo $nomor;?></td>
												<td><a href="<?php echo $linkmod."/relawan/".$norelawan;?>/read/" title="<?php echo $namerelawan;?>"><?php echo $namerelawan;?></a></td>
												<td class="text-center"><?php echo $nikrelawan;?></td>
												<td><?php echo $addressrelawan;?></td>
												<td class="text-center"><?php echo $phonerelawan;?></td>
											</tr>
											<?php
											$nomor++;
										} 
										?>
										</tbody>
										</table>
										<?php
									}
								}
								?>
							</div>
						</div>
						<?php
					}




					if (empty ($act)) { 
						dashboard_caleg();
					}
					elseif ($act=="kecamatan") {
						dashboard_calegkecamatan();
					}
					else {
						dashboard_caleg();
					}
					?>

					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_ADMIN.'/app/asset-body.php'; ?>
	
</body>
 
</html>