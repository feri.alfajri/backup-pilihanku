<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_ADMIN.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_ADMIN.'/app/sidebar.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_ADMIN.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<?php
					$title = 'Koordinator Kecamatan';  
					
					$menu_label = 'Tambah';
					$menu_action = 'create';
					
					$table_name = 'relawan';
					$table_label = 'gambar,nama,kecamatan,kabupaten,telepon';
					$table_column = 'picture,name,no_kecamatan,no_kabupaten,phone';
					$table_width = '10,40,15,15,15'; 
					$table_button = 'update,read,account,pendukung';  
					$table_condition = " WHERE korcam = '1' ";
					
					$create_type = 'picture';
					$create_label = 'nama,no. KTP,TTL,jenis kelamin,telepon,alamat,kabupaten,kecamatan,gambar';
					$create_form = 'name,nik,birth,sex,phone,address,no_kabupaten,no_kecamatan,picture';
					$create_condition = "korcam,1";
					
					$read_label = 'gambar,nama,no. KTP,TTL,jenis kelamin,telepon,alamat,kabupaten,kecamatan,facebook,instagram,twitter,tiktok,scan KTP';
					$read_content = 'picture,name,nik,birth,sex,phone,address,no_kabupaten,no_kecamatan,facebook,instagram,twitter,tiktok,scanktp';
					$read_condition = "WHERE no='$no'";
					
					$update_type = 'picture';
					$update_label = 'gambar,nama,no. KTP,TTL,jenis kelamin,telepon,alamat,kabupaten,kecamatan,facebook,instagram,twitter,tiktok';
					$update_form = 'picture,name,nik,birth,sex,phone,address,no_kabupaten,no_kecamatan,facebook,instagram,twitter,tiktok';
					$update_condition = "WHERE no='$no'";
					
					$delete_condition = "WHERE no='$no'";
					
					if (empty ($act)) {
					
						$admin->menu($menu_label,$menu_action);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					elseif ($act == 'create') {
	
						$admin->title('Tambah '.$title);
						$admin->create($table_name,$create_type,$create_label,$create_form,$create_condition);
						
					} 
					elseif ($act == 'read') {
						
						$admin->title('Detail '.$title);
						$admin->read($table_name,$read_label,$read_content,$read_condition); 
						
					} 
					elseif ($act == 'update') {
						
						$admin->title('Ubah '.$title);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act == 'account') {
						
						$update_type = '';
						$update_label = 'username,password';
						$update_form = 'username,password';
						$admin->title('Ubah Account '.$title);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act == 'scanktp') {
						
						$update_type = 'picture';
						$update_label = 'skan KTP';
						$update_form = 'scanktp';
						$admin->title('Scan KTP '.$title);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act == 'pendukung') {
						
						$admin->title('Daat Pendukung Dari '.$title);
						$read_label = 'nama,telepon,kabupaten,kecamatan';
						$read_content = 'name,phone,no_kabupaten,no_kecamatan';
						$admin->read($table_name,$read_label,$read_content,$read_condition); 
						
						echo '<h5>Data Pendukung</h5>';
						$table_name = 'pendukung';
						$table_label = 'nama,nik,alamat,desa,telepon';
						$table_column = 'name,nik,address,no_desa,phone';
						$table_width = '40,15,30,15,15'; 
						$table_button = '';  
						$table_condition = " WHERE no_relawan = '$no' ";	
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					elseif ($act == 'delete') {
	
						$admin->menu($menu_label,$menu_action);
						//$admin->delete($table_name,$delete_condition);		
						echo '<div class="alert alert-danger" role="alert">Demi Keamanan, Fitur Hapus Dinonaktifkan</div>';
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					else {
	
						$admin->menu($menu_label,$menu_action);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					}
					?>
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_ADMIN.'/app/asset-body.php'; ?>
	
</body>
 
</html>