<section id="content">
	<div class="content-wrap"> 
		<div class="container">

			<div class="heading-block center border-bottom-0">
				<h1>Page is Under Maintenance</h1>
				<span>Please check back in sometime.</span>
			</div>

			<div class="row justify-content-center">
				<div class="col-sm-6 col-lg-4">
					<div class="feature-box fbox-center fbox-light fbox-plain">
						<div class="fbox-icon">
							<a href="#"><i class="icon-warning-sign"></i></a>
						</div>
						<div class="fbox-content">
							<h3>Why is the Site Down?</h3>
							<p>The site is under maintenance probably because we are working to improve this website drastically.</p>
						</div>
					</div>
				</div>

				<div class="col-sm-6 col-lg-4">
					<div class="feature-box fbox-center fbox-light fbox-plain">
						<div class="fbox-icon">
							<a href="#"><i class="icon-time"></i></a>
						</div>
						<div class="fbox-content">
							<h3>What is the Downtime?</h3>
							<p>We are usually back within 10-15 minutes but it definitely depends on the issue.</p>
						</div>
					</div>
				</div>

				<div class="col-sm-6 col-lg-4">
					<div class="feature-box fbox-center fbox-light fbox-plain">
						<div class="fbox-icon">
							<a href="#"><i class="icon-email3"></i></a>
						</div>
						<div class="fbox-content">
							<h3>Do you need Support?</h3>
							<p>You may simply send us an Email at <a href="mailto:<?php echo $emailweb;?>"><?php echo $emailweb;?></a> if you need urgent support.</p>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>