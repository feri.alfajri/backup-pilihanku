<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_ADMIN.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_ADMIN.'/app/sidebar.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_ADMIN.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<?php
					$title = 'Pendukung';  
					
					$menu_label = 'Tambah';
					$menu_action = 'create';
					
					$table_name = 'pendukung';
					$table_label = 'nama,dusun,desa,kecamatan,kabupaten,telepon';
					$table_column = 'name,no_dusun,no_desa,no_kecamatan,no_kabupaten,phone';
					$table_width = '40,12,12,12,12,12'; 
					$table_button = 'update,read,delete';  
					$table_condition = " ";
					
					$create_type = '';
					$create_label = 'nama,no. KTP,jenis kelamin,telepon,alamat,kabupaten,kecamatan,desa,dusun,tps,relawan';
					$create_form = 'name,nik,sex,phone,address,no_kabupaten,no_kecamatan,no_desa,no_dusun,no_tps,no_relawan';
					$create_condition = "";
					
					$read_label = 'nama,no. KTP,jenis kelamin,telepon,alamat,kabupaten,kecamatan,desa,dusun,tps,relawan,scan KTP';
					$read_content = 'name,nik,sex,phone,address,no_kabupaten,no_kecamatan,no_desa,no_dusun,no_tps,no_relawan,scanktp';
					$read_condition = "WHERE no='$no'";
					
					$update_type = '';
					$update_label = 'nama,no. KTP,jenis kelamin,telepon,alamat,kabupaten,kecamatan,desa,dusun,tps,relawan';
					$update_form = 'name,nik,sex,phone,address,no_kabupaten,no_kecamatan,no_desa,no_dusun,no_tps,no_relawan';
					$update_condition = "WHERE no='$no'";
					
					$delete_condition = "WHERE no='$no'";
					
					if (empty ($act)) {
					
						$admin->menu($menu_label,$menu_action);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					elseif ($act == 'create') {
	
						$admin->title('Tambah '.$title);
						$admin->create($table_name,$create_type,$create_label,$create_form,$create_condition);
						
					} 
					elseif ($act == 'read') {
						
						$admin->title('Detail '.$title);
						$admin->read($table_name,$read_label,$read_content,$read_condition); 
						
					} 
					elseif ($act == 'update') {
						
						$admin->title('Ubah '.$title);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act == 'scanktp') {
						
						$update_type = 'picture';
						$update_label = 'skan KTP';
						$update_form = 'scanktp';
						$admin->title('Scan KTP '.$title);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act == 'delete') {
	
						$admin->menu($menu_label,$menu_action);
						//$admin->delete($table_name,$delete_condition);		
						echo '<div class="alert alert-danger" role="alert">Demi Keamanan, Fitur Hapus Dinonaktifkan</div>';
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					else {
	
						$admin->menu($menu_label,$menu_action);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					}
					?>
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_ADMIN.'/app/asset-body.php'; ?>
	
</body>
 
</html>