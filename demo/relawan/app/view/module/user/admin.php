<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_ADMIN.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_ADMIN.'/app/sidebar.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_ADMIN.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<?php
					$title = 'User';  
					
					$menu_label = 'Tambah';
					$menu_action = 'create';
					
					$table_name = 'user';
					$table_label = 'gambar,nama,username,telepon';
					$table_column = 'picture,name,username,phone';
					$table_width = '10,50,15,15'; 
					$table_button = 'update,read,delete,account';  
					$table_condition = '';
					
					$create_type = '';
					$create_label = 'username,password,nama,telepon,email';
					$create_form = 'username,password,name,phone,email';
					$create_condition = '';
					
					$read_label = 'foto,username,password,kategori,nama,alamat,kota,provinsi,telepon,email,login terakhir,ip terakhir';
					$read_content = 'picture,username,password,category,name,address,city,province,phone,email,last_login,last_ipaddress';
					$read_condition = "WHERE no='$no'";
					
					$update_type = 'picture';
					$update_label = 'nama,alamat,kota,provinsi,telepon,email,gambar';
					$update_form = 'name,address,city,province,phone,email,picture';
					$update_condition = "WHERE no='$no'";
					
					$delete_condition = "WHERE no='$no'";
					
					if (empty ($act)) {
					
						$admin->menu($menu_label,$menu_action);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					elseif ($act == 'create') {
	
						$admin->title('Tambah '.$title);
						$admin->create($table_name,$create_type,$create_label,$create_form,$create_condition);
						
					} 
					elseif ($act == 'read') {
						
						$admin->title('Detail '.$title);
						$admin->read($table_name,$read_label,$read_content,$read_condition); 
						
					} 
					elseif ($act == 'update') {
						
						$admin->title('Ubah '.$title);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act == 'account') {
						
						$update_type = '';
						$update_label = 'username,password';
						$update_form = 'username,password';
						$admin->title('Ubah Account '.$title);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act == 'delete') {
	
						$admin->menu($menu_label,$menu_action);
						//$admin->delete($table_name,$delete_condition);		
						echo '<div class="alert alert-danger" role="alert">Demi Keamanan, Fitur Hapus Dinonaktifkan</div>';
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					else {
	
						$admin->menu($menu_label,$menu_action);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					}
					?>
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_ADMIN.'/app/asset-body.php'; ?>
	
</body>
 
</html>