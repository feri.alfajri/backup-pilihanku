<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_ADMIN.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_ADMIN.'/app/sidebar.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_ADMIN.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<?php
					date_default_timezone_set('Asia/Jakarta');
					$thnini=date('Y');
					$blnini=date('m');
					$tglini=date('d');
					$waktuini=mktime(23, 59, 59, $blnini, $tglini, $thnini);
					$jamskrg=date("Y-m-d H:i:s", $waktuini);
	
	
					$title = 'Riwayat Kegiatan';  
					
					$menu_label = 'Tambah';
					$menu_action = 'create';
					
					$table_name = 'event';
					$table_label = 'nama,lokasi,tanggal';
					$table_column = 'name,location,tanggal';
					$table_width = '50,15,15'; 
					$table_button = 'update,read,delete';  
					$table_condition = " WHERE tanggal < '$jamskrg' ";
					
					$create_type = '';
					$create_label = 'name,lokasi,tanggal';
					$create_form = 'name,location,tanggal';
					$create_condition = '';
					
					$read_label = 'name,lokasi,tanggal,evaluation';
					$read_content = 'name,location,tanggal,evaluation';
					$read_condition = "WHERE no='$no'";
					
					$update_type = '';
					$update_label = 'name,lokasi,tanggal,evaluation';
					$update_form = 'name,location,tanggal,evaluation';
					$update_condition = "WHERE no='$no'";
					
					$delete_condition = "WHERE no='$no'";
					
					if (empty ($act)) {
					
						//$admin->menu($menu_label,$menu_action);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					elseif ($act == 'create') {
	
						$admin->title('Tambah '.$title);
						$admin->create($table_name,$create_type,$create_label,$create_form,$create_condition);
						
					} 
					elseif ($act == 'read') {
						
						$admin->title('Detail '.$title);
						$admin->read($table_name,$read_label,$read_content,$read_condition); 
						
					} 
					elseif ($act == 'update') {
						
						$admin->title('Ubah '.$title);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act == 'delete') {
	
						//$admin->menu($menu_label,$menu_action);
						//$admin->delete($table_name,$delete_condition);		
						echo '<div class="alert alert-danger" role="alert">Demi Keamanan, Fitur Hapus Dinonaktifkan</div>';
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					else {
	
						//$admin->menu($menu_label,$menu_action);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					}
					?>
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_ADMIN.'/app/asset-body.php'; ?>
	
</body>
 
</html>