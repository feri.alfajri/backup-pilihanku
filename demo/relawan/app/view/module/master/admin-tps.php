<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_ADMIN.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_ADMIN.'/app/sidebar.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_ADMIN.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<?php
					$title = 'TPS';  
					
					$menu_label = 'Tambah';
					$menu_action = 'create';
					
					$table_name = 'tps';
					$table_label = 'nama,desa,kecamatan,kabupaten,zona';
					$table_column = 'name,no_desa,no_kecamatan,no_kabupaten,zona';
					$table_width = '10,20,20,20,10'; 
					$table_button = 'update,read,delete';  
					$table_condition = '';
					
					$create_type = '';
					$create_label = 'nama,alamat,kabupaten,kecamatan,desa';
					$create_form = 'name,address,no_kabupaten,no_kecamatan,no_desa';
					$create_condition = '';
					
					$read_label = 'nama,alamat,kabupaten,kecamatan,desa,zona,jumlah pemilih';
					$read_content = 'name,address,no_kabupaten,no_kecamatan,no_desa,zona,dpt';
					$read_condition = "WHERE no='$no'";
					
					$update_type = '';
					$update_label = 'nama,alamat,kabupaten,kecamatan,desa,zona,jumlah pemilih';
					$update_form = 'name,address,no_kabupaten,no_kecamatan,no_desa,zona,dpt';
					$update_condition = "WHERE no='$no'";
					
					$delete_condition = "WHERE no='$no'";
					
					if (empty ($act)) {
					
						$admin->menu($menu_label,$menu_action);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					elseif ($act == 'create') {
	
						$admin->title('Tambah '.$title);
						$admin->create($table_name,$create_type,$create_label,$create_form,$create_condition);
						
					} 
					elseif ($act == 'read') {
						
						$admin->title('Detail '.$title);
						$admin->read($table_name,$read_label,$read_content,$read_condition); 
						
					} 
					elseif ($act == 'update') {
						
						$admin->title('Ubah '.$title);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act == 'delete') {
	
						$admin->menu($menu_label,$menu_action);
						//$admin->delete($table_name,$delete_condition);		
						echo '<div class="alert alert-danger" role="alert">Demi Keamanan, Fitur Hapus Dinonaktifkan</div>';
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					else {
	
						$admin->menu($menu_label,$menu_action);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					}
					?>
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_ADMIN.'/app/asset-body.php'; ?>
	
</body>
 
</html>