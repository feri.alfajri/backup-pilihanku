<!DOCTYPE html>
<html dir="ltr" lang="id-ID">
    
<head><?php
    include (DIR_THEME.THEME.'/app/asset-head.php');
    include (DIR_THEME.THEME.'/app/metatag.php'); 
    include (DIR_THEME.THEME.'/app/opengraph.php');
    include (DIR_THEME.THEME.'/app/favicon.php');
    include (DIR_THEME.THEME.'/app/title.php');
    ?>
</head>

<body>
	<?php
	include (DIR_THEME.THEME.'/app/header.php');
	include (DIR_THEME.THEME.'/app/pagetitle.php');	
	?>
	
	<div id="main-wrapper">
        <div class="site-wrapper-reveal">
         	<?php
    		if ($picturetext=="") {
    		} 
    		else { 
				?>
    		    <img src="<?php echo URL_PICTURE.$picturetext;?>" class="img-responsive" alt="<?php echo $name;?>" align="left"/>
				<?php 
    		}	
			echo $contenttext;
			?>
		</div>
	</div>
	
	<?php
	include (DIR_THEME.THEME.'/app/footer.php');
	include (DIR_THEME.THEME.'/app/asset-body.php');
	?>
</body>

</html>