<!DOCTYPE html>
<html lang="en">
    
<head>
    <?php include DIR_THEME.THEME_ADMIN.'/app/asset-head.php';?>
</head>

<body>
    
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_ADMIN.'/app/sidebar.php'; ?>
		
		<div id="content-wrapper"> 
		    
			<div id="content">
				<?php include DIR_THEME.THEME_ADMIN.'/app/topbar.php'; ?>
				
				<div class="container-fluid"> 
				    
                    <h2>Halaman Tidak Ada...!</h2>
    				<p>Maaf, Halaman yang Anda cari tidak ditemukan.</p>
                    
				</div>
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_ADMIN.'/app/asset-body.php'; ?>
	
</body>

</html>