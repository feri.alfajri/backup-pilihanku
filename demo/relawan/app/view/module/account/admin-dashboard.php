<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_ADMIN.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_ADMIN.'/app/sidebar.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_ADMIN.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<?php
					function dashboard_caleg() {
						$admin=new admin();
						$admin->get_variable();
						$admin->koneksi();
						$link=$admin->link;
						$menu=$admin->menu;
						$linkmod=URL_DOMAIN.$menu; 
						$qcalon=mysqli_query($admin->koneksi,"SELECT name,address,phone,target_kordes,target_korcam,target_suara,target_relawan,picture FROM setting LIMIT 0,1");
						$dcalon=$dcalon=mysqli_fetch_array($qcalon); 
						$namecalon=$dcalon['name'];
						$addresscalon=$dcalon['address'];
						$phonecalon=$dcalon['phone'];
						if ($dcalon['picture']=="") {  $pictcalon="member-big.png"; } else { $pictcalon=$dcalon['picture'];  } 
						$target_suara=$dcalon['target_suara'];
						$target_relawan=$dcalon['target_relawan'];
						$target_kordes=$dcalon['target_kordes'];
						$target_korcam=$dcalon['target_korcam'];
						$qrelawan=mysqli_query($admin->koneksi,"SELECT no FROM relawan");
						$jrelawan=mysqli_num_rows($qrelawan);
						$qpendukung=mysqli_query($admin->koneksi,"SELECT no FROM pendukung");
						$jpendukung=mysqli_num_rows($qpendukung);
						if ($target_suara==0) { $prosenpendukung=0; } else { $prosenpendukung=round($jpendukung/$target_suara*100,0);  } 
						if ($target_relawan==0) { $prosenrelawan=0; } else { $prosenrelawan=round($jrelawan/$target_relawan*100,0);  }?>
						<h2>Dashboard</h2>
						<div class="card shadow mb-4">
							<div class="card-header py-3">
								<h6 class="m-0 font-weight-bold text-primary">Bagan Relawan & Pendukung</h6>
							</div>
							<div class="card-body">
								<div class="chart-bar">
									<canvas id="myBarChart"></canvas>
								</div>
							</div>
						</div>
						
						<!--
						<div class="panel panel-primary">
							<div class="panel-heading">Selamat Datang, <?php echo $_SESSION['name'];?></div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-6">
										<img src="<?php echo URL_PICTURE.$pictcalon;?>" class="img-responsive" style="margin-right:15px;" align="left"/>
										<b style="font-size:24px;"><?php echo $namecalon;?></b><br/>
										Alamat : <?php echo $addresscalon;?><br/>
										Telepon. <?php echo $phonecalon;?><br/><br/>
									</div>
									<div class="col-md-6">
										<b style="font-size:20px;">Target</b><br/>
										<table class="panel-table">
											<tbody>
												<tr><td width="150">Target Korcam</td><td width="10">:</td><td><?php echo $target_korcam;?></td></tr>
												<tr><td>Target Kordes</td><td>:</td><td><?php echo $target_kordes;?></td></tr>
												<tr><td>Target Relawan</td><td>:</td><td><?php echo $target_relawan;?></td></tr>
												<tr><td>Target Suara</td><td>:</td><td><?php echo $target_suara;?></td></tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						-->
						<div class="row">
							<div class="col-md-6">
								<div class="card bg-danger mb-4 text-white">
									<div class="card-body">
										<h4 class="panel-title">PROGRESS PENDUKUNG</h4>
										<table width="100%" class="panel-table">
											<tbody>
												<td width="100" class="text-center">
													<div  class="h3 bg-warning"><span class="h1"><?php echo $prosenpendukung;?></span>%</div>
												</td>
												<td style="padding-left:10px;">
													<table width="100%">
														<tr style="border-bottom:1px solid #ffffff;"><td width="150">Target</td><td width="10">:</td><td class="text-right"><?php echo number_format($target_suara);?></td></tr>
														<tr><td>Tercapai</td><td>:</td><td class="text-right"><?php echo number_format($jpendukung);?></td></tr>
													</table>
												</td>
											</tbody>
										</table>
										<table width="100%" class="panel-table">
											<tbody><?php
												$qkec=mysqli_query($admin->koneksi,"SELECT no,name FROM kabupaten ORDER BY urutan ASC");
												while($dkec=mysqli_fetch_array($qkec)){ 
													$nokec=$dkec['no'];
													$namekec=$dkec['name'];
													$qpendukung=mysqli_query($admin->koneksi,"SELECT no FROM pendukung WHERE no_kabupaten='$nokec'");
													$jpendukung=mysqli_num_rows($qpendukung);
													?>
													<tr style="border-bottom:1px solid #ffffff;"><td width="150"><?php echo $namekec;?></td><td width="10">:</td><td class="text-right"><?php echo $jpendukung;?></td></tr><?php
												} ?>
											</tbody>
										</table>
									</div>
								</div>
								<div class="card mb-4">
									<div class="card-header  bg-danger"><span style="font-size:16px;font-weight:bold;">KECAMATAN PENDUKUNG TERBANYAK</span></div>
									<div class="card-body">
										<table width="100%" class="panel-table">
											<tbody><?php
											$qkecamatan=mysqli_query($admin->koneksi,"SELECT no,name,(SELECT COUNT(publish) FROM pendukung WHERE pendukung.no_kecamatan=kecamatan.no) as jumlahpendukung
											FROM kecamatan ORDER BY jumlahpendukung DESC LIMIT 10");
											while($dkecamatan=mysqli_fetch_array($qkecamatan)){ 							
												$nokecamatan=$dkecamatan['no'];
												$namekecamatan=$dkecamatan['name']; 
												$jumlahpendukung=$dkecamatan['jumlahpendukung'];  ?>
												<tr style="border-bottom:1px solid #cccccc;">
													<td><?php echo $namekecamatan;?></td>
													<td width="100" style="text-align:right;"><?php echo $jumlahpendukung;?></td>
												</tr><?php
											} ?>
											</tbody>
										</table>
									</div>
								</div>
								<div class="card mb-4">
									<div class="card-header bg-danger"><span style="font-size:16px;font-weight:bold;">DESA PENDUKUNG TERBANYAK</span></div>
									<div class="card-body">
										<table width="100%" class="panel-table">
											<tbody><?php
											$qdesa=mysqli_query($admin->koneksi,"SELECT no,name,(SELECT COUNT(publish) FROM pendukung WHERE pendukung.no_desa=desa.no) as jumlahpendukung
											FROM desa ORDER BY jumlahpendukung DESC LIMIT 10");
											while($ddesa=mysqli_fetch_array($qdesa)){ 							
												$nodesa=$ddesa['no'];
												$namedesa=$ddesa['name']; 
												$jumlahpendukung=$ddesa['jumlahpendukung'];  ?>
												<tr style="border-bottom:1px solid #cccccc;">
													<td><?php echo $namedesa;?></td>
													<td width="100" style="text-align:right;"><?php echo $jumlahpendukung;?></td>
												</tr><?php
											} ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card bg-primary mb-4 text-white">
									<div class="card-body">
										<h4 class="panel-title">PROGRESS RELAWAN</h4>
										<table width="100%" class="panel-table">
											<tbody>
												<td width="100" class="text-center">
													<div  class="h3 bg-info"><span class="h1"><?php echo $prosenrelawan;?></span>%</div>
												</td>
												<td style="padding-left:10px;">
													<table width="100%">
														<tr style="border-bottom:1px solid #ffffff;"><td width="150">Target</td><td width="10">:</td><td class="text-right"><?php echo number_format($target_relawan);?></td></tr>
														<tr><td>Tercapai</td><td>:</td><td class="text-right"><?php echo number_format($jrelawan);?></td></tr>
													</table>
												</td>
											</tbody>
										</table>
										<table width="100%" class="panel-table">
											<tbody><?php
												$qkec=mysqli_query($admin->koneksi,"SELECT no,name FROM kabupaten ORDER BY urutan ASC");
												while($dkec=mysqli_fetch_array($qkec)){ 
													$nokec=$dkec['no'];
													$namekec=$dkec['name'];
													$qrelawan=mysqli_query($admin->koneksi,"SELECT no FROM relawan WHERE no_kabupaten='$nokec'");
													$jrelawan=mysqli_num_rows($qrelawan);
													?>
													<tr style="border-bottom:1px solid #ffffff;"><td width="150"><?php echo $namekec;?></td><td width="10">:</td><td class="text-right"><?php echo $jrelawan;?></td></tr><?php
												} ?>
											</tbody>
										</table>	
									</div>
								</div>
								<div class="card mb-4 ">
									<div class="card-header bg-info"><span style="font-size:16px;font-weight:bold;">KECAMATAN RELAWAN TERBANYAK</span></div>
									<div class="card-body">
										<table width="100%" class="panel-table">
											<tbody><?php
											$qkecamatan=mysqli_query($admin->koneksi,"SELECT no,name,(SELECT COUNT(publish) FROM relawan WHERE relawan.no_kecamatan=kecamatan.no) as jumlahrelawan
											FROM kecamatan ORDER BY jumlahrelawan DESC LIMIT 10");
											while($dkecamatan=mysqli_fetch_array($qkecamatan)){ 							
												$nokecamatan=$dkecamatan['no'];
												$namekecamatan=$dkecamatan['name']; 
												$jumlahrelawan=$dkecamatan['jumlahrelawan'];  ?>
												<tr style="border-bottom:1px solid #cccccc;">
													<td><?php echo $namekecamatan;?></td>
													<td width="100" style="text-align:right;"><?php echo $jumlahrelawan;?></td>
												</tr><?php
											} ?>
											</tbody>
										</table>
									</div>
								</div>
								<div class="card mb-4">
									<div class="card-header bg-info"><span style="font-size:16px;font-weight:bold;">DESA RELAWAN TERBANYAK</span></div>
									<div class="card-body">
										<table width="100%" class="panel-table">
											<tbody><?php
											$qdesa=mysqli_query($admin->koneksi,"SELECT no,name,(SELECT COUNT(publish) FROM relawan WHERE relawan.no_desa=desa.no) as jumlahrelawan
											FROM desa ORDER BY jumlahrelawan DESC LIMIT 10");
											while($ddesa=mysqli_fetch_array($qdesa)){ 							
												$nodesa=$ddesa['no'];
												$namedesa=$ddesa['name']; 
												$jumlahrelawan=$ddesa['jumlahrelawan'];  ?>
												<tr style="border-bottom:1px solid #cccccc;">
													<td><?php echo $namedesa;?></td>
													<td width="100" style="text-align:right;"><?php echo $jumlahrelawan;?></td>
												</tr><?php
											} ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<?php
					}

					dashboard_caleg();
				?>
				
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	
	<?php include DIR_THEME.THEME_ADMIN.'/app/asset-body.php'; ?>
	<script src="<?php echo URL_THEME.THEME_ADMIN;?>/vendor/chart.js/Chart.min.js"></script>
	<script src="j<?php echo URL_THEME.THEME_ADMIN;?>/js/demo/chart-area-demo.js"></script>
	<script src="<?php echo URL_THEME.THEME_ADMIN;?>/js/demo/chart-pie-demo.js"></script>
	<script>
		// Set new default font family and font color to mimic Bootstrap's default styling
		Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
		Chart.defaults.global.defaultFontColor = '#858796';

		function number_format(number, decimals, dec_point, thousands_sep) {
		  // *     example: number_format(1234.56, 2, ',', ' ');
		  // *     return: '1 234,56'
		  number = (number + '').replace(',', '').replace(' ', '');
		  var n = !isFinite(+number) ? 0 : +number,
			prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
			sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
			dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
			s = '',
			toFixedFix = function(n, prec) {
			  var k = Math.pow(10, prec);
			  return '' + Math.round(n * k) / k;
			};
		  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
		  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
		  if (s[0].length > 3) {
			s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
		  }
		  if ((s[1] || '').length < prec) {
			s[1] = s[1] || '';
			s[1] += new Array(prec - s[1].length + 1).join('0');
		  }
		  return s.join(dec);
		}

		// Bar Chart Example
		var ctx = document.getElementById("myBarChart");
		var myBarChart = new Chart(ctx, {
		  type: 'bar',
		  data: {
			labels: ["Temanggung", "Magelang", "Wonosobo", "Purworejo", "Kota Magelang"],
			datasets: [{
			  label: "Revenue",
			  backgroundColor: "#4e73df",
			  hoverBackgroundColor: "#2e59d9",
			  borderColor: "#4e73df",
			  data: [69, 2, 0, 0, 0],
			}],
		  },
		  options: {
			maintainAspectRatio: false,
			layout: {
			  padding: {
				left: 10,
				right: 25,
				top: 25,
				bottom: 0
			  }
			},
			scales: {
			  xAxes: [{
				time: {
				  unit: 'month'
				},
				gridLines: {
				  display: false,
				  drawBorder: false
				},
				ticks: {
				  maxTicksLimit: 6
				},
				maxBarThickness: 40,
			  }],
			  yAxes: [{
				ticks: {
				  min: 0,
				  max: 2000,
				  maxTicksLimit: 5,
				  padding: 10,
				  // Include a dollar sign in the ticks
				  callback: function(value, index, values) {
					return  number_format(value);
				  }
				},
				gridLines: {
				  color: "rgb(234, 236, 244)",
				  zeroLineColor: "rgb(234, 236, 244)",
				  drawBorder: false,
				  borderDash: [2],
				  zeroLineBorderDash: [2]
				}
			  }],
			},
			legend: {
			  display: false
			},
			tooltips: {
			  titleMarginBottom: 10,
			  titleFontColor: '#6e707e',
			  titleFontSize: 14,
			  backgroundColor: "rgb(255,255,255)",
			  bodyFontColor: "#858796",
			  borderColor: '#dddfeb',
			  borderWidth: 1,
			  xPadding: 15,
			  yPadding: 15,
			  displayColors: false,
			  caretPadding: 10,
			  callbacks: {
				label: function(tooltipItem, chart) {
				  var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
				  return datasetLabel + ': $' + number_format(tooltipItem.yLabel);
				}
			  }
			},
		  }
		});

	</script>
</body>
 
</html>