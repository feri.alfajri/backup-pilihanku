<!DOCTYPE html> 
<html lang="en">
    
<head>

    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
	
</head>

<body>
    
	<div id="wrapper"> 
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar.php'; ?>
		
		<div id="content-wrapper"> 
		    
			<div id="content">
			
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
				    
					<div class="card mb-4">
						<div class="card-header"><?php echo $titleweb;?></div>
						<div class="card-body">
							<?php
							
							$uname = $_SESSION['uname'];
							$table_name = 'member';							
							$update_type = 'picture';
							$update_label = 'gambar';
							$update_form = 'picture';
							$update_condition = "WHERE username='$uname'";							
								
							$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
								
							?>
						</div> 	
					</div>
                    
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>

</html>