<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_ADMIN.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_ADMIN.'/app/sidebar.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_ADMIN.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					<?php
					
					$uname = $_SESSION['uname'];
					$table_name = 'user';							
					
					$admin->title('Ubah '.$titlepage);
					echo '<div class="card mb-4">';
						echo '<div class="card-body">';
							$password->editpassword($table_name,$uname);
						echo '</div>';
					echo '</div>';
					?>
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_ADMIN.'/app/asset-body.php'; ?>
	
</body>
 
</html>