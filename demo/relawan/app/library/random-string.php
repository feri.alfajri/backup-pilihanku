<?php
// Author: Sumaryono
// Last Updated: April 2019

class RandomString {
    private $characters;
    
    public function __construct($characters = null) {
        $this->setCharacters($characters);
    }
    
    public function setCharacters($characters = null) {
        if (empty($characters)) {
            $characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        }
        
        $this->characters = $characters;
        
        return $this;
    }
    
    public function getCharacters($characters = null) {
        return $this->characters;
    }
    
    private function countCharacters() {
        return strlen($this->characters);
    }
    
    public function generateRandomString($stringCounts) {
        $characterCounts = $this->countCharacters();
        $characters = $this->characters;
        $words = '';
        
        for ($index = 1; $index <= $stringCounts; $index++) {
            $randomNumbers = rand(0, ($characterCounts - 1));
            $words .= $characters[$randomNumbers];
        }
        
        return $words;
    }
}