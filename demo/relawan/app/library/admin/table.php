<?php  
class table extends admin { 
	
	function showtablebig($titlemod,$tabel,$label,$kolom,$lebar,$tombolact,$syarat) {
		$admin=new admin();
		$admin->get_variable();
		$admin->koneksi();
		$link=$admin->link;
		$menu=$admin->menu;
		if ($admin->no=="") { $no=1; } else { $no=$admin->no;  }
		if ($admin->act=="") { $act="urut"; } else { $act=$admin->act; }
		if ($admin->orderby=="") { $orderby="date"; } else { $orderby=$admin->orderby; } 
		if ($admin->by=="") { $by="desc"; } else { $by=$admin->by; } 
		$page=$admin->page;
		$linkmod=URL_DOMAIN.$menu."/".$link;  ?>
		<h2 style="display:table;width:100%;">
			<span><?php echo $titlemod;?></span>
			<span><a href="<?php echo $linkmod;?>/1/tambah/" class="btn btn-primary btn-xs" title="Tambah">Tambah</a></span>
			<div class="cari">
				<form action="" method="post">
					<input type="text" name="keyword" class="form-control" id="keyword" value="Kata Kunci" style="float:left;" onblur="if (this.value=='') this.value='Kata Kunci';" onfocus="if (this.value=='Kata Kunci') this.value='';"/>
					<input name="cari" type="submit" value="" onClick="return cekkeyword();" id="tombol" class="btn btn-default" style="cursor:pointer;float:left;height:30px;"/>
				</form>
			</div>
		</h2><?php
		$batas=30;
		if (empty($_POST['keyword'])) { 
			if ($orderby=="date") { $keycari=""; } else { $keycari=$orderby;  }
			$keycari=""; 
			if ($syarat=="") { $syaratadd=""; } else { $syaratadd=$syarat; }
		} 
		else { 
			$keycari=strip_tags($_POST['keyword']); $keycari=str_replace('"','',$keycari);$keycari=str_replace("'","",$keycari); 
			$kol=explode(",",$kolom);
			$jumkolom=count($kol);
			for($x=0;$x<1;$x++){ $kolomawal=$kol[$x]; }			
			if ($syarat=="") { $syaratadd=" WHERE $kolomawal LIKE '%$keycari%' "; } else { $syaratadd=$syarat." AND $kolomawal LIKE '%$keycari%' "; }
		}
		$linkpage=$linkmod."/".$no."/".$act."/".$orderby."/".$by;
		if ($page=="") { $posisi=0; $page=1; } else { $posisi=($page-1)*$batas; }	
		$nomor=$posisi+1;					
		$qjum=mysqli_query($admin->koneksi,"SELECT $kolom,date,no,publish FROM $tabel $syaratadd ");
		$query=mysqli_query($admin->koneksi,"SELECT $kolom, date,no,publish FROM $tabel $syaratadd ORDER BY $orderby $by LIMIT $posisi,$ ");
		$jumlah=mysqli_num_rows($qjum);
		$jumhal=ceil($jumlah/$batas); 
		if ($jumlah==0) { $admin->notify("empty"); }
		else { ?>
			<div class="dataTable_wrapper">
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr style="text-transform:uppercase;">
							<th width="40" class="text-center">No</th><?php
							$lab=explode(",",$label);
							$kol=explode(",",$kolom);
							$leb=explode(",",$lebar);
							$jumkolom=count($kol);
							for($i=0;$i<$jumkolom;$i++){
								$align="text-center"; 
								if ($lab[$i]=="judul" or $lab[$i]=="nama" or $lab[$i]=="deskripsi" or $lab[$i]=="pertanyaan" or $lab[$i]=="subdomain") { $width=""; $align="text-left";  } else { $width=$leb[$i]; }
								if ($by=="desc") { $bybaru="asc"; } elseif ($by=="asc") { $bybaru="desc"; } else { $bybaru="asc"; }
								$linkkolom=$linkmod."/".$no."/".$act."/".$kol[$i]."/".$bybaru."/"; ?>
								<th width="<?php echo $width;?>%" class="<?php echo $align;?>"><a href="<?php echo $linkkolom;?>" title="Urut <?php echo $lab[$i];?>"><?php echo $lab[$i];?></a></th><?php
							}?>
						</tr>
					</thead>
					<tbody><?php					
					while($data=mysqli_fetch_array($query)) {
						$nodata=$data['no'];
						$pub=$data['publish'];
						$date=$data['date'];
						if ($pub==0) { $warna="#CC0000"; } else { $warna="none;"; } ?>
						<tr style="color:<?php echo $warna;?>">
							<td class="text-center"><?php echo $nomor;?></td><?php
							for($j=0;$j<$jumkolom;$j++){
								if ($kol[$j]=="title" or $kol[$j]=="name" or $kol[$j]=="description" or  $kol[$j]=="question" or  $kol[$j]=="no_product" or $kol[$j]=="subdomain") { $align="text-left"; } else { $align="text-center"; }
								if ($kol[$j]=="title" or $kol[$j]=="name" or  $kol[$j]=="question" or $kol[$j]=="subdomain") { $buttonact=1; } else { $buttonact=0; } ?>
								<td class="<?php echo $align;?>"><?php 
									$pisah=explode("_",$kol[$j]); 
									$jumpisah=count($pisah);
									if ($jumpisah<=1) {  $namekolom=$kol[$j]; } 
									else { list ($namekolom,$tabelkolom)=explode("_",$kol[$j]); }									
									if ($namekolom=="picture") { if ($data[$namekolom]!="") { ?><center><img src="<?php echo URL_UPLOAD;?>image.php/<?php echo $data[$namekolom];?>?width=80&nocache&quality=100&image=<?php echo URL_PICTURE.$data[$namekolom];?>" alt="image" style="width:100%; max-width:80px;"/></center><?php } }
									elseif ($namekolom=="icon") { ?><center><i class="fa <?php echo $data[$namekolom];?>" style="font-size:32px;"></i></center><?php }
									elseif ($namekolom=="date") { echo $data['date']; }
									elseif ($namekolom=="value" or $namekolom=="price"  or $namekolom=="total" or $namekolom=="amount") { echo number_format($data[$kol[$j]],0,',','.'); }
									elseif ($namekolom=="no") { 
										$nokolom=$data[$kol[$j]];
										$qkolom=mysqli_query($admin->koneksi,"SELECT * FROM $tabelkolom WHERE no='$nokolom'  LIMIT 0,1");
										$dkolom=mysqli_fetch_array($qkolom); 
										echo $dkolom['name']; 									
									}
									elseif ($namekolom=="id") { 
										$nokolom=$data[$kol[$j]];
										$qkolom=mysqli_query($admin->koneksi,"SELECT * FROM $tabelkolom WHERE no='$nokolom'  LIMIT 0,1");
										$dkolom=mysqli_fetch_array($qkolom); 
										echo $dkolom['title']; 									
									}
									else { echo $data[$kol[$j]]; }
								if ($buttonact==1) {?>
									<div><?php
									$tomact=explode(",",$tombolact); $jumtomact=count($tomact);
									for($k=0;$k<$jumtomact;$k++){ $this->tombolaction($linkmod,$tomact[$k],$nodata); } ?>
									</div><?php							
								}  ?>
								</td><?php 
							} ?>
						</tr><?php
						$nomor++;
					} ?>
					</tbody>
				</table><?php
				$this->paging($jumhal,$linkpage,$page); ?>
			</div><?php
		}
	}
	
	
	
	function tombolaction($linkmod,$tipe,$nodata) { 
		if ($tipe=="hapus" or $tipe=="delete") { $onclick="onclick=\"return confirm('Apakah Anda yakin akan menghapus data ini ?');\""; } else { $onclick=""; } 
		$linkaction=$linkmod."/".$nodata."/".$tipe; ?>
		<span class="menuaction"><a href="<?php echo $linkaction;?>/" title="<?php echo $tipe;?>" <?php echo $onclick;?>><?php echo $tipe;?></a></span><?php
	}
	
	
	function paging($jumhal,$linkhal,$page) { ?>
		<div><?php
			if ($page>1) { 
				$prev=$page-1; ?>
				<a href="<?php echo $linkhal;?>/"  title="First"><li class="btn btn-default">&laquo;</li></a><a href="<?php echo $linkhal;?>/page/<?php echo $prev;?>/" title="Prev"><li class="btn btn-default">&lsaquo;</li></a><?php
			} 
			else { ?><li class="btn btn-default">&laquo;</li><li class="btn btn-default">&lsaquo;</li><?php } 
			$sebakhir=$jumhal-1;
			if ($jumhal<=3) { $awal=1; $akhir=$jumhal; }
			else {
				if ($page==1) { $awal=1; $akhir=$page+2; }
				elseif ($page==2) { $awal=1; $akhir=$page+2; }
				elseif ($page==$jumhal) { $awal=$page-2; $akhir=$jumhal; }
				elseif ($page==$sebakhir) { $awal=$page-2; $akhir=$jumhal; }
				else { $awal=$page-2; $akhir=$page+2; }
			}
			for ($i=$awal; $i<=$akhir; $i++) {
				if ($i!=$page) { ?><a href="<?php echo $linkhal;?>/page/<?php echo $i;?>/" title="<?php echo $i;?>"><li class="btn btn-default"><?php echo $i;?></li></a><?php }  
				else { ?><li class="btn btn-info"><?php echo $i;?></li><?php } 
			}
			if ($page<$jumhal){ 
				$next=$page+1; ?><a href="<?php echo $linkhal;?>/page/<?php echo $next;?>/" title="Next"><li class="btn btn-default">&rsaquo;</li></a><a href="<?php echo $linkhal;?>/page/<?php echo $jumhal;?>/"  title="Last"><li class="btn btn-default">&raquo;</li></a><?php
			} 
			else { ?><li class="btn btn-default">&rsaquo;</li><li class="btn btn-default">&raquo;</li><?php } ?>
		</div><?php
	}
}
?>