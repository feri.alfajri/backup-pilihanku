<?php
if (empty ($_SESSION['uname']) or empty ($_SESSION['pword'])) {
    
    include DIR_MODULE.'account/admin-login.php';
    
}
else { 
	
	$access = 'admin';
	
	switch ($link) {
		
		case '': 
			$titlepage = 'Dashboard';
			include DIR_CONTROLLER.'AccountController.php';  
			break;
		
		case 'profile': 
			$titlepage = 'Profil'; 
			include DIR_CONTROLLER.'AccountController.php';  
			break;
			
		case 'password': 
			$titlepage = 'Password'; 
			include DIR_CONTROLLER.'AccountController.php';  
			break;
			
		case 'photo': 
			$titlepage = 'Foto'; 
			include DIR_CONTROLLER.'AccountController.php';  
			break;
		
		case 'relawan': 
			$titlepage = 'Relawan'; 
			include DIR_CONTROLLER.'RelawanController.php';  
			break;
		
		case 'dashboard-relawan': 
			$titlepage = 'Dashboard Relawan'; 
			include DIR_CONTROLLER.'RelawanController.php';  
			break;
			
		case 'kordes': 
			$titlepage = 'Koordinator Desa'; 
			include DIR_CONTROLLER.'RelawanController.php';  
			break;
			
		case 'korcam': 
			$titlepage = 'Koordinator Kecamatan'; 
			include DIR_CONTROLLER.'RelawanController.php';  
			break;
			
		case 'pendukung': 
			$titlepage = 'Pendukung'; 
			include DIR_CONTROLLER.'PendukungController.php';  
			break;
		
		case 'dashboard-pendukung': 
			$titlepage = 'Dashboard Pendukung'; 
			include DIR_CONTROLLER.'PendukungController.php';  
			break;
			
		case 'send': 
			$titlepage = 'Kirim Pesan'; 
			include DIR_CONTROLLER.'MessageController.php';  
			break;
			
		case 'message': 
			$titlepage = 'Riwayat Pesan'; 
			include DIR_CONTROLLER.'MessageController.php';  
			break;
			
		case 'event-add': 
			$titlepage = 'Tambah Event'; 
			include DIR_CONTROLLER.'EventController.php';  
			break;
			
		case 'event-next': 
			$titlepage = 'Event Terdekat'; 
			include DIR_CONTROLLER.'EventController.php';  
			break;
			
		case 'event-history': 
			$titlepage = 'Riwayat Event'; 
			include DIR_CONTROLLER.'EventController.php';  
			break;
			
		case 'image': 
			$titlepage = 'Gambar'; 
			include DIR_CONTROLLER.'MediaController.php';  
			break;
			
		case 'video': 
			$titlepage = 'Video'; 
			include DIR_CONTROLLER.'MediaController.php';  
			break;
			
		case 'tps': 
			$titlepage = 'TPS'; 
			include DIR_CONTROLLER.'MasterController.php';  
			break;
			
		case 'dusun': 
			$titlepage = 'Dusun'; 
			include DIR_CONTROLLER.'MasterController.php';  
			break;
			
		case 'desa': 
			$titlepage = 'Desa'; 
			include DIR_CONTROLLER.'MasterController.php';  
			break;
			
		case 'kecamatan': 
			$titlepage = 'Kecamatan'; 
			include DIR_CONTROLLER.'MasterController.php';  
			break;
			
		case 'kabupaten': 
			$titlepage = 'Kabupaten'; 
			include DIR_CONTROLLER.'MasterController.php';  
			break;
			
		case 'user': 
			$titlepage = 'User / Admin'; 
			include DIR_CONTROLLER.'UserController.php';  
			break;
						
		default:
			$titlepage = 'Error';
			include DIR_CONTROLLER.'TextController.php';
			
	}

}
?>