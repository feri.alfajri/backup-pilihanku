<?php
if (empty ($_SESSION['uname']) or empty ($_SESSION['pword'])) {
    
    include DIR_MODULE.'account/member-login.php';
    
}
else { 

	$access='member';

	switch ($link) {
		
		case '': 
			$titlepage='Dashboard';
			include DIR_CONTROLLER.'AccountController.php';  
			break;
			
		case 'profile': 
			$titlepage='Profil'; 
			include DIR_CONTROLLER.'AccountController.php';  
			break;
			
		case 'password': 
			$titlepage='Password'; 
			include DIR_CONTROLLER.'AccountController.php';  
			break;
			
		case 'photo': 
			$titlepage='Foto'; 
			include DIR_CONTROLLER.'AccountController.php';  
			break;
			
		default:
			$titlepage='Error';
			include DIR_CONTROLLER.'TextController.php';
			
	}

}
?>