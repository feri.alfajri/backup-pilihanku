<?php
if ($access == 'admin') {  

    include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
    
    if ($link == 'menu') {		
        include DIR_MODULE.'text/admin.php';		
    }
    else {		
        include DIR_MODULE.'text/error-admin.php';		
    }

} 
elseif ($access == 'member') {
	
	include DIR_MODULE.'text/error-member.php';		
	
}
else {
	
    include DIR_MODEL.'menu.php';
    $menuclass = new menu();
	$menuclass -> check($model -> menu);
	$jmenu = $menuclass -> jmenu;	
	
    if ($jmenu == 0) {		
        include DIR_MODULE.'text/error.php';		
    }
    else {
		
		$menuclass -> detail($model -> menu);
		$nametext = $menuclass -> namemenu;
		$linktext = $menuclass -> linkmenu;
		$contenttext = $menuclass -> contentmenu;
		$picturetext = $menuclass -> picturemenu;
		$datetext = $menuclass -> datemenu; 
		
		$titleweb = $menuclass -> namemenu;
		$metadesc = $menuclass -> metadescriptionmenu;
		
        include DIR_MODULE.'text/public.php';		
    }
    
}
?>