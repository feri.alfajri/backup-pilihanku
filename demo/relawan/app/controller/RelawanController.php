<?php
if ($access == 'admin') {  

    include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
    
    if ($link == 'relawan') {	
	
        include DIR_MODULE.'relawan/admin-relawan.php';	
		
    }
	elseif ($link == 'dashboard-relawan') {		
	
        include DIR_MODULE.'relawan/admin-dashboard.php';	
		
    }
	elseif ($link == 'kordes') {		
	
        include DIR_MODULE.'relawan/admin-kordes.php';	
		
    }
	elseif ($link == 'korcam') {		
	
        include DIR_MODULE.'relawan/admin-korcam.php';	
		
    }
    else {		
	
        include DIR_MODULE.'relawan/admin-relawan.php';	
		
    }

} 
else {
	
    
}
?>