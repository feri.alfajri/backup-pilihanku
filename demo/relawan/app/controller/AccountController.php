<?php 
if ($access == 'admin') { 
	
	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
	
	if($link == 'profile'){
        
		$titleweb = 'Ubah Profil';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/admin-profile.php';
			
    }
    elseif($link == 'password'){
        
		include(DIR_LIBRARY."admin/password.php");
		$password = new password();
	
		$titleweb = 'Ubah Password';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/admin-password.php';
		
    }
	elseif($link == 'photo'){
        
		$titleweb = 'Ubah Foto Profil';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/admin-photo.php';
			
    }
    else{
        
		$titleweb = 'Dashboard';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/admin-dashboard.php';
        
    }
    
}
elseif ($access == 'member') { 
	
	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
	
    if($link == 'profile'){
        
		$titleweb = 'Ubah Profil';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/member-profile.php';
			
    }
    elseif($link == 'password'){
        
		include(DIR_LIBRARY."admin/password.php");
		$password=new password();
		
		$titleweb = 'Ubah Password';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/member-password.php';
		
    }
	elseif($link == 'photo'){
        
		$titleweb = 'Ubah Foto Profil';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/member-photo.php';
		
    }
    else{
        
        $titleweb = 'Foto Profil';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/member-dashboard.php';
		
    }
	
}
else {
    
    
    
    
}
?>