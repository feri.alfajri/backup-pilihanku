-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 27, 2022 at 09:31 AM
-- Server version: 10.1.48-MariaDB
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pilihanku_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `no` int(11) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `title` varchar(200) NOT NULL,
  `no_blog_category` int(11) NOT NULL,
  `link` varchar(200) NOT NULL,
  `author` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `meta_title` varchar(200) NOT NULL,
  `meta_description` varchar(200) NOT NULL,
  `visitors` int(11) NOT NULL,
  `tag` text NOT NULL,
  `comments` int(11) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `blog_category`
--

CREATE TABLE `blog_category` (
  `no` int(2) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `blog_comment`
--

CREATE TABLE `blog_comment` (
  `no` int(11) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `id_blog` int(11) NOT NULL,
  `no_blog_comment` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `circulation`
--

CREATE TABLE `circulation` (
  `no` int(11) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `no_inventory` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `date_return` date NOT NULL,
  `status` enum('out','in') NOT NULL DEFAULT 'out',
  `note` text NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `circulation`
--

INSERT INTO `circulation` (`no`, `subdomain`, `name`, `link`, `phone`, `no_inventory`, `amount`, `date_return`, `status`, `note`, `date`, `publish`) VALUES
(1, 'emasjid', 'Khoerul Umam', '', '', 3, 20, '2022-11-01', 'in', '', '2022-10-18 11:41:48', '0'),
(2, 'emasjid', 'Speaker Aktif', '', '', 4, 2, '2022-11-09', 'out', '', '2022-10-18 18:31:30', '0'),
(3, 'emasjid', 'Fuad', '', '081390957111', 3, 1, '0000-00-00', 'out', '', '2022-11-10 14:46:50', '1');

-- --------------------------------------------------------

--
-- Table structure for table `commission`
--

CREATE TABLE `commission` (
  `no` int(11) NOT NULL,
  `status` enum('debit','kredit') NOT NULL DEFAULT 'debit',
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `origin` varchar(100) NOT NULL,
  `code` varchar(5) NOT NULL,
  `value` int(11) NOT NULL,
  `fee_transfer` int(11) NOT NULL,
  `news` text NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commission`
--

INSERT INTO `commission` (`no`, `status`, `name`, `link`, `username`, `origin`, `code`, `value`, `fee_transfer`, `news`, `date`, `publish`) VALUES
(1, 'debit', 'Komisi Referensi', 'komisi', 'umam', 'bumiteknoindonesia@gmail.com', 'KR', 200000, 0, '', '2021-11-09 14:09:49', '1'),
(2, 'debit', 'Komisi Referensi', 'komisi', 'umam', 'safari.achmad@gmail.com', 'KR', 600000, 0, '', '2021-11-09 14:09:49', '1'),
(3, 'debit', 'Komisi Referensi', 'komisi', 'umam', 'maskhoerul86@gmail.com', 'KR', 200000, 0, '', '2021-11-09 14:09:49', '1'),
(5, 'kredit', 'Transfer Bonus', 'transfer-bonus', 'umam', '', 'TK', 750000, 0, 'BRI No. Rek. 65168451351 A.N. Khoerul Umam', '2021-11-09 14:10:58', '1'),
(7, 'debit', 'Komisi Referensi', 'komisi', 'budi', '', 'KR', 500000, 0, '', '2021-11-09 14:11:49', '1');

-- --------------------------------------------------------

--
-- Table structure for table `cv`
--

CREATE TABLE `cv` (
  `no` int(11) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(50) NOT NULL,
  `birth` varchar(100) NOT NULL,
  `sex` enum('L','P') NOT NULL DEFAULT 'L',
  `address` varchar(200) NOT NULL,
  `city` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `religion` varchar(50) NOT NULL,
  `relationship` varchar(50) NOT NULL,
  `education` text NOT NULL,
  `organization` text NOT NULL,
  `training` text NOT NULL,
  `award` text NOT NULL,
  `work` text NOT NULL,
  `profile` text NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` int(11) NOT NULL,
  `publish` enum('1','0') DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cv`
--

INSERT INTO `cv` (`no`, `subdomain`, `name`, `link`, `birth`, `sex`, `address`, `city`, `phone`, `email`, `religion`, `relationship`, `education`, `organization`, `training`, `award`, `work`, `profile`, `picture`, `date`, `publish`) VALUES
(11, 'agungbm', 'Agung Budi Margono', '', '', 'L', 'Jalan Tembalang', 'Semarang', '08123456789', 'info@pilihanku.id', '', '', '', '', '', '', '', '', '3620206agung-bm.jpeg', 0, '1'),
(10, 'sofari', 'Sofari', '', '', 'L', 'Jalan Apa Saja No. 100', 'Temanggung', '085729878710', 'info@pilihanku.id', '', '', '', '', '', '', '', '', '113908974471856INOD.jpg', 0, '1'),
(9, 'emasjid', 'E-Masjid', '', '', 'L', 'Jalan Apa Saja', 'Temanggung', '085740000146', 'info@pilihanku.id', '', '', '', '', '', '', '', '', '38118257masjid.jpg', 0, '1'),
(7, 'subhan', 'Subhan', '', '', 'L', 'Jalan Apa Saja', 'Temanggung', '085740000146', 'info@pilihanku.id', '', '', '', '', '', '', '', '', '512246611040553IMG_20180224_203113.jpg', 0, '1'),
(8, 'kolim', 'Kolim', '', '', 'L', 'Jalan Apa Saja', 'Temanggung', '085740000146', 'info@pilihanku.id', '', '', '', '', '', '', '', '', '188537757281646DSC_0002.jpg', 0, '1'),
(5, 'triastuti', 'Tri Astuti', '', '', 'L', 'Kebumen II, Kebumen, Pringsurat', 'Temanggung', '085799993240', 'info@pilihanku.id', '', '', '', '', '', '', '', '', '2953621triastuti.jpg', 0, '1'),
(12, 'subkhan', 'Subkhan', '', '', 'L', 'Jalan Semarang', 'Semarang', '08123456789', 'bumiteknoindonesia@gmail.com', '', '', '', '', '', '', '', '', '72945831subkhan.jpeg', 0, '1'),
(13, 'agungbudimargono', 'Agung Budi Margono', '', '', 'L', 'Jalan Tembalang', 'Semarang', '08123456789', 'bumiteknoindonesia@gmail.com', '', '', '', '', '', '', '', '', '86477887agung-bm.jpeg', 0, '1');

-- --------------------------------------------------------

--
-- Table structure for table `discussion`
--

CREATE TABLE `discussion` (
  `no` int(11) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `discussion`
--

INSERT INTO `discussion` (`no`, `subdomain`, `name`, `link`, `phone`, `email`, `question`, `answer`, `date`, `publish`) VALUES
(6, 'triastuti', 'Khoerul Umam', '', '081390957111', 'maskhoerul86@gmail.com', 'Coba Saja', 'OK Bro', '2022-11-11 21:54:24', '1');

-- --------------------------------------------------------

--
-- Table structure for table `donation`
--

CREATE TABLE `donation` (
  `no` int(2) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `value` int(11) NOT NULL,
  `status` enum('unpaid','confirm','paid') NOT NULL DEFAULT 'unpaid',
  `note` text NOT NULL,
  `date_transfer` date NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donation`
--

INSERT INTO `donation` (`no`, `subdomain`, `name`, `link`, `phone`, `email`, `value`, `status`, `note`, `date_transfer`, `date`, `publish`) VALUES
(6, 'triastuti', 'Khoerul Umam', '', '081390957111', 'maskhoerul86@gmail.com', 100000, 'unpaid', 'Coba Saja', '0000-00-00', '2022-11-11 21:40:03', '1'),
(5, 'emasjid', 'Khoerul Umam', 'khoerul-umam', '081390957111', 'maskhoerul@gmail.com', 100000, 'confirm', 'Coba-coba saja', '2022-11-10', '2022-11-10 16:42:43', '1');

-- --------------------------------------------------------

--
-- Table structure for table `ebook`
--

CREATE TABLE `ebook` (
  `no` int(2) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `url` varchar(200) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ebook`
--

INSERT INTO `ebook` (`no`, `name`, `link`, `content`, `url`, `picture`, `date`, `publish`) VALUES
(8, 'Khutbah Jumat', 'kumpulan-doa--matsurat', 'Informasi lengkap tentang apa itu Aplikasi EMasjid.', 'https://drive.google.com/file/d/1MCeYqZaJlmcpT1WpSNNjiDM-6Cq8OBax/view?usp=sharing', 'khutbah_1.png', '2021-06-15 17:06:38', '1'),
(6, 'Khutbah Jumat', 'sop-haji--umroh', 'Informasi lengkap tentang apa itu Aplikasi EMasjid.', 'https://drive.google.com/file/d/15dHTyBUAsl3-AN4fL1yHaxdtfEjN2DYE/view?usp=sharing', 'khutbah_2.jpg', '2021-06-15 16:52:42', '1'),
(7, 'Khutbah Jumat', 'sop-pelaksanaan-umroh', 'Informasi lengkap tentang apa itu Aplikasi EMasjid.', 'https://drive.google.com/file/d/1MoZVY1lN2wVxZf1yTNy_fIyHsbv3HxjJ/view?usp=sharing', 'khutbah_3.jpg', '2021-06-15 17:01:10', '1');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `no` int(11) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `content` text NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`no`, `subdomain`, `name`, `link`, `tanggal`, `jam`, `content`, `picture`, `date`, `publish`) VALUES
(3, 'emasjid', 'Pengajian Akbar', 'umroh-serta-mengembangkan-pesantren', '0000-00-00', '00:00:00', 'Menjalankan ibadah umroh sekaligus bisa menjalankan 2 amal dalam 1 waktu adalah kenikmatan ibadah yang sempurna\r\n<br>Bagaimana umroh bisa sekaligus membantu pengembangan pondok pesantren? <br>Mengembangkan pondok pesantren pada umumnya akan menelan biaya yang sangat besar karena butuh berbagai macam material. Namun taukah anda jika hanya dengan membayar 1 paket umroh saja anda bisa mengembangkan pondok pesantren? <br>Hasanah tours Umroh sekaligus berinfaq <br>Sebuah terobosan layanan dari Hasanah Tours dan Travel dengan memberikan sebuah layanan Umroh sekaligus Berinfaq. Setiap anda mendaftarkan umroh bersama Hasanah otomatis anda berinfaq sebesar 100 ribu melalui Rumah Zakat yang selajutnya dana tersebut akan digunakan untuk program pengembangan Pondok Pesantren Baitul Qurï¿½an Sragen. <br>Sampai kapan program ini berjalan? <br>Hasanah telah sepakat untuk melakukan kerjasama dengan Rumah Zakat untuk menjalankan program tersebut selama 6 bulan dimulai sejak tanggal 1 Ramadhan. \r\nKapan lagi anda bisa menjalankan 2 amal sekaligus dalam 1 waktu, umroh sekaligus berinfaq untuk Pengembangan Pondok Pesantren.\r\n <br>jangan tunda untuk melakukan kebaikan ini.', '52262371mysch.jpg', '2018-04-04 10:23:24', '1'),
(2, '', 'Manajemen Akun / Pengguna E-Masjid\n', 'umroh-sekaligus-berdayakan-desa', '0000-00-00', '00:00:00', 'Menikmati keindahan perjalanan ibadah umroh namun juga tidak lupa untuk tetap berbagi dengan sesama adalah kenikmatan ibadah yang sempurna\n<br>Bagaimana umroh bisa sekaligus mengumrohkan seseorang?\n<br>Mengumrohkan seseorang dalam pikiran orang awam pada umumnya akan menelan biaya yang sangat besar karena menjadi berlipat ganda. Namun taukah anda jika hanya dengan membayar 1 paket umroh saja anda bisa mengumrohkan sesorang?\n<br>Hasanah tours Umroh sekaligus berinfaq\n<br>Sebuah terobosan layanan dari Hasanah Tours dan Travel dengan memberikan sebuah layanan Umroh sekaligus Berinfaq. Setiap anda mendaftarkan umroh bersama Hasanah otomatis anda berinfaq sebesar 100 ribu melalui Rumah Zakat yang selajutnya dana tersebut akan digunakan untuk program Umroh Marbot Masjid.\n<br>Sampai kapan program ini berjalan?\n<br>Hasanah telah sepakat untuk melakukan kerjasama dengan Rumah Zakat untuk menjalankan program tersebut selama 6 bulan dimulai sejak tanggal 1 Ramadhan.\n<br>Kapan lagi anda bisa mennjalankan 2 amal sekaligus dalam 1 waktu, umroh sekaligus berinfaq untuk mengumrohkan para marbot masjid.\n<br>jangan tunda untuk melakukan kebaikan ini.\n', '', '2018-04-04 10:23:24', '1'),
(1, '', 'Apa itu PilihanKu', 'apa-itu-pilihanku', '0000-00-00', '00:00:00', 'Menikmati keindahan perjalanan ibadah umroh namun juga tidak lupa untuk tetap berbagi dengan sesama adalah kenikmatan ibadah yang sempurna\n<br>Bagaimana umroh bisa sekaligus mengumrohkan seseorang?\n<br>Mengumrohkan seseorang dalam pikiran orang awam pada umumnya akan menelan biaya yang sangat besar karena menjadi berlipat ganda. Namun taukah anda jika hanya dengan membayar 1 paket umroh saja anda bisa mengumrohkan sesorang?\n<br>Hasanah tours Umroh sekaligus berinfaq\n<br>Sebuah terobosan layanan dari Hasanah Tours dan Travel dengan memberikan sebuah layanan Umroh sekaligus Berinfaq. Setiap anda mendaftarkan umroh bersama Hasanah otomatis anda berinfaq sebesar 100 ribu melalui Rumah Zakat yang selajutnya dana tersebut akan digunakan untuk program Umroh Marbot Masjid.\n<br>Sampai kapan program ini berjalan?\n<br>Hasanah telah sepakat untuk melakukan kerjasama dengan Rumah Zakat untuk menjalankan program tersebut selama 6 bulan dimulai sejak tanggal 1 Ramadhan.\n<br>Kapan lagi anda bisa mennjalankan 2 amal sekaligus dalam 1 waktu, umroh sekaligus berinfaq untuk mengumrohkan para marbot masjid.\n<br>jangan tunda untuk melakukan kebaikan ini.\n', '', '2022-10-26 15:33:37', '1');

-- --------------------------------------------------------

--
-- Table structure for table `event_routine`
--

CREATE TABLE `event_routine` (
  `no` int(11) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `tanggal` int(11) NOT NULL,
  `hari` varchar(50) NOT NULL,
  `jam` time NOT NULL,
  `content` text NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event_routine`
--

INSERT INTO `event_routine` (`no`, `subdomain`, `name`, `link`, `tanggal`, `hari`, `jam`, `content`, `picture`, `date`, `publish`) VALUES
(3, 'emasjid', 'Shalat Jumat', 'umroh-serta-mengembangkan-pesantren', 0, 'Jumat', '12:00:00', 'Shalat Jumat', '', '2018-04-04 10:23:24', '1'),
(2, 'emasjid', 'Kajian Subuh', 'umroh-sekaligus-berdayakan-desa', 0, 'Senin', '05:00:00', 'Kajian Subuh', '', '2018-04-04 10:23:24', '1'),
(1, '', 'Apa itu PilihanKu', 'apa-itu-pilihanku', 0, '', '00:00:00', 'Menikmati keindahan perjalanan ibadah umroh namun juga tidak lupa untuk tetap berbagi dengan sesama adalah kenikmatan ibadah yang sempurna\n<br>Bagaimana umroh bisa sekaligus mengumrohkan seseorang?\n<br>Mengumrohkan seseorang dalam pikiran orang awam pada umumnya akan menelan biaya yang sangat besar karena menjadi berlipat ganda. Namun taukah anda jika hanya dengan membayar 1 paket umroh saja anda bisa mengumrohkan sesorang?\n<br>Hasanah tours Umroh sekaligus berinfaq\n<br>Sebuah terobosan layanan dari Hasanah Tours dan Travel dengan memberikan sebuah layanan Umroh sekaligus Berinfaq. Setiap anda mendaftarkan umroh bersama Hasanah otomatis anda berinfaq sebesar 100 ribu melalui Rumah Zakat yang selajutnya dana tersebut akan digunakan untuk program Umroh Marbot Masjid.\n<br>Sampai kapan program ini berjalan?\n<br>Hasanah telah sepakat untuk melakukan kerjasama dengan Rumah Zakat untuk menjalankan program tersebut selama 6 bulan dimulai sejak tanggal 1 Ramadhan.\n<br>Kapan lagi anda bisa mennjalankan 2 amal sekaligus dalam 1 waktu, umroh sekaligus berinfaq untuk mengumrohkan para marbot masjid.\n<br>jangan tunda untuk melakukan kebaikan ini.\n', '', '2022-10-26 15:33:37', '1');

-- --------------------------------------------------------

--
-- Table structure for table `evideo`
--

CREATE TABLE `evideo` (
  `no` int(2) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(200) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `evideo`
--

INSERT INTO `evideo` (`no`, `name`, `link`, `description`, `url`, `picture`, `date`, `publish`) VALUES
(5, 'Strategi Digital Marketing Untuk Memasarkan Umroh dan Haji', 'reflika-web-hasanahtour-to-cabang', '', 'https://www.youtube.com/watch?v=MW_LxT3XHg8', 'manajemen-masjid.jpg\r\n', '2021-11-29 15:43:22', '1');

-- --------------------------------------------------------

--
-- Table structure for table `finance_account`
--

CREATE TABLE `finance_account` (
  `no` int(2) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `status` enum('debit','credit') NOT NULL DEFAULT 'debit',
  `code` varchar(10) NOT NULL,
  `description` text NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `finance_account`
--

INSERT INTO `finance_account` (`no`, `subdomain`, `name`, `link`, `status`, `code`, `description`, `date`, `publish`) VALUES
(1, 'emasjid', 'Pemasukan Jumat', '', 'debit', 'PJ', 'Jenis Pemasukan Infak Rutin Setiap hari Jumat<br>', '2022-11-07 16:04:26', '1'),
(2, 'emasjid', 'Pemasukan Donatur', '', 'debit', 'PD', 'Jenis Pemasukan Infak Dari Donatur Perorangan', '2022-11-10 16:57:53', '1'),
(3, 'emasjid', 'Biaya Rumah Tangga', '', 'credit', 'BR', 'Biaya Rumah Tangga', '2022-11-10 16:58:13', '1');

-- --------------------------------------------------------

--
-- Table structure for table `flyer`
--

CREATE TABLE `flyer` (
  `no` int(2) NOT NULL,
  `username` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `theme` varchar(50) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flyer`
--

INSERT INTO `flyer` (`no`, `username`, `name`, `link`, `theme`, `picture`, `date`, `publish`) VALUES
(4, 'umam', 'Khoerul Umam', '', 'pan.png', '97040301ceo.jpg', '2022-11-08 10:01:19', '1');

-- --------------------------------------------------------

--
-- Table structure for table `flyer_theme`
--

CREATE TABLE `flyer_theme` (
  `no` int(2) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `no_partai` int(11) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flyer_theme`
--

INSERT INTO `flyer_theme` (`no`, `name`, `link`, `description`, `no_partai`, `picture`, `date`, `publish`) VALUES
(3, 'Hari Guru', 'hari-guru', 'Hari Guru', 1, 'pks-hari-guru.png', '2022-11-10 14:42:51', '1'),
(2, 'Ulang Tahun PAN', 'ulang-tahun', 'pan', 5, 'pan-ulang-tahun-sample.png', '2022-11-07 21:18:05', '1');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `no` int(2) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `url` varchar(100) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`no`, `subdomain`, `name`, `link`, `url`, `picture`, `date`, `publish`) VALUES
(1, 'demo', 'Umroh Bersama', 'umroh-rihlah-tazkiah-1', '', 'g_1.jpg', '2018-05-31 00:00:00', '1'),
(2, 'demo', 'Umroh Rihlah Tazkiah 2', 'umroh-rihlah-tazkiah-2', '', 'g_2.jpg', '2018-05-31 00:00:00', '1'),
(3, 'demo', 'Umroh Rihlah Tazkiah 3', 'umroh-rihlah-tazkiah-3', '', 'g_3.jpg', '2018-05-31 00:00:00', '1'),
(4, 'demo', 'Galeri Kegiatan', '', '', 'g_4.jpg', '2022-11-02 09:00:59', '1'),
(49, 'budiprihanto', 'Koala', '', '-', '2728817810274174Koala.jpg', '2022-11-08 10:15:25', '1'),
(50, 'emasjid', 'Hallo', '', 'google.com', '75134954mysch.jpg', '2022-11-10 12:52:10', '1'),
(51, 'triastuti', 'Khoerul Umam', '', 'google.com', '55925086about-bg.jpg', '2022-11-11 21:59:06', '1');

-- --------------------------------------------------------

--
-- Table structure for table `help`
--

CREATE TABLE `help` (
  `no` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `no_help_category` int(11) NOT NULL,
  `content` text NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `help`
--

INSERT INTO `help` (`no`, `name`, `link`, `no_help_category`, `content`, `picture`, `date`, `publish`) VALUES
(3, 'Mengubah Password', 'mengubah-password', 0, 'Content sedang dibuat.', '', '2018-04-04 10:23:24', '1'),
(2, 'Mengubah Profil', 'mengubah-profil', 0, 'Content sedang dibuat.', '', '2018-04-04 10:23:24', '1'),
(1, 'Apa itu PilihanKu', 'apa-itu-pilihanku', 0, 'Content sedang dibuat.', '', '2022-10-26 15:33:37', '1');

-- --------------------------------------------------------

--
-- Table structure for table `help_category`
--

CREATE TABLE `help_category` (
  `no` int(2) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `help_category`
--

INSERT INTO `help_category` (`no`, `name`, `link`, `date`, `publish`) VALUES
(1, 'About', '', '2022-11-07 16:04:26', '1');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `no` int(11) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `unit` varchar(50) NOT NULL,
  `amount` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `status` enum('in','out') NOT NULL DEFAULT 'in',
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`no`, `subdomain`, `name`, `link`, `description`, `unit`, `amount`, `price`, `status`, `picture`, `date`, `publish`) VALUES
(4, 'emasjid', 'Speaker Aktif', '', 'Speaker Aktif', 'pc', 2, 2000000, '', '45970181speaker.jpg', '2022-11-10 14:09:25', '0'),
(3, 'emasjid', 'Coba', '', 'Coba', 'pc', 2, 2000000, '', '85559282karpetmasjid.jpg', '2022-11-10 14:08:29', '0');

-- --------------------------------------------------------

--
-- Table structure for table `jamaah`
--

CREATE TABLE `jamaah` (
  `no` int(11) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `nik` varchar(50) NOT NULL,
  `link` varchar(100) NOT NULL,
  `birth` varchar(100) NOT NULL,
  `sex` enum('L','P') NOT NULL DEFAULT 'L',
  `blood` enum('A','B','AB','O') NOT NULL DEFAULT 'A',
  `marital` enum('menikah','belum menikah','janda/duda') NOT NULL DEFAULT 'belum menikah',
  `job` varchar(100) NOT NULL,
  `economic_status` enum('mampu','kurang mampu','tidak mampu') NOT NULL DEFAULT 'mampu',
  `jamaah_status` enum('jamaah','imam','muadzin','non jamaah') NOT NULL DEFAULT 'jamaah',
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jamaah`
--

INSERT INTO `jamaah` (`no`, `subdomain`, `name`, `nik`, `link`, `birth`, `sex`, `blood`, `marital`, `job`, `economic_status`, `jamaah_status`, `address`, `city`, `phone`, `email`, `picture`, `date`, `publish`) VALUES
(1, 'emasjid', 'Khoerul Umam', '234567890', '', 'Brebes, 27 Maret 1986', 'L', 'A', 'belum menikah', '', 'mampu', 'jamaah', 'Jalan Apa Saja No. 10', 'Temanggung', '081390957111', '', '4104518318608893FB_IMG_1507784432272.jpg', '2022-10-18 11:41:48', '0'),
(2, 'emasjid', 'Afkar Naufal', '', '', 'Temanggung, 20 Agustus 1980', 'L', 'A', 'belum menikah', '', 'mampu', 'jamaah', 'Jalan Apa Saja', '', '0823467899876', '', '14075738444306FB_IMG_1520153104212.jpg', '2022-10-18 18:24:35', '0');

-- --------------------------------------------------------

--
-- Table structure for table `jamaah_category`
--

CREATE TABLE `jamaah_category` (
  `no` int(2) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jamaah_category`
--

INSERT INTO `jamaah_category` (`no`, `subdomain`, `name`, `link`, `date`, `publish`) VALUES
(2, 'emasjid', 'Coba', '', '2022-11-10 16:35:20', '1');

-- --------------------------------------------------------

--
-- Table structure for table `masjid`
--

CREATE TABLE `masjid` (
  `no` int(11) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `maps` varchar(200) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `url_facebook` varchar(200) NOT NULL,
  `url_instagram` varchar(200) NOT NULL,
  `url_youtube` varchar(200) NOT NULL,
  `url_tiktok` varchar(200) NOT NULL,
  `luas_tanah` int(11) NOT NULL,
  `luas_bangunan` int(11) NOT NULL,
  `status_tanah` varchar(100) NOT NULL,
  `tahun_berdiri` year(4) NOT NULL,
  `legalitas` varchar(100) NOT NULL,
  `logo` varchar(200) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masjid`
--

INSERT INTO `masjid` (`no`, `subdomain`, `name`, `link`, `description`, `address`, `city`, `maps`, `phone`, `email`, `url_facebook`, `url_instagram`, `url_youtube`, `url_tiktok`, `luas_tanah`, `luas_bangunan`, `status_tanah`, `tahun_berdiri`, `legalitas`, `logo`, `picture`, `date`, `publish`) VALUES
(1, 'emasjid', 'Masjid El-Huda', 'masjid-alhuda', 'Hallo, Apa Kabar ?', 'Jalan Apa Saja No. 10', 'Temanggung', '123,123', '081390957111', 'info@emasjid.id', 'https://www.facebook.com/emasjid', 'https://www.instagram.com/emasjid', 'https://www.youtube.com/emasjid', 'https://www.tiktok.com/emasjid', 500, 120, 'Wakaf', 2000, 'OK', 'logoalhuda.png', 'masjidalhuda.png', '2022-10-18 11:41:48', '1');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `no` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `category` enum('cabang','mitra','member') NOT NULL DEFAULT 'member',
  `replika` varchar(50) NOT NULL,
  `referral` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `number_id` varchar(50) NOT NULL,
  `birth` varchar(100) NOT NULL,
  `sex` enum('L','P') NOT NULL DEFAULT 'L',
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `bank_name` varchar(50) NOT NULL,
  `bank_branch` varchar(50) NOT NULL,
  `bank_account_number` varchar(50) NOT NULL,
  `bank_account_name` varchar(100) NOT NULL,
  `last_login` datetime NOT NULL,
  `last_ipaddress` varchar(50) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`no`, `username`, `password`, `category`, `replika`, `referral`, `name`, `link`, `number_id`, `birth`, `sex`, `address`, `city`, `phone`, `email`, `bank_name`, `bank_branch`, `bank_account_number`, `bank_account_name`, `last_login`, `last_ipaddress`, `picture`, `date`, `publish`) VALUES
(1, 'umam', 'e10adc3949ba59abbe56e057f20f883e', 'cabang', 'umam', '', 'Khoerul Umam', 'khoerul-umam', '123456789', 'Semarang, 20 Januari 1980', 'L', 'Jalan Apa Saja', 'Temanggung', '085740000146', 'maskhoerul@gmail.com', '', '', '', '', '2022-12-19 09:05:29', '36.72.213.117', '96302125umam.jpg', '2018-03-18 00:00:00', '1'),
(10, 'bumiteknoindonesia@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'member', '', 'umam', 'Bumitekno Indonesia', '', '', '', 'L', '', '', '08123456789', 'bumiteknoindonesia@gmail.com', '', '', '', '', '2022-11-16 12:50:51', '182.2.72.52', '', '2022-11-14 11:21:15', '1'),
(11, 'safari.achmad@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'member', '', 'umam', 'Sofari', '', '', '', 'L', '', '', '085729878710', 'safari.achmad@gmail.com', '', '', '', '', '2022-11-15 13:04:11', '36.73.121.173', '', '2022-11-15 13:01:19', '1'),
(14, 'indradiitya1@gmail.com', '72cbd8b66192d423f1ddafee1c62c9f0', 'member', '', '', 'Aditya ', '', '', '', 'L', '', '', '085740506995', 'indradiitya1@gmail.com', '', '', '', '', '2022-11-24 15:22:45', '36.78.42.42', '', '2022-11-24 15:22:45', '1'),
(13, 'maskhoerul86@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'member', '', '', 'Khoerul Umam', '', '', '', 'L', '', '', '081390957111', 'maskhoerul86@gmail.com', '', '', '', '', '2022-11-23 09:13:33', '180.245.196.44', '', '2022-11-23 09:13:33', '1'),
(15, 'smuhjab@gmail.com', '773f02fa75ce9324e0d496aee5b9ba13', 'member', '', '', 'SAIFUL MUHJAB', '', '', '', 'L', '', '', '(+62)85640810091', 'smuhjab@gmail.com', '', '', '', '', '2022-12-14 10:26:01', '36.79.81.163', '', '2022-12-14 10:26:01', '1');

-- --------------------------------------------------------

--
-- Table structure for table `member_reset`
--

CREATE TABLE `member_reset` (
  `no` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `token` varchar(100) NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_expired` datetime NOT NULL,
  `date_accessed` datetime NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member_reset`
--

INSERT INTO `member_reset` (`no`, `username`, `token`, `ip_address`, `date_created`, `date_expired`, `date_accessed`, `date`, `publish`) VALUES
(5, 'maskhoerul86@gmail.com', 'eyJtZW1iZXJfaWQiOiJtYXNraG9lcnVsODZAZ21haWwuY29tIiwidG9rZW4iOiJVUkxzakdINiJ9', '180.253.130.102', '2022-11-14 08:55:06', '2022-11-14 09:25:06', '2022-11-14 09:09:37', '2022-11-14 08:55:06', '1'),
(6, 'maskhoerul86@gmail.com', 'eyJtZW1iZXJfaWQiOiJtYXNraG9lcnVsODZAZ21haWwuY29tIiwidG9rZW4iOiI5UGc5djA5dSJ9', '182.2.41.109', '2022-11-14 11:14:53', '2022-11-14 11:44:53', '2022-11-14 11:27:34', '2022-11-14 11:14:53', '1'),
(7, 'bumiteknoindonesia@gmail.com', 'eyJtZW1iZXJfaWQiOiJidW1pdGVrbm9pbmRvbmVzaWFAZ21haWwuY29tIiwidG9rZW4iOiJrYkpRRVZ2dyJ9', '', '2022-11-14 11:23:23', '2022-11-14 11:53:23', '0000-00-00 00:00:00', '2022-11-14 11:23:23', '1'),
(8, 'maskhoerul86@gmail.com', 'eyJtZW1iZXJfaWQiOiJtYXNraG9lcnVsODZAZ21haWwuY29tIiwidG9rZW4iOiJ1RkN1RDQ4eSJ9', '150.129.59.5', '2022-11-21 10:49:38', '2022-11-21 11:19:38', '2022-11-21 10:50:27', '2022-11-21 10:49:38', '1');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `no` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `content` longtext NOT NULL,
  `meta_title` varchar(200) NOT NULL,
  `meta_description` varchar(200) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`no`, `name`, `link`, `content`, `meta_title`, `meta_description`, `picture`, `date`, `publish`) VALUES
(1, 'Tentang Kami', 'tentang-kami', '<div id=\"main-wrapper\">\r\n	<div class=\"site-wrapper-reveal\">\r\n		<div class=\"feature-large-images-wrapper section-space--ptb_60\">\r\n			<div class=\"container\">\r\n				<p>\r\n				PilihanKu.net adalah salah satu program layanan kampanye digital dari PT. Bumi Tekno Indonesia. Kami menyediakan layanan paling mudah kepada kontestan dan pelaku politik dalam melakukan kampanye secara cerdas menggunakan media digital untuk mendukung budaya politik yang sehat di Indonesia. Misi PilihanKu.net adalah menyediakan layanan pembuatan website kampanye yang profesional, terpercaya, tercepat, dan termudah meskipun pengguna belum pernah memiliki pengalaman membuat website sama sekali. Selain itu PilihanKu.net juga menyediakan fitur yang mempermudah penggunanya dalam membuat media kampanye visual berupa flyer yang bisa dibuat sendiri dengan proses yang sangat sederhana, cepat, dan dapat dilakukan oleh siapapun meskipun tidak memiliki background sebagai grafis desainer.\r\n				</p>\r\n				<br/>\r\n				<h5 class=\"mb-10\">Sejarah Singkat PilihanKu.net</h5>\r\n				<p>\r\n				Berawal dari kegelisaan kami melihat budaya politik yang tidak sehat dengan menyalahgunakan isu SARA sebagai bahan kampanye yang dapat menimbulkan perpecahan antar golongan. Selain itu praktik politik yang transaksional yang menghiasai hampir setiap event politik dari mulai tingkat desa, pilkada kabupaten-kota, pilkada gubernur, maupun pemilu partai politik, dan pemilihan presiden. Tentunya kami sebagai putra bangsa mengharapkan budaya dan praktik politik semakin lebih baik kedepannya. Sehingga muncullah platform PilihanKu.net sebagai upaya sederhana untuk membangun budaya politik yang sehat agar terlahir pemimpin-pemimpin yang akan membawa bangsa dan negera ini ke arah yang lebih baik.\r\n				</p>\r\n				<br/>\r\n				<h5 class=\"mb-10\">Layanan PilihanKu.net</h5>\r\n				<h6>Pembuatan Website Kampanye</h6>\r\n				<p>\r\n				Layanan utama PilihanKu.net adalah pembuatan website kampanye. Website kampanye PilihanKu.net dirancang untuk memudahkan pengguna mendesain website kampanye sendiri meskipun belum berpengalaman membuat website. Website kampanye PilihanKu.net didesain secara modern menggunakan teknologi terkini sehingga tidak ketinggalan zaman. \r\n				Anda dapat membuat website kampanye sendiri. Cukup dengan memasukkan data-data pada formulir yang tersedia. Kurang dari 1 menit website kampanye Anda, langsung jadi, langsung online. \r\n				</p>\r\n				<h6>Pembuatan Flyer Digital</h6>\r\n				<p>\r\n				Flyer Digital adalah media visual berupa gambar yang dapat digunakan oleh user untuk mensosialisasikan kampanye politiknya. Pembuatan Flyer Digital bisa dilakukan oleh siapa saja meskipun tidak memiliki kemampuan sebagai desainer grafis. Disini Anda bisa membuatnya sendiri dengan hanya memilih pola / template desain yang telah tersedia, kemudian memilih foto terbaik Anda. Hanya beberapa detik, Flyer Digital untuk kampanye Anda sudah jadi. Dan siap untuk dishare / dibagikan melalui platform sosial media Anda.\r\n				</p>\r\n				<h6>Pengelolaan Sosial Media</h6>\r\n				<p>\r\n				Bagi Anda yang ingin memaksimalkan kampanye melalui berbagai platform sosial media seperti facebook, instagram, tiktok, youtube, dsb. Kami siap membantu Anda mengelola akun sosial media Anda untuk mensukseskan kampanye politik Anda. Kami menyediakan berbagai layanan mulai dari riset hastag, perencanaan konten, pembuatan desain gambar, pembuatan narasi / deskripsi, sampai dengan distribusi materi ke berbagai platform sosial media baik secara organik, maupun melalui media iklan berbayar. Dengan dibantu pengelolaan sosial media yang profesional dan berpengalaman, Anda dapat menjangkau audience / target pemilih Anda lebih cepat, lebih akurat, dan lebih luas. \r\n				</p>\r\n				<h6>Aplikasi Pemenangan Caleg</h6>\r\n				<p>\r\n				Aplikasi Pemenangan Caleg adalah sebuah software / aplikasi yang dibuat khusus untuk memudahkan calon legislatif untuk mengelola database relawan, menggerakkan relawan, dan mengevaluasi kinerja relawan. Aplikasi Pemenangan Caleg juga dapat mendata calon pemilih yang berhasil direkrut oleh para relawan politik Anda. Anda dapat memonitor progress kerja-kerja relawan berdasarkan sebaran wilayahnya melalui dashboard yang bisa Anda akses dimana saja dan kapan saja. Menyajikan data real time yang dihimpun oleh tim sukses dan relawan politik Anda. Aplikasi Pemenangan Caleg akan memudahkan Anda dalam mengukur tingkat keberhasilan program kampanye Anda.\r\n				</p>\r\n				<br/>\r\n				<h5 class=\"mb-10\">Legalitas Perusahaan</h5>\r\n				\r\n				<div class=\"row\">\r\n					<div class=\"col-lg-3\">\r\n						<img src=\"https://www.bumitekno.com/cms_web/upload/picture/office.jpg\" alt=\"kantor pt bumi tekno\" width=\"100%\">\r\n					</div>\r\n					<div class=\"col-lg-9\">\r\n						<p>\r\n						Legalitas perusahaan atau badan usaha merupakan unsur terpenting. Legalitas merupakan jati diri yang melegalkan atau mengesahkan suatu badan usaha sehingga diakui oleh masyarakat. Dengan adanya legalitas, suatu perusahaan dianggap sah menurut undang-undang dan peraturan yang berlaku, dimana perusahaan tersebut dilindungi dengan berbagai dokumen yang sah di mata hukum.\r\n						</p>\r\n						<table width=\"100%\" style=\"vertical-align:top\">\r\n							<tbody>\r\n								<tr>\r\n									<td width=\"150\">Nama Perusahaan</td><td width=\"10\">:</td>\r\n									<td>PT. Bumi Tekno Indonesia</td>\r\n								</tr>\r\n								<tr>\r\n									<td>Bentuk Usaha</td><td>:</td>\r\n									<td>Perseroan Terbatas (PT)</td>\r\n								</tr>\r\n								<tr>\r\n									<td>Bidang Perusahaan</td><td>:</td>\r\n									<td>Teknologi Informasi dan Komunikasi, pengembangan perangkat lunak, dan konsultasi di bidang TI</td>\r\n								</tr>\r\n								<tr>\r\n									<td>Alamat</td><td>:</td>\r\n									<td>Kebumen, Kecamatan Pringsurat, Kabupaten Temanggung, Jawa Tengah</td>\r\n								</tr>\r\n								<tr>\r\n									<td>Akta Notaris</td><td>:</td>\r\n									<td>No. 71 Tahun 2019, Tanggal 26 Desember 2019</td>\r\n								</tr>\r\n								<tr>\r\n									<td>Surat Ijin Usaha</td><td>:</td>\r\n									<td>No. 0220106112111, Tanggal 21 Januari 2020</td>\r\n								</tr>\r\n								<tr>\r\n									<td>NPWP</td><td>:</td>\r\n									<td>No. 93.894.341.2-533.000, Tanggal 3 Januari 2020</td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n					</div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n\r\n		<div class=\"contact-us-area infotechno-contact-us-bg section-space--ptb_60\">\r\n			<div class=\"container\">\r\n				<h3 class=\"heading mb-20 text-center\">Core Value</h3>\r\n				<div class=\"row text-center\">\r\n					<div class=\"col-lg-2\">\r\n						<h3>S</h3>\r\n						<p>Solid in Solving Problem</p>\r\n					</div>\r\n					<div class=\"col-lg-2\">\r\n						<h3>I</h3>\r\n						<p>Initiative to Innovate</p>\r\n					</div>\r\n					<div class=\"col-lg-2\">\r\n						<h3>M</h3>\r\n						<p>Make Progress</p>\r\n					</div>\r\n					<div class=\"col-lg-2\">\r\n						<h3>P</h3>\r\n						<p>Professional</p>\r\n					</div>\r\n					<div class=\"col-lg-2\">\r\n						<h3>L</h3>\r\n						<p>Learn Continuously</p>\r\n					</div>\r\n					<div class=\"col-lg-2\">\r\n						<h3>E</h3>\r\n						<p>End User Focus</p>\r\n					</div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		\r\n	</div>\r\n</div>', '', '', '', '2022-11-05 03:49:07', '1'),
(3, 'Rekening Perusahaan', 'rekening', '<div id=\"main-wrapper\">	<div class=\"site-wrapper-reveal\">		<div class=\"contact-us-section-wrappaer section-space--ptb_60\">			<div class=\"container\">				<div class=\"row\">					<div class=\"col-lg-6\">						<div class=\"card mb-20\">														<div class=\"card-body\">								<img class=\"img-fulid\" src=\"https://www.mysch.id/cms_web/upload/image/mandiri.jpg\" height=\"33\" alt=\"mandiri\"/>								<p><b>Bank Mandiri</b><br/>No. Rek. 1360010201660 A. N. Tri Astuti</p>							</div>						</div>						<div class=\"card mb-20\">														<div class=\"card-body\">								<img class=\"img-fulid\" src=\"https://www.mysch.id/cms_web/upload/image/bca.jpg\" height=\"33\" alt=\"bca\">								<p><b>Bank BCA</b><br/>No. Rek. 8030112343 A. N. Tri Astuti</p>							</div>						 </div>					</div>					<div class=\"col-lg-6\">						<div class=\"card mb-20\">													<div class=\"card-body\">								<img class=\"img-fulid\" src=\"https://www.mysch.id/cms_web/upload/image/bni.png\" height=\"33\" alt=\"bni\">								<p><b>Bank BNI</b><br/>No. Rek. 0850844796 A. N. Tri Astuti</p>							</div>						</div>						<div class=\"card mb-20\">							<div class=\"card-body\">								<img class=\"img-fulid\" src=\"https://www.mysch.id/cms_web/upload/image/bri.jpg\" height=\"33\" alt=\"bri\">								<p><b>Bank BRI</b><br/>No. Rek. 144701001148505 A. N. Tri Astuti</p>							</div>						</div>					</div>				</div>				<div class=\"card bg-gray-3 \">					<div class=\"card-body\">						<h5>Catatan: </h5>						<p>Pembayaran dianggap <strong>sah</strong> hanya jika dilakukan melalui salah satu rekening yang tercantum pada halaman ini.</p>					</div>				</div>			</div>		</div>	</div></div>', '', '', '', '2022-11-05 03:49:53', '1'),
(4, 'Program Affiliate', 'affiliate', '', '', '', '', '2022-11-05 03:49:53', '1'),
(5, 'Syarat dan Ketentuan', 'syarat-dan-ketentuan', '<div id=\"main-wrapper\">     	<div class=\"site-wrapper-reveal\"> 				<div class=\"feature-large-images-wrapper section-space--ptb_60\"> 						<div class=\"container\"> 				<p>				Syarat & Ketentuan yang ditetapkan di bawah ini disusun sebagai suatu aturan bagi setiap pengguna platform PilihanKu.net. Pengguna disarankan untuk membaca dan memahami syarat dan ketentuan yang ditetapkan ini terlebih dahulu. Hal itu dimaksudkan supaya setiap hak dan kewajiban yang melekat kepada pengguna maupun PT. Bumi Tekno Indonesia (selaku owner dari platform PilihanKu.net) dapat terjaga dengan baik.				</p>				<p>				Pengguna dianggap telah membaca, mengerti, memahami dan menyetujui setiap hal yang diatur dalam Syarat & Ketentuan pada saat mulai menggunakan atau pada saat telah mendaftarkan diri pada platform PilihanKu.net.				</p>				<p>				Syarat & Ketentuan penggunaan platform PilihanKu.net merupakan perjanjian yang mengikat dan sah antara Pengguna dan pemilik platform PilihanKu.net yang persetujuannya ditandai dengan digunakannya atau didaftarkannya Pengguna pada platform PilihanKu.net sehingga hal demikian dianggap sebagai syarat sahnya pengikatan.				</p>				<br/>				<h5>Definisi</h5>				<p>				PT. Bumi Tekno Indonesia merupakan pemilik dan pengelola platform PilihanKu.net, yang dikenal dengan sebutan PilihanKu.net, sebuah platform layanan kampanye politik secara digital di Indonesia. PilihanKu.net ingin menghadirkan budaya politik baru yaitu dengan meningkatkan partisipasi masyarakat melalui digitalisasi politik agar terlahir pemimpin-pemimpin yang akan membawa Indonesia lebih baik. Pengguna merupakan setiap pihak yang menggunakan platform PilihanKu.net, baik yang telah mendaftar atau yang belum mendaftar namun telah mengakses platform PilihanKu.net.				</p>				<br/>				<h5>Ruang Lingkup</h5>				<p>				PilihanKu.net menyediakan berbagai layanan yang dibutuhkan dalam kampanye secara digital diantaranya :				<ol>					<li><b>Website Kampanye</b>, yaitu sekumpulan halaman website yang berisi informasi dengan tujuan sebagai media kampanye politik;</li>					<li><b>Flyer Digital</b>, yaitu alat untuk membuat media visual dengan menggabungkan pola / template desain dengan foto / gambar yang diberikan oleh Pengguna;</li>					<li><b>Kampanye Sosial Media</b>, yaitu layanan kampanye melalui sosial media mulai dari perencanaan, desain media visual, distribusi ke platfom sosial media;</li>					<li><b>Aplikasi Pemenangan</b>, yaitu aplikasi yang berisi database relawan politik dan calon pemilih / pendukung dan dikelola secara mandiri oleh user.</li>				</ol>				</p>				<br/>				<h5>Pendaftaran Pengguna</h5>				<p>				Pendaftaran Pengguna ditujukan untuk memperoleh akun dan password yang sifatnya individual sehingga login untuk mengakses berbagai informasi atau data dapat dilakukan oleh Pengguna. Proses pendaftaran ditetapkan secara penuh oleh PilihanKu.net melalui platform PilihanKu.net.				</p>				<p>				Pengguna menjamin kebenaran dan keabsahan informasi yang digunakan untuk melakukan pendaftaran sebagai data personal yang dimilikinya. Bagi Pengguna, yang usianya dibawah 17 (tujuh belas) tahun dan/atau dalam masa tanggungan orang lain, menjamin bahwa penggunaan atau pendaftaran pada platform PilihanKu.net telah mendapatkan izin atau persetujuan dari orangtua atau walinya yang sah.				</p>				<p>				Pengguna menyatakan setuju untuk memberikan informasi tentang data dirinya sebagai syarat untuk melakukan pendaftaran.				</p>				<p>				Pengguna tidak diperkenankan untuk menginformasikan akun dan password kepada pihak manapun. Setiap terjadinya penyalahgunaan akun dan password yang merugikan Pengguna sendiri atau PilihanKu.net atau pihak ketiga lainnya menjadi tanggung jawab Pengguna sepenuhnya.				</p>				<br/>				<h5>Biaya, Pembayaran dan Pengembalian Dana</h5>				<p>				PilihanKu.net menyediakan beberapa fitur yang gratis dan ada pula yang berbayar seperti Website Kampanye, Pengelolaan Social Media, dan Aplikasi Pemenangan.				</p>				<p>				PilihanKu.net tidak menjamin pengembalian pembayaran apabila terjadi pembatalan penggunaan layanan berbayar.				</p>				<p>				Jika seorang pengguna melanggar atau berkali-kali melanggar hak atas kekayaan intelektual kami atau pihak lain, kami dapat, dalam diskresi kami, mengakhiri atau menolak akses terhadap dan penggunaan Layanan. Dalam hal ini, kami tidak memiliki kewajiban untuk memberikan suatu pengembalian terhadap jumlah apa pun yang telah dibayarkan kepada kami sebelumnya.				</p>				<br/>				<h5>Penghentian Akun Pengguna</h5>				<p>				Akun dan password yang diberikan kepada Pengguna sifatnya individual, tidak dapat dipindahtangankan atau diperdagangkan, ditujukan semata-semata untuk layanan kampanye Pengguna, memperhatikan ketentuan peraturan yang berlaku, termasuk namun tidak terbatas pada Syarat & Ketentuan ini.				</p>				<p>				Setiap pelanggaran yang dilakukan oleh Pengguna termasuk namun tidak terbatas pada Syarat dan Ketentuan ini, termasuk diantaranya pembajakan, penyampaian isu SARA (suku agama ras antar golongan), pelanggaran hak kekayaan intelektual, pengiriman spam, penyebaran virus, maka akan dikenakan sanksi penghentian atau pemblokiran akun secara langsung oleh PilihanKu.net.				</p>				<p>				Penghentian atau pemblokiran akun pengguna tidak serta merta menghentikan kewajiban hukum atas pelanggaran yang dilakukan oleh Pengguna sesuai ketentuan hukum dan peraturan perundang-undangan yang berlaku.				</p>				<br/>				<h5>Kerahasiaan</h5>				<p>				Pengguna bersedia memberikan informasi pribadi termasuk namun tidak terbatas pada nama dan informasi kontak. Semua Informasi pribadi akan dipergunakan, dijaga dan dirahasiakan sesuai dengan ketentuan hukum yang berlaku.				</p>				<p>				Pengguna dapat menonaktifkan akunnya setelah pengguna tidak lagi terdaftar (misal karena diblok). Semua Informasi pribadi akan disimpan, dipergunakan, dijaga dan dirahasiakan sesuai dengan ketentuan hukum yang berlaku.				</p>				<p>				<br/>				<h5>Batasan Tanggung Jawab</h5>				<p>				Setiap informasi yang ada atau tersedia dalam platform PilihanKu.net berasal dari Kontributor dan secara internal telah dilakukan upaya-upaya yang diperlukan untuk memastikan kevalidan informasi tersebut. Namun demikian, Pengguna memahami bahwa informasi tersebut tidak pernah dijaminkan kebenaran sepenuhnya oleh PilihanKu.net. Dalam situasi dimana ditemukan kesalahan atau kekeliruan atau perubahan informasi yang telah disajikan sebelumnya, maka hal-hal demikian akan disesuaikan dan tidak akan pernah menimbulkan permasalahan terhadap pihak manapun.				</p>				<p>				PilihanKu.net telah melakukan upaya teknis yang wajar untuk memberikan pengamanan atas data Pengguna dari risiko kehilangan atau akses, penggunaan, perubahan, dan pengungkapan oleh pihak yang tidak berwenang. Namun demikian, PilihanKu.net tidak dapat menjamin bahwa tidak akan pernah ada pihak ketiga yang tidak akan mampu melewati pengamanan tersebut atau menggunakan informasi pribadi Pengguna untuk maksud yang tidak sepantasnya.				</p>				<br/>				<h5>Hukum yang Berlaku</h5>				<p>Hukum yang berlaku adalah Hukum Negara Kesatuan Republik Indonesia.</p>				<br/>				<h5>Penyelesaian Sengketa</h5>				<p>				Setiap terjadi permasalahan, para pihak akan menyelesaikannya dengan cara musyawarah. Apabila telah dilakukan musyawarah dalam 30 (tiga puluh) hari kalender sejak musyawarah pertama diadakan dan tetap tidak menghasilkan solusi maka para pihak sepakat untuk menyelesaikan perselisihan melalui jalur hukum.				</p>				<br/>				<h5>Lain-lain</h5>				<p>Syarat dan Ketentuan ini merupakan keseluruhan kesepakatan antara para pihak sebagai dasar pemanfaatan platform PilihanKu.net.</p>				<p>				Apabila dikemudian hari terdapat salah satu atau beberapa ketentuan yang diatur terbukti menjadi ilegal, tidak sah, maupun tidak dapat dilaksanakan, maka ketentuan tersebut akan segera diperbaharui oleh PilihanKu.net yang secara prinsip diupayakan untuk tidak mengubah mekanisme yang ada namun tetap memperhatikan ketentuan hukum yang berlaku.				</p>				<p>				Syarat dan ketentuan dapat berubah sewaktu-waktu apabila diperlukan dengan kewenangan penuh PilihanKu.net. Tidak ada kewajiban PilihanKu.net untuk menginformasikan setiap perubahan sehingga Pengguna bersedia untuk memeriksa Syarat & Ketentuan ini secara berkala agar perubahannya dapat diketahui. Dengan tetap mengakses dan menggunakan platform PilihanKu.net, maka Pengguna dianggap menyetujui perubahan-perubahan dalam Syarat & Ketentuan.				</p>			</div>		</div>	</div></div>', '', '', '', '2022-11-05 03:50:26', '1'),
(6, 'Kebijakan Privasi', 'kebijakan-privasi', '<div id=\"main-wrapper\">     <div class=\"site-wrapper-reveal\"> 		<div class=\"feature-large-images-wrapper section-space--ptb_60\"> 			<div class=\"container\"> 								<p> 				Kebijakan Privasi ini mengatur Anda dalam mengakses dan menggunakan layanan-layanan, situs yang disediakan PilihanKu.net. Akses dan penggunaan Layanan oleh Anda adalah sesuai dengan penerimaan dan kepatuhan Anda terhadap Ketentuan-Ketentuan ini. Dengan mengakses dan menggunakan Layanan, Anda setuju untuk tunduk dan terikat pada Ketentuan ini. 				</p> 				<p> 				Kebijakan Privasi ini (Kebijakan Privasi) menjelaskan kebijakan dan prosedur PilihanKu.net dalam memperoleh, menggunakan, mengungkapkan dan membagi informasi Anda pada saat Anda menggunakan Layanan PilihanKu.net, melalui situs PilihanKu.net, konten PilihanKu.net yang ditautkan ke halaman situs lainnya, melalui telepon genggam Anda atau melalui salah satu aplikasi PilihanKu.net untuk perangkat bergerak. Kami tidak akan menggunakan atau membagikan informasi Anda dengan pihak manapun kecuali dinyatakan dalam Kebijakan Privasi ini. Kebijakan Privasi ini tidak berlaku untuk informasi yang yang kami peroleh dengan cara lain (termasuk offline) atau dari pihak lain. 				</p> 				<p> 				PilihanKu.net menggunakan informasi yang kami peroleh untuk menganalisa tentang bagaimana Layanan digunakan, mendiagnosa persoalan layanan dan persoalan teknis, memelihara keamanan, personalisasi konten, mengingat informasi untuk memudahkan Anda dalam mengakses akun Anda secara efisien, memantau jumlah tertentu seperti total pengunjung, lalu-lintas akses dan pola demografi serta mengikuti Konten Pengguna dan pengguna sebagaimana dibutuhkan menurut ketentuan hukum yang berlaku. Informasi Yang Disediakan Pengguna Anda menyediakan kepada kami informasi tentang diri Anda, meliputi nama Anda, alamat surat elektonik dan media sosial apabila Anda mendaftar sebagai akun pengguna Layanan. Nama Anda dan segala informasi yang tentukan untuk dimuat dalam profil Anda akan dapat dilihat oleh publik dalam Layanan. Kami dapat menggunakan alat surat elektronik Anda untuk mengirimkan pemberitahuan berkaitan dengan layanan (termasuk segala pemberitahuan yang diharuskan oleh undang-undang, sebagai pengganti komunikasi melalui surat pos tercatat). Kami juga dapat menggunakan kontak informasi Anda untuk mengirimkan Anda surel tentang pemasaran. Apabila Anda tidak berkenan menerima pesan surel tersebut, Anda dapat mengaturnya dengan mengikuti instruksi yang terdapat dalam surel. Apabila Anda berkomunikasi dengan kami melalui surel, kami dapat menyimpan isi dari pesan surel Anda, alamat surel Anda dan balasan dari kami. 				</p> 				<p> 				Apabila Anda menggunakan akun Facebook, Google atau akun media sosial Anda lainnya (Akun Medsos) untuk mendaftar ke PilihanKu.net, maka kami akan mengumpulkan dan menyimpan identitas pengguna Anda. Apabila Anda mengubungkan PilihanKu.net dengan Akun Medsos Anda (seperti mengizinkan untuk posting dari PilihanKu.net), kami meminta izin dari Anda untuk memperoleh informasi tertentu dari Akun Medsos Anda (seperti informasi dari profil Akun Medsos Anda). Anda juga menyediakan informasi dalam Konten Pengguna yang Anda kirimkan ke Layanan. Pertanyaan, jawaban, dan kontribusi Anda lainnya dalam Layanan, dan metadata terhadapnya (seperti waktu Anda menayangkannya), dapat dilihat oleh publik dalam Layanan, bersama dengan nama Anda (kecuali Layanan mengizinkan Anda untuk menayangkan secara anonim). Informasi ini mungkin dapat dicari melalui mesin pencari dan dipublikasikan ulang di tempat lainnya dalam jaringan sesuai dengan ketentuan dalam Ketentuan Layanan kami. 				</p> 				\r\n<br/>\r\n<h5>Informasi Yang Diperoleh Secara Otomatis</h5> 				<p> 				Pada saat Anda menggunakan Layanan, kami menggunakan session cookies dan persistent cookies (informasi lebih lanjut mengenai kuki dapat dilihat pada bagian bawah) dan teknologi pelacak lainnya seperti log files, clear GIFs, dan teknologi flash untuk (i) menyimpan nama pengguna dan kata sandi Anda; (ii) menganalisa penggunaan Layanan; (iii) mengatur Layanan sesuai dengan preferensi Anda; (iv) mengatur iklan yang yang ditayangkan dalam Layanan. Kami juga dapat menyertakan clear GIFS dalam surel berbasis HTML yang dikirimkan kepada pengguna kami untuk mengetahui apakah pesan telah dibuka. Sesuai dengan teknologi tambahan yang mungkin kami adopsi, kami juga dapat mengumpulakn informasi tambahan melalui metode lainnya. 				</p> 				<p> 				Kami menggunakan teknologi otomatis untuk mengumpulkan dan menganalisa jenis-jenis informasi tertentu, meliputi (i) informasi berkaitan dengan perangkat atau penjelajah yang Anda gunakan untuk mengakses atau berhubungan dengan Layanan, seperti : alama IP, informasi geolokasi, pengidentifikasi unik perangkat and informasi lainnya tentang telepon genggam Anda atau perangkat bergerak lainnya, jenis penjelajah, bahasa penjelajah, nomor atau kode unik dalam kuki; dan (ii) informasi berkaitan tentang bagaimana cara Anda berinteraksi dengan Layanan, seperti: mengarahkan atau keluar dari halaman atau URL, jenis platform, jumlah klik, nama domain, halaman pembuka, halaman yang dilihat dan urutan dari halaman tersebut. Jumlah waktu yang dihabiskan dalam halaman tertentu. Tanggal dan waktu Anda menggunakan Layanan, dan informasi sejenis lainnya. Kami juga dapat memperoleh data lainnya seperti kriteria dan hasil pencarian. 				</p> 			</div> 		</div> 	</div> </div>', '', '', '', '2022-11-05 03:50:26', '1');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `no` int(2) NOT NULL,
  `username` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `url` varchar(200) NOT NULL,
  `status` enum('read','unread') NOT NULL DEFAULT 'unread',
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`no`, `username`, `name`, `link`, `content`, `url`, `status`, `picture`, `date`, `publish`) VALUES
(8, 'umam', 'Khutbah Jumat', 'kumpulan-doa--matsurat', 'Informasi lengkap tentang apa itu Aplikasi EMasjid.', 'https://drive.google.com/file/d/1MCeYqZaJlmcpT1WpSNNjiDM-6Cq8OBax/view?usp=sharing', 'read', '', '2021-06-15 17:06:38', '1'),
(6, 'umam', 'Khutbah Jumat', 'sop-haji--umroh', 'Informasi lengkap tentang apa itu Aplikasi EMasjid.', 'https://drive.google.com/file/d/15dHTyBUAsl3-AN4fL1yHaxdtfEjN2DYE/view?usp=sharing', 'read', 'khutbah_2.jpg', '2021-06-15 16:52:42', '1'),
(11, 'indradiitya1@gmail.com', 'Selamat Datang di PilihanKu.net', '', '<p>Salam, Aditya </p><p>Selamat datang di PilihanKu.net</p><p>Pendaftaran Anda sebagai anggota telah masuk ke sistem kami.<br/>Anda dapat menggunakan layanan dengan menekan tombol menu yang ada di sebelah kiri halaman ini.<br/>Informasi lebih lanjut mengenai layanan kami, silahkan menghubungi Customer Service.</p><p>Semoga kesuksesan senantiasa menyertai kita semua.</p><p>Best Regards<br/>PilihanKu.net</p>', '', 'unread', '', '2022-11-24 15:22:45', '1'),
(10, 'maskhoerul86@gmail.com', 'Selamat Datang di PilihanKu.net', '', '<p>Salam, Khoerul Umam</p><p>Selamat datang di PilihanKu.net</p><p>Pendaftaran Anda sebagai anggota telah masuk ke sistem kami.<br/>Anda dapat menggunakan layanan dengan menekan tombol menu yang ada di sebelah kiri halaman ini.<br/>Informasi lebih lanjut mengenai layanan kami, silahkan menghubungi Customer Service.</p><p>Semoga kesuksesan senantiasa menyertai kita semua :</p><p>Best Regards<br/>PilihanKu.net</p>', '', 'read', '', '2022-11-23 09:13:33', '1'),
(12, 'smuhjab@gmail.com', 'Selamat Datang di PilihanKu.net', '', '<p>Salam, SAIFUL MUHJAB</p><p>Selamat datang di PilihanKu.net</p><p>Pendaftaran Anda sebagai anggota telah masuk ke sistem kami.<br/>Anda dapat menggunakan layanan dengan menekan tombol menu yang ada di sebelah kiri halaman ini.<br/>Informasi lebih lanjut mengenai layanan kami, silahkan menghubungi Customer Service.</p><p>Semoga kesuksesan senantiasa menyertai kita semua.</p><p>Best Regards<br/>PilihanKu.net</p>', '', 'unread', '', '2022-12-14 10:26:01', '1');

-- --------------------------------------------------------

--
-- Table structure for table `partai`
--

CREATE TABLE `partai` (
  `no` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `number` int(11) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partai`
--

INSERT INTO `partai` (`no`, `name`, `link`, `number`, `picture`, `date`, `publish`) VALUES
(1, 'PKS', 'pks', 3, 'pks.jpg', '2017-12-21 00:00:00', '1'),
(2, 'PDIP', 'pdip', 4, 'pdip.jpg', '2017-12-21 00:00:00', '1'),
(3, 'PKB', 'pkb', 2, 'pkb.jpg', '2018-01-12 00:00:00', '1'),
(4, 'PPP', 'ppp', 9, 'ppp.jpg', '2018-01-12 00:00:00', '1'),
(5, 'PAN', 'pan', 8, 'pan.jpg', '2018-01-12 00:00:00', '1'),
(6, 'Hanura', 'hanura', 10, 'hanura.jpg', '2018-01-12 00:00:00', '1'),
(7, 'Golkar', 'golkar', 5, 'golkar.jpg', '2018-01-12 00:00:00', '1'),
(8, 'Gerindra', 'gerindra', 6, 'gerindra.jpg', '2018-01-12 00:00:00', '1'),
(9, 'Demokrat', 'demokrat', 7, 'demokrat.jpg', '2018-01-12 00:00:00', '1'),
(10, 'Nasdem', 'nasdem', 1, 'nasdem.jpg', '2018-01-12 00:00:00', '1'),
(11, 'Perindo', 'perindo', 11, 'perindo.jpg', '2018-01-12 00:00:00', '1'),
(12, 'PSI', 'psi', 12, 'psi.jpg', '2018-01-12 00:00:00', '1'),
(13, 'Garuda', 'garuda', 13, 'garuda.jpg', '0000-00-00 00:00:00', '1'),
(14, 'Berkarya', 'berkarya', 14, 'berkarya.jpg', '0000-00-00 00:00:00', '1'),
(15, 'Gelora', 'gelora', 99, 'gelora.jpg', '2022-11-07 15:58:56', '1');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `no` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `meta_title` varchar(200) NOT NULL,
  `meta_description` varchar(200) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`no`, `name`, `link`, `content`, `meta_title`, `meta_description`, `picture`, `date`, `publish`) VALUES
(4, 'Kampanye Social Media', 'social-campaign', 'Menikmati keindahan perjalanan ibadah umroh namun juga tidak lupa untuk tetap berbagi dengan sesama adalah kenikmatan ibadah yang sempurna\r\n<br>Bagaimana umroh bisa sekaligus mengumrohkan seseorang?\r\n<br>Mengumrohkan seseorang dalam pikiran orang awam pada umumnya akan menelan biaya yang sangat besar karena menjadi berlipat ganda. Namun taukah anda jika hanya dengan membayar 1 paket umroh saja anda bisa mengumrohkan sesorang?\r\n<br>Hasanah tours Umroh sekaligus berinfaq\r\n<br>Sebuah terobosan layanan dari Hasanah Tours dan Travel dengan memberikan sebuah layanan Umroh sekaligus Berinfaq. Setiap anda mendaftarkan umroh bersama Hasanah otomatis anda berinfaq sebesar 100 ribu melalui Rumah Zakat yang selajutnya dana tersebut akan digunakan untuk program Umroh Marbot Masjid.\r\n<br>Sampai kapan program ini berjalan?\r\n<br>Hasanah telah sepakat untuk melakukan kerjasama dengan Rumah Zakat untuk menjalankan program tersebut selama 6 bulan dimulai sejak tanggal 1 Ramadhan.\r\n<br>Kapan lagi anda bisa mennjalankan 2 amal sekaligus dalam 1 waktu, umroh sekaligus berinfaq untuk mengumrohkan para marbot masjid.\r\n<br>jangan tunda untuk melakukan kebaikan ini.\r\n', '', '', '', '2018-04-04 10:23:24', '1'),
(1, 'Aplikasi Caleg', 'app-caleg', '<br>Menjalankan ibadah umroh sekaligus bisa menjalankan 2 amal dalam 1 waktu adalah kenikmatan ibadah yang sempurna</b>\r\n<br>Bagaimana umroh bisa sekaligus membantu pengembangan pondok pesantren?</b> <br>Mengembangkan pondok pesantren pada umumnya akan menelan biaya yang sangat besar karena butuh berbagai macam material. Namun taukah anda jika hanya dengan membayar 1 paket umroh saja anda bisa mengembangkan pondok pesantren?</b> <br>Hasanah tours Umroh sekaligus berinfaq</b> <br>Sebuah terobosan layanan dari Hasanah Tours dan Travel dengan memberikan sebuah layanan Umroh sekaligus Berinfaq. Setiap anda mendaftarkan umroh bersama Hasanah otomatis anda berinfaq sebesar 100 ribu melalui Rumah Zakat yang selajutnya dana tersebut akan digunakan untuk program pengembangan Pondok Pesantren Baitul Qur’an Sragen.</b> <br>Sampai kapan program ini berjalan?</b> <br>Hasanah telah sepakat untuk melakukan kerjasama dengan Rumah Zakat untuk menjalankan program tersebut selama 6 bulan dimulai sejak tanggal 1 Ramadhan. \r\nKapan lagi anda bisa menjalankan 2 amal sekaligus dalam 1 waktu, umroh sekaligus berinfaq untuk Pengembangan Pondok Pesantren.\r\n</b> <br>jangan tunda untuk melakukan kebaikan ini.</b>', '', '', '', '2018-04-04 10:23:24', '1'),
(2, 'Template Flyer Kampanye', 'template-flyer', '<br>Menjalankan ibadah umroh sekaligus memberikan manfaat ekonomi dengan sesama adalah kenikmatan ibadah yang sempurna</b>\r\n<br>Bagaimana umroh bisa sekaligus Berdayakan Desa?</b> <br>Berdayakan Desa dalam pikiran kita pada umumnya akan menelan biaya yang sangat besar dan membutuhkan berbagai macam aspek. Namun taukah anda jika hanya dengan membayar 1 paket umroh saja anda bisa Berdayakan Desa?</b> <br>Hasanah tours Umroh sekaligus berinfaq</b> <br>Sebuah terobosan layanan dari Hasanah Tours dan Travel dengan memberikan sebuah layanan Umroh sekaligus Berinfaq. Setiap anda mendaftarkan umroh bersama Hasanah otomatis anda berinfaq sebesar 100 ribu melalui Rumah Zakat yang selajutnya dana tersebut akan digunakan untuk program Desa Berdaya.</b> <br>Sampai kapan program ini berjalan?</b> <br>Hasanah telah sepakat untuk melakukan kerjasama dengan Rumah Zakat untuk menjalankan program tersebut selama 6 bulan dimulai sejak tanggal 1 Ramadhan. \r\nKapan lagi anda bisa mennjalankan 2 amal sekaligus dalam 1 waktu, umroh sekaligus berinfaq untuk membantu program Desa Berdaya.\r\n</b> <br>jangan tunda untuk melakukan kebaikan ini.</b>', '', '', '', '2018-04-04 10:23:24', '1');

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `no` int(11) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`no`, `subdomain`, `name`, `link`, `content`, `picture`, `date`, `publish`) VALUES
(3, 'emasjid', 'User / Pengguna E-Masjid\n', 'umroh-serta-mengembangkan-pesantren', 'Menjalankan ibadah umroh sekaligus bisa menjalankan 2 amal dalam 1 waktu adalah kenikmatan ibadah yang sempurna</b>\r\n<br>Bagaimana umroh bisa sekaligus membantu pengembangan pondok pesantren?</b> <br>Mengembangkan pondok pesantren pada umumnya akan menelan biaya yang sangat besar karena butuh berbagai macam material. Namun taukah anda jika hanya dengan membayar 1 paket umroh saja anda bisa mengembangkan pondok pesantren?</b> <br>Hasanah tours Umroh sekaligus berinfaq</b> <br>Sebuah terobosan layanan dari Hasanah Tours dan Travel dengan memberikan sebuah layanan Umroh sekaligus Berinfaq. Setiap anda mendaftarkan umroh bersama Hasanah otomatis anda berinfaq sebesar 100 ribu melalui Rumah Zakat yang selajutnya dana tersebut akan digunakan untuk program pengembangan Pondok Pesantren Baitul Qur’an Sragen.</b> <br>Sampai kapan program ini berjalan?</b> <br>Hasanah telah sepakat untuk melakukan kerjasama dengan Rumah Zakat untuk menjalankan program tersebut selama 6 bulan dimulai sejak tanggal 1 Ramadhan. \r\nKapan lagi anda bisa menjalankan 2 amal sekaligus dalam 1 waktu, umroh sekaligus berinfaq untuk Pengembangan Pondok Pesantren.\r\n</b> <br>jangan tunda untuk melakukan kebaikan ini.</b>', '', '2018-04-04 10:23:24', '1'),
(2, 'emasjid', 'Manajemen Akun / Pengguna E-Masjid\n', 'umroh-sekaligus-berdayakan-desa', 'Menikmati keindahan perjalanan ibadah umroh namun juga tidak lupa untuk tetap berbagi dengan sesama adalah kenikmatan ibadah yang sempurna\n<br>Bagaimana umroh bisa sekaligus mengumrohkan seseorang?\n<br>Mengumrohkan seseorang dalam pikiran orang awam pada umumnya akan menelan biaya yang sangat besar karena menjadi berlipat ganda. Namun taukah anda jika hanya dengan membayar 1 paket umroh saja anda bisa mengumrohkan sesorang?\n<br>Hasanah tours Umroh sekaligus berinfaq\n<br>Sebuah terobosan layanan dari Hasanah Tours dan Travel dengan memberikan sebuah layanan Umroh sekaligus Berinfaq. Setiap anda mendaftarkan umroh bersama Hasanah otomatis anda berinfaq sebesar 100 ribu melalui Rumah Zakat yang selajutnya dana tersebut akan digunakan untuk program Umroh Marbot Masjid.\n<br>Sampai kapan program ini berjalan?\n<br>Hasanah telah sepakat untuk melakukan kerjasama dengan Rumah Zakat untuk menjalankan program tersebut selama 6 bulan dimulai sejak tanggal 1 Ramadhan.\n<br>Kapan lagi anda bisa mennjalankan 2 amal sekaligus dalam 1 waktu, umroh sekaligus berinfaq untuk mengumrohkan para marbot masjid.\n<br>jangan tunda untuk melakukan kebaikan ini.\n', '', '2018-04-04 10:23:24', '1'),
(1, 'emasjid', 'Apa itu PilihanKu', 'apa-itu-pilihanku', 'Menikmati keindahan perjalanan ibadah umroh namun juga tidak lupa untuk tetap berbagi dengan sesama adalah kenikmatan ibadah yang sempurna\n<br>Bagaimana umroh bisa sekaligus mengumrohkan seseorang?\n<br>Mengumrohkan seseorang dalam pikiran orang awam pada umumnya akan menelan biaya yang sangat besar karena menjadi berlipat ganda. Namun taukah anda jika hanya dengan membayar 1 paket umroh saja anda bisa mengumrohkan sesorang?\n<br>Hasanah tours Umroh sekaligus berinfaq\n<br>Sebuah terobosan layanan dari Hasanah Tours dan Travel dengan memberikan sebuah layanan Umroh sekaligus Berinfaq. Setiap anda mendaftarkan umroh bersama Hasanah otomatis anda berinfaq sebesar 100 ribu melalui Rumah Zakat yang selajutnya dana tersebut akan digunakan untuk program Umroh Marbot Masjid.\n<br>Sampai kapan program ini berjalan?\n<br>Hasanah telah sepakat untuk melakukan kerjasama dengan Rumah Zakat untuk menjalankan program tersebut selama 6 bulan dimulai sejak tanggal 1 Ramadhan.\n<br>Kapan lagi anda bisa mennjalankan 2 amal sekaligus dalam 1 waktu, umroh sekaligus berinfaq untuk mengumrohkan para marbot masjid.\n<br>jangan tunda untuk melakukan kebaikan ini.\n', '', '2022-10-26 15:33:37', '1');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `no` int(11) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `domain` varchar(50) NOT NULL,
  `package` enum('free','pro','custom') NOT NULL DEFAULT 'free',
  `username` varchar(50) NOT NULL,
  `referral` varchar(50) NOT NULL,
  `no_partai` int(11) NOT NULL,
  `tingkat` varchar(100) NOT NULL,
  `dapil` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `slogan` varchar(100) NOT NULL,
  `meta_title` varchar(200) NOT NULL,
  `meta_description` varchar(200) NOT NULL,
  `meta_keyword` varchar(200) NOT NULL,
  `meta_tag` text NOT NULL,
  `logo` varchar(200) NOT NULL,
  `banner` varchar(200) NOT NULL,
  `favicon` varchar(200) NOT NULL,
  `footer` varchar(200) NOT NULL,
  `theme` varchar(50) NOT NULL,
  `address` varchar(200) NOT NULL,
  `city` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `instagram` varchar(100) NOT NULL,
  `youtube` varchar(100) NOT NULL,
  `tiktok` varchar(100) NOT NULL,
  `content_welcome` text NOT NULL,
  `content_volunteer` text NOT NULL,
  `content_donation` text NOT NULL,
  `content_cta` text NOT NULL,
  `upgrade` enum('pro','custom','') NOT NULL DEFAULT '',
  `upgrade_value` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`no`, `subdomain`, `domain`, `package`, `username`, `referral`, `no_partai`, `tingkat`, `dapil`, `name`, `link`, `slogan`, `meta_title`, `meta_description`, `meta_keyword`, `meta_tag`, `logo`, `banner`, `favicon`, `footer`, `theme`, `address`, `city`, `phone`, `email`, `facebook`, `twitter`, `instagram`, `youtube`, `tiktok`, `content_welcome`, `content_volunteer`, `content_donation`, `content_cta`, `upgrade`, `upgrade_value`, `date`, `publish`) VALUES
(13, 'kolim', '', 'free', 'umam', '', 1, 'DPRD Kabupaten / Kota', 'Pringsurat, Kranggan, Kaloran', 'Kolim', '', 'Pokoke Joss', '', '', '', '', '188537757281646DSC_0002.jpg', '', 'favicon.png', '2022 - kolim', 'mitech', 'Jalan Apa Saja', 'Temanggung', '085740000146', 'info@pilihanku.id', '', '', '', '', '', '\n    							<p><b>Selamat Datang di Website Kolim</b></p>\n                                <p>Terima kasih sudah terhubung dengan saya melalui website pribadi saya ini. Website ini ditujukan sebagai media untuk saling mengenal dan berinteraksi sebagai bagian dari komitmen saya untuk ikut membangun negeri. </p>\n                                <p>Semoga jalan perjuangan kita semua sebagai bangsa merdeka, diridhoi oleh Tuhan Yang Maha Esa.</p>\n                                <br/>\n                                <p>Salam hangat,</p>\n                                <p>Kolim</p>', '<p>KAMI BUKAN SIAPA-SIAPA TANPA ANDA! </p>\n                                <p>adalah prinsip yang selalu kami junjung tinggi dalam setiap gerakan kami. MENJADI RELAWAN adalah sebuah gerakan solidaritas baru. </p>\n                                <p>Ibarat kanvas yang masih bersih, Indonesia yang baru akan kita lukis kembali. Kami mengajak semua dari KITA untuk menorehkan warna kita masing-masing di atas kanvas Indonesia Baru.</p>\n                                <p>Tunjukkan bahwa Indonesia tidak hanya dibangun diatas warna-warna yang kusam dan membosankan. Indonesia Baru bisa kita lukis dengan warna-warni yang membentuk mozaik indah di garis khatulistiwa. </p>\n                                <p>Setitik kecil yang anda berikan adalah sumbangan besar untuk masa depan Indonesia yang kita cintai.</p>\n                                <br/>\n                                <p>Mari Gabung Menjadi Relawan Gerakan Kami. OK</p>', '<p>Kami menyadari bahwa gerakan kami tidak akan maksimal tanpa bantuan dan partisipasi Anda. \n                                Kami menerima donasi sebagai dukungan Anda terhadap gerakan kami. Anda bisa mengirimkan donasi ke rekening di bawah ini :</p>\n                                <p><b>BANK ABC</b><br>No. Rek. 1234567890<br>A.N. Nama Orang</p>\n                                <p>Setelah melakukan transfer donasi, silahkan konfirmasi pada formulir dibawah/disamping.</p>\n                                <p>Semoga bantuan dan donasi Anda mendapatkan keutamaan yang berlipat dan dibalas oleh Tuhan Yang Maha Esa.</p>', 'Ayo, Dukung dan bergabunglah bersama kami, dan jadikan Indonesia lebih baik !', 'pro', 1000000, '2022-11-09 11:24:10', '1'),
(12, 'subhan', '', 'free', 'umam', '', 1, 'DPR RI', 'Jateng VI', 'Subhan', '', 'Seduluran Saklawase', '', '', '', '', '512246611040553IMG_20180224_203113.jpg', '', 'favicon.png', '2022 - subhan', 'mitech', 'Jalan Apa Saja', 'Temanggung', '085740000146', 'info@pilihanku.id', '', '', '', '', '', '\n    							<p><b>Selamat Datang di Website Subhan</b></p>\n                                <p>Terima kasih sudah terhubung dengan saya melalui website pribadi saya ini. Website ini ditujukan sebagai media untuk saling mengenal dan berinteraksi sebagai bagian dari komitmen saya untuk ikut membangun negeri. </p>\n                                <p>Semoga jalan perjuangan kita semua sebagai bangsa merdeka, diridhoi oleh Tuhan Yang Maha Esa.</p>\n                                <br/>\n                                <p>Salam hangat,</p>\n                                <p>Subhan</p>', '<p>KAMI BUKAN SIAPA-SIAPA TANPA ANDA! </p>\n                                <p>adalah prinsip yang selalu kami junjung tinggi dalam setiap gerakan kami. MENJADI RELAWAN adalah sebuah gerakan solidaritas baru. </p>\n                                <p>Ibarat kanvas yang masih bersih, Indonesia yang baru akan kita lukis kembali. Kami mengajak semua dari KITA untuk menorehkan warna kita masing-masing di atas kanvas Indonesia Baru.</p>\n                                <p>Tunjukkan bahwa Indonesia tidak hanya dibangun diatas warna-warna yang kusam dan membosankan. Indonesia Baru bisa kita lukis dengan warna-warni yang membentuk mozaik indah di garis khatulistiwa. </p>\n                                <p>Setitik kecil yang anda berikan adalah sumbangan besar untuk masa depan Indonesia yang kita cintai.</p>\n                                <br/>\n                                <p>Mari Gabung Menjadi Relawan Gerakan Kami. OK</p>', '<p>Kami menyadari bahwa gerakan kami tidak akan maksimal tanpa bantuan dan partisipasi Anda. \n                                Kami menerima donasi sebagai dukungan Anda terhadap gerakan kami. Anda bisa mengirimkan donasi ke rekening di bawah ini :</p>\n                                <p><b>BANK ABC</b><br>No. Rek. 1234567890<br>A.N. Nama Orang</p>\n                                <p>Setelah melakukan transfer donasi, silahkan konfirmasi pada formulir dibawah/disamping.</p>\n                                <p>Semoga bantuan dan donasi Anda mendapatkan keutamaan yang berlipat dan dibalas oleh Tuhan Yang Maha Esa.</p>', 'Ayo, Dukung dan bergabunglah bersama kami, dan jadikan Indonesia lebih baik !', '', 0, '2022-11-08 15:08:55', '1'),
(10, 'triastuti', '', 'free', 'umam', '', 1, 'DPRD Kabupaten / Kota', 'Pringsurat, Kranggan, Kaloran', 'Tri Astuti', '', 'Seduluran Saklawase', '', '', '', '', '2953621triastuti.jpg', 'banner.png', 'favicon.png', '2022 - triastuti', 'mitech', 'Kebumen II, Kebumen, Pringsurat', 'Temanggung', '085799993240', 'info@pilihanku.id', '', '', '', '', '', '\n    							<p><b>Selamat Datang di Website Tri Astuti</b></p>\n                                <p>Terima kasih sudah terhubung dengan saya melalui website pribadi saya ini. Website ini ditujukan sebagai media untuk saling mengenal dan berinteraksi sebagai bagian dari komitmen saya untuk ikut membangun negeri. </p>\n                                <p>Semoga jalan perjuangan kita semua sebagai bangsa merdeka, diridhoi oleh Tuhan Yang Maha Esa.</p>\n                                <br/>\n                                <p>Salam hangat,</p>\n                                <p>Tri Astuti</p>\n                                <p>Terima Kasih</p>', '<p><b>KAMI BUKAN SIAPA-SIAPA TANPA ANDA! </b></p>\r\n                                <p>adalah prinsip yang selalu kami junjung tinggi dalam setiap gerakan kami. MENJADI RELAWAN adalah sebuah gerakan solidaritas baru. </p>\r\n                                <p>Ibarat kanvas yang masih bersih, Indonesia yang baru akan kita lukis kembali. Kami mengajak semua dari KITA untuk menorehkan warna kita masing-masing di atas kanvas Indonesia Baru.</p>\r\n                                <p>Tunjukkan bahwa Indonesia tidak hanya dibangun diatas warna-warna yang kusam dan membosankan. Indonesia Baru bisa kita lukis dengan warna-warni yang membentuk mozaik indah di garis khatulistiwa. </p>\r\n                                <p>Setitik kecil yang anda berikan adalah sumbangan besar untuk masa depan Indonesia yang kita cintai.</p>\r\n                                <br>\r\n                                <p>Mari Gabung Menjadi Relawan Gerakan Kami. OK</p>', '<p>Kami menyadari bahwa gerakan kami tidak akan maksimal tanpa bantuan dan partisipasi Anda. \r\n                                Kami menerima donasi sebagai dukungan Anda terhadap gerakan kami. Anda bisa mengirimkan donasi ke rekening di bawah ini :</p>\r\n                                <p><b>BANK ABCDEF</b><br>No. Rek. 1234567890<br>A.N. Nama Orang</p>\r\n                                <p>Setelah melakukan transfer donasi, silahkan konfirmasi pada formulir dibawah/disamping.</p>\r\n                                <p>Semoga bantuan dan donasi Anda mendapatkan keutamaan yang berlipat dan dibalas oleh Tuhan Yang Maha Esa.</p>', 'Ayo, Dukung dan bergabunglah bersama kami, dan jadikan Indonesia lebih baik !', '', 0, '2022-11-08 14:51:17', '1'),
(14, 'emasjid', '', 'free', 'umam', '', 1, 'DPR RI', 'Jateng', 'E-Masjid', '', 'Masjid Go Digital', '', '', '', '', 'logoalhuda.jpg', '', 'favicon.png', '2022 - emasjid', 'mitech', 'Jalan Apa Saja', 'Temanggung', '085740000146', 'info@pilihanku.id', '', '', '', '', '', '\n    							<p><b>Selamat Datang di Website E-Masjid</b></p>\n                                <p>Terima kasih sudah terhubung dengan saya melalui website pribadi saya ini. Website ini ditujukan sebagai media untuk saling mengenal dan berinteraksi sebagai bagian dari komitmen saya untuk ikut membangun negeri. </p>\n                                <p>Semoga jalan perjuangan kita semua sebagai bangsa merdeka, diridhoi oleh Tuhan Yang Maha Esa.</p>\n                                <br/>\n                                <p>Salam hangat,</p>\n                                <p>E-Masjid</p>', '<p>KAMI BUKAN SIAPA-SIAPA TANPA ANDA! </p>\n                                <p>adalah prinsip yang selalu kami junjung tinggi dalam setiap gerakan kami. MENJADI RELAWAN adalah sebuah gerakan solidaritas baru. </p>\n                                <p>Ibarat kanvas yang masih bersih, Indonesia yang baru akan kita lukis kembali. Kami mengajak semua dari KITA untuk menorehkan warna kita masing-masing di atas kanvas Indonesia Baru.</p>\n                                <p>Tunjukkan bahwa Indonesia tidak hanya dibangun diatas warna-warna yang kusam dan membosankan. Indonesia Baru bisa kita lukis dengan warna-warni yang membentuk mozaik indah di garis khatulistiwa. </p>\n                                <p>Setitik kecil yang anda berikan adalah sumbangan besar untuk masa depan Indonesia yang kita cintai.</p>\n                                <br/>\n                                <p>Mari Gabung Menjadi Relawan Gerakan Kami. OK</p>', '<p>Kami menyadari bahwa gerakan kami tidak akan maksimal tanpa bantuan dan partisipasi Anda. \n                                Kami menerima donasi sebagai dukungan Anda terhadap gerakan kami. Anda bisa mengirimkan donasi ke rekening di bawah ini :</p>\n                                <p><b>BANK ABC</b><br>No. Rek. 1234567890<br>A.N. Nama Orang</p>\n                                <p>Setelah melakukan transfer donasi, silahkan konfirmasi pada formulir dibawah/disamping.</p>\n                                <p>Semoga bantuan dan donasi Anda mendapatkan keutamaan yang berlipat dan dibalas oleh Tuhan Yang Maha Esa.</p>', 'Ayo, Dukung dan bergabunglah bersama kami, dan jadikan Indonesia lebih baik !', '', 0, '2022-11-09 14:52:56', '1'),
(15, 'sofari', '', 'free', 'safari.achmad@gmail.com', '', 2, 'DPRD Kabupaten / Kota', 'Kedu Kandangan', 'Sofari', '', 'Maju Terus Pantang Mundur', '', '', '', '', '113908974471856INOD.jpg', '', 'favicon.png', '2022 - sofari', 'mitech', 'Jalan Apa Saja No. 100', 'Temanggung', '085729878710', 'info@pilihanku.id', '', '', '', '', '', '\n    							<p><b>Selamat Datang di Website Sofari</b></p>\n                                <p>Terima kasih sudah terhubung dengan saya melalui website pribadi saya ini. Website ini ditujukan sebagai media untuk saling mengenal dan berinteraksi sebagai bagian dari komitmen saya untuk ikut membangun negeri. </p>\n                                <p>Semoga jalan perjuangan kita semua sebagai bangsa merdeka, diridhoi oleh Tuhan Yang Maha Esa.</p>\n                                <br/>\n                                <p>Salam hangat,</p>\n                                <p>Sofari</p>', '<p>KAMI BUKAN SIAPA-SIAPA TANPA ANDA! </p>\n                                <p>adalah prinsip yang selalu kami junjung tinggi dalam setiap gerakan kami. MENJADI RELAWAN adalah sebuah gerakan solidaritas baru. </p>\n                                <p>Ibarat kanvas yang masih bersih, Indonesia yang baru akan kita lukis kembali. Kami mengajak semua dari KITA untuk menorehkan warna kita masing-masing di atas kanvas Indonesia Baru.</p>\n                                <p>Tunjukkan bahwa Indonesia tidak hanya dibangun diatas warna-warna yang kusam dan membosankan. Indonesia Baru bisa kita lukis dengan warna-warni yang membentuk mozaik indah di garis khatulistiwa. </p>\n                                <p>Setitik kecil yang anda berikan adalah sumbangan besar untuk masa depan Indonesia yang kita cintai.</p>\n                                <br/>\n                                <p>Mari Gabung Menjadi Relawan Gerakan Kami. OK</p>', '<p>Kami menyadari bahwa gerakan kami tidak akan maksimal tanpa bantuan dan partisipasi Anda. \n                                Kami menerima donasi sebagai dukungan Anda terhadap gerakan kami. Anda bisa mengirimkan donasi ke rekening di bawah ini :</p>\n                                <p><b>BANK ABC</b><br>No. Rek. 1234567890<br>A.N. Nama Orang</p>\n                                <p>Setelah melakukan transfer donasi, silahkan konfirmasi pada formulir dibawah/disamping.</p>\n                                <p>Semoga bantuan dan donasi Anda mendapatkan keutamaan yang berlipat dan dibalas oleh Tuhan Yang Maha Esa.</p>', 'Ayo, Dukung dan bergabunglah bersama kami, dan jadikan Indonesia lebih baik !', '', 0, '2022-11-15 13:03:08', '1'),
(16, 'agungbm', '', 'free', 'bumiteknoindonesia@gmail.com', '', 1, 'DPRD Provinsi', 'Kota Semarang', 'Agung Budi Margono', '', 'Asik asik Josh', '', '', '', '', '3620206agung-bm.jpeg', '', 'favicon.png', '2022 - agungbm', 'mitech', 'Jalan Tembalang', 'Semarang', '08123456789', 'info@pilihanku.id', '', '', '', '', '', '\n    							<p><b>Selamat Datang di Website Agung Budi Margono</b></p>\n                                <p>Terima kasih sudah terhubung dengan saya melalui website pribadi saya ini. Website ini ditujukan sebagai media untuk saling mengenal dan berinteraksi sebagai bagian dari komitmen saya untuk ikut membangun negeri. </p>\n                                <p>Semoga jalan perjuangan kita semua sebagai bangsa merdeka, diridhoi oleh Tuhan Yang Maha Esa.</p>\n                                <br/>\n                                <p>Salam hangat,</p>\n                                <p>Agung Budi Margono</p>', '<p>KAMI BUKAN SIAPA-SIAPA TANPA ANDA! </p>\n                                <p>adalah prinsip yang selalu kami junjung tinggi dalam setiap gerakan kami. MENJADI RELAWAN adalah sebuah gerakan solidaritas baru. </p>\n                                <p>Ibarat kanvas yang masih bersih, Indonesia yang baru akan kita lukis kembali. Kami mengajak semua dari KITA untuk menorehkan warna kita masing-masing di atas kanvas Indonesia Baru.</p>\n                                <p>Tunjukkan bahwa Indonesia tidak hanya dibangun diatas warna-warna yang kusam dan membosankan. Indonesia Baru bisa kita lukis dengan warna-warni yang membentuk mozaik indah di garis khatulistiwa. </p>\n                                <p>Setitik kecil yang anda berikan adalah sumbangan besar untuk masa depan Indonesia yang kita cintai.</p>\n                                <br/>\n                                <p>Mari Gabung Menjadi Relawan Gerakan Kami. OK</p>', '<p>Kami menyadari bahwa gerakan kami tidak akan maksimal tanpa bantuan dan partisipasi Anda. \n                                Kami menerima donasi sebagai dukungan Anda terhadap gerakan kami. Anda bisa mengirimkan donasi ke rekening di bawah ini :</p>\n                                <p><b>BANK ABC</b><br>No. Rek. 1234567890<br>A.N. Nama Orang</p>\n                                <p>Setelah melakukan transfer donasi, silahkan konfirmasi pada formulir dibawah/disamping.</p>\n                                <p>Semoga bantuan dan donasi Anda mendapatkan keutamaan yang berlipat dan dibalas oleh Tuhan Yang Maha Esa.</p>', 'Ayo, Dukung dan bergabunglah bersama kami, dan jadikan Indonesia lebih baik !', '', 0, '2022-11-16 12:23:55', '1'),
(17, 'subkhan', '', 'free', 'bumiteknoindonesia@gmail.com', '', 1, 'DPR RI', 'Jateng VI', 'Subkhan', '', 'Maju Terus Pantang Mundur', '', '', '', '', '72945831subkhan.jpeg', '', 'favicon.png', '2022 - subkhan', 'mitech', 'Jalan Semarang', 'Semarang', '08123456789', 'bumiteknoindonesia@gmail.com', '', '', '', '', '', '\n    							<p><b>Selamat Datang di Website Subkhan</b></p>\n                                <p>Terima kasih sudah terhubung dengan saya melalui website pribadi saya ini. Website ini ditujukan sebagai media untuk saling mengenal dan berinteraksi sebagai bagian dari komitmen saya untuk ikut membangun negeri. </p>\n                                <p>Semoga jalan perjuangan kita semua sebagai bangsa merdeka, diridhoi oleh Tuhan Yang Maha Esa.</p>\n                                <br/>\n                                <p>Salam hangat,</p>\n                                <p>Subkhan</p>', '<p>KAMI BUKAN SIAPA-SIAPA TANPA ANDA! </p>\n                                <p>adalah prinsip yang selalu kami junjung tinggi dalam setiap gerakan kami. MENJADI RELAWAN adalah sebuah gerakan solidaritas baru. </p>\n                                <p>Ibarat kanvas yang masih bersih, Indonesia yang baru akan kita lukis kembali. Kami mengajak semua dari KITA untuk menorehkan warna kita masing-masing di atas kanvas Indonesia Baru.</p>\n                                <p>Tunjukkan bahwa Indonesia tidak hanya dibangun diatas warna-warna yang kusam dan membosankan. Indonesia Baru bisa kita lukis dengan warna-warni yang membentuk mozaik indah di garis khatulistiwa. </p>\n                                <p>Setitik kecil yang anda berikan adalah sumbangan besar untuk masa depan Indonesia yang kita cintai.</p>\n                                <br/>\n                                <p>Mari Gabung Menjadi Relawan Gerakan Kami. OK</p>', '<p>Kami menyadari bahwa gerakan kami tidak akan maksimal tanpa bantuan dan partisipasi Anda. \n                                Kami menerima donasi sebagai dukungan Anda terhadap gerakan kami. Anda bisa mengirimkan donasi ke rekening di bawah ini :</p>\n                                <p><b>BANK ABC</b><br>No. Rek. 1234567890<br>A.N. Nama Orang</p>\n                                <p>Setelah melakukan transfer donasi, silahkan konfirmasi pada formulir dibawah/disamping.</p>\n                                <p>Semoga bantuan dan donasi Anda mendapatkan keutamaan yang berlipat dan dibalas oleh Tuhan Yang Maha Esa.</p>', 'Ayo, Dukung dan bergabunglah bersama kami, dan jadikan Indonesia lebih baik !', '', 0, '2022-11-16 12:33:17', '1');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `no` int(2) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(100) NOT NULL,
  `url_text` varchar(100) NOT NULL,
  `number` int(11) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`no`, `subdomain`, `name`, `link`, `description`, `url`, `url_text`, `number`, `picture`, `date`, `publish`) VALUES
(12, 'demo', 'haji1', 'haji1', 'haji1<br>', 'https://www.google.com/apakah', 'Coba', 1, 'ss_1.jpg', '0000-00-00 00:00:00', '1'),
(13, 'demo', 'haji2', 'haji2', '', '', '', 0, 'ss_2.jpg', '0000-00-00 00:00:00', '1'),
(17, 'triastuti', 'Hallo', '', 'Hallo', 'google.com', 'Hallo', 1, '29860533haji1.jpg', '2022-11-11 22:27:36', '1');

-- --------------------------------------------------------

--
-- Table structure for table `takmir`
--

CREATE TABLE `takmir` (
  `no` int(11) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(50) NOT NULL,
  `no_takmir_jabatan` int(11) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `takmir`
--

INSERT INTO `takmir` (`no`, `subdomain`, `name`, `link`, `no_takmir_jabatan`, `address`, `phone`, `email`, `picture`, `date`, `publish`) VALUES
(8, 'emasjid', 'Mujiyono, S.Pd.', '', 2, 'Jalan Apa Saja No. 1, Temanggung', '08145678967898', 'mujiyono@gmail.com', '899219625254259558327475Lekat1.jpg', '2022-11-10 11:44:33', '1'),
(4, 'demo', 'Askana Sakhi Syauqia', '', 0, 'jalan Apa Saja', '08126351723', 'askana@gmail.com', '', '2022-11-02 11:21:56', '1'),
(3, 'demo', 'Tri Astuti', '', 0, 'jalan Apa Saja', '02439213814', 'nanas@gmail.com', '', '2022-11-02 11:20:26', '1'),
(5, 'demo', '', '', 0, '', '', '', '5146141010916642IMG-20180501-WA0006.jpg', '2022-11-02 11:22:04', '1'),
(6, 'demo', 'Subhan', '', 0, 'Jalan Apa Saja', '0812346587', 'subhan@gmail.com', '689310998190IMG-20180102-WA0005.jpg', '2022-11-02 14:06:45', '1'),
(7, 'emasjid', 'Khoerul Umam', '', 1, 'Jl. Apa Saja No. 10, Kebumen, Pringsurat, Temanggung', '08234567890', 'mashoerul@gmail.com', '397117944444540421852908FB_IMG_1525257954640.jpg', '2022-11-10 11:36:49', '1');

-- --------------------------------------------------------

--
-- Table structure for table `takmir_jabatan`
--

CREATE TABLE `takmir_jabatan` (
  `no` int(2) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `takmir_jabatan`
--

INSERT INTO `takmir_jabatan` (`no`, `name`, `link`, `date`, `publish`) VALUES
(1, 'Ketua', '', '2022-11-07 16:04:26', '1'),
(2, 'Penasihat', '', '2022-11-10 11:34:07', '1');

-- --------------------------------------------------------

--
-- Table structure for table `theme`
--

CREATE TABLE `theme` (
  `no` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `theme` varchar(50) NOT NULL,
  `date` datetime NOT NULL,
  `picture` varchar(200) NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `theme`
--

INSERT INTO `theme` (`no`, `name`, `link`, `theme`, `date`, `picture`, `publish`) VALUES
(1, 'White', 'white', 'white', '2015-10-20 00:00:00', '', '1'),
(2, 'Blue', 'blue', 'blue', '2015-10-21 00:00:00', '', '1'),
(3, 'Full Banner', 'fullbanner', 'fullbanner', '2015-11-02 00:00:00', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `no` int(2) NOT NULL,
  `no_product` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `referral` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `value` int(11) NOT NULL,
  `status` enum('unpaid','confirm','paid') NOT NULL DEFAULT 'unpaid',
  `note` text NOT NULL,
  `date_transfer` date NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`no`, `no_product`, `name`, `link`, `username`, `referral`, `phone`, `email`, `value`, `status`, `note`, `date_transfer`, `date`, `publish`) VALUES
(6, 1, 'Khoerul Umam', '', 'umam', 'umam', '081390957111', 'maskhoerul86@gmail.com', 100000, 'unpaid', 'Coba Saja', '0000-00-00', '2022-11-11 21:40:03', '1'),
(5, 2, 'Khoerul Umam', 'khoerul-umam', 'umam', 'umam', '081390957111', 'maskhoerul@gmail.com', 100000, 'confirm', 'Coba-coba saja', '2022-11-10', '2022-11-10 16:42:43', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `no` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `province` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `category` enum('super','admin') NOT NULL DEFAULT 'admin',
  `last_login` datetime NOT NULL,
  `last_ipaddress` varchar(20) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`no`, `name`, `link`, `address`, `city`, `province`, `phone`, `email`, `username`, `password`, `category`, `last_login`, `last_ipaddress`, `picture`, `date`, `publish`) VALUES
(1, 'SuperAdmin, ST', 'superadmin', 'Jalan Ganesha No. 10', 'Temanggung', 'Jawa Tengah', '081298123312', 'maskhoerul@gmail.com', 'mysuper', 'e10adc3949ba59abbe56e057f20f883e', 'super', '2022-11-11 21:43:36', '180.253.130.102', '360756154471856INOD.jpg', '2022-11-07 04:28:18', '1'),
(2, 'Admin Pusat', '', 'Jalan Apa Saja', 'Temanggung', 'Jawa Tengah', '08123456789', 'maskhoerul@gmail.com', 'adminpusat', 'e10adc3949ba59abbe56e057f20f883e', 'admin', '0000-00-00 00:00:00', '', '', '2022-11-07 15:53:48', '0');

-- --------------------------------------------------------

--
-- Table structure for table `volunteer`
--

CREATE TABLE `volunteer` (
  `no` int(11) NOT NULL,
  `subdomain` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `link` varchar(50) NOT NULL,
  `address` varchar(200) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `motivation` varchar(200) NOT NULL,
  `contribution` varchar(200) NOT NULL,
  `status` enum('tolak','terima','') NOT NULL DEFAULT '',
  `picture` varchar(200) NOT NULL,
  `date` datetime NOT NULL,
  `publish` enum('1','0') DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `volunteer`
--

INSERT INTO `volunteer` (`no`, `subdomain`, `name`, `link`, `address`, `phone`, `email`, `motivation`, `contribution`, `status`, `picture`, `date`, `publish`) VALUES
(1, 'demo', 'Ahmad', 'ahmad', 'Jalan Apa Saja No. 10', '081390957111', 'maskhoerul@gmail.com', 'Pengen Masuk Surga', '', '', '', '0000-00-00 00:00:00', '1'),
(4, 'demo', 'Askana Sakhi Syauqia', '', 'jalan Apa Saja', '08126351723', 'askana@gmail.com', 'COba ', 'Pengen AJa', '', '', '2022-11-02 11:21:56', '1'),
(3, 'demo', 'Tri Astuti', '', 'jalan Apa Saja', '02439213814', 'nanas@gmail.com', 'Pengen Coba Saja', 'Belum tau', '', '', '2022-11-02 11:20:26', '1'),
(5, 'demo', '', '', '', '', '', '', '', '', '5146141010916642IMG-20180501-WA0006.jpg', '2022-11-02 11:22:04', '1'),
(6, 'demo', 'Subhan', '', 'Jalan Apa Saja', '0812346587', 'subhan@gmail.com', 'Pengen Coba Saja', 'Duit Aja', '', '689310998190IMG-20180102-WA0005.jpg', '2022-11-02 14:06:45', '1'),
(7, 'triastuti', 'Afkar Naufal', '', 'Jalan Apa Saja No. 100', '081390957111', 'maskhoerul86@gmail.com', 'Pengen Coba', 'Mbuh Lah', '', '', '2022-11-11 21:51:52', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `blog_category`
--
ALTER TABLE `blog_category`
  ADD UNIQUE KEY `no` (`no`);

--
-- Indexes for table `blog_comment`
--
ALTER TABLE `blog_comment`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `circulation`
--
ALTER TABLE `circulation`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `commission`
--
ALTER TABLE `commission`
  ADD PRIMARY KEY (`no`),
  ADD KEY `idmember` (`username`);

--
-- Indexes for table `cv`
--
ALTER TABLE `cv`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `discussion`
--
ALTER TABLE `discussion`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `donation`
--
ALTER TABLE `donation`
  ADD UNIQUE KEY `no` (`no`);

--
-- Indexes for table `ebook`
--
ALTER TABLE `ebook`
  ADD PRIMARY KEY (`no`),
  ADD UNIQUE KEY `no` (`no`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `event_routine`
--
ALTER TABLE `event_routine`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `evideo`
--
ALTER TABLE `evideo`
  ADD UNIQUE KEY `no` (`no`);

--
-- Indexes for table `finance_account`
--
ALTER TABLE `finance_account`
  ADD UNIQUE KEY `no` (`no`);

--
-- Indexes for table `flyer`
--
ALTER TABLE `flyer`
  ADD UNIQUE KEY `no` (`no`);

--
-- Indexes for table `flyer_theme`
--
ALTER TABLE `flyer_theme`
  ADD UNIQUE KEY `no` (`no`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD UNIQUE KEY `no` (`no`);

--
-- Indexes for table `help`
--
ALTER TABLE `help`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `help_category`
--
ALTER TABLE `help_category`
  ADD UNIQUE KEY `no` (`no`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `jamaah`
--
ALTER TABLE `jamaah`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `jamaah_category`
--
ALTER TABLE `jamaah_category`
  ADD UNIQUE KEY `no` (`no`);

--
-- Indexes for table `masjid`
--
ALTER TABLE `masjid`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `member_reset`
--
ALTER TABLE `member_reset`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`no`),
  ADD UNIQUE KEY `no` (`no`);

--
-- Indexes for table `partai`
--
ALTER TABLE `partai`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`no`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD UNIQUE KEY `no` (`no`);

--
-- Indexes for table `takmir`
--
ALTER TABLE `takmir`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `takmir_jabatan`
--
ALTER TABLE `takmir_jabatan`
  ADD UNIQUE KEY `no` (`no`);

--
-- Indexes for table `theme`
--
ALTER TABLE `theme`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD UNIQUE KEY `no` (`no`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `volunteer`
--
ALTER TABLE `volunteer`
  ADD PRIMARY KEY (`no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog_category`
--
ALTER TABLE `blog_category`
  MODIFY `no` int(2) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog_comment`
--
ALTER TABLE `blog_comment`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `circulation`
--
ALTER TABLE `circulation`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `commission`
--
ALTER TABLE `commission`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `cv`
--
ALTER TABLE `cv`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `discussion`
--
ALTER TABLE `discussion`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `donation`
--
ALTER TABLE `donation`
  MODIFY `no` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ebook`
--
ALTER TABLE `ebook`
  MODIFY `no` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `event_routine`
--
ALTER TABLE `event_routine`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `evideo`
--
ALTER TABLE `evideo`
  MODIFY `no` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `finance_account`
--
ALTER TABLE `finance_account`
  MODIFY `no` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `flyer`
--
ALTER TABLE `flyer`
  MODIFY `no` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `flyer_theme`
--
ALTER TABLE `flyer_theme`
  MODIFY `no` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `no` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `help`
--
ALTER TABLE `help`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `help_category`
--
ALTER TABLE `help_category`
  MODIFY `no` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jamaah`
--
ALTER TABLE `jamaah`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jamaah_category`
--
ALTER TABLE `jamaah_category`
  MODIFY `no` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `masjid`
--
ALTER TABLE `masjid`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `member_reset`
--
ALTER TABLE `member_reset`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `no` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `partai`
--
ALTER TABLE `partai`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=227;

--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `no` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `takmir`
--
ALTER TABLE `takmir`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `takmir_jabatan`
--
ALTER TABLE `takmir_jabatan`
  MODIFY `no` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `theme`
--
ALTER TABLE `theme`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `no` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `volunteer`
--
ALTER TABLE `volunteer`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
