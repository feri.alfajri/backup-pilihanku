<?php 
if ($access == 'super') { 
	
	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
	
	include DIR_MODULE.'volunteer/super-volunteer.php';    
	
}
elseif ($access == 'admin') { 
	
	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
	
	include DIR_MODULE.'volunteer/admin-volunteer.php';    
    
}
else {
    
	if (empty($_POST['process'])) { }
	elseif ($_POST['process']=='save') { 
		$name=strip_tags($_POST['name']);$name=str_replace('"','',$name);$name=str_replace("'","",$name);
		$address=strip_tags($_POST['address']);	 $address=str_replace('"','',$address);$address=str_replace("'","",$address);
		$phone=strip_tags($_POST['phone']); $phone=str_replace('"','',$phone);$phone=str_replace("'","",$phone);$phone=str_replace(" ","",$phone); 
		$email=strip_tags($_POST['email']); $email=str_replace('"','',$email);$email=str_replace("'","",$email);$email=str_replace(" ","",$email); 
		$motivation=strip_tags($_POST['motivation']); $motivation=str_replace('"','',$motivation);$motivation=str_replace("'","",$motivation);
		$contribution=strip_tags($_POST['contribution']); $contribution=str_replace('"','',$contribution);$contribution=str_replace("'","",$contribution);
		$provinsi=strip_tags($_POST['provinsi']);$provinsi=str_replace('"','',$provinsi);$provinsi=str_replace("'","",$provinsi);
		$kota=strip_tags($_POST['kota']);$kota=str_replace('"','',$kota);$kota=str_replace("'","",$kota);
		$kecamatan=strip_tags($_POST['kecamatan']);$kecamatan=str_replace('"','',$kecamatan);$kecamatan=str_replace("'","",$kecamatan);
		$desa=strip_tags($_POST['desa']);$desa=str_replace('"','',$desa);$desa=str_replace("'","",$desa);

		$query = mysqli_query($model->koneksi,"SELECT no FROM volunteer WHERE (phone='$phone' OR email='$email') AND subdomain='".SUBDOMAIN."' ");
		$jumlah = mysqli_num_rows($query);

		if ($jumlah >= 1)  {  
			$notif = 'Mohon maaf, Anda sudah terdaftar sebelumnya.';
			$alert = 'alert-danger';			
		}
		else {
			mysqli_query($model->koneksi,"INSERT INTO volunteer (subdomain,name,address,phone,email,motivation,contribution,date,no_provinsi,no_kota,no_kecamatan,no_desa) 
				VALUES ('".SUBDOMAIN."','$name','$address','$phone','$email','$motivation','$contribution',sysdate(),'$provinsi','$kota','$kecamatan','$desa')"); 
			$notif = 'Selamat, Pendaftaran Anda sebagai relawan telah masuk ke sistem kami.<br/>Kami akan segera menghubungi Anda.';
			$alert = 'alert-success';
		}
	}
	
	include DIR_MODEL.'province.php';
	
	$modelProvinsi = new province;
    $dataProvinsi = $modelProvinsi->getDataAll();
	
    include DIR_MODULE.'volunteer/subdomain.php';            
    
}
?>