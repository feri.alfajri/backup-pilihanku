<?php 
if ($access == 'super') { 
	
	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
	
	if($act == 'profile'){
        
		$titleweb = 'Ubah Profil';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/super-profile.php';
			
    }
    elseif($act == 'password'){
        
		include(DIR_LIBRARY."admin/password.php");
		$password = new password();
	
		$titleweb = 'Ubah Password';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/super-password.php';
		
    }
	elseif($act == 'avatar'){
        
		$titleweb = 'Ubah Foto Profil';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/super-photo.php';
			
    }
    else{
		$titleweb = 'Dashboard';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/super-dashboard.php';
        
    }
    
}
elseif ($access == 'admin') { 
	
	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
	
	if($act == 'profile'){
        
		$titlepage = 'Ubah Profil';
		include DIR_MODULE.'account/admin-profile.php';
			
    }
    elseif($act == 'password'){
        
		include(DIR_LIBRARY."admin/password.php");
		$password = new password();
	
		$titlepage = 'Ubah Password';
		include DIR_MODULE.'account/admin-password.php';
		
    }
	elseif($act == 'avatar'){
        
		$titlepage = 'Ubah Foto Profil';
		include DIR_MODULE.'account/admin-photo.php';
			
    }
    else{
        
		$titlepage = 'Dashboard';
		include DIR_MODULE.'account/admin-dashboard.php';
        
    }
    
}
elseif ($access == 'member') { 
	
	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
	
	
    if($act == 'profile'){
        
		$titleweb = 'Ubah Profil';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/member-profile.php';
			
    }
    elseif($act == 'password'){
        
		include(DIR_LIBRARY."admin/password.php");
		$password=new password();
		
		$titleweb = 'Ubah Password';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/member-password.php';
		
    }
	elseif($act == 'avatar'){
        
		$titleweb = 'Ubah Avatar';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/member-photo.php';
		
    }
    else{
        
        $titleweb = 'Dashboard';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/member-dashboard.php';
		
    }
	
}
else {
    
	
	if(!isset($_POST['reg'])){
		$_SESSION['kode']=rand(72352732,237263782);
	}
	
	if(!isset($_POST['losspass'])){
		$_SESSION['kode']=rand(72352732,237263782);
	}
	
	if(isset($_POST['reg'])){
		if(!empty($_SESSION['kode'])){
			$name=strip_tags($_POST['name']);$name=str_replace('"','',$name);$name=str_replace("'","",$name);			
			$phone=strip_tags($_POST['phone']); $phone=str_replace('"','',$phone);$phone=str_replace("'","",$phone);$phone=str_replace(" ","",$phone); 
			$email=strip_tags($_POST['email']); $email=str_replace('"','',$email);$email=str_replace("'","",$email);$email=str_replace(" ","",$email); 
			$password=strip_tags($_POST['password']); $password=str_replace('"','',$password);$password=str_replace("'","",$password); $password=str_replace(" ","",$password); 
			$pass=md5($password);
			$query = mysqli_query($model->koneksi,"SELECT no FROM member WHERE phone='$phone' OR email='$email' OR username='$email' ");
			$jumlah = mysqli_num_rows($query);
			if ($jumlah >= 1)  {  
				$notif = 'Mohon maaf, Anda sudah terdaftar sebelumnya.';
				$alert = 'alert-danger';			
			}
			else {
			    //AUTOLOGIN
			    $_SESSION['uname']=$email;
				$_SESSION['name']=$name;
				$_SESSION['pword']=$pass; 
				$_SESSION['pict']=''; 
				$_SESSION['cat']='member'; 
				$ipaddress=$_SERVER['REMOTE_ADDR']; 
			    //INPUT DATABASE
				mysqli_query($model->koneksi,"INSERT INTO member (name,phone,email,username,password,category,date,publish) 
					VALUES ('$name','$phone','$email','$email','$pass','member',sysdate(),'1')"); 
				//UPDATE LAST LOGIN
				mysqli_query($model->koneksi, "UPDATE member SET last_login=sysdate(), last_ipaddress='$ipaddress' WHERE username='$email'");
				//INPUT NOTIFIKASI
				$notifContent = '<p>Salam, '.$name.'</p>'.
				    '<p>Selamat datang di PilihanKu.net</p>'.
				    '<p>Pendaftaran Anda sebagai anggota telah masuk ke sistem kami.'.
				    '<br/>Anda dapat menggunakan layanan dengan menekan tombol menu yang ada di sebelah kiri halaman ini.'.
				    '<br/>Informasi lebih lanjut mengenai layanan kami, silahkan menghubungi Customer Service.</p>'.
				    '<p>Semoga kesuksesan senantiasa menyertai kita semua.</p>'.
				    '<p>Best Regards<br/>PilihanKu.net</p>';
				mysqli_query($model->koneksi,"INSERT INTO notification (username,name,content,date,publish)  VALUES ('$email','Selamat Datang di PilihanKu.net','$notifContent',sysdate(),'1')"); 
				//KIRIM EMAIL
				include  (DIR_LIBRARY.'class.phpmailer.php');
				date_default_timezone_set('Asia/Jakarta');
				$body='<html>' .
					'<head></head>' .
					'<body>' .
					'<label><img src="'.URL_IMAGE.$logo.'" alt="logo" width="250"></label><br/><br>				
					<p>Salam, '.$name.'</p>
					<p>Selamat datang di PilihanKu.net.</p>	
					<p>Pendaftaran Anda sebagai anggota telah masuk ke sistem kami. Berikut ini adalah data-data pendaftaran Anda :</p>			
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr><td align=left width=140>Nama Lengkap</td><td align=left>: &nbsp; '.$name.'</td></tr>
						<tr><td align=left>Username / Email</td><td align=left >: &nbsp; '.$email.'</td></tr>
						<tr><td align=left>Password</td><td align=left >: &nbsp; '.$password.'</td></tr>
						<tr><td align=left>Telepon</td><td align=left>: &nbsp; '.$phone.'</td></tr>
					</table>
					<p>Anda dapat login ke member area dengan menggunakan username dan password diatas.</p>
					<p>Informasi lebih lanjut mengenai layanan kami, silahkan menghubungi Customer Service. Semoga kesuksesan senantiasa menyertai kita semua.</p>
					<p>Best Regards</p>
					<p>PilihanKu.net</p>
					 </body>' .
					'</html>'; 
				$mail=new PHPMailer();
				$mail->Subject="Pendaftaran PilihanKu.net";
				$mail->MsgHTML($body);
				$mail->addReplyTo(ASAL_EMAIL,"PilihanKu.net");
				$mail->SetFrom(ASAL_EMAIL, "PilihanKu.net");
				$mail->AddAddress($email, $name);
				unset($_SSESSION['kode']);
				if(!$mail->Send()) {  echo "Mailer Error: " . $mail->ErrorInfo; } 
				header('lcation:'.URL_DOMAIN.'member');
			}
		}
	}
    
    
    if(isset($_POST['losspass'])){
		if(!empty($_SESSION['kode'])){
			$email=strip_tags($_POST['email']); $email=str_replace('"','',$email);$email=str_replace("'","",$email);$email=str_replace(" ","",$email); 
			$query = mysqli_query($model->koneksi,"SELECT no,name,username FROM member WHERE email='$email' ");
			$jumlah = mysqli_num_rows($query);
			if ($jumlah == 0)  {  
				?>
                <script type="text/javascript">alert('Username atau Email Tidak Terdaftar. Silahkan cek lagi!');</script>
                <?php 		
			}
			else {
			    $data = mysqli_fetch_array($query);
			    $memberId = $data['no'];
                $name = $data['name'];
                $memberUsername = $data['username'];
                $sqlRequest = "SELECT no, token, date_expired FROM member_reset WHERE publish = '1' AND username = '$memberUsername' AND date_expired > NOW() LIMIT 1";
                $qRequest = mysqli_query($model->koneksi, $sqlRequest);
                $jRequest = mysqli_num_rows($qRequest);
                if ($jRequest > 0) {
                    $dRequest = mysqli_fetch_assoc($qRequest);
                    $token = $dRequest['token'];
                    $expiredDate = $dRequest['date_expired'];
                    $expiredDate = date('d M Y H:i', strtotime($expiredDate));
                } 
                else {
                    include_once DIR_LIBRARY.'random-string.php';
                    $randomString = new RandomString();
                    $currentTime = date('Y-m-d H:i:s');
                    $halfAnHourFromCurrent = date('Y-m-d H:i:s', strtotime("+30 minutes", strtotime($currentTime)));
                    $randCode = $randomString->generateRandomString(8);
                    $payload = array('member_id' => $memberUsername, 'token' => $randCode);
                    $payload = json_encode($payload);
                    $rawToken = base64_encode($payload);
                    $token = str_replace(
                        array('+', '/', '='),
                        array('-', '_', ''),
                        $rawToken
                    );
                    $expiredDate = date('d M Y H:i', strtotime($halfAnHourFromCurrent));
                    $sqlInsert = "INSERT INTO member_reset SET publish = '1', username = '$memberUsername', token = '$token', date = '$currentTime', date_created = '$currentTime', date_expired = '$halfAnHourFromCurrent'";
                    mysqli_query($model->koneksi, $sqlInsert);
                }
                $resetUrl = URL_DOMAIN.'account/createpassword?q='.$token;
				//KIRIM EMAIL
				include  (DIR_LIBRARY.'class.phpmailer.php');
				$body='<html>' .
					'<head></head>' .
					'<body>' .
					'<label><img src="'.URL_IMAGE.$logo.'" alt="logo" width="250"></label><br/><br>				
					<p>Salam, '.$name.'</p>
					<p>Selamat datang di PilihanKu.net.</p>	
					<p>Kami menerima laporan bahwa Anda meminta perubahan password melalui sistem kami.</p>	
					<p>&nbsp;</p>
					<p>Untuk melakukan reset, silahkan berikut ini :</p>
					<p><span style="border:1px solid #DADADA;background:#CCC;padding:10px;"><a href="'.$resetUrl.'">KLIK DISINI</a></span></p>
					<p>Link di atas akan valid hingga '.$expiredDate.'.</p>
					<p>Pastikan Anda menggunakan sesaat masih valid.</p>
					<p>&nbsp;</p>
					<p>Informasi lebih lanjut mengenai layanan kami, silahkan menghubungi Customer Service. Semoga kesuksesan senantiasa menyertai kita semua.</p>
					<p>Best Regards</p>
					<p>PilihanKu.net</p>
					 </body>' .
					'</html>'; 
				date_default_timezone_set('Asia/Jakarta');
				$mail=new PHPMailer();
				$mail->Subject="Reset Password PilihanKu.net";
				$mail->MsgHTML($body);
				$mail->addReplyTo(ASAL_EMAIL,"PilihanKu.net");
				$mail->SetFrom(ASAL_EMAIL, "PilihanKu.net");
				$mail->AddAddress($email, $name);
				unset($_SSESSION['kode']);
				if(!$mail->Send()) {  echo "Mailer Error: " . $mail->ErrorInfo; } 
				?>
                <script type="text/javascript">alert('Silahkan cek email Anda. Dan ikuti pentunjuk berikutnya!');</script>
                <?php
			}
		}
	}
    

    if($link == 'register'){
        
		$titleweb = 'Register';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/public-register.php';
			
    }
    elseif($link == 'losspassword'){
        
		$titleweb = 'Lupa Password';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/public-losspassword.php';
		
    }
    elseif($link == 'createpassword'){
        
		$titleweb = 'Buat Password';
		$metadesc = $titleweb;
		include DIR_MODULE.'account/public-createpassword.php';
		
    }
    elseif($link == 'autologin'){
        
		
		include DIR_LIBRARY.'uri.php';
        $uri = new uri;
        echo $uname = $uri->get('u');
        echo $pword = $uri->get('p');
        
		include DIR_MODULE.'account/member-login.php';
		
    }
	else {
		
		
    
		include DIR_MODULE.'account/member-login.php';
		
    }
    
}
?>