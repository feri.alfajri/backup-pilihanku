<?php 
if ($access == 'super') { 
	
	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
	
	include DIR_MODULE.'gallery/super-gallery.php';
    
}
elseif ($access == 'admin') { 
	
	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
	
	include DIR_MODULE.'gallery/admin-gallery.php';
    
}
else {
    
	include DIR_MODEL.'gallery.php';
	
    $galleryModel = new gallery; 
    
    $limit = 1000;
    if ($page == "") { $posisi = 0; $page = 1; } else { $posisi = ($page-1)*$limit; }
    $galleryData = $galleryModel -> getDataAll($limit,$posisi);
	$count = $galleryModel -> count;
    
    include DIR_MODULE.'gallery/subdomain.php';
    
}
?>