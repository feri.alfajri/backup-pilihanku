<?php 
if ($access == 'super') { 
	
	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
	
	include DIR_MODULE.'profile/super-profile.php';
    
}
elseif ($access == 'admin') { 
	
	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
	
	include DIR_MODULE.'cv/admin-cv.php';
	
    
}
else {
    
    include DIR_MODEL.'cv.php';
	
    $cvModel 		= new cv; 
	$cvDetail 		= $cvModel -> getData();
	$count 			= $cvModel -> count;
	
	$name			= $cvDetail['name'];
	$birth			= $cvDetail['birth'];
	$address		= $cvDetail['address'];
	$phone			= $cvDetail['phone'];
	$education		= $cvDetail['education'];
	$organization	= $cvDetail['organization'];
	$training		= $cvDetail['training'];
	$work			= $cvDetail['work'];
	$award			= $cvDetail['award'];
	$profile		= $cvDetail['profile'];
	$picture		= $cvDetail['picture'];

    include DIR_MODULE.'profile/subdomain.php';
    
}
?>