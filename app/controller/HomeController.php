<?php
if ($access=='super') { 

	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
    
    include DIR_MODULE.'home/super-home.php';		
	
}
elseif ($access=='admin') { 

	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
    
    include DIR_MODULE.'home/admin-home.php';		
	
}
else {
	
	if ($access=='subdomain'){
		
		include DIR_MODEL.'cv.php';
		include DIR_MODEL.'gallery.php';
		include DIR_MODEL.'blog.php';
		$blogModel = new blog; 
	
		$cvModel 		= new cv; 		
		$galleryModel 	= new gallery; 
		$blogModel 		= new blog; 
		
		$cvDetail 		= $cvModel -> getData();
		$cvName			= $cvDetail['name'];
		$cvProfile		= $cvDetail['profile'];
		$cvPicture		= $cvDetail['picture'];
		
		$galleryData = $galleryModel -> getDataAll(8,0);
		$galleryCount = $galleryModel -> count;
		
		$blogData = $blogModel -> getDataAll(3,0);
		$blogCount = $blogModel -> count;
		
		include DIR_MODULE.'home/subdomain.php';
	
	}
	else {
		
		include DIR_MODULE.'home/public.php';
		
	}

}
?>