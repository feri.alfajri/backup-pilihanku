<?php
if ($access == 'super') {  

    include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
    
    if ($link == 'menu') {		
        include DIR_MODULE.'text/super-menu.php';		
    }
    else {		
        include DIR_MODULE.'text/super-error.php';		
    }

} 
if ($access == 'admin') {  

    include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
    
    if ($link == 'menu') {		
        include DIR_MODULE.'text/admin-menu.php';		
    }
    else {		
        include DIR_MODULE.'text/admin-error.php';		
    }

} 
elseif ($access == 'member') {
	
	include DIR_MODULE.'text/member-error.php';		
	
}
elseif ($access == 'subdomain') {
	
	include DIR_MODULE.'text/subdomain-error.php';		
	
}
else {
	
    include DIR_MODEL.'menu.php';
    $menuclass = new menu();
	$menuclass -> check($model -> menu);
	$jmenu = $menuclass -> jmenu;	
	
    if ($jmenu == 0) {		
        include DIR_MODULE.'text/public-error.php';		
    }
    else {
		
		$menuclass -> detail($model -> menu);
		$nametext = $menuclass -> namemenu;
		$linktext = $menuclass -> linkmenu;
		$contenttext = $menuclass -> contentmenu;
		$picturetext = $menuclass -> picturemenu;
		$datetext = $menuclass -> datemenu; 
		
		$titleweb = $menuclass -> namemenu;
		$metadesc = $menuclass -> metadescriptionmenu;
		
        include DIR_MODULE.'text/public.php';		
    }
	
}
?>