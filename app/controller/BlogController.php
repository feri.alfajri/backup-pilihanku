<?php 
if ($access == 'super') { 
	
	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
    
    if ($link == 'article') {		
        include DIR_MODULE.'blog/super-article.php';		
    }
	elseif ($link == 'blog_category') {		
        include DIR_MODULE.'blog/super-blog-category.php';		
    }
	elseif ($link == 'blog_comment') {		
        include DIR_MODULE.'blog/super-blog-comment.php';		
    }
    else {		
        include DIR_MODULE.'blog/super-blog.php';		
    }
    
}
elseif ($access == 'admin') { 
	
	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
	
	include DIR_MODULE.'blog/admin-blog.php';
    
}
else {
    
    include DIR_MODEL.'blog.php';
	include DIR_LIBRARY.'uri.php';
	
    $blogModel = new blog; 
    $uri = new uri;
    
    //TERBARU
    $limit = 10;
    if ($page == "") { $posisi = 0; $page = 1; } else { $posisi = ($page-1)*$limit; }
    $blogDataLast = $blogModel -> getDataAll($limit,$posisi);
    $count = $blogModel -> count;
	//KATA KUNCI
    $key = $uri -> get('key');
    
    if($no == ''){
		
		if(!empty($key)){
            //PENCARIAN
            $limit = 10;
            if ($page == "") { $posisi = 0; $page = 1; } else { $posisi = ($page-1)*$limit; }
            $blogData = $blogModel -> getDataSearch($key,$posisi,$limit);
            $count = $blogModel -> count;
            $keybaru = str_replace('-',' ',$key);
            $titleweb = $keybaru;
            $metaurl = '';
            $metapict = '';
        }
        else { 
			$limit = 10;
			if ($page == "") { $posisi = 0; $page = 1; } else { $posisi = ($page-1)*$limit; }
			$blogData = $blogModel -> getDataAll($limit,$posisi);
			$count = $blogModel -> count;
			$titleweb = 'Blog';
			$metaurl = '';
			$metapict = '';		
        }
		
    }
    else{
        
        $blogDetail = $blogModel -> getDataByID($model -> no);
        $blogDetailCount = $blogModel -> count; 
        if ($blogDetailCount == 0) { }
        else {
            $blogNo = $blogDetail['no'];
            $blogTitle = $blogDetail['title'];
            $blogLink = $blogDetail['link'];
            $blogAuthor = $blogDetail['author'];
            $blogContent = $blogDetail['content'];
            $blogVisitor = $blogDetail['visitors'];
            $blogComment = $blogDetail['comments'];
            $blogMetaDesc = $blogDetail['meta_description'];
            $blogMetaKey = explode(", ",$blogMetaDesc); 
            $blogMetaCount = count($blogMetaKey);
            $blogDate = $blogDetail['date'];
            $blogPicture = $blogDetail['picture'];
            $blogNoCategory = $blogDetail['no_blog_category'];
            $blogURL = URL_DOMAIN.$menu.'/detail/'.$blogNo.'/'.$blogLink.'/';		
            //DATA KOMENTAR
            $blogcommentDaftar = $blogModel -> getComment($blogNo);
            $blogcommentCount = $blogModel -> count;
            //META GOOGLE
            $titleweb = $blogTitle;
            $metadesc = $blogMetaDesc;
        }       
        
    } 
    
	if ($access == 'public') {
		include DIR_MODULE.'blog/public.php';
	}
	else {
		include DIR_MODULE.'blog/subdomain.php';
	}
}
?>