<?php
if ($access == 'super') {

	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
    
    if ($link == 'help_category') {		
		include DIR_MODULE.'help/super-help-category.php';	
	}
	else {
		include DIR_MODULE.'help/super-help.php';
	}

}
elseif ($access == 'admin'){  

    include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
    
    include DIR_MODULE.'help/admin-help.php';	

} 
elseif ($access == 'member') {
	
	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
    
    include DIR_MODULE.'help/member-help.php';	
	
}
else {
	
    include DIR_MODEL.'help.php';
	include DIR_LIBRARY.'uri.php';
	
    $helpModel = new help; 
    $uri = new uri;
    
    //TERBARU
    $limit = 10;
    if ($page == "") { $posisi = 0; $page = 1; } else { $posisi = ($page-1)*$limit; }
    $helpDataLast = $helpModel -> getDataAll($limit,$posisi);
    $count = $helpModel -> count;
	//KATA KUNCI
    $key = $uri -> get('key');
	
	if($no == ''){

		if(!empty($key)){
            //PENCARIAN
            $limit = 10;
            if ($page == "") { $posisi = 0; $page = 1; } else { $posisi = ($page-1)*$limit; }
            $helpData = $helpModel -> getDataSearch($key,$posisi,$limit);
            $count = $helpModel -> count;
            $keybaru = str_replace('-',' ',$key);
            $titleweb = $keybaru;
            $metaurl = '';
            $metapict = '';
        }
        else { 
			$limit = 10;
			if ($page == "") { $posisi = 0; $page = 1; } else { $posisi = ($page-1)*$limit; }
			$helpData = $helpModel -> getDataAll($limit,$posisi);
			$count = $helpModel -> count;
			$titleweb = 'Bantuan';
			$metaurl = '';
			$metapict = '';		
        }
		
    }
    else{
        
        $helpDetail = $helpModel -> getDataByID($model -> no);
        $helpDetailCount = $helpModel -> count; 
        if ($helpDetailCount == 0) { }
        else {
            $helpNo = $helpDetail['no'];
            $helpName = $helpDetail['name'];
            $helpLink = $helpDetail['link'];
            $helpContent = $helpDetail['content'];
            $helpDate = $helpDetail['date'];
            $helpPicture = $helpDetail['picture'];
            $helpURL = URL_DOMAIN.$menu.'/detail/'.$helpNo.'/'.$helpLink.'/';		
        }       
        
    } 
	
	include DIR_MODULE.'help/public.php';
}
?>