<?php 
if ($access == 'super') { 
	
	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
	
	include DIR_MODULE.'website/super-website.php';	
	
}
elseif ($access == 'admin') { 
	
	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
	
    if ($link == 'slider') {
		
		include DIR_MODULE.'website/admin-slider.php';
		
	}
	elseif ($link == 'menu') {
		
		include DIR_MODULE.'website/admin-menu.php';
		
	}
	elseif ($link == 'blog') {
		
		include DIR_MODULE.'website/admin-blog.php';
		
	}
	elseif ($link == 'gallery') {
		
		include DIR_MODULE.'website/admin-gallery.php';
		
	}
	elseif ($link == 'theme') {
		
		include DIR_MODULE.'website/admin-theme.php';
		
	}
	elseif ($link == 'setting') {
		
		include DIR_MODULE.'website/admin-setting.php';
		
	}
	else {
		
		include DIR_MODULE.'website/admin-website.php';
	
	}
    
}
elseif ($access == 'member') { 
	
	include DIR_LIBRARY.'admin/admin.php';
    $admin = new admin();
	
    if ($act == 'create') {
		
		include DIR_MODULE.'website/member-create.php';
		
	}
	else {
		
		include DIR_MODULE.'website/member-website.php';
	
	}
	
}
else {
    
    
    
    
}
?>