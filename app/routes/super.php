<?php
if (empty ($_SESSION['uname']) or empty ($_SESSION['pword'])) {
    
    include DIR_MODULE.'account/super-login.php';
    
}
else { 
	
	$access = 'super';
	
	switch ($link) {
		
		case '': 
			$titlepage = 'Dashboard';
			include DIR_CONTROLLER.'AccountController.php';  
			break;
			
		case 'account': 
			$titlepage = 'Akun'; 
			include DIR_CONTROLLER.'AccountController.php';  
			break;
		
		case 'website': 
			$titlepage = 'Website'; 
			include DIR_CONTROLLER.'WebsiteController.php';  
			break;
			
		case 'flyer': 
			$titlepage = 'Flyer Online'; 
			include DIR_CONTROLLER.'FlyerController.php';  
			break;	
			
		case 'flyer_theme': 
			$titlepage = 'Tema Flyer Online'; 
			include DIR_CONTROLLER.'FlyerController.php';  
			break;	
			
		case 'member': 
			$titlepage = 'Member'; 
			include DIR_CONTROLLER.'MemberController.php';  
			break;	
			
		case 'agent': 
			$titlepage = 'Mitra'; 
			include DIR_CONTROLLER.'MemberController.php';  
			break;	
		
		case 'home': 
			$titlepage = 'Beranda'; 
			include DIR_CONTROLLER.'HomeController.php';  
			break;	
		
		case 'profile': 
			$titlepage = 'Profil'; 
			include DIR_CONTROLLER.'ProfileController.php';  
			break;
			
		case 'article': 
			$titlepage = 'Artikel'; 
			include DIR_CONTROLLER.'BlogController.php';  
			break;
			
		case 'gallery': 
			$titlepage = 'Galeri'; 
			include DIR_CONTROLLER.'GalleryController.php';  
			break;
			
		case 'discussion': 
			$titlepage = 'Tanya Jawab'; 
			include DIR_CONTROLLER.'DiscussionController.php';  
			break;
			
		case 'volunteer': 
			$titlepage = 'Relawan'; 
			include DIR_CONTROLLER.'VolunteerController.php';  
			break;
			
		case 'donation': 
			$titlepage = 'Donasi Online'; 
			include DIR_CONTROLLER.'DOnationController.php';  
			break;
			
		case 'blog': 
			$titlepage = 'Blog'; 
			include DIR_CONTROLLER.'BlogController.php';  
			break;
			
		case 'blog_category': 
			$titlepage = 'Kategori Blog'; 
			include DIR_CONTROLLER.'BlogController.php';  
			break;
		
		case 'blog_comment': 
			$titlepage = 'Komentar Blog'; 
			include DIR_CONTROLLER.'BlogController.php';  
			break;
			
		case 'help_category': 
			$titlepage = 'Kategori Bantuan'; 
			include DIR_CONTROLLER.'HelpController.php';  
			break;
			
		case 'help': 
			$titlepage = 'Bantuan'; 
			include DIR_CONTROLLER.'HelpController.php';  
			break;
			
		case 'help_category': 
			$titlepage = 'Kategori Bantuan'; 
			include DIR_CONTROLLER.'HelpController.php';  
			break;			
			
		case 'partai': 
			$titlepage = 'Partai'; 
			include DIR_CONTROLLER.'SettingController.php';  
			break;
			
		case 'theme': 
			$titlepage = 'Tema Desain'; 
			include DIR_CONTROLLER.'SettingController.php';  
			break;
			
		case 'user': 
			$titlepage = 'User / Admin'; 
			include DIR_CONTROLLER.'UserController.php';  
			break;
		
		case 'menu': 
			$titlepage = 'Menu'; 
			include DIR_CONTROLLER.'TextController.php';  
			break;	
			
		default:
			$titlepage = 'Error';
			include DIR_CONTROLLER.'TextController.php';
			
	}

}
?>