<?php
if (empty ($_SESSION['uname']) or empty ($_SESSION['pword'])) {
   
    include DIR_MODULE.'account/member-login.php';
    
}
else { 

	$access='member';

	switch ($link) {
		
		case '': 
			$titlepage='Dashboard';
			include DIR_CONTROLLER.'AccountController.php';  
			break;
			
		case 'account': 
			$titlepage='Profil'; 
			include DIR_CONTROLLER.'AccountController.php';  
			break;	
			
		case 'website': 
			$titlepage='Website Anda'; 
			include DIR_CONTROLLER.'WebsiteController.php';  
			break;
			
		case 'flyer': 
			$titlepage='Flyer Digital'; 
			include DIR_CONTROLLER.'FlyerController.php';  
			break;					
		
		case 'product': 
			$titlepage='Produk'; 
			include DIR_CONTROLLER.'ProductController.php';  
			break;
			
		case 'upgrade': 
			$titlepage='Upgrade'; 
			include DIR_CONTROLLER.'ProductController.php';  
			break;
			
		case 'help': 
			$titlepage='Bantuan'; 
			include DIR_CONTROLLER.'HelpController.php';  
			break;
		
		case 'member': 
			$titlepage='Member'; 
			include DIR_CONTROLLER.'MemberController.php';  
			break;
			
		case 'transaction': 
			$titlepage='Transaksi'; 
			include DIR_CONTROLLER.'TransactionController.php';  
			break;
			
		case 'commission': 
			$titlepage='Komisi'; 
			include DIR_CONTROLLER.'CommissionController.php';  
			break;
			
		case 'notification': 
			$titlepage='Notifikasi'; 
			include DIR_CONTROLLER.'NotificationController.php';  
			break;
			
		default:
			$titlepage='Error';
			include DIR_CONTROLLER.'TextController.php';
			
	}

}
?>