<?php
if (empty ($_SESSION['uname']) or empty ($_SESSION['pword'])) {
	include DIR_LIBRARY.'uri.php';
    $uri = new uri;
    $uname = $uri->get('u');
    $pword = $uri->get('p');
    if ($uname=='' OR $pword=='') { }
    else { 
        $query = "SELECT * FROM member WHERE password ='$pword' AND username = '$uname' AND 'subdomain' = '$subdomain' ";
    	$sql = mysqli_query($model->koneksi, $query);
    	$count = mysqli_num_rows($sql);                                            
    	if ($count<=0) { } 
    	else {  
    	    $memberData = mysqli_fetch_assoc($sql);
			$_SESSION['name'] = $memberData['name'];
			$_SESSION['uname'] = $memberData['username'];
			$_SESSION['pword'] = $memberData['password'];
			$_SESSION['pict'] = $memberData['picture'];
			$_SESSION['cat'] = 'member';
			$ipaddress=$_SERVER['REMOTE_ADDR'];  
			"UPDATE member SET last_login = sysdate(), last_ipaddress = '$ipaddress' WHERE username = '$uname'";
			mysqli_query($model->koneksi, "UPDATE member SET last_login = sysdate(), last_ipaddress = '$ipaddress' WHERE username = '$uname'");
			
			header('location:'.URL_DOMAIN.'adm'); 
			?>
			<!--<meta http-equiv="refresh" content="0" url="<?php echo URL_DOMAIN.$menu;?>">-->
			<?php
    	}
    }
    include DIR_MODULE.'account/admin-login.php';
    
}
else { 
	
	$access = 'admin';
	
	switch ($link) {
		
		case '': 
			$titlepage = 'Dashboard';
			include DIR_CONTROLLER.'AccountController.php';  
			break;
		
		case 'home': 
			$titlepage = 'Beranda'; 
			include DIR_CONTROLLER.'HomeController.php';  
			break;
			
		case 'account': 
			$titlepage = 'Akun'; 
			include DIR_CONTROLLER.'AccountController.php';  
			break;
			
		case 'upgrade': 
			$titlepage = 'Upgrade Paket'; 
			include DIR_CONTROLLER.'ProductController.php';  
			break;
			
		case 'cv': 
			$titlepage = 'Profil / CV'; 
			include DIR_CONTROLLER.'CVController.php';  
			break;
		
		/*
		case 'blog': 
			$titlepage = 'Blog'; 
			include DIR_CONTROLLER.'BlogController.php';  
			break;
			
		case 'gallery': 
			$titlepage = 'Galeri'; 
			include DIR_CONTROLLER.'GalleryController.php';  
			break;
		*/
		
		case 'discussion': 
			$titlepage = 'Tanya Jawab'; 
			include DIR_CONTROLLER.'DiscussionController.php';  
			break;
			
		case 'volunteer': 
			$titlepage = 'Relawan'; 
			include DIR_CONTROLLER.'VolunteerController.php';  
			break;
			
		case 'donation': 
			$titlepage = 'Donasi'; 
			include DIR_CONTROLLER.'DonationController.php';  
			break;
			
		case 'contact': 
			$titlepage = 'Kontak'; 
			include DIR_CONTROLLER.'SettingController.php';  
			break;
		
		case 'setting': 
			$titlepage = 'Setting'; 
			include DIR_CONTROLLER.'SettingController.php';  
			break;
			
		case 'help': 
			$titlepage = 'Bantuan'; 
			include DIR_CONTROLLER.'HelpController.php';  
			break;
		
		// MENU MASJID
		
		case 'masjid': 
			$titlepage='Masjid'; 
			include DIR_CONTROLLER.'MasjidController.php';  
			break;
			
		case 'masjid_takmir': 
			$titlepage='Takmir Masjid'; 
			include DIR_CONTROLLER.'MasjidController.php';  
			break;
			
		case 'masjid_program': 
			$titlepage='Program / Layanan Masjid'; 
			include DIR_CONTROLLER.'MasjidController.php';  
			break;
			
		case 'finance_transaction': 
			$titlepage='Transaksi Keuangan'; 
			include DIR_CONTROLLER.'FinanceController.php';  
			break;
			
		case 'finance_report': 
			$titlepage='Laporan Keuangan'; 
			include DIR_CONTROLLER.'FinanceController.php';  
			break;
			
		case 'finance_account': 
			$titlepage='Akun / Kategori Keuangan'; 
			include DIR_CONTROLLER.'FinanceController.php';  
			break;
		
		case 'jamaah': 
			$titlepage='Jamaah'; 
			include DIR_CONTROLLER.'JamaahController.php';  
			break;
			
		case 'jamaah-stats': 
			$titlepage='Statistik Jamaah'; 
			include DIR_CONTROLLER.'JamaahController.php';  
			break;
			
		case 'ebook': 
			$titlepage='EBook'; 
			include DIR_CONTROLLER.'PustakaController.php';  
			break;
			
		case 'evideo': 
			$titlepage='Video'; 
			include DIR_CONTROLLER.'PustakaController.php';  
			break;
			
		case 'inventory': 
			$titlepage='Inventaris Masjid'; 
			include DIR_CONTROLLER.'InventoryController.php';  
			break;
			
		case 'circulation': 
			$titlepage='Peminjaman Inventaris'; 
			include DIR_CONTROLLER.'InventoryController.php';  
			break;
			
		case 'event_routine': 
			$titlepage='Kegiatan Rutin'; 
			include DIR_CONTROLLER.'EventController.php';  
			break;
			
		case 'event': 
			$titlepage='Kegiatan Insidental'; 
			include DIR_CONTROLLER.'EventController.php';  
			break;
			
		case 'slider': 
			$titlepage='Slider'; 
			include DIR_CONTROLLER.'WebsiteController.php';  
			break;
		
		case 'menu': 
			$titlepage='Halaman'; 
			include DIR_CONTROLLER.'WebsiteController.php';  
			break;
			
		case 'blog': 
			$titlepage='Artikel'; 
			include DIR_CONTROLLER.'WebsiteController.php';  
			break;
			
		case 'gallery': 
			$titlepage='Galeri'; 
			include DIR_CONTROLLER.'WebsiteController.php';  
			break;
			
		case 'theme': 
			$titlepage='Tema Desain'; 
			include DIR_CONTROLLER.'WebsiteController.php';  
			break;
		
		case 'maps': 
			$titlepage='Maps Masjid'; 
			include DIR_CONTROLLER.'MapsController.php';  
			break;
		
		case 'dai': 
			$titlepage='Dai'; 
			include DIR_CONTROLLER.'DaiController.php';  
			break;
			
		case 'marketplace': 
			$titlepage='Marketplace'; 
			include DIR_CONTROLLER.'MarketplaceController.php';  
			break;
			
		//
		
		default:
			$titlepage = 'Error';
			include DIR_CONTROLLER.'TextController.php';
			
	}

}
?>