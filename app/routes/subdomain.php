<?php

$access='subdomain';

    
switch ($menu) {
    
    case '': 
        include DIR_CONTROLLER.'HomeController.php'; 
        break;
    
	case 'profile': 
        include DIR_CONTROLLER.'ProfileController.php'; 
        break;
		
        case 'blog': 
        include DIR_CONTROLLER.'BlogController.php'; 
        break;
		
	case 'gallery': 
        include DIR_CONTROLLER.'GalleryController.php'; 
        break;
    
	case 'discussion': 
        include DIR_CONTROLLER.'DiscussionController.php'; 
        break;
		
	case 'volunteer': 
        include DIR_CONTROLLER.'VolunteerController.php'; 
        break;
		
	case 'donation': 
        include DIR_CONTROLLER.'DonationController.php'; 
        break;
		
	case 'contact': 
        include DIR_CONTROLLER.'ContactController.php'; 
        break;
        
    //AJAX

        case 'kota-form': 
        include DIR_AJAX.'city/city-all.php'; 
        break;

        case 'kecamatan-form': 
        include DIR_AJAX.'district/district_by_city.php'; 
        break;

        case 'village-form': 
        include DIR_AJAX.'village/village_by_district.php'; 
        break;
            
            
    default:
        include DIR_CONTROLLER.'TextController.php';
        
}

?>