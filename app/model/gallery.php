<?php
class gallery extends model {	  	

	function getDataAll ($limit,$posisi){
		$sqlall = "SELECT * FROM gallery WHERE publish='1' AND subdomain='".SUBDOMAIN."' ORDER BY date DESC";
		$queryall = mysqli_query($this->koneksi,$sqlall);
		$count = mysqli_num_rows($queryall);
		$this->count = $count;
		$sqllimit = "SELECT * FROM gallery WHERE publish='1' AND subdomain='".SUBDOMAIN."' ORDER BY date DESC LIMIT $posisi,$limit";
		$querylimit = mysqli_query($this->koneksi,$sqllimit);		
		$countlimit = mysqli_num_rows($querylimit);
		$this -> countlimit = $countlimit;
		$data = array();
		if ($countlimit > 0) {
			while ($row = mysqli_fetch_array($querylimit)) {
				$data[] = $row;
			}
		}
		return $data;
	}	

	function getDataByID($nodata) { 	    
		$sql="SELECT *,DATE_FORMAT(date,'%d %M %Y, Pukul %H:%i') as date FROM gallery WHERE publish='1'  AND no='$nodata' AND subdomain='".SUBDOMAIN."' LIMIT 1";
		$query = mysqli_query($this->koneksi, $sql);
        $count = mysqli_num_rows($query);
		$this -> count = $count;
        $result = array();
        if ($count > 0) {
            while ($row = mysqli_fetch_assoc($query)) {
                $result = $row;
            }
        }        
        return $result;
	}
		
}
?>