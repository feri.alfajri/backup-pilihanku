<?php
class blog extends model {	  	

	function getDataAll ($limit,$posisi){
		$sqlall = "SELECT * FROM blog WHERE publish='1' AND subdomain='".SUBDOMAIN."' ORDER BY date DESC";
		$queryall = mysqli_query($this->koneksi,$sqlall);
		$count = mysqli_num_rows($queryall);
		$this->count = $count;
		$sqllimit = "SELECT * FROM blog WHERE publish='1' AND subdomain='".SUBDOMAIN."' ORDER BY date DESC LIMIT $posisi,$limit";
		$querylimit = mysqli_query($this->koneksi,$sqllimit);		
		$countlimit = mysqli_num_rows($querylimit);
		$this -> countlimit = $countlimit;
		$data = array();
		if ($countlimit > 0) {
			while ($row = mysqli_fetch_array($querylimit)) {
				$data[] = $row;
			}
		}
		return $data;
	}	

	function getDataSearch($keyword,$posisi,$limit){
	    $key = explode("-",$keyword);
		$jumkey = count($key);
		$jumlast = $jumkey-1;
		if ($jumkey == 1) {
		    $qtambahan = " title LIKE '%$keyword%' ";
		}
		else {
    		$qtambahan = " ( ";
    		for($i = 0; $i<$jumkey; $i++){
    		    if ($i < $jumlast) {
    		        $key_check = $key[$i];
    		        $qtambahan = $qtambahan . " title LIKE '%$key_check%' OR ";
    		    }
    		    else {
    		        $qtambahan = $qtambahan . " title LIKE '%$key_check%' ) ";
    		    }
    		    
    		}
		}
		$sqlall = "SELECT * FROM blog WHERE publish='1' AND subdomain='".SUBDOMAIN."' AND  $qtambahan ORDER BY date DESC";
		$queryall = mysqli_query($this->koneksi,$sqlall);
		$count = mysqli_num_rows($queryall);
		$this -> count = $count;
		$sqllimit = "SELECT * FROM blog where publish='1' AND subdomain='".SUBDOMAIN."' AND $qtambahan ORDER BY date DESC LIMIT $posisi,$limit";
		$querylimit = mysqli_query($this->koneksi,$sqllimit);
		$countlimit = mysqli_num_rows($querylimit);
		$this -> countlimit = $countlimit;
		$data = array();
		if ($countlimit > 0) {
			while ($row = mysqli_fetch_array($querylimit)) {
				$data[] = $row;
			}
		}
		return $data;
	}	

	function getDataCategory ($category,$orderby,$limit,$posisi){
		$sqlall = "SELECT * FROM blog WHERE no_blog_category='$category' AND subdomain='".SUBDOMAIN."' AND publish='1' ORDER BY date DESC";
		$queryall = mysqli_query($this->koneksi,$sqlall);
		$count = mysqli_num_rows($queryall);
		$this->count = $count;
		$sqllimit = "SELECT * FROM blog WHERE no_blog_category='$category' AND subdomain='".SUBDOMAIN."' AND publish='1' ORDER BY date DESC LIMIT $posisi,$limit";
		$querylimit = mysqli_query($this->koneksi,$sqllimit);		
		$countlimit = mysqli_num_rows($querylimit);
		$this -> countlimit = $countlimit;
		$data = array();
		if ($countlimit > 0) {
		   while ($row = mysqli_fetch_array($querylimit)) {
			  $data[] = $row;
		   }
		}
		return $data;
	}
	
	function getDataByID($noblog) { 	    
		$sql="SELECT *,DATE_FORMAT(date,'%d %M %Y, Pukul %H:%i') as date FROM blog WHERE publish='1' AND subdomain='".SUBDOMAIN."'  AND no='$noblog' LIMIT 1";
		$query = mysqli_query($this->koneksi, $sql);
        $count = mysqli_num_rows($query);
		$this -> count = $count;
        $result = array();
        if ($count > 0) {
            while ($row = mysqli_fetch_assoc($query)) {
                $result = $row;
            }
        }        
        return $result;
	}

	function addVisitor($visitor,$noblog){
		$visitor++;
		$sql="UPDATE blog
		SET visitors = '$visitor'
		WHERE no='$noblog'";
		$query = mysqli_query($this->koneksi, $sql);
	}
			
	
	//KATEGORI
	function getCategory () {	
		$sql = "SELECT * FROM blog_category WHERE publish = '1' AND subdomain='".SUBDOMAIN."' ORDER BY name ";
		$query = mysqli_query($this->koneksi,$sql);
		$count = mysqli_num_rows($query);
		$this -> count = $count;
		$data = array();
		if ($count > 0) {
			while ($row = mysqli_fetch_array($query)) {
				$data[] = $row;
			}
		}
		return $data;
	}

	function getCategoryByLink($link) {	    
		$sql="SELECT * FROM blog_category WHERE publish = '1' AND link = '$link' AND subdomain='".SUBDOMAIN."' LIMIT 1";
		$query = mysqli_query($this->koneksi, $sql);
        $count = mysqli_num_rows($query);
		$this -> count = $count;
        $data = array(); 
        if ($count > 0) {
            while ($row = mysqli_fetch_assoc($query)) {
                $data = $row;
            }
        }        
        return $data;
	}


	//COMMENT
	function getComment($noblog){
		$sql = "SELECT *, DATE_FORMAT(date,'%d %M %Y, %H:%i') as date FROM blog_comment WHERE  publish = '1' AND id_blog = '$noblog' AND subdomain='".SUBDOMAIN."' ORDER BY date ASC ";
		$query=mysqli_query($this->koneksi,$sql);
		$count=mysqli_num_rows($query);
		$this->count=$count;
		$data = array();
        if ($count > 0) {
            while ($row = mysqli_fetch_array($query)) {
               $data[] = $row;
            }
         }
         return $data;
	}

	//MEMBER
	function getDataByMember ($member,$limit,$posisi){
		$sqlall = "SELECT * FROM blog WHERE member='$member' AND publish='1' AND subdomain='".SUBDOMAIN."' ORDER BY date DESC";
		$queryall = mysqli_query($this->koneksi,$sqlall);
		$count = mysqli_num_rows($queryall);
		$this->count = $count;
		$sqllimit = "SELECT * FROM blog WHERE member='$member' AND publish='1' AND subdomain='".SUBDOMAIN."' ORDER BY date DESC LIMIT $posisi,$limit";
		$querylimit = mysqli_query($this->koneksi,$sqllimit);		
		$countlimit = mysqli_num_rows($querylimit);
		$this -> countlimit = $countlimit;
		$data = array();
		if ($countlimit > 0) {
		   while ($row = mysqli_fetch_array($querylimit)) {
			  $data[] = $row;
		   }
		}
		return $data;
	}
}
?>