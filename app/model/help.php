<?php
class help extends model {	  	

	function getDataAll ($limit,$posisi){
		$sqlall = "SELECT * FROM help WHERE publish='1' ORDER BY date DESC";
		$queryall = mysqli_query($this->koneksi,$sqlall);
		$count = mysqli_num_rows($queryall);
		$this->count = $count;
		$sqllimit = "SELECT * FROM help WHERE publish='1' ORDER BY date DESC LIMIT $posisi,$limit";
		$querylimit = mysqli_query($this->koneksi,$sqllimit);		
		$countlimit = mysqli_num_rows($querylimit);
		$this -> countlimit = $countlimit;
		$data = array();
		if ($countlimit > 0) {
			while ($row = mysqli_fetch_array($querylimit)) {
				$data[] = $row;
			}
		}
		return $data;
	}	

	function getDataSearch($keyword,$posisi,$limit){
	    $key = explode("-",$keyword);
		$jumkey = count($key);
		$jumlast = $jumkey-1;
		if ($jumkey == 1) {
		    $qtambahan = " title LIKE '%$keyword%' ";
		}
		else {
    		$qtambahan = " ( ";
    		for($i = 0; $i<$jumkey; $i++){
    		    if ($i < $jumlast) {
    		        $key_check = $key[$i];
    		        $qtambahan = $qtambahan . " title LIKE '%$key_check%' OR ";
    		    }
    		    else {
    		        $qtambahan = $qtambahan . " title LIKE '%$key_check%' ) ";
    		    }
    		    
    		}
		}
		$sqlall = "SELECT * FROM help WHERE publish='1' AND  $qtambahan ORDER BY date DESC";
		$queryall = mysqli_query($this->koneksi,$sqlall);
		$count = mysqli_num_rows($queryall);
		$this -> count = $count;
		$sqllimit = "SELECT * FROM help where publish='1' AND $qtambahan ORDER BY date DESC LIMIT $posisi,$limit";
		$querylimit = mysqli_query($this->koneksi,$sqllimit);
		$countlimit = mysqli_num_rows($querylimit);
		$this -> countlimit = $countlimit;
		$data = array();
		if ($countlimit > 0) {
			while ($row = mysqli_fetch_array($querylimit)) {
				$data[] = $row;
			}
		}
		return $data;
	}	

	
	function getDataByID($noblog) { 	    
		$sql="SELECT *,DATE_FORMAT(date,'%d %M %Y, Pukul %H:%i') as date FROM help WHERE publish='1'  AND no='$noblog' LIMIT 1";
		$query = mysqli_query($this->koneksi, $sql);
        $count = mysqli_num_rows($query);
		$this -> count = $count;
        $result = array();
        if ($count > 0) {
            while ($row = mysqli_fetch_assoc($query)) {
                $result = $row;
            }
        }        
        return $result;
	}
			
	
}
?>