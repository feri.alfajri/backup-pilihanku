<ul class="navbar-nav bg-success sidebar sidebar-dark accordion" id="accordionSidebar">    
	<a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo URL_DOMAIN.$menu;?>" style="background:#fff">
	    <div class="sidebar-brand-icon">
          <img src="<?php echo URL_IMAGE;?>logo-emasjid.png"  width="90%"/> 
        </div>
	</a> 
	<hr class="sidebar-divider my-0">
	<div class="text-white">
        <li class="nav-item">
        	<a class="nav-link">
        		<small><b>MANAJEMEN MASJID</b></small>
        	</a>
        </li>
        <li class="nav-item <?php if ($link=='') { echo 'active'; } ?>">
            <a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>">
        		<i class="fas fa-fw fa-desktop"></i><span>Dashboard</span>
        	</a>
        </li>
        <li class="nav-item <?php if ($link=='masjid' or $link=='masjid_takmir' or $link=='masjid_program') { echo 'active'; } ?>">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#masjid" aria-expanded="true" aria-controls="masjid">
                <i class="fas fa-fw fa-info"></i><span>Profil Masjid</span>
            </a>
            <div id="masjid" class="collapse <?php if ($link=='masjid' or $link=='masjid_takmir'  or $link=='masjid_program') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a href="<?php echo URL_DOMAIN.$menu;?>/masjid/0/data" class="collapse-item <?php if ($act=='data') { echo 'active'; } ?>" title="Data Masjid">Data Masjid</a>
                    <a href="<?php echo URL_DOMAIN.$menu;?>/masjid_takmir" class="collapse-item <?php if ($link=='masjid_takmir') { echo 'active'; } ?>" title="Takmir Masjid">Takmir Masjid</a>
                    <a href="<?php echo URL_DOMAIN.$menu;?>/masjid_program" class="collapse-item <?php if ($link=='masjid_program') { echo 'active'; } ?>" title="Program Masjid">Program Masjid</a>
        		</div>
            </div>
        </li>
        <li class="nav-item <?php if ($link=='finance_report' or $link=='finance_transaction' or $link=='finance_account') { echo 'active'; } ?>">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#finance" aria-expanded="true" aria-controls="finance">
                <i class="fas fa-fw fa-dollar-sign"></i><span>Keuangan</span>
            </a>
            <div id="finance" class="collapse <?php if ($link=='finance_report' or $link=='finance_transaction' or $link=='finance_account') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a href="<?php echo URL_DOMAIN.$menu;?>/finance_transaction" class="collapse-item <?php if ($link=='finance_transaction') { echo 'active'; } ?>" title="Transaksi">Transaksi</a>
                    <a href="<?php echo URL_DOMAIN.$menu;?>/finance_report" class="collapse-item <?php if ($link=='finance_report') { echo 'active'; } ?>" title="Laporan">Laporan</a>
                    <a href="<?php echo URL_DOMAIN.$menu;?>/finance_account" class="collapse-item <?php if ($link=='finance_account') { echo 'active'; } ?>" title="Akun / Kategori">Akun / Kategori</a>
                </div>
            </div>
        </li>
        <li class="nav-item <?php if ($link=='donation') { echo 'active'; } ?>">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#donation" aria-expanded="true" aria-controls="donation">
                <i class="fas fa-fw fa-gift"></i><span>Donasi</span>
            </a>
            <div id="donation" class="collapse <?php if ($link=='donation') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a href="<?php echo URL_DOMAIN.$menu;?>/donation/0/confirm" class="collapse-item <?php if ($act=='confirm') { echo 'active'; } ?>" title="Konfirmasi">Konfirmasi</a>
                    <a href="<?php echo URL_DOMAIN.$menu;?>/donation/0/prospect" class="collapse-item <?php if ($act=='prospect') { echo 'active'; } ?>" title="Prospek">Prospek</a>
                    <a href="<?php echo URL_DOMAIN.$menu;?>/donation/0/history" class="collapse-item <?php if ($act=='history') { echo 'active'; } ?>" title="Riwayat">Riwayat</a>
                </div>
            </div>
        </li>
        <li class="nav-item <?php if ($link=='jamaah' or $link=='jamaah_stats') { echo 'active'; } ?>">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#jamaah" aria-expanded="true" aria-controls="jamaah">
                <i class="fas fa-fw fa-users"></i><span>Jamaah</span>
            </a>
            <div id="jamaah" class="collapse <?php if ($link=='jamaah' or $link=='jamaah_stats') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a href="<?php echo URL_DOMAIN.$menu;?>/jamaah" class="collapse-item <?php if ($link=='jamaah') { echo 'active'; } ?>" title="Data Jamaah">Data Jamaah</a>
                    <a href="<?php echo URL_DOMAIN.$menu;?>/jamaah-stats" class="collapse-item <?php if ($link=='jamaah_stats') { echo 'active'; } ?>" title="Statistik Jamaah">Statistik Jamaah</a>
                </div>
            </div>
        </li>
        <li class="nav-item <?php if ($link=='inventory' or $link=='circulation') { echo 'active'; } ?>">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#inventory" aria-expanded="true" aria-controls="inventory">
                <i class="fas fa-fw fa-list"></i><span>Inventaris</span>
            </a>
            <div id="inventory" class="collapse <?php if ($link=='inventory' or $link=='circulation') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a href="<?php echo URL_DOMAIN.$menu;?>/inventory" class="collapse-item <?php if ($link=='inventory') { echo 'active'; } ?>" title="Data Inventaris">Data Inventaris</a>
                    <a href="<?php echo URL_DOMAIN.$menu;?>/circulation" class="collapse-item <?php if ($link=='circulation') { echo 'active'; } ?>" title="Peminjaman">Peminjaman</a>
                </div>
            </div>
        </li>
        <hr class="sidebar-divider my-0">
        
        <li class="nav-item">
        	<a class="nav-link">
        		<small><b>KONTEN WEBSITE</b></small>
        	</a>
        </li>
        <li class="nav-item <?php if ($link=='home') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/home" title="Beranda">
				<i class="fas fa-fw fa-home"></i><span>Beranda</span>
			</a>
		</li>
		<li class="nav-item <?php if ($link=='profile') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/profile" title="Profil">
				<i class="fas fa-fw fa-info"></i><span>Profil</span>
			</a>
		</li>
		<li class="nav-item <?php if ($link=='event') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/event" title="Kegiatan">
				<i class="fas fa-fw fa-heart"></i><span>Kegiatan</span>
			</a>
		</li>
        <li class="nav-item <?php if ($link=='blog') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/blog" title="Artikel">
				<i class="fas fa-fw fa-edit"></i><span>Artikel</span>
			</a>
		</li>
		<li class="nav-item <?php if ($link=='gallery') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/gallery" title="Galeri">
				<i class="fas fa-fw fa-image"></i><span>Galeri</span>
			</a>
		</li>
		<li class="nav-item <?php if ($link=='contact') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/contact" title="Kontak">
				<i class="fas fa-fw fa-phone"></i><span>Kontak</span>
			</a>
		</li>
        <li class="nav-item <?php if ($link=='menu' or $link=='theme' or $link=='header' or $link=='seo') { echo 'active'; } ?>">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#setting" aria-expanded="true" aria-controls="setting">
                <i class="fas fa-fw fa-check"></i><span>Pengaturan</span>
            </a>
            <div id="setting" class="collapse <?php if ($link=='menu' or $link=='theme' or $link=='header' or $link=='seo') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a href="<?php echo URL_DOMAIN.$menu;?>/menu" class="collapse-item <?php if ($link=='menu') { echo 'active'; } ?>" title="Menu / Halaman">Menu / Halaman</a>
                    <a href="<?php echo URL_DOMAIN.$menu;?>/theme" class="collapse-item <?php if ($link=='theme') { echo 'active'; } ?>" title="Tema Desain">Tema Desain</a>
                    <a href="<?php echo URL_DOMAIN.$menu;?>/header" class="collapse-item <?php if ($link=='header') { echo 'active'; } ?>" title="Header & Footer">Header & Footer</a>
                    <a href="<?php echo URL_DOMAIN.$menu;?>/seo" class="collapse-item <?php if ($link=='seo') { echo 'active'; } ?>" title="SEO">SEO</a>
                </div>
            </div>
        </li>
        
        
        <hr class="sidebar-divider my-0">
        
        <li class="nav-item">
        	<a class="nav-link">
        		<small><b>JARINGAN MASJID</b></small>
        	</a>
        </li>
        <li class="nav-item <?php if ($link=='maps') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/maps" title="Maps Masjid">
				<i class="fas fa-fw fa-map"></i><span>Maps Masjid</span>
			</a>
		</li>
		<li class="nav-item <?php if ($link=='dai') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/dai" title="Data Dai">
				<i class="fas fa-fw fa-user-tie"></i><span>Data Dai</span>
			</a>
		</li>
		<li class="nav-item <?php if ($link=='marketplace') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/marketplace" title="Marketplace">
				<i class="fas fa-fw fa-shopping-cart"></i><span>Marketplace</span>
			</a>
		</li>
		<li class="nav-item <?php if ($link=='ebook' or $link=='evideo') { echo 'active'; } ?>">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pustaka" aria-expanded="true" aria-controls="pustaka">
                <i class="fas fa-fw fa-book"></i><span>Pustaka Digital</span>
            </a>
            <div id="pustaka" class="collapse <?php if ($link=='ebook' or $link=='evideo') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a href="<?php echo URL_DOMAIN.$menu;?>/ebook" class="collapse-item <?php if ($link=='ebook') { echo 'active'; } ?>" title="E-Book">E-Book</a>
                    <a href="<?php echo URL_DOMAIN.$menu;?>/evideo" class="collapse-item <?php if ($link=='evideo') { echo 'active'; } ?>" title="Video">Video</a>
                </div>
            </div>
        </li>
        <li class="nav-item <?php if ($link=='help') { echo 'active'; } ?>">
			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#help" aria-expanded="true" aria-controls="help">
				<i class="fas fa-fw fa-question"></i><span>Bantuan</span>
			</a>
			<div id="help" class="collapse <?php if ($link=='help') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded">
					<a href="<?php echo URL_DOMAIN.$menu;?>/help" class="collapse-item <?php if ($link=='help') { echo 'active'; } ?>" title="Bantuan">Bantuan</a>
					<a href="https://api.whatsapp.com/send?phone=<?php echo $mobile;?>" target="_blank" class="collapse-item" title="Konsultasi">Konsultasi</a>
				</div>
			</div>
		</li>
        
    </div>
	<div class="text-center d-none d-md-inline mt-3">
		<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>
</ul>