<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow"> 
    
	<button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3"><i class="fa fa-bars"></i></button>
	
	<span class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
		<b style="text-transform:uppercase;"><?php echo $titlepage;?></b>          
	</span>
	
	<ul class="navbar-nav ml-auto">
	    <?php
		$textWa = 'Perkenalkan%2C%0ANama%20%3A%0ASaya%20mau%20bertanya%20tentang%20';
		if ($_SESSION['pict']=='') { $pictuser = 'member.png'; }  else { $pictuser = $_SESSION['pict']; }
		include('notification.php');
		?>		
		<div class="topbar-divider d-none d-sm-block"></div>
		
		<li class="nav-item dropdown no-arrow">
			<a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<span class="mr-2 d-none d-lg-inline" style="color:#333333"><?php echo $_SESSION['name'];?></span>
				<img class="img-profile rounded-circle border" alt="image" src="<?php echo URL_UPLOAD;?>image.php/<?php echo $pictuser;?>?width=48&cropratio=1:1&nocache&quality=100&image=<?php echo URL_PICTURE.$pictuser;?>">
			</a>
			<div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
				<a class="dropdown-item" href="<?php echo URL_DOMAIN.$menu;?>/account/0/profile" title="Profil">
					<i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>Profil
				</a>
				<a class="dropdown-item" href="<?php echo URL_DOMAIN.$menu;?>/account/0/password" title="Password">
					<i class="fas fa-lock fa-sm fa-fw mr-2 text-gray-400"></i>Password
				</a>
				<a class="dropdown-item" href="<?php echo URL_DOMAIN.$menu;?>/account/0/avatar" title="Foto Profil">
					<i class="fas fa-image fa-sm fa-fw mr-2 text-gray-400"></i>Foto Profil
				</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="<?php echo URL_DOMAIN;?>logout" title="Logout" onclick="return confirm('Yakin Keluar ?');">
					<i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>Logout
				</a>
			</div>
		</li>
		
	</ul>
	
</nav>