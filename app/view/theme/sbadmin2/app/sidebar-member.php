<ul class="navbar-nav bg-danger sidebar sidebar-dark accordion" id="accordionSidebar">    
	<a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo URL_DOMAIN.$menu;?>" style="background:#fff">
	    <div class="sidebar-brand-icon" >
          <img src="<?php echo URL_IMAGE;?>logo2.png"  width="90%"/> 
        </div>
	</a> 
	<hr class="sidebar-divider my-0">
	<div class="text-white">
		<?php
		if ($_SESSION['pict']=='') { $pictuser = 'member.png'; }  else { $pictuser = $_SESSION['pict']; }
		?>
		<div class="text-center p-2">
			<img class="img-profile rounded-circle" src="<?php echo URL_UPLOAD;?>image.php/<?php echo $pictuser;?>?width=120&cropratio=1:1&nocache&quality=100&image=<?php echo URL_PICTURE.$pictuser;?>" width="75%" style="max-width:100px">
			<div>
				<b class="text-uppercase"><?php echo $_SESSION['name'];?></b>
				<br/>
				<small><?php echo $_SESSION['uname'];?></small>
				<br/>
				<a class="text-white btn btn-warning btn-sm" href="<?php echo URL_DOMAIN?>logout" onclick="return confirm('Yakin Keluar ?');">
					<i class="fas fa-sign-out-alt text-white"></i> <span>Logout</span>
				</a>
			</div>
		</div>
		<hr class="sidebar-divider my-0">
		<li class="nav-item <?php if ($link=='') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>" title="Dashboard">
				<i class="fas fa-fw fa-compass"></i><span>Dashboard</span>
			</a>
		</li>
		<li class="nav-item <?php if ($link=='website') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/website" title="Website Anda">
				<i class="fas fa-fw fa-desktop"></i><span>Website Anda</span>
			</a>
		</li>
		<li class="nav-item <?php if ($link=='flyer') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/flyer" title="Flyer Digital">
				<i class="fas fa-fw fa-folder"></i><span>Flyer Digital</span>
			</a>
		</li>
		<li class="nav-item <?php if ($link=='transaction') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/transaction" title="Transaksi">
				<i class="fas fa-fw fa-shopping-cart"></i><span>Transaksi</span>
			</a>
		</li>
		<hr class="sidebar-divider my-0">
		<!--
		<li class="nav-item">
			<a class="nav-link">
				<small><b>MENANG KAMPANYE</b></small>
			</a>
		</li>	
		<li class="nav-item <?php if ($link=='header') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/product/0/social-campaign" title="Kampanye Social Media">
				<i class="fas fa-fw fa-layer-group"></i><span>Kampanye Social Media</span>
			</a>
		</li>
		<li class="nav-item <?php if ($link=='seo') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/product/0/app-caleg" title="Aplikasi Caleg">
				<i class="fas fa-fw fa-eye"></i><span>Aplikasi Caleg</span>
			</a>
		</li>
		<li class="nav-item <?php if ($link=='flyer-ppt') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/product/0/template-flyer" title="Template Flyer PPT">
				<i class="fas fa-fw fa-paint-roller"></i><span>Template Flyer PPT</span>
			</a>
		</li>
		<hr class="sidebar-divider my-0">
		-->
		<?php
		if ($_SESSION['cat']=='cabang' or $_SESSION['cat']=='mitra') {
		    ?>
		    <li class="nav-item <?php if ($link=='member') { echo 'active'; } ?>">
    			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/member" title="Member">
    				<i class="fas fa-fw fa-users"></i><span>Member</span>
    			</a>
    		</li>
		    <li class="nav-item <?php if ($link=='commission') { echo 'active'; } ?>">
    			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/commission" title="Komisi">
    				<i class="fas fa-fw fa-dollar-sign"></i><span>Komisi</span>
    			</a>
    		</li>
		    <hr class="sidebar-divider my-0">
		    <?php
		}
		?>
		<li class="nav-item <?php if ($link=='help') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/help" title="Pusat Bantuan">
				<i class="fas fa-fw fa-question"></i><span>Bantuan</span>
			</a>
		</li>
		<li class="nav-item <?php if ($link=='consultation') { echo 'active'; } ?>">
			<a class="nav-link" href="https://api.whatsapp.com/send?phone=<?php echo $mobile;?>" title="Konsultasi" target="_blank">
				<i class="fas fa-fw fa-comment"></i><span>Konsultasi</span>
			</a>
		</li>
	</div>
	<div class="text-center d-none d-md-inline mt-3">
		<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>
	
</ul>