<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="<?php echo URL_IMAGE;?>favicon.png" rel="shortcut icon" type="image/x-icon">
<link href="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> 

<link href="<?php echo URL_THEME.THEME_MEMBER;?>/css/sb-admin-2.min.css" rel="stylesheet">
<link href="<?php echo URL_THEME.THEME_MEMBER;?>/css/mystyle.css" rel="stylesheet">
<link href="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<script src="<?php echo URL_THEME.THEME_MEMBER;?>/js/nicEdit.js"></script>	
<script src="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_THEME.THEME_MEMBER;?>/js/ajaxadmin.js"></script>
<script type="text/javascript">bkLib.onDomLoaded(function() { new nicEditor({fullPanel : true}).panelInstance('editor');});</script>

<title><?php echo $titlepage;?></title> 