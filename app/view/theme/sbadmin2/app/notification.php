<?php
$uname = $_SESSION['uname'];
$query = mysqli_query($model->koneksi,"SELECT * FROM notification WHERE username='$uname' AND status='unread' AND publish='1' ORDER BY date DESC LIMIT 5");
$jumlah = mysqli_num_rows($query);
?>
<li class="nav-item dropdown no-arrow mx-1">
    <a class="nav-link dropdown-toggle" href="#" title="Notifikasi" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-bell fa-fw"></i>
        <span class="badge badge-danger badge-counter"><?php echo $jumlah;?></span>
    </a>
    <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
        <?php
        while ($data = mysqli_fetch_array($query)) { 
        	$notifNo = $data['no'];
			$notifName = $data['name'];
			$notifDate = $data['date'];
            ?>
            <a class="dropdown-item d-flex align-items-center" href="<?php echo URL_DOMAIN.$menu.'/notification/'.$notifNo.'/detail/';?>" title="Lihat Notifikasi">
                <div class="mr-2">
                    <div class="icon-circle bg-primary">
                        <i class="fas fa-bell text-white"></i>
                    </div>
                </div>
                <div>
                    <div class="small text-gray-500"><?php echo $notifDate;?></div>
                    <span class="font-weight-bold"><?php echo $notifName;?></span>
                </div>
            </a>
            <?php
        }
        ?>
        <a class="dropdown-item text-center small text-gray-500" href="<?php echo URL_DOMAIN.$menu;?>/notification" title="Lihat semua notifikasi">Lihat semua notifikasi</a>
    </div>
</li>
<?php
if ($access=='admin') {
    ?>
    <li class="nav-item no-arrow mx-1">
        <a class="nav-link" href="<?php echo URL_DOMAIN;?>" title="Lihat Website" target="_blank">
            <i class="fas fa-desktop fa-fw"></i>
        </a>
    </li>
    <?php
}
?>