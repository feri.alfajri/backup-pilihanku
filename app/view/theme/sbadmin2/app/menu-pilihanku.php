<ul class="navbar-nav bg-danger sidebar sidebar-dark accordion" id="accordionSidebar">    
	<a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo URL_DOMAIN.$menu;?>" style="background:#fff;">
	    <div class="sidebar-brand-icon">
          <img src="<?php echo URL_IMAGE;?>logo2.png"  width="90%"/> 
        </div>
	</a> 
	<hr class="sidebar-divider my-0">
	<div class="text-white">
        <li class="nav-item">
			<a class="nav-link">
				<small><b>KONTEN</b></small>
			</a>
		</li>
		<li class="nav-item <?php if ($link=='upgrade') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/upgrade" title="Upgrade">
				<i class="fas fa-fw fa-star text-warning"></i><span class="text-warning">UPGRADE</span>
			</a>
		</li>
		<li class="nav-item <?php if ($link=='home') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/home" title="Beranda">
				<i class="fas fa-fw fa-home"></i><span>Beranda</span>
			</a>
		</li>		
		<li class="nav-item <?php if ($link=='cv') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/cv" title="Profil / CV">
				<i class="fas fa-fw fa-info"></i><span>Profil / CV</span>
			</a>
		</li>
		<li class="nav-item <?php if ($link=='blog') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/blog" title="Blog">
				<i class="fas fa-fw fa-edit"></i><span>Blog / Artikel</span>
			</a>
		</li>
		<li class="nav-item <?php if ($link=='gallery') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/gallery" title="Galeri">
				<i class="fas fa-fw fa-image"></i><span>Galeri Foto</span>
			</a>
		</li>
		<!-- <li class="nav-item <?php if ($link=='discussion') { echo 'active'; } ?>">
			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#discussion" aria-expanded="true" aria-controls="discussion">
				<i class="fas fa-fw fa-comments"></i><span>Tanya Jawab</span>
			</a>
			<div id="discussion" class="collapse <?php if ($link=='discussion') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded">
					<a href="<?php echo URL_DOMAIN.$menu;?>/discussion/0/unreplied" class="collapse-item <?php if ($act=='unreplied') { echo 'active'; } ?>" title="Belum Dibalas">Belum Dibalas</a>
					<a href="<?php echo URL_DOMAIN.$menu;?>/discussion/0/replied" class="collapse-item <?php if ($act=='replied') { echo 'active'; } ?>" title="Sudah Dibalas">Sudah Dibalas</a>
				</div>
			</div>
		</li> -->
		<li class="nav-item <?php if ($link=='volunteer') { echo 'active'; } ?>">
			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#volunteer" aria-expanded="true" aria-controls="volunteer">
				<i class="fas fa-fw fa-users"></i><span>Jadi Relawan</span>
			</a>
			<div id="volunteer" class="collapse <?php if ($link=='volunteer') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded">
					<a href="<?php echo URL_DOMAIN.$menu;?>/volunteer/0/request" class="collapse-item <?php if ($act=='request') { echo 'active'; } ?>" title="Request Relawan">Request Relawan</a>
					<a href="<?php echo URL_DOMAIN.$menu;?>/volunteer" class="collapse-item <?php if ($act=='') { echo 'active'; } ?>" title="Semua Relawan">Semua Relawan</a>
					<a href="<?php echo URL_DOMAIN.$menu;?>/volunteer/0/description" class="collapse-item <?php if ($act=='description') { echo 'active'; } ?>" title="Penjelasan Relawan">Penjelasan Relawan</a>
				</div>
			</div>
		</li>
		<!-- <li class="nav-item <?php if ($link=='donation') { echo 'active'; } ?>">
			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#donation" aria-expanded="true" aria-controls="donation">
				<i class="fas fa-fw fa-dollar-sign"></i><span>Donasi Online</span>
			</a>
			<div id="donation" class="collapse <?php if ($link=='donation') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded">
					<a href="<?php echo URL_DOMAIN.$menu;?>/donation/0/confirm" class="collapse-item <?php if ($act=='confirm') { echo 'active'; } ?>" title="Konfirmasi Donasi">Konfirmasi Donasi</a>
					<a href="<?php echo URL_DOMAIN.$menu;?>/donation/0/prospect" class="collapse-item <?php if ($act=='prospect') { echo 'active'; } ?>" title="Prospek Donasi">Prospek Donasi</a>
					<a href="<?php echo URL_DOMAIN.$menu;?>/donation/0/history" class="collapse-item <?php if ($act=='history') { echo 'active'; } ?>" title="Riwayat Donasi">Riwayat Donasi</a>
					<a href="<?php echo URL_DOMAIN.$menu;?>/donation/0/description" class="collapse-item <?php if ($act=='description') { echo 'active'; } ?>" title="Penjelasan Donasi">Penjelasan Donasi</a>
				</div>
			</div>
		</li> -->
		<li class="nav-item <?php if ($link=='contact') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/contact" title="Kontak">
				<i class="fas fa-fw fa-envelope"></i><span>Kontak Anda</span>
			</a>
		</li>
		<hr class="sidebar-divider my-0">
		<li class="nav-item">
			<a class="nav-link">
				<small><b>PENGATURAN</b></small>
			</a>
		</li>
		<li class="nav-item <?php if ($act=='partai') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/setting/0/parpol" title="Partai Politik">
				<i class="fas fa-fw fa-check"></i><span>Partai Politik</span>
			</a>
		</li>
		<!-- <li class="nav-item <?php if ($act=='theme') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/setting/0/theme" title="Tema Desain">
				<i class="fas fa-fw fa-paint-roller"></i><span>Tema Desain</span>
			</a>
		</li> -->
		<li class="nav-item <?php if ($act=='header') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/setting/0/header" title="Header & Footer">
				<i class="fas fa-fw fa-layer-group"></i><span>Header & Footer</span>
			</a>
		</li>
		<li class="nav-item <?php if ($act=='seo') { echo 'active'; } ?>">
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/setting/0/seo" title="SEO Google">
				<i class="fas fa-fw fa-eye"></i><span>SEO Google</span>
			</a>
		</li>
		<li class="nav-item <?php if ($link=='help') { echo 'active'; } ?>">
			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#help" aria-expanded="true" aria-controls="help">
				<i class="fas fa-fw fa-question"></i><span>Bantuan</span>
			</a>
			<div id="help" class="collapse <?php if ($link=='help') { echo 'show'; } ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded">
					<a href="<?php echo URL_DOMAIN.$menu;?>/help" class="collapse-item <?php if ($link=='help') { echo 'active'; } ?>" title="Pusat Bantuan">Pusat Bantuan</a>
					<a href="https://api.whatsapp.com/send?phone=<?php echo $mobile;?>" target="_blank" class="collapse-item" title="Konsultasi">Konsultasi</a>
				</div>
			</div>
		</li>
		<hr class="sidebar-divider my-0">
			</div>
	<div class="text-center d-none d-md-inline mt-3">
		<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>
	
</ul>