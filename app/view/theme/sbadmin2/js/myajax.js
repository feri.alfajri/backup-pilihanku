	var xmlHttp = buatObjekXmlHttp ();
	
	function buatObjekXmlHttp () {
		var obj=null;
		if (window.ActiveXObject) 
			obj=new ActiveXObject("Microsoft.XMLHTTP");
		else
			if (window.XMLHttpRequest) obj=new XMLHttpRequest();
		
		if (obj==null)  document.write("Browser tidak mendukung XMLHttpRequest"); return obj;
	}
	
	function ambilData (sumber_data, id_elemen) {
		if (xmlHttp!=null) {
			var obj=document.getElementById(id_elemen);
			xmlHttp.open("GET", sumber_data);
			xmlHttp.onreadystatechange=function () {
				if (xmlHttp.readyState==4 && xmlHttp.status==200) { obj.innerHTML=xmlHttp.responseText; }
			}
			xmlHttp.send(null);
		}
	}
	
	
	function cekLogin() {
		var username=document.getElementById('username').value;
		var password=document.getElementById('password').value;
		if(username==''){ alert('Masukan Username !'); return false; } 
		else if(password==''){ alert('Masukkan Password !'); return false; } 
	}


	function cekcari() {
		var kata_kunci=document.getElementById('kata_kunci').value;
		if(kata_kunci=='Kata Kunci'){ alert('Masukan Kata Kunci!'); return false; } 
	}
	
	function cekpassword() {
		var oldpass=document.getElementById('oldpass').value;
		var newpass=document.getElementById('newpass').value;
		var renewpass=document.getElementById('renewpass').value;
		if(oldpass==''){ alert('Masukan Password Lama !'); return false; } 
		else if(newpass==''){ alert('Masukkan Password Baru !'); return false; } 
		else if(renewpass==''){ alert('Masukkan Password Baru Sekali Lagi !'); return false; } 
	}
	
	function ceknama() {
		var nama=document.getElementById('nama').value;
		if(nama==''){ alert('Masukan Nama!'); return false; } 
	}

	function checkAll(){
		var frm = document.form_tabel;
		for (var i=0; i < frm.elements.length;i++){ frm.elements[i].checked = true;	}						
	}
	
	function uncheckAll(){
		var frm = document.form_tabel;
		for (var i=0; i < frm.elements.length;i++){ frm.elements[i].checked = false; }						
	}

	function konfirmHapus(){
		var frm = document.form_tabel;
		var status=0;
		for(var i=0;i < frm.elements.length;i++){ if(frm.elements[i].checked==true){ status++; } }
		if(status == frm.elements.length){ if(confirm("Apakah Anda ingin menghapus semua data ?")){ frm.submit(); } }
		else if(status == 0){ alert("Silahkan pilih data yang akan dihapus terlebih dahulu"); }
		else if(status < frm.elements.length){ if(confirm("Apakah Anda ingin menghapus data yang dipilih ?")){ frm.submit(); } }
	}
	
	function transaksi_bentuk(bentuk,id_elemen) {
		var elemen_bentuk=document.getElementById("bentuk");
		var bentuk=elemen_bentuk.value;
		var url="transaksi_bentuk.php?bentuk="+bentuk;
		ambilData(url,id_elemen);
	}
	
	function transaksi_mobil(mobil,id_elemen) {
		var elemen_mobil=document.getElementById("mobil");
		var mobil=elemen_mobil.value;
		var url="transaksi_mobil.php?mobil="+mobil;
		ambilData(url,id_elemen);
	}

	function transaksi_tarif(tarif,id_elemen) {
		var elemen_tarif=document.getElementById("tarif");
		var tarif=elemen_tarif.value;
		var url="transaksi_tarif.php?tarif="+tarif;
		ambilData(url,id_elemen);
	}	
	
	function transaksi_bookmobil(mobil,id_elemen) {
		var elemen_mobil=document.getElementById("mobil");
		var mobil=elemen_mobil.value;
		var url="transaksi_bookmobil.php?mobil="+mobil;
		ambilData(url,id_elemen);
	}
	
	function transaksi_booktarif(tarif,id_elemen) {
		var elemen_tarif=document.getElementById("tarif");
		var tarif=elemen_tarif.value;
		var url="transaksi_booktarif.php?tarif="+tarif;
		ambilData(url,id_elemen);
	}	
	
	
	function cektransaksikat() {
		var kodeakun=document.getElementById('kodeakun').value;
		var keterangan=document.getElementById('keterangan').value;
		var tanggal=document.getElementById('tanggal').value;
		var nilai=document.getElementById('nilai').value;
		if(kodeakun=='-'){ alert('Pilih Nama Akun !'); return false; } 
		else if(keterangan==''){ alert('Masukkan Keterangan !'); return false; } 
		else if(nilai==''){ alert('Masukkan Nilai Transaksi !'); return false; } 
		else if(tanggal==''){ alert('Pilih Tanggal Transaksi !'); return false; }		
	}
	
	function cektransaksihutpiut() {
		var kodeakun=document.getElementById('kodeakun').value;
		var keterangan=document.getElementById('keterangan').value;
		var tanggal_jatuhtempo=document.getElementById('tanggal_jatuhtempo').value;
		var tanggal=document.getElementById('tanggal').value;
		var nilai=document.getElementById('nilai').value;
		if(kodeakun=='-'){ alert('Pilih Nama Akun !'); return false; } 
		else if(keterangan==''){ alert('Masukkan Nama Kreditur / Debitur !'); return false; } 
		else if(nilai==''){ alert('Masukkan Nilai Transaksi !'); return false; } 
		else if(tanggal_jatuhtempo==''){ alert('Pilih Tanggal Jatuh Tempo !'); return false; }
		else if(tanggal==''){ alert('Pilih Tanggal Transaksi !'); return false; }
	}
	
	function cektransaksicicil() {
		var keterangan=document.getElementById('keterangan').value;
		var tanggal=document.getElementById('tanggal').value;
		var nilai=document.getElementById('nilai').value;
		if(keterangan==''){ alert('Masukkan Keterangan !'); return false; } 
		else if(nilai==''){ alert('Masukkan Nilai Transaksi !'); return false; } 
		else if(tanggal==''){ alert('Pilih Tanggal Transaksi !'); return false; }		
	}
	
	function lookup(inputString) {
		if(inputString.length == 0) { $('#suggestions').hide();	} 
		else {
			$.post("transaksi_jamaah.php", {queryString: ""+inputString+""}, function(data){
				if(data.length >0) {
					$('#suggestions').show();
					$('#autoSuggestionsList').html(data);
				}
			});
		}
	} 
	function fill(IdValue,NamaValue) {
		$('#IDString').val(IdValue);
		$('#NamaString').val(NamaValue);
		setTimeout("$('#suggestions').hide();", 200);
	}