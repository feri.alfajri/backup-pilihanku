 <div class="header-area header-area--default">

	<div class="header-bottom-wrap header-sticky">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="header default-menu-style position-relative">

						<div class="header__logo">
							<a href="<?php echo URL_DOMAIN;?>" title="<?php echo $nameweb;?>">
								<img src="<?php echo URL_IMAGE;?>logo.png" alt="Logo">
							</a>
						</div>
						<div class="header-midle-box">
							<div class="header-bottom-wrap d-md-block d-none">
								<div class="header-bottom-inner">
									<div class="header-bottom-left-wrap">
									</div>
								</div>
							</div>
						</div>
						<div class="header-right-box">
						
							<div class="header-right-inner" id="hidden-icon-wrapper">
								<div class="header__navigation d-none d-xl-block">
									<nav class="navigation-menu primary--menu">
										<ul>
											<li>
												<a href="<?php echo URL_DOMAIN;?>" title="Home"><span>Home</span></a>
											</li>
											<li>
												<a href="<?php echo URL_DOMAIN;?>explore" title="Search"><span>Daftar Caleg</span></a>
											</li>
											<!--
											<li>
												<a href="<?php echo URL_UTAMA;?>create" title="Buat Website"><span>Buat Website</span></a>
											</li>
											-->
										</ul>
									</nav>
								</div>

								<div class="language-menu btn">
									<ul>
										<li >
											<a href="<?php echo URL_DOMAIN.'twibbon';?>" title="Twibbon">
												<span class="text-white text-center m-0">Buat Twibbon <i class="fas fa-portrait"></i></span>
											</a>

											<!-- <a href="<?php echo URL_DOMAIN;?>member" title="Login"> -->
											    <!-- <?php
											    // if (empty($_SESSION['uname']) and empty($_SESSION['pword'])) {
											    //     ?>
												//     <span class="text-white">Login <i class="fa fa-sign-in-alt"></i></span>
												//     <?php
											    // }
											    // else {
											    //     ?>
												//     <span class="text-white">Ke Member Area <i class="fa fa-user"></i></span>
												//     <?php
											    // }
												?> -->
											<!-- </a> -->

										</li>
									</ul>
								</div>

							</div>

							<div class="mobile-navigation-icon d-block d-xl-none" id="mobile-menu-trigger">
								<i></i>
							</div>
							
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

</div>