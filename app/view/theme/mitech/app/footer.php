<div class="footer-area-wrapper bg-gray">
    <div class="footer-area section-space--ptb_60">
        <div class="container">
            <div class="row footer-widget-wrapper">
                <div class="col-lg-4 col-md-6 col-sm-6 footer-widget"> 
                    <div class="footer-widget__logo mb-20"> 
                        <img src="<?php echo URL_IMAGE;?>logo.png" style="margin-right:15px;" alt="logo oomah" width="180" align="left">
                        PilihanKu.net merupakan platform digital untuk mendukung event politik seperti pemilu dan pilkada di Indonesia. 
						Kami ingin menghadirkan budaya politik baru dengan meningkatkan partisipasi masyarakat melalui digitalisasi politik agar terlahir pemimpin-pemimpin yang siap membawa Indonesia lebih baik.


                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 footer-widget">
                    <h6 class="footer-widget__title mb-20">Layanan</h6>
                    <ul class="footer-widget__list">
                        <li><a class="hover-style-link" href="<?php echo URL_DOMAIN;?>product/website-kampanye" title="Website Kampanye"><span>Website Kampanye</span></a></li>
                    	<li><a class="hover-style-link" href="<?php echo URL_DOMAIN;?>product/flyer-digital" title="Flyer Digital"><span>Flyer Digital</span></a></li>
						<li><a class="hover-style-link" href="<?php echo URL_DOMAIN;?>product/kampanye-social-media" title="Kampanye Social Media"><span>Kampanye Social Media</span></a></li>
						<li><a class="hover-style-link" href="<?php echo URL_DOMAIN;?>product/aplikasi-caleg" title="Aplikasi Caleg"><span>Aplikasi Caleg</span></a></li>
					</ul>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 footer-widget">
                    <h6 class="footer-widget__title mb-20">Bantuan</h6>
                    <ul class="footer-widget__list">
                        <li><a class="hover-style-link" href="<?php echo URL_DOMAIN;?>blog" title="Blog"><span>Blog</span></a></li>
                    	<li><a class="hover-style-link" href="<?php echo URL_DOMAIN;?>help" title="Bantuan"><span>Bantuan</span></a></li>
                    	<li><a class="hover-style-link" href="<?php echo URL_DOMAIN;?>syarat-dan-ketentuan" title="Syarat dan Ketentuan"><span>Syarat dan Ketentuan</span></a></li>
                    	<li><a class="hover-style-link" href="<?php echo URL_DOMAIN;?>kebijakan-privasi" title="Kebijakan Privasi"><span>Kebijakan Privasi</span></a></li>
                    </ul>
                </div>
				<div class="col-lg-2 col-md-2 col-sm-6 footer-widget">
                    <h6 class="footer-widget__title mb-20">Profil</h6>
                    <ul class="footer-widget__list">
                        <li><a class="hover-style-link" href="<?php echo URL_DOMAIN;?>tentang-kami" title="Tentang Kami"><span>Tentang Kami</span></a></li>
                    	<li><a class="hover-style-link" href="<?php echo URL_DOMAIN;?>rekening" title="Rekening Resmi"><span>Rekening Resmi</span></a></li>
                    	<li><a class="hover-style-link" href="<?php echo URL_DOMAIN;?>affiliate" title="Program Affiliate"><span>Program Affiliate</span></a></li>
                    	<li><a class="hover-style-link" href="<?php echo URL_DOMAIN;?>contact" title="Kontak Kami"><span>Kontak Kami</span></a></li>
					</ul>
                </div>
				<div class="col-lg-2 col-md-2 col-sm-6 footer-widget">
                    <h6 class="footer-widget__title mb-20">Kontak</h6>
                    <ul class="footer-widget__list">
                        <li><?php echo $address.', '.$city;?></li>
                    	<li>Telp : <?php echo $phone;?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <div class="footer-copyright-area section-space--pb_30" style="border-top:1px solid #fff">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 text-center text-md-left">
                    <span class="copyright-text">&copy; 2022 <a href="https://www.budimark.com/" title="budimark.com">PilihanKu.net</a> - All Rights Reserved.</span>
                </div>
                <div class="col-md-6 text-center text-md-right">
                    <ul class="list ht-social-networks solid-rounded-icon">
                        <li class="item">
                            <a href="<?php echo $facebook;?>" target="_blank" aria-label="Facebook" class="social-link hint--bounce hint--top hint--primary">
                                <i class="fab fa-facebook-f link-icon"></i>
                            </a>
                        </li>
                        <li class="item">
                            <a href="<?php echo $instagram;?>" target="_blank" aria-label="Instagram" class="social-link hint--bounce hint--top hint--primary">
                                <i class="fab fa-instagram link-icon"></i>
                            </a>
                        </li>
                        <li class="item">
                            <a href="<?php echo $linkedin;?>" target="_blank" aria-label="Linkedin" class="social-link hint--bounce hint--top hint--primary">
                                <i class="fab fa-linkedin link-icon"></i>
                            </a>
                        </li>
                        <li class="item">
                            <a href="<?php echo $twitter;?>" target="_blank" aria-label="Twitter" class="social-link hint--bounce hint--top hint--primary">
                                <i class="fab fa-twitter link-icon"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
		<!--
        <div class="container small" style="color:#888888;">
			<p>PT. Bumi Tekno adalah sebuah industri kreatif yang bergerak dalam bidang teknologi informasi. Kami berdiri pada tahun 2015 di Kabupaten Temanggung. Meskipun lokasi kami terletak di daerah perdesaan, tetapi kami berisi sumber daya yang sangat berkompeten pada bidangnya, memiliki sertifikat serta portofolio yang sangat baik. Selain itu produk-produk kami yang telah digunakan oleh puluhan ribu pelanggan kami di seluruh Indonesia.</p>
			<p>Selain memiliki produk aplikasi yang siap pakai, kami juga memberikan layanan konsultansi IT. Kami akan membantu Anda dalam merancang aplikasi yang Anda butuhkan untuk memecahkan setiap masalah di institusi Anda. Kami akan membawa Anda ke dalam proses pengembangan aplikasi yang luar biasa, mulai dari melakukan perencanaan, desain, development, sampai implementasi aplikasi pada lembanga Anda. Dan yang paling penting, Anda dapat melihat dengan sendiri perubahan, pertumbuhan, dan perkembangan dari masalah Anda menjadi hal mudah, menyenangkan, dan dapat meningkatkan keuntungan. Pengalaman inilah yang membuat kami dipercaya oleh ribuan pelanggan setia kami hingga saat ini.</p>
			<p>Perusahaan kami memiliki slogan "Make IT Simple But Significant". Artinya kami ingin membuat masalah yang rumit menjadi sederhana. Tetapi yang terjadi justru sebaliknya, banyak orang menganggap bahwa penerapan teknologi membuat pekerjaan menjadi lebih rumit, menambah pekerjaan baru, dan menjadi tidak efektif efesien. Kami sangat memahami bahwa perubahan itu tidak hanya pada teknologinya, tetapi juga harus dibarengi dengan sumberdaya manusianya, budaya kerjanya, serta sistemnya. Oleh karena itu, kami selalu membantu mitra kami dengan pendidikan, pelatihan, serta pendampingan ketika implementasi teknologi pada lembaganya. Dan yang paling penting adalah spirit significant, yaitu teknologi yang kami kembangkan harapannya membawa perubahan yang significant, bertumbuh dan berkembang. Oleh karena itu, kami selalu memonitor before dan afternya dengan menyajikan dashboard yang mudah, sederhana, dan mengena. </p>
		</div>
		-->
    </div>
    
</div>

<a href="#" class="scroll-top" id="scroll-top">
    <i class="arrow-top fal fa-long-arrow-up"></i>
    <i class="arrow-bottom fal fa-long-arrow-up"></i>
</a>

<?php include 'mobilemenu.php';?>