 <div class="header-area header-area--default" style="border-bottom:1px solid #EAEAEA; background:#FAFAFA;">

	<div class="header-bottom-wrap">
		<div class="container-fluid">
			<?php
            if (SUBDOMAIN=='emasjid') { $logo_top = 'logo-emasjid.png'; }
            else { $logo_top = 'logo.png'; }
            ?>
			<div class="header default-menu-style position-relative ">
				<div style="display:table;width:100%">
					<div style="float:left">
						<a href="<?php echo URL_UTAMA;?>" title="Logo">
							<img src="<?php echo URL_IMAGE;?><?php echo $logo_top;?>" style="height:28px;margin-top:10px;margin-bottom:10px;" alt="Logo">
						</a>
					</div>
					<div style="float:right;text-align:right">
						<nav class="d-md-block d-none">
							<ul>
								<li style="float:left;padding:10px 8px;">
									<a href="<?php echo URL_UTAMA;?>explore" title="Explore"><span>Explore</span></a>
								</li>
								<li style="float:left;padding:10px 8px;">
									<a href="<?php echo URL_UTAMA;?>create" title="Buat Website"><span>Buat Website</span></a>
								</li>
								<!-- <li style="float:left;padding:10px 8px;">
									<a href="<?php echo URL_UTAMA;?>member" title="Buat Website">
										<span>Login <i class="fa fa-sign-in-alt"></i></span>
									</a>
								</li> -->
							</ul>
						</nav>
						<nav class="d-xl-none">
							<ul>
								<li style="float:left;padding:10px 8px;">
									<a href="<?php echo URL_UTAMA;?>explore" title="Explore">
										<span><i class="fa fa-search"></i></span>
									</a>
								</li>
								<li style="float:left;padding:10px 8px;">
									<a href="<?php echo URL_UTAMA;?>create" title="Buat Website">
										<span><i class="fa fa-sign-in-alt"></i></span>
									</a>
								</li>
							</ul>
						</nav>
						
					</div>
				</div>
			</div>
			
		</div>
	</div>

</div>