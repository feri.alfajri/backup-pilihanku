<div class="mobile-menu-overlay" id="mobile-menu-overlay">
    <div class="mobile-menu-overlay__inner  bg-secondary">
        
        <div class="mobile-menu-overlay__header">
            <div class="container-fluid">
                <div class="row align-items-center"> 
                    <div class="col-md-6 col-8">
                        <div class="logo">
                            <a href="<?php echo URL_DOMAIN;?>" title="<?php echo $nameweb;?>">
                                <img src="<?php echo URL_IMAGE;?>logo.png" class="img-fluid" alt="Logo Bumitekno">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 col-4">
                        <div class="mobile-menu-content text-right">
                            <span class="mobile-navigation-close-icon" id="mobile-menu-close-trigger"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="mobile-menu-overlay__body">
            <nav class="offcanvas-navigation offcanvas-navigation--onepage">
                <ul>
                    
                    <li><a href="<?php echo URL_DOMAIN;?>" title="Home">Home</a></li>
					<li><a href="<?php echo URL_DOMAIN;?>profile" title="Explore">Profil</a></li>                    
                    <li><a href="<?php echo URL_DOMAIN;?>gallery" title="Galeri">Galeri</a></li>
                    <li><a href="<?php echo URL_DOMAIN;?>blog" title="Blog">Blog</a></li>
                    <li><a href="<?php echo URL_DOMAIN;?>discusstion" title="Tanya Jawab">Tanya Jawab</a></li>
                    <li><a href="<?php echo URL_DOMAIN;?>volunteer" title="Jadi Relawan">Jadi Relawan</a></li>
                    <li><a href="<?php echo URL_DOMAIN;?>donation" title="Donasi">Donasi</a></li>
                    <li><a href="<?php echo URL_DOMAIN;?>contact" title="Kontak">Kontak</a></li>
                </ul>
            </nav>
        </div>
        
    </div>
</div>