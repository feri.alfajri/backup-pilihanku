<style type="text/css">
.logo_image { width:120px; border-radius:50%; -moz-border-radius:50%; -webkit-border-radius:50%; text-align:left; }
.logo_head { font-size:40px; line-height:44px; text-align:left; }
.logo_slogan1 { font-size:18px; color:#888; text-align:left; }
@media only screen and (max-width:480px) {
	.logo_image { width:80px; border-radius:50%; -moz-border-radius:50%; -webkit-border-radius:50%; text-align:center; }
	.logo_head { font-size:22px; line-height:30px line-height:20px; }
	.logo_slogan1 { font-size:15px; color:#888;  }
}
</style>

<div class="header-area bg-white border-bottom">
	<div class="container">
		<div class="row">
			<div class="col-9">
				<div class="pt-4 pb-4">	
					<div class="row">
						<div class="col-md-2">
							<a href="<?php echo URL_DOMAIN;?>" title="<?php echo $nameweb;?>">
								<img src="<?php echo URL_UPLOAD;?>image.php/<?php echo $logo;?>?width=120&cropratio=1:1&nocache&quality=100&image=<?php echo URL_PICTURE.$logo;?>" class="logo_image" alt="Logo">
							</a>
						</div>
						<div class="col-md-10">
							<a href="<?php echo URL_DOMAIN;?>" title="<?php echo $nameweb;?>">
								<h1 class="logo_head"><?php echo $nameweb;?></h1>
								<p class="logo_slogan1">Calon Anggota <?php echo $tingkat.' Dapil ' .$dapil;?></p>
								<h5 style="font-size:18px;font-style:italic" class="mhide">"<?php echo $slogan;?>"</h5>
							</a>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-3">
				<div class="pt-5">	
					<div class="mobile-navigation-icon d-block d-xl-none" id="mobile-menu-trigger">
						<i></i>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>


<div class="header-bottom-wrap border-bottom d-md-block d-none header-sticky" style="background:#FAFAFA;">
	<div class="container">
	
		<div class="header-bottom-inner">
		
			<div class="header-bottom-left-wrap">
				<div class="header__navigation d-none d-xl-block">
					<nav class="navigation-menu">
						<ul>
							<li><a href="<?php echo URL_DOMAIN;?>" title="Beranda"><span>Beranda</span></a></li>
							<li><a href="<?php echo URL_DOMAIN;?>profile" title="Profil"><span>Profil</span></a></li>
							<li><a href="<?php echo URL_DOMAIN;?>gallery" title="Galeri"><span>Galeri</span></a></li>
							<li><a href="<?php echo URL_DOMAIN;?>blog" title="Artikel"><span>Artikel</span></a></li>
							<li><a href="<?php echo URL_DOMAIN;?>discussion" title="Tanya Jawab"><span>Tanya Jawab</span></a></li>
							<li><a href="<?php echo URL_DOMAIN;?>volunteer" title="Jadi Relawan"><span>Jadi Relawan</span></a></li>
							<li><a href="<?php echo URL_DOMAIN;?>donation" title="Donasi"><span>Donasi</span></a></li>
							<li><a href="<?php echo URL_DOMAIN;?>contact" title="Kontak"><span>Kontak</span></a></li>
						</ul>
					</nav>
				</div>
			</div>

			<div class="header-search-form">
				<div class="header-social-networks style-icons">
					<div class="inner">
						<a class=" social-link hint--bounce hint--bottom-left" aria-label="Facebook" href="<?php echo $facebook;?>" data-hover="Facebook" target="_blank">
							<i class="social-icon fab fa-facebook-f"></i>
						</a>
						<a class=" social-link hint--bounce hint--bottom-left" aria-label="Twitter" href="<?php echo $twitter;?>" data-hover="Twitter" target="_blank">
							<i class="social-icon fab fa-twitter"></i>
						</a>						
						<a class=" social-link hint--bounce hint--bottom-left" aria-label="Instagram" href="<?php echo $instagram;?>" data-hover="Instagram" target="_blank">
							<i class="social-icon fab fa-instagram"></i>
						</a>
						<a class=" social-link hint--bounce hint--bottom-left" aria-label="Youtube" href="<?php echo $youtube;?>" data-hover="Youtube" target="_blank">
							<i class="social-icon fab fa-youtube"></i>
						</a>
					</div>
				</div>
			</div>
			
		</div>

	</div>
</div>

