<meta http-equiv="content-type" content="text/html; charset=utf-8" /> 
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet" href="<?php echo URL_THEME;?>assets/css/vendor/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo URL_THEME;?>assets/css/vendor/cerebrisans.css">
<link rel="stylesheet" href="<?php echo URL_THEME;?>assets/css/vendor/fontawesome-all.min.css">
<link rel="stylesheet" href="<?php echo URL_THEME;?>assets/css/plugins/swiper.min.css">
<link rel="stylesheet" href="<?php echo URL_THEME;?>assets/css/plugins/animate-text.css">
<link rel="stylesheet" href="<?php echo URL_THEME;?>assets/css/plugins/animate.min.css">
<link rel="stylesheet" href="<?php echo URL_THEME;?>assets/css/plugins/lightgallery.min.css">
<link rel="stylesheet" href="<?php echo URL_THEME;?>assets/css/style.css">
<link rel="stylesheet" href="<?php echo URL_THEME;?>assets/css/mystyle.css">

