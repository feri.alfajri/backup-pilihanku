<nav class="topnav navbar navbar-expand shadow justify-content-between justify-content-sm-start navbar-light bg-white" id="sidenavAccordion">
		
		<button class="btn btn-icon btn-transparent-dark order-1 order-lg-0 me-2 ms-lg-2 me-lg-0" id="sidebarToggle"><i data-feather="menu"></i></button>
		
		<a class="navbar-brand pe-3 ps-4 ps-lg-2" href="<?php echo URL_DOMAIN.$menu;?>">SuperAdmin</a>
		
		
		<form class="form-inline me-auto d-none d-lg-block me-3">
			<div class="input-group input-group-joined input-group-solid">
				<input class="form-control pe-0" type="search" placeholder="Search" aria-label="Search" />
				<div class="input-group-text"><i data-feather="search"></i></div>
			</div>
		</form>
		
		
		<ul class="navbar-nav align-items-center ms-auto">
			
			<li class="nav-item dropdown no-caret me-3 d-lg-none">
				<a class="btn btn-icon btn-transparent-dark dropdown-toggle" id="searchDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="search"></i></a>
				<div class="dropdown-menu dropdown-menu-end p-3 shadow animated--fade-in-up" aria-labelledby="searchDropdown">
					<form class="form-inline me-auto w-100">
						<div class="input-group input-group-joined input-group-solid">
							<input class="form-control pe-0" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
							<div class="input-group-text"><i data-feather="search"></i></div>
						</div>
					</form>
				</div>
			</li>
			
			<?php 
			include 'notification.php';
			if ($_SESSION['pict']=='') { $pictuser = 'member.png'; }  else { $pictuser = $_SESSION['pict']; }
			?>
			
			<li class="nav-item dropdown no-caret dropdown-user me-3 me-lg-4">
				<a class="btn btn-icon btn-transparent-dark dropdown-toggle" id="navbarDropdownUserImage" href="javascript:void(0);" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<img class="img-fluid" src="<?php echo URL_PICTURE.$pictuser;?>" />
				</a>
				<div class="dropdown-menu dropdown-menu-end border-0 shadow animated--fade-in-up" aria-labelledby="navbarDropdownUserImage">
					<h6 class="dropdown-header d-flex align-items-center">
						<?php
						
						?>
						<img class="dropdown-user-img" src="<?php echo URL_PICTURE.$pictuser;?>" />
						<div class="dropdown-user-details">
							<div class="dropdown-user-details-name"><?php echo $_SESSION['name'];?></div>
							<div class="dropdown-user-details-email"><?php echo $_SESSION['uname'];?></div>
						</div>
					</h6>
					<div class="dropdown-divider"></div>
					
					<a class="dropdown-item" href="<?php echo URL_DOMAIN.$menu;?>/account/0/profile">
						<div class="dropdown-item-icon"><i data-feather="user"></i></div>
						Profile
					</a>
					<a class="dropdown-item" href="<?php echo URL_DOMAIN.$menu;?>/account/0/password">
						<div class="dropdown-item-icon"><i data-feather="lock"></i></div>
						Password
					</a>
					<a class="dropdown-item" href="<?php echo URL_DOMAIN.$menu;?>/account/0/avatar">
						<div class="dropdown-item-icon"><i data-feather="image"></i></div>
						Avatar
					</a>
					<a class="dropdown-item" href="<?php echo URL_DOMAIN;?>/logout" onclick="return confirm('Yakin Keluar ?');">
						<div class="dropdown-item-icon"><i data-feather="log-out"></i></div>
						Logout
					</a>
				</div>
			</li>
		</ul>
	</nav>