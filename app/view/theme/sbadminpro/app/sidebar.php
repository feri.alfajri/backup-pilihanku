<nav class="sidenav shadow-right sidenav-light">
	<div class="sidenav-menu">
		<div class="nav accordion" id="accordionSidenav">
		
			<!-- If Mobile menu -->
			<div class="sidenav-menu-heading d-sm-none">Account</div>
			<a class="nav-link d-sm-none" href="#!">
				<div class="nav-link-icon"><i data-feather="bell"></i></div>
				Alerts
				<span class="badge bg-warning-soft text-warning ms-auto">4 New!</span>
			</a>
			
			<!-- PRODUK & USER -->
			<div class="sidenav-menu-heading">Produk & User</div>
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>">
				<div class="nav-link-icon"><i data-feather="compass"></i></div>
				Dashboard
			</a>
			<a class="nav-link collapsed" href="javascript:void(0);" data-bs-toggle="collapse" data-bs-target="#website" aria-expanded="false" aria-controls="website">
				<div class="nav-link-icon"><i data-feather="globe"></i></div>
				Website
				<div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
			</a>
			<div class="collapse" id="website" data-bs-parent="#accordionSidenav">
				<nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPagesMenu">
					<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/website/0/custom" title="Custom">Custom</a>
					<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/website/0/pro" title="Profesional">Profesional</a>
					<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/website/0/free" title="Free">Free</a>
					<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/website" title="Semua">Semua</a>
				</nav>
			</div>
			<a class="nav-link collapsed" href="javascript:void(0);" data-bs-toggle="collapse" data-bs-target="#flyer" aria-expanded="false" aria-controls="flyer">
				<div class="nav-link-icon"><i data-feather="file-text"></i></div>
				Flyer Online
				<div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
			</a>
			<div class="collapse" id="flyer" data-bs-parent="#accordionSidenav">
				<nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPagesMenu">
					<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/flyer" title="Flyer">Flyer</a>
					<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/flyer_theme" title="Tema">Tema</a>
				</nav>
			</div>
			<a class="nav-link collapsed" href="javascript:void(0);" data-bs-toggle="collapse" data-bs-target="#member" aria-expanded="false" aria-controls="member">
				<div class="nav-link-icon"><i data-feather="users"></i></div>
				Member
				<div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
			</a>
			<div class="collapse" id="member" data-bs-parent="#accordionSidenav">
				<nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPagesMenu">
					<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/member/0/active" title="Aktif">Aktif</a>
					<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/member/0/passive" title="Non Aktif">Non Aktif</a>
				</nav>
			</div>
			<a class="nav-link collapsed" href="javascript:void(0);" data-bs-toggle="collapse" data-bs-target="#agent" aria-expanded="false" aria-controls="agent">
				<div class="nav-link-icon"><i data-feather="user-check"></i></div>
				Mitra
				<div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
			</a>
			<div class="collapse" id="agent" data-bs-parent="#accordionSidenav">
				<nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPagesMenu">
					<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/agent/0/cabang" title="Cabang">Cabang</a>
					<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/agent/0/mitra" title="Mitra">Mitra</a>
				</nav>
			</div>			
			<!-- KONTEN USER -->
			<div class="sidenav-menu-heading">Konten User</div>
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/home" title="Home">
				<div class="nav-link-icon"><i data-feather="home"></i></div>Beranda
			</a>
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/profile" title="Profile">
				<div class="nav-link-icon"><i data-feather="info"></i></div>Profil / CV
			</a>
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/article" title="Artikel">
				<div class="nav-link-icon"><i data-feather="edit"></i></div>Artikel
			</a>
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/gallery" title="Gallery">
				<div class="nav-link-icon"><i data-feather="image"></i></div>Galeri
			</a>
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/discussion" title="Tanya Jawab">
				<div class="nav-link-icon"><i data-feather="message-square"></i></div>Tanya Jawab
			</a>
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/volunteer" title="Relawan">
				<div class="nav-link-icon"><i data-feather="users"></i></div>Relawan
			</a>
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/donation" title="Donasi">
				<div class="nav-link-icon"><i data-feather="gift"></i></div>Donasi
			</a>
			<!-- KONTEN UTAMA -->
			<div class="sidenav-menu-heading">Konten PilihanKu.net</div>
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/menu" title="Menu">
				<div class="nav-link-icon"><i data-feather="grid"></i></div>Menu
			</a>
			<a class="nav-link collapsed" href="javascript:void(0);" data-bs-toggle="collapse" data-bs-target="#blog" aria-expanded="false" aria-controls="blog">
				<div class="nav-link-icon"><i data-feather="edit"></i></div>
				Blog 
				<div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
			</a>
			<div class="collapse" id="blog" data-bs-parent="#accordionSidenav">
				<nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPagesMenu">
					<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/blog" title="Blog">Blog</a>
					<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/blog_category" title="Kategori">Kategori</a>
					<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/blog_comment" title="Komentar">Komentar</a>
				</nav>
			</div>
			<a class="nav-link collapsed" href="javascript:void(0);" data-bs-toggle="collapse" data-bs-target="#help" aria-expanded="false" aria-controls="help">
				<div class="nav-link-icon"><i data-feather="help-circle"></i></div>
				Bantuan 
				<div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
			</a>
			<div class="collapse" id="help" data-bs-parent="#accordionSidenav">
				<nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPagesMenu">
					<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/help" title="Bantuan">Bantuan</a>
					<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/help_category" title="Kategori">Kategori</a>
				</nav>
			</div>
			<a class="nav-link collapsed" href="javascript:void(0);" data-bs-toggle="collapse" data-bs-target="#master" aria-expanded="false" aria-controls="master">
				<div class="nav-link-icon"><i data-feather="settings"></i></div>
				Master 
				<div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
			</a>
			<div class="collapse" id="master" data-bs-parent="#accordionSidenav">
				<nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPagesMenu">
					<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/partai" title="Partai">Partai</a>
					<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/theme" title="Theme Website">Theme Website</a>
				</nav>
			</div>
			<a class="nav-link" href="<?php echo URL_DOMAIN.$menu;?>/user" title="Menu">
				<div class="nav-link-icon"><i data-feather="user"></i></div>User
			</a>
			&nbsp;
		</div>
	</div>
</nav>