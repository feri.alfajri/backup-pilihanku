<header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
	<div class="container-xl px-4">
		<div class="page-header-content">
			<div class="row align-items-center justify-content-between pt-3">
				<div class="col-auto mb-3">
					<b><?php echo $titlepage;?></b>
				</div>
				<div class="col-12 col-xl-auto"></div>
			</div>
		</div>
	</div>
</header>