<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="<?php echo URL_IMAGE;?>favicon.png" rel="shortcut icon" type="image/x-icon">

<link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
<link href="<?php echo URL_THEME.THEME_SUPER;?>/css/styles.css" rel="stylesheet" />
<link href="<?php echo URL_THEME.THEME_SUPER;?>/css/mystyle.css" rel="stylesheet">
<script src="<?php echo URL_THEME.THEME_SUPER;?>/js/ajaxadmin.js"></script>
<script src="<?php echo URL_THEME.THEME_SUPER;?>/js/nicEdit.js"></script>
<script data-search-pseudo-elements defer src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/all.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js" crossorigin="anonymous"></script>
<script type="text/javascript">bkLib.onDomLoaded(function() { new nicEditor({fullPanel : true}).panelInstance('editor');});</script>
<title><?php echo $titlepage;?></title> 