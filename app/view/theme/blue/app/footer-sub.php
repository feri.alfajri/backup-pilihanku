<div class="footer-area-wrapper bg-gray">    
    <div class="footer-copyright-area section-space--ptb_30">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 text-center text-md-left">
                    <span class="copyright-text">&copy; 2022 <a href="https://www.pilihanku.net/" title="PilihanKu.net">PilihanKu.net</a> - All Rights Reserved.</span>
                </div>
                <div class="col-md-6 text-center text-md-right">
                    <ul class="list ht-social-networks solid-rounded-icon">
                        <li class="item">
                            <a href="<?php echo $facebook;?>" target="_blank" aria-label="Facebook" class="social-link hint--bounce hint--top hint--primary">
                                <i class="fab fa-facebook-f link-icon"></i>
                            </a>
                        </li>
                        <li class="item">
                            <a href="<?php echo $instagram;?>" target="_blank" aria-label="Instagram" class="social-link hint--bounce hint--top hint--primary">
                                <i class="fab fa-instagram link-icon"></i>
                            </a>
                        </li>
                        <li class="item">
                            <a href="<?php echo $youtube;?>" target="_blank" aria-label="Youtube" class="social-link hint--bounce hint--top hint--primary">
                                <i class="fab fa-youtube link-icon"></i>
                            </a>
                        </li>
                        <li class="item">
                            <a href="<?php echo $twitter;?>" target="_blank" aria-label="Twitter" class="social-link hint--bounce hint--top hint--primary">
                                <i class="fab fa-twitter link-icon"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
</div>

<a href="#" class="scroll-top" id="scroll-top">
    <i class="arrow-top fal fa-long-arrow-up"></i>
    <i class="arrow-bottom fal fa-long-arrow-up"></i>
</a>

<?php include 'mobilemenu-sub.php';?>