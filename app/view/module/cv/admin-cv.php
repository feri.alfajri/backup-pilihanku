<!DOCTYPE html> 
<html lang="en">
    
<head>

    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
	
</head>

<body>
    
	<div id="wrapper"> 
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>
		
		<div id="content-wrapper"> 
		    
			<div id="content">
			
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					<?php
					
					function menu_admin ($menu,$link,$act) {
					    ?>
					    <div style="display:table;width:100%;margin-top:10px; margin-bottom:-8px;">
    						<?php  if ($act=='') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
    						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>" class="btn <?php echo $btnstyle;?> ml-3 mr-2 pb-3 text-white" title="Curicullum Vitae">Curicullum Vitae</a>
    						<?php  if ($act=='editbio') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
    						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>/0/editbio" class="btn <?php echo $btnstyle;?> mr-2 pb-3 text-white" title="Biodata">Biodata</a>
    						<?php  if ($act=='editedu') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
    						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>/0/editedu" class="btn <?php echo $btnstyle;?> mr-2 pb-3 text-white" title="Pendidikan">Pendidikan</a>
    						<?php  if ($act=='editorg') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
    						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>/0/editorg" class="btn <?php echo $btnstyle;?> mr-2 pb-3 text-white" title="Organisasi">Organisasi</a>
    						<?php  if ($act=='editwork') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
    						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>/0/editwork" class="btn <?php echo $btnstyle;?> mr-2 pb-3 text-white" title="Pekerjaan">Pekerjaan</a>
    						<?php  if ($act=='editaward') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
    						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>/0/editaward" class="btn <?php echo $btnstyle;?> mr-2 pb-3 text-white" title="Penghargaan">Penghargaan</a>
    					</div>
					    <?php
					}
					
					function cv () {
					    include DIR_MODEL.'cv.php';
	
                        $cvModel 		= new cv; 
                    	$cvDetail 		= $cvModel -> getData();
                    	$count 			= $cvModel -> count;
                    	
                    	$name			= $cvDetail['name'];
                    	$birth			= $cvDetail['birth'];
                    	$address		= $cvDetail['address'];
                    	$phone			= $cvDetail['phone'];
                    	$education		= $cvDetail['education'];
                    	$organization	= $cvDetail['organization'];
                    	$training		= $cvDetail['training'];
                    	$work			= $cvDetail['work'];
                    	$award			= $cvDetail['award'];
                    	$profile		= $cvDetail['profile'];
                    	$picture		= $cvDetail['picture'];
					    ?>
					    <div class="card">
					        <div class="card-body">
        					    <h4 class="mb-10"><?php echo $name;?></h4>
            					<div class="row">						
            						<?php
            						if ($picture=="") { $col="col-md-12"; }
            						else {
            							$col="col-md-10"; ?>
            							<div class="col-md-2">
            								<img src="<?php echo URL_UPLOAD;?>image.php/<?php echo $picture;?>?width=200&cropratio=5:6&nocache&quality=100&image=<?php echo URL_PICTURE.$picture;?>" alt="foto" width="100%"/>					
            							</div><?php
            						} ?>
            						<div class="<?php echo $col;?>">							
            							<div id="view">
            								<div class="view_label">Tempat Tanggal Lahir</div><div class="view_dot">:</div><div class="view_content"><?php echo $birth;?></div>
            							</div>
            							<div id="view">
            								<div class="view_label">Alamat</div><div class="view_dot">:</div><div class="view_content"><?php echo $address;?></div>
            							</div>
            							<div id="view">
            								<div class="view_label">No. Telepon</div><div class="view_dot">:</div><div class="view_content"><?php echo $phone;?></div>
            							</div>
            							<div id="view">
            								<div class="view_label">Email</div><div class="view_dot">:</div><div class="view_content"><?php echo $email;?></div>
            							</div>
            						</div>
            					</div>
            					<br/>
            					<h5>Profil</h5>
            					<p>
            					<?php echo $profile;?>
            					</p>
            					<div id="view">
            						<div class="view_label"><b>Riwayat Pendidikan</b></div><div class="view_dot">:</div><div class="view_content"><?php echo $education;?></div>
            					</div>
            					<div id="view">
            						<div class="view_label"><b>Riwayat Organisasi</b></div><div class="view_dot">:</div><div class="view_content"><?php echo $organization;?></div>
            					</div>
            					<div id="view">
            						<div class="view_label"><b>Riwayat Pelatihan</b></div><div class="view_dot">:</div><div class="view_content"><?php echo $training;?></div>
            					</div>
            					<div id="view">
            						<div class="view_label"><b>Riwayat Pekerjaan</b></div><div class="view_dot">:</div><div class="view_content"><?php echo $work;?></div>
            					</div>
            					<div id="view">
            						<div class="view_label"><b>Riwayat Penghargaan</b></div><div class="view_dot">:</div><div class="view_content"><?php echo $award;?></div>
            					</div>
            				</div>
    					</div>
    					<?php
					}
					
					
					$uname = $_SESSION['uname'];
					$table_name = 'cv';							
					$update_type = '';	
					$update_label = 'nama lengkap,TTL,alamat,telepon,email';
					$update_form = 'name,birth,address,phone,email';					
					$update_condition = "WHERE subdomain='".SUBDOMAIN."'";							
					
					if (empty ($act)) {
						
						$admin->title('Ubah CV');
						menu_admin ($menu,$link,$act);
						cv ();
					
					}
					elseif ($act=="editbio") {
						$admin->title('Ubah CV');
						menu_admin ($menu,$link,$act);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);		
					}
					elseif ($act=="editedu") {
						$admin->title('Ubah Riwayat Pendidikan');
						$update_label = 'Riwayat Pendidikan';
						$update_form = 'education';		
						menu_admin ($menu,$link,$act);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);	
					}
					elseif ($act=="editorg") {
						$admin->title('Ubah Riwayat Organisasi');
						$update_label = 'Riwayat Organisasi';
						$update_form = 'organization';	
						menu_admin ($menu,$link,$act);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);	
					}
					elseif ($act=="editaward") {
						$admin->title('Ubah Riwayat Penghargaan');
						$update_label = 'Riwayat Penghargaan';
						$update_form = 'award';		
						menu_admin ($menu,$link,$act);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);	
					}
					elseif ($act=="editwork") {
						$admin->title('Ubah Riwayat Pekerjaan');
						$update_label = 'Riwayat Pekerjaan';
						$update_form = 'work';	
						menu_admin ($menu,$link,$act);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);	
					}
					else {
						
						$admin->title('Ubah CV');
						menu_admin ($menu,$link,$act);
						cv ();
						
					}
					?>
                    
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>

</html>