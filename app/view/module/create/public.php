<!DOCTYPE html>
<html dir="ltr" lang="id-ID">
    
<head><?php
    include (DIR_THEME.THEME.'/app/asset-head.php');
    include (DIR_THEME.THEME.'/app/metatag.php'); 
    include (DIR_THEME.THEME.'/app/opengraph.php');
    include (DIR_THEME.THEME.'/app/favicon.php');
    include (DIR_THEME.THEME.'/app/title.php');
    ?>
</head>

<body>
	<?php
	include (DIR_THEME.THEME.'/app/loader.php');
	include (DIR_THEME.THEME.'/app/header.php');
	include DIR_LIBRARY . 'class.phpmailer.php';

	//include 'problem.php';
	//include 'about.php';
	//include 'feature.php';
	//include 'gallery.php';
	//include 'testimonial.php';
	//include 'cta.php';
	?>
		<?php
		if ($act=='' OR $act=='check') {?>
			<div class="feature-images-wrapper section-space--ptb_60 bg-gray">
				<div class="container ">
					<div class="pricing-table-title-area position-relative">
						<div class="container">
							<div class="section-title-wrapper text-center section-space--mb_60 wow move-up">
								<h3 class="heading">Step 1 : Tentukan Nama Domain Profil Anda</h3>
							</div>
						</div>
					</div>

					<div class="row text-center">
						<div class="col-lg-3 ofset"></div>
						<div class="col-lg-6">
							<form action="<?php echo URL_DOMAIN . $menu . '/' . $link . '1/check'; ?>" method="post">
								<input name='cek' value='domain' hidden>
								<input name='paket' value='<?php echo $paket; ?>' hidden>
								<div class="widget-search mb-20">
									<input name="domain" class="form-control bg-white border" type="text" placeholder="Silahkan Cek Domain Website…" required>
									<button class="search-submit" style="width:100px;cursor:default">.pilihanku.net</button>
								</div>
								<button type="submit" name="submit" class="ht-btn ht-btn-md btn--secondary">CEK
									DOMAIN</button>
							</form>
							<?php
							if (empty($_POST['cek'])) {  }
							else{
								$subdomain=strip_tags($_POST['domain']);	$subdomain=strtolower($subdomain);
								$subdomain=str_replace('"','',$subdomain); $subdomain=str_replace("'","",$subdomain); $subdomain=str_replace(" ","",$subdomain);
								$subdomain=str_replace("#","","$subdomain"); $subdomain=str_replace("~","","$subdomain"); $subdomain=str_replace("`","","$subdomain");
								$subdomain=str_replace("!","","$subdomain"); $subdomain=str_replace("@","","$subdomain"); $subdomain=str_replace("#","","$subdomain");
								$subdomain=str_replace("$","","$subdomain"); $subdomain=str_replace("%","","$subdomain"); $subdomain=str_replace("^","","$subdomain");
								$subdomain=str_replace("&","","$subdomain"); $subdomain=str_replace("*","","$subdomain"); $subdomain=str_replace("(","","$subdomain");
								$subdomain=str_replace(")","","$subdomain"); $subdomain=str_replace("_","","$subdomain"); $subdomain=str_replace("+","","$subdomain");
								$subdomain=str_replace("=","","$subdomain"); $subdomain=str_replace("|","","$subdomain"); $subdomain=str_replace("{","","$subdomain");
								$subdomain=str_replace("}","","$subdomain"); $subdomain=str_replace("[","","$subdomain"); $subdomain=str_replace("]","","$subdomain");
								$subdomain=str_replace(":","","$subdomain"); $subdomain=str_replace(";","","$subdomain"); $subdomain=str_replace("'","","$subdomain");
								$subdomain=str_replace(">","","$subdomain"); $subdomain=str_replace("<","","$subdomain"); $subdomain=str_replace("?","","$subdomain");
								$subdomain=str_replace(",","","$subdomain"); $subdomain=str_replace(".","","$subdomain"); $subdomain=str_replace("/","","$subdomain");
								$panjangdomain=strlen($subdomain);
								
								if ($panjangdomain<5) {
									$respon='warning';
									$domainresmi=$subdomain.'.pilihanku.net';
								}
								else {
								
										$qceksub=mysqli_query($model->koneksi, "SELECT no FROM setting WHERE subdomain='$subdomain'");
										$jumceksub=mysqli_num_rows($qceksub);
										$domainresmi=$subdomain.'.pilihanku.net';
										//$jumceksub=0;
										if ($jumceksub==0) {
											$respon='success';
										}
										else {
											$respon='failed';
										}
								}


								if ($respon=='warning') {?>
									<div class="ht-message-box style-error mt-30" role="alert">
										<h6 color="#FF0000">Maaf, <?php echo $domainresmi; ?> Terlalu Pendek. Minimal 5
											Karakter.<br />Silahkan coba nama domain yang lain.</h6>
									</div>
									<?php
								}
								elseif ($respon=='failed') {
									?>
									<div class="ht-message-box style-error mt-30" role="alert">
										<h6 color="#FF0000">Maaf, <?php echo $domainresmi; ?> Tidak Dapat Digunakan.<br />Silahkan coba
											nama domain yang lain.</h6>
									</div>
									<?php
								}
								elseif ($respon=='success') {?>
									<div class="ht-message-box style-success mt-30" role="alert">
										<h6 color="#FF0000">Selamat, <?php echo $domainresmi; ?> Dapat Dipesan</h6>
										<form action="<?php echo URL_DOMAIN . $menu . '/' . $link . '1/form'; ?>" method="post">
											<input name='subdomain' value='<?php echo $subdomain; ?>' hidden>
											<button type="submit" name="submit" class="ht-btn ht-btn-md mt-10">LANJUTKAN <span
													class="btn-icon"><i class="far fa-arrow-right"></i></span></button>
									</div>
									<?php
								}
							}
							?>
						</div>
					</div>
				</div>
			</div>
		<?php
		}else if($act == 'form'){

			if (empty($_POST['subdomain'])) {
				echo 'Balikin Ke Depan';
			}
			else {
			$_SESSION['subdomain'] = strip_tags($_POST['subdomain']);
			$subdomain = $_SESSION['subdomain'];
			}

			$randkode=rand(111111,999999);
    	    $_SESSION['kode']=$randkode;
			?>
			<div class="feature-images-wrapper section-space--ptb_60">
				<div class="container ">
					<div class="pricing-table-title-area position-relative">
						<div class="container">
							<div class="section-title-wrapper text-center section-space--mb_60 wow move-up">
								<h3 class="heading">Step 2 : Isi Data Diri Anda</h3>
							</div>
						</div>
					</div>
					<form action="<?php echo URL_DOMAIN . $menu . '/' . $link . '1/finish'; ?>" method="post">
						<div class="row">
							<div class="col-lg-6">
								<div class="contact-form-wrap">
									<h5 class="mb-10 text-left">Data Diri</h5>
									<div class="contact-form">
										<input name="diri" value="diri" hidden>
										<div class="form-group">
											<label>Nama calon</label>
											<input type="text" name="name" class="form-control" placeholder="Nama calon">
										</div>

										<div class="form-group">
											<label>Email</label>
											<input type="email" name="email" class="form-control" placeholder="Email">
										</div>

										<div class="form-group">
											<label>Alamat</label>
											<input type="text" name="alamat" class="form-control" placeholder="Alamat calon">
										</div>

										<div class="form-group">
											<label>Kota</label>
											<input type="text" name="kota" class="form-control" placeholder="Kota">
										</div>

									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="contact-form-wrap">
									<h5 class="mb-10  text-left">Data Lain</h5>
									<div class="contact-form ">
										
										<div class="form-group">
											<label>Tingkat</label>
											<select class="form-control" name="tingkat" id="tingkat">		
												<option value="DPR RI">DPR RI</option>
												<option value="DPRD Provinsi">DPRD Provinsi</option>
												<option value="DPRD Kabupaten / Kota">DPRD Kabupaten / Kota</option>
												<option value="DPR RI">DPR RI</option>
											</select>
										</div>

										<div class="form-group">
											<label>Partai</label>
											<select class="form-control" name="partai">
												<?php
													foreach($partaiData as $val) {
												?>		
													<option value="<?= $val["number"] ?>"><?= $val["name"] ?></option>
												<?php
													}
												?>
											</select>
										</div>

										<div class="form-group">
											<label>Dapil</label>
											<input type="text" name="dapil" class="form-control" placeholder="Dapil">
										</div>

										<div class="form-group">
											<label>Nomor WA</label>
											<input class="form-control" name="phone" type="text" placeholder="Gunakan 62 Contoh : 62815********" required>
											<small class="text-danger">(*)Aktivasi akan dikirimkan via wa ke nomor ini</small>
										</div>
										
									</div>

								</div>
							</div>
						</div>
						<button type="submit" name="submit" class="ht-btn ht-btn-lg" />LANJUTKAN <span
							class="btn-icon"><i class="far fa-arrow-right"></i></span></button>
					</form>
				</div>
			</div>
		<?php 
		}else if($act == 'finish'){ 
			$subdomain = $_SESSION['subdomain'];
			if (empty($_SESSION['kode'])) {
				echo 'Balikin Ke Depan';
			}elseif (empty($_POST['diri'])) {
				echo 'Balikin Ke Depan';
			}else {
				if (empty($_SESSION['kode'])) { } else { unset($_SESSION['kode']); }
				$name=strip_tags($_POST['name']); $name=str_replace('"','',$name); $name=str_replace("'","",$name);
				$email=strip_tags($_POST['email']); $email=str_replace('"','',$email); $email=str_replace("'","",$email);
				$alamat=strip_tags($_POST['alamat']); $alamat=str_replace('"','',$alamat); $alamat=str_replace("'","",$alamat);
				$kota=strip_tags($_POST['kota']); $kota=str_replace('"','',$kota); $kota=str_replace("'","",$kota);
				$tingkat=strip_tags($_POST['tingkat']); $tingkat=str_replace('"','',$tingkat); $tingkat=str_replace("'","",$tingkat);
				$dapil=strip_tags($_POST['dapil']); $dapil=str_replace('"','',$dapil); $dapil=str_replace("'","",$dapil);
				$phone=strip_tags($_POST['phone']); $phone=str_replace('"','',$phone); $phone=str_replace("'","",$phone);
				$theme='free'; 
				$partai=strip_tags($_POST['partai']);
				$karakter   = 'ABCDEF1234567890';
				$pword      = '';   for ($i=0; $i<6; $i++) { $pos=rand(0,strlen($karakter)-1); $pword.= $karakter{$pos}; }
				$password   = md5($pword);

				$content_welcome = "
				<p><b>Selamat Datang di Website $name</b></p>
				<p>Terima kasih sudah terhubung dengan saya melalui website pribadi saya ini. Website ini ditujukan sebagai media untuk saling mengenal dan berinteraksi sebagai bagian dari komitmen saya untuk ikut membangun negeri. </p>
				<p>Semoga jalan perjuangan kita semua sebagai bangsa merdeka, diridhoi oleh Tuhan Yang Maha Esa.</p>
				<br/>
				<p>Salam hangat,</p>
				<p>$name</p>";

				$content_volunteer = '<p>KAMI BUKAN SIAPA-SIAPA TANPA ANDA! </p>
				<p>adalah prinsip yang selalu kami junjung tinggi dalam setiap gerakan kami. MENJADI RELAWAN adalah sebuah gerakan solidaritas baru. </p>
				<p>Ibarat kanvas yang masih bersih, Indonesia yang baru akan kita lukis kembali. Kami mengajak semua dari KITA untuk menorehkan warna kita masing-masing di atas kanvas Indonesia Baru.</p>
				<p>Tunjukkan bahwa Indonesia tidak hanya dibangun diatas warna-warna yang kusam dan membosankan. Indonesia Baru bisa kita lukis dengan warna-warni yang membentuk mozaik indah di garis khatulistiwa. </p>
				<p>Setitik kecil yang anda berikan adalah sumbangan besar untuk masa depan Indonesia yang kita cintai.</p>
				<br/>
				<p>Mari Gabung Menjadi Relawan Gerakan Kami. OK</p>';

				$content_donation='<p>Kami menyadari bahwa gerakan kami tidak akan maksimal tanpa bantuan dan partisipasi Anda. 
				Kami menerima donasi sebagai dukungan Anda terhadap gerakan kami. Anda bisa mengirimkan donasi ke rekening di bawah ini :</p>
				<p><b>BANK ABC</b><br>No. Rek. 1234567890<br>A.N. Nama Orang</p>
				<p>Setelah melakukan transfer donasi, silahkan konfirmasi pada formulir dibawah/disamping.</p>
				<p>Semoga bantuan dan donasi Anda mendapatkan keutamaan yang berlipat dan dibalas oleh Tuhan Yang Maha Esa.</p>';

				$content_cta='Ayo, Dukung dan bergabunglah bersama kami, dan jadikan Indonesia lebih baik !';
				
				function textToSlug($text='') {
					$text = trim($text);
					if (empty($text)) return '';
					  $text = preg_replace("/[^a-zA-Z0-9\-\s]+/", "", $text);
					  $text = strtolower(trim($text));
					  $text = str_replace(' ', '-', $text);
					  $text = $text_ori = preg_replace('/\-{2,}/', '-', $text);
					  return $text;
				}

				$link = textToSlug($name);

				// SETTING
				mysqli_query($model->koneksi, "INSERT INTO member (username, password, category, name, link, sex, address, city, phone, email, subdomain) 
				VALUES ('$email','$password','member','$name','$link','L','$alamat','$kota','$phone','$email','$subdomain')");

				mysqli_query($model->koneksi, "INSERT INTO setting (subdomain, package, username, no_partai, tingkat, dapil, name, slogan, logo, favicon, theme, address, city, phone, email , content_welcome, content_volunteer, content_donation, content_cta)
				VALUES ('$subdomain','pro','$name','$partai','$tingkat','$dapil','$name','Maju Terus Pantang Mundur','profile.png','','mitech','$alamat','$kota','$phone','$email','$content_welcome','$content_volunteer','$content_donation','$content_cta')");

				mysqli_query($model->koneksi, "INSERT INTO cv (subdomain, name, link, sex, address, city, phone, email)
				VALUES ('$subdomain','$name','$link','L','$alamat','$kota','$phone','$email')");
				

				//EMAIL
				$isi='<html>
						<body>
						<label><img src=https://pilihanku.id/cms_produk/upload/picture/logo.png alt=logo></label>
						<br/>
						<br/>
						Salam, '.$name.'
						<br/>
						Terima kasih telah melakukan pemesanan website profil caleg di pilihanku.net ,&nbsp;Website&nbsp;'.$name.' telah masuk ke database kami
						<br/>
						Berikut ini adalah data-data pendaftaran Anda :<br/>
						<br/>
						<table width="300" cellpadding="0" cellspacing="0" border="0">
							<tr><th colspan="2" align=left>DATA</th></tr>
							<tr><td align=left width="120">Nama</td><td align=left>:&nbsp;'.$name.'</td></tr>
							<tr><td align=left>Email</td><td align=left>:&nbsp;'.$email.'</td>
							<tr><th colspan="2" align=left>DATA ADMIN</th></tr>
							<tr><td align=left>Nama</td><td align=left>:&nbsp;'.$name.'</td>
							<tr><td align=left>No. Handphone</td><td align=left >:&nbsp;'.$phone.'</td></tr>
							<tr><td align=left>Password</td><td align=left>:&nbsp;'.$pword.'</td></tr>
							</tr>
						</table>
						(Data username dan password berikut juga digunakan untuk login ke halaman CLIENT ZONE sebagai media konsultasi kendala website Anda)
						<br/><br/>'
				;
				$isi2='Untuk login , klik <a href=http://'.$subdomain.'.pilihanku.net/adm/>https://'.$subdomain.'.pilihanku.net/adm</a><br/>
						Dengan Username&nbsp; :&nbsp;'.$email.' &nbsp;&nbsp;Password&nbsp; :&nbsp;'.$pword.'<br/>'
				;
				$isi3='Demikian informasi mengenai pendaftaran Anda.<br/>
						Informasi lebih lanjut mengenai layanan Pilihanku.net, silahkan menghubungi Customer Service kami.<br/>
						Semoga kesuksesan senantiasa menyertai daya upaya kita bersama.<br/>
						Best Regards,<br/>
						Pilihanku.net
						</body>
						</html>'
				;

				$mail = new PHPMailer();
				
				//$mail->isSMTP();                 
				$mail->Host = 'smtp.mailgun.org';
				$mail->SMTPAuth = true;                 
				$mail->Username = 'blesta@post.mynet.id';    
				$mail->Password = '4aa0b6f142799dadae5281d2a3866f56-07a637b8-822785f0';         
				$mail->SMTPSecure = 'TLS';                        
				$mail->Port = '465';
				//$mail->SMTPDebug  = 1;
				
				
				$body = $isi.$isi2.$isi3;
				$mail->isHTML(true);
				$mail->SetFrom('info@pilihanku.net', 'pilihanku.net');
				$mail->Subject= 'pilihanku';
				$mail->MsgHTML($body);

				//WA
				$salam="
					Salam, $name
					Terima kasih telah melakukan pemesanan website di pilihanku.net. Website $name telah masuk ke database kami
					Berikut ini adalah data-data pendaftaran Anda :
				";
				
				$penutup="
					Demikian informasi mengenai pendaftaran Anda.
					Hubungi kami:
					https://wa.me/6281235067234 untuk informasi website
					
					Semoga kesuksesan senantiasa menyertai daya upaya kita bersama.
					Best Regards,
					pilihanku.net
				";

				//wa untuk free
				$wafree="
					$salam
					Lihat website desa Anda klik http://$subdomain.pilihanku.net
					Masuk ke halaman admin klik http://$subdomain.pilihanku.net/adm
					Username  : $email
					Password  : $pword
				";

				// fungsi ke wa_api bikinan dev
				        
			        
				function wa_notif($url, $body = null){
					$curl = curl_init();
					curl_setopt_array($curl, array(
						CURLOPT_URL => $url,
						  CURLOPT_RETURNTRANSFER => true,
						  CURLOPT_ENCODING => "",
						  CURLOPT_MAXREDIRS => 10,
						  CURLOPT_TIMEOUT => 0,
						  CURLOPT_FOLLOWLOCATION => true,
						  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						  CURLOPT_CUSTOMREQUEST => "POST",
						  CURLOPT_POSTFIELDS => $body,
						  CURLOPT_HTTPHEADER => array(
							'Content-Type: application/json'
						  ),
					));

					$response = curl_exec($curl);
					curl_close($curl);
					return json_decode($response, true);
				}
		
				// Send Notif Whatsapp Pesan gambar biasa
					
					$data_array = array(
						'api_key' => KEY_WA,
						'sender'  => SENDER_WA,
						'number' => $phone,
						'message' => $wafree,
						'url' => IMAGE_WA,
						'type' => 'image'
					);
					$send_notif = wa_notif(URLWA_MEDIA, json_encode($data_array));
				?>
			

				<div class="contact-us-section-wrappaer section-space--pt_70 section-space--pb_70 bg-white">
					<div class="container text-center">
						<div class="pricing-table-title-area position-relative">
							<div class="container">
								<div class="section-title-wrapper text-center section-space--mb_60 wow move-up">
									<h3 class="heading">Selesai</h3>
								</div>
							</div>
						</div>
					</div>
					<div class="text-left">
						<center>
								<img src="<?php echo URL_IMAGE; ?>success.png" />
								<h5>Selamat, Website <?php echo $subdomain; ?>.pilihanku.net Telah Aktif</h5>
								<h5>Dapatkan bonus menarik berupa gratis input data awal khusus aktivasi website baru minggu
									ini saja!</h5>
								Klik <b><a href="http://<?php echo $subdomain; ?>.pilihanku.net" target="_blank"
										title="Detail">DISINI</a></b> untuk melihat tampilan website
								<?php echo $subdomain; ?>.pilihanku.net<br>
								Informasi Lebih Detail, Silahkan Cek Email Anda.<br />
								<a class="btn btn-primary" href="http://<?php echo $subdomain; ?>.pilihanku.net" target="_blank" title="LIHAT WEBSITE">LIHAT WEBSITE</a>
						</center>
					</div>
				</div>	
		<?php 
			}
		} 
		?>

	<?php
	include (DIR_THEME.THEME.'/app/footer.php');
	include (DIR_THEME.THEME.'/app/asset-body.php');
	?>
</body>

</html>