<!DOCTYPE html>
<html dir="ltr" lang="id-ID">
    
<head><?php
    include (DIR_THEME.THEME.'/app/asset-head.php');
    include (DIR_THEME.THEME.'/app/metatag.php'); 
    include (DIR_THEME.THEME.'/app/opengraph.php');
    include (DIR_THEME.THEME.'/app/favicon.php');
    include (DIR_THEME.THEME.'/app/title.php');
    ?>
</head>

<body>
	<?php
	include (DIR_THEME.THEME.'/app/loader.php');
	include (DIR_THEME.THEME.'/app/header.php');
	?>
	
	
	<div class="feature-images-wrapper section-space--ptb_60 bg-gray">
		<div class="container ">
			<div style="background:#fff;border-radius:5px;padding:15px 25px;font-weight:bold;display:table;width:100%;">
			    <ul>
					<li style="float:left;padding:12px 8px;font-weight:normal;">Pilih Kategori :</li>
					<li style="float:left;padding:12px 8px;">
						<a href="<?php echo URL_DOMAIN.$menu;?>?key=ri" title="DPR RI"><span>DPR RI</span></a>
					</li>
					<li style="float:left;padding:12px 8px;">
						<a href="<?php echo URL_DOMAIN.$menu;?>?key=provinsi" title="DPR RI"><span>DPR Provinsi</span></a>
					</li>
					<li style="float:left;padding:12px 8px;">
						<a href="<?php echo URL_DOMAIN.$menu;?>?key=kabupaten" title="DPR RI"><span>DPR Kabupaten/Kota</span></a>
					</li>
					<li style="float:left;padding:12px 8px;">
						<a href="<?php echo URL_DOMAIN.$menu;?>?key=dpd" title="DPR RI"><span>DPD RI</span></a>
					</li>
					<li style="float:right;padding:0px;">
						<div class="sidebar-widget search-post wow move-up">
							<form method='get' action="<?php echo URL_DOMAIN.$menu;?>">
								<div class="widget-search">
									<input type="text" name='key' placeholder="Cari Calon…">
									<button type="submit" class="search-submit">
										<span class="search-btn-icon fa fa-search"></span>
									</button>
								</div>
							</form>
						</div>
					</li>
				</ul>
			</div>
			<div class="feature-images__two small-mt__20">
				<div class="modern-grid-image-box row row--30">
					
					<?php
					if ($count!=0) {
    					foreach($settingData as $data){
    						$settingNo = $data['no'];
    						$settingName = $data['name'];
    						$settingTingkat = $data['tingkat'];
    						$settingDapil = $data['dapil'];
    						$settingLogo = $data['logo'];
    						$settingPackage = $data['package'];
    						$settingSubdomain = $data['subdomain'];
    						$settingDomain = $data['domain'];
    						$settingURL = 'http://'.$settingSubdomain.'.pilihanku.net';
    						?>
    						<div class="single-item wow move-up col-lg-4 col-md-6">
    							<div class="ht-box-images style-01">
    								<div class="image-box-wrap">
    										<a href="<?php echo $settingURL;?>" title="<?php echo $settingName;?>" target="_blank">
    											<img class="img-fulid mb-20" style="border-radius:50%;" width="120" alt="<?php echo $settingName;?>" src="<?php echo URL_UPLOAD;?>image.php/<?php echo $settingLogo;?>?width=120&cropratio=1:1&nocache&quality=100&image=<?php echo URL_PICTURE.$settingLogo;?>">
    											<h6 class="heading"><?php echo $settingName;?></h6>
    										</a>
    										<div class="text">Calong Anggota <?php echo $settingTingkat;?></div>
    										<div class="text">Dapil <?php echo $settingDapil;?></div>
    										<div class="circle-arrow">
    											<div class="middle-dot"></div>
    											<div class="middle-dot dot-2"></div>
    											<a href="<?php echo $settingURL;?>" title="<?php echo $settingName;?>">
    												<i class="far fa-long-arrow-right"></i>
    											</a>
    										</div>
    								</div>
    							</div>
    						</div>
    						<?php
    					}
					}
					else {
					    ?>
					    <div class="col-md-12 mt-30 mb-30">
    						<div class="alert alert-warning" role="alert">
    							Mohon maaf, data yang Anda cari tidak ditemukan.
    						</div>
						</div>
						<?php
					}
					?>	
				</div>
				<?php
				if ($count>0) {
    				?>
    				<div class="ht-pagination mt-30 pagination justify-content-center">
    					<div class="pagination-wrapper">
    						<ul class="page-pagination">
    							<?php
    							$jumhal=ceil($count/$limit); 
    							$linkhal="#";
    							if ($page>1) { $prev=$page-1; ?><li><a class="prev btn btn--secondary text-white mr-1" href="<?php echo URL_DOMAIN.$menu;?>" title="First"><<</a></li><li><a class="prev btn btn--secondary text-white mr-1" href="<?php echo URL_DOMAIN.$menu."/page/".$prev;?>" title="Prev"><</li><?php } 
    							else { ?><li><a class="prev btn btn--secondary text-white mr-1" href="#" title="First"><<</a></li><li><a class="prev btn btn--secondary text-white mr-1" href="#" title="Prev"><</a></li><?php } 
    							$sebakhir=$jumhal-1;
    							if ($jumhal<=3) { $awal=1; $akhir=$jumhal; }
    							else {
    								if ($page==1) { $awal=1; $akhir=$page+2; }
    								elseif ($page==2) { $awal=1; $akhir=$page+2; }
    								elseif ($page==$jumhal) { $awal=$page-2; $akhir=$jumhal; }
    								elseif ($page==$sebakhir) { $awal=$page-2; $akhir=$jumhal; }
    								else { $awal=$page-2; $akhir=$page+2; }
    							}
    							for ($i=$awal; $i<=$akhir; $i++) {
    								if ($i!=$page) { ?><li><a class="btn btn--secondary text-white mr-1" href="<?php echo URL_DOMAIN.$menu."/page/".$i;?>" title="<?php echo $i;?>"><?php echo $i;?></a></li><?php }  
    								else { ?><li><a class="btn btn--primary text-white mr-1" href="#"><?php echo $i;?></a></li><?php } 
    							}
    							if ($page<$jumhal){ $next=$page+1; ?><li><a class="next btn btn--secondary text-white mr-1" href="<?php echo URL_DOMAIN.$menu."/page/".$next;?>" title="Next">></a></li><li><a class="next btn btn--secondary text-white mr-1" href="<?php echo URL_DOMAIN.$menu."/page/".$jumhal;?>" title="Last">>></a></li><?php } 
    							else { ?><li><a class="next btn btn--secondary text-white mr-1" href="#" title="Next">></a></li><li><a class="next btn btn--secondary text-white mr-1" href="#" title="Last">>></a></li><?php } 
    							?>
    						</ul>
    					</div>
    				</div>
    				<?php
				}	
    			?>
			</div>
		</div>
	</div>
	
	<?php
	include DIR_THEME.THEME.'/app/footer.php';
	include DIR_THEME.THEME.'/app/asset-body.php';
	?>
</body>

</html>