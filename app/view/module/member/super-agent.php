<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_SUPER.'/app/asset-head.php';?>
</head>

<body class="nav-fixed">
   
	<?php include DIR_THEME.THEME_SUPER.'/app/topbar.php'; ?>
	
	<div id="layoutSidenav">
	
        <div id="layoutSidenav_nav">
			<?php include DIR_THEME.THEME_SUPER.'/app/sidebar.php'; ?>			
		</div>
		
		<div id="layoutSidenav_content">
			<main>
				
				<?php include DIR_THEME.THEME_SUPER.'/app/header.php'; ?>

				<div class="container-xl px-4 mt-4">
					
					<?php                   
					$admin = new admin();
					
					$title = 'Mitra';  
					
					$table_name = 'member';
					$table_label = 'gambar,nama,username,telepon,tanggal';
					$table_column = 'picture,name,username,phone,date';
					$table_width = '5,15,20,12,10'; 
					$table_button = 'update,read,account';  
					$table_condition = "WHERE category = 'mitra'";
					
					$read_label = 'gambar,username,category,replika,referral,nama,nomor ktp,TTL,jenis kelamin,alamat,kota,telepon,email,bank,nomor rekening,atas nama,login terakhir,ip terakhir,tanggal';
					$read_content = 'picture,username,category,replika,referral,name,number_id,birth,sex,address,city,phone,email,bank_name,bank_account_number,bank_account_name,last_login,last_ipaddress,date';
					$read_condition = "WHERE no='$no'";
					
					$update_type = 'picture';
					$update_label = 'gambar,username,category,replika,referral,nama,nomor ktp,TTL,jenis kelamin,alamat,kota,telepon,email,bank,nomor rekening,atas nama';
					$update_form = 'picture,username,category,replika,referral,name,number_id,birth,sex,address,city,phone,email,bank_name,bank_account_number,bank_account_name';
					$update_condition = "WHERE no='$no'";
					
					$delete_condition = "WHERE no='$no'";
					
					if (empty ($act)) {
						
						$admin->title($title);
						$admin->tablepro($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 	
					elseif ($act=='cabang') {						
						
						$admin->title('Cabang');
						$table_condition = "WHERE category = 'cabang'";
						$admin->tablepro($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					elseif ($act=='read') {						
						
						$admin->title($title);
						$admin->read($table_name,$read_label,$read_content,$read_condition); 
						$admin->button ('back','Kembali','','btn-primary');
						
					} 
					elseif ($act=='update') {
						
						$admin->title('Ubah '.$title);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 		
					elseif ($act=='account') {
						
						$admin->title('Ubah Password '.$title);
						$update_type = '';
						$update_label = 'password';
						$update_form = 'password';
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
					} 	
					elseif ($act=='delete') {	
										
						//$admin->delete($table_name,$delete_condition);
						$admin->title($title);
						echo '<div class="alert alert-danger" role="alert">Demi Keamanan, Fitur Hapus Dinonaktifkan</div>';
						$admin->tablepro($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
						
					} 
					else {						
						
						$admin->title($title);
						$admin->tablepro($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					}
					?>	
					
				</div>
				
			</main>
		</div>
		
	</div>	
	
	<?php include DIR_THEME.THEME_SUPER.'/app/asset-body.php'; ?>
	
</body>
 
</html>