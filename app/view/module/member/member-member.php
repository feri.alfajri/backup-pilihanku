<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-member.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					    
                    <h2 class="mb-4">Anggota Referensi Anda</h2>
					
					<?php
					$uname = $_SESSION['uname'];
					
					if ($no=='') {
						
						$query = mysqli_query($model->koneksi,"SELECT * FROM member WHERE referral='$uname' AND publish='1' ORDER BY name DESC");
						$jumlah = mysqli_num_rows($query);
						if ($jumlah==0) { 
						    ?>
					        <div class="alert alert-warning" role="alert">
								Maaf, Anda belum memiliki Anggota dari referensi Anda.
							</div>
							<?php
						}
						else {
						    ?>
						    <div class="row"> 
    						    <?php
        						while ($data = mysqli_fetch_array($query)) { 
        							$memberNo = $data['no'];
        							$memberUsername = $data['username'];
        							$memberName = $data['name'];
        							$memberPicture = $data['picture']; if ($memberPicture=='') { $memberPicture = 'member.png';}
        							$qwebsite = mysqli_query($admin->koneksi,"SELECT * FROM setting WHERE username='$memberUsername' AND publish='1'");
    						        $jwebsite = mysqli_num_rows($qwebsite);
    						        $qwebsitefree = mysqli_query($admin->koneksi,"SELECT * FROM setting WHERE username='$memberUsername' AND package='free' AND publish='1'");
    						        $jwebsitefree = mysqli_num_rows($qwebsitefree);
    						        $qwebsitepro = mysqli_query($admin->koneksi,"SELECT * FROM setting WHERE username='$memberUsername' AND package!='free' AND publish='1'");
    						        $jwebsitepro = mysqli_num_rows($qwebsitepro);
        							?>
        							<div class="col-md-6"> 
        								<div class="card mb-4">
        									<div class="card-body">
        									    <a href="<?php echo URL_DOMAIN.$menu.'/'.$link.'/'.$memberNo.'/detail';?>" title="<?php echo $memberName;?>">
            										<img class="mr-2" src="<?php echo URL_UPLOAD;?>image.php/<?php echo $memberPicture;?>?width=64&cropratio=1:1&nocache&quality=100&image=<?php echo URL_PICTURE.$memberPicture;?>" alt="user" align="left" style="border-radius:50%"/>
            										<h4 class="mt-1"><?php echo $memberName;?></h4>
        										</a>
        										<span><?php echo $jwebsite;?> Web  (<?php echo $jwebsitefree;?> Free - <?php echo $jwebsitepro;?> Premium)</span>
        									</div>
        								</div>
        							</div>
        							<?php
        						} 
        						?>
    						</div>
    						<?php
						}
					
					}
					else {
					    
					    $query = mysqli_query($model->koneksi,"SELECT * FROM member WHERE referral='$uname' AND no='$no' LIMIT 1");
					    $jumlah = mysqli_num_rows($query);
						if ($jumlah == 0) {
							?>
							<div class="alert alert-warning" role="alert">
								Maaf, Data yang Anda cari tidak tersedia.
							</div>
							<?php
						}
						else {
							$data = mysqli_fetch_array($query);
							$memberNo = $data['no'];
							$memberUsername = $data['username'];
							$memberName = $data['name'];
							$memberAddress = $data['address'];
							$memberCity = $data['city'];
							$memberPhone = $data['phone'];
							$memberEmail = $data['email'];
							$memberPicture = $data['picture']; if ($memberPicture=='') { $memberPicture = 'member.png';}
							?>
							<div class="card mb-3">
								<div class="card-body">
                                    <div class="row">						
            							<div class="col-md-2">
            								<img src="<?php echo URL_UPLOAD;?>image.php/<?php echo $memberPicture;?>?width=100&cropratio=1:1&nocache&quality=100&image=<?php echo URL_PICTURE.$memberPicture;?>" alt="foto" width="100%"/>					
            							</div>
                						<div class="col-md-10">	
                						    <h4><?php echo $memberName;?></h4>
                							<div id="view">
                								<div class="view_label">Alamat</div><div class="view_dot">:</div><div class="view_content"><?php echo $memberAddress.', '.$memberCity;?></div>
                							</div>
                							<div id="view">
                								<div class="view_label">No. Telepon</div><div class="view_dot">:</div><div class="view_content"><?php echo $memberPhone;?></div>
                							</div>
                							<div id="view">
                								<div class="view_label">Email</div><div class="view_dot">:</div><div class="view_content"><?php echo $memberEmail;?></div>
                							</div>
                						</div>
                					</div>
								</div>
							</div>
						    
    						<?php
    						$query = "SELECT no,subdomain,domain,name,package,logo FROM setting WHERE username='$memberUsername' AND publish='1' ORDER BY date DESC";
    						$sqlquery = mysqli_query($admin->koneksi,$query);
    						$jumlah = mysqli_num_rows($sqlquery);
    						if ($jumlah==0) { 	}
    						else {
    						    ?>
    						    <h3>Daftar Website</h3>
						        <div class="row"> 
    						        <?php
            						while ($data = mysqli_fetch_array($sqlquery)) { 
            							$websiteNo = $data['no'];
            							$websiteName = $data['name'];
            							$websiteLogo = $data['logo']; if ($websiteLogo=='') { $websiteLogo = 'user.png';}
            							$websitePackage = $data['package'];
            							$websiteDomain = $data['domain'];
            							$websiteSubdomain = $data['subdomain'];
            							$websitePackage = $data['package'];
            							if ($websitePackage == 'free') { $websiteURL = 'http://'.$websiteSubdomain.'.pilihanku.net'; } 
            							else { $websiteURL = 'https://'.$websiteDomain; } 
            							$websiteURLSementara = URL_DOMAIN.'subdomain';
            							?>
            							<div class="col-md-6"> 
            								<div class="card mb-4">
            									<div class="card-body">
            										<img class="mr-3" src="<?php echo URL_UPLOAD;?>image.php/<?php echo $websiteLogo;?>?width=90&cropratio=1:1&nocache&quality=100&image=<?php echo URL_PICTURE.$websiteLogo;?>" alt="logo" align="left" style="border-radius:50%"/>
            										<h5 style="float:right;text-transform:uppercase;"><?php  echo $websitePackage; ?></h5>
            										<h4><?php echo $websiteName;?></h4>
            										<span style="color:#888;">URL : <?php  echo $websiteURL; ?></span><br/>
            								        <a class="btn btn-sm btn-success font-white" href="<?php echo $websiteURL;?>" title="Lihat Website" target="_blank"><i class="fas fa-desktop mr-2"></i> Lihat Website</a>
            									</div>
            								</div>
            							</div>
            							<?php
            						} 
            						?>
        						</div>
        						<?php
    						}
    						?>
							<a href="<?php echo URL_DOMAIN.$menu.'/'.$link;?>" class="btn btn-primary mt-1" title="Kembali">Kembali</a>
							<?php
						} 
					}
					?>
						
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>
 
</html>