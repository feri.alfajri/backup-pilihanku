<!DOCTYPE html> 
<html lang="en">
    
<head>

    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
	
</head>

<body>
    
	<div id="wrapper"> 
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>
		
		<div id="content-wrapper"> 
		    
			<div id="content">
			
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
				    
					<?php
					function package_free ($package) {
						$admin=new admin();
						$admin->get_variable(); 
						$admin->koneksi();
						$menu=$admin->menu; 
						$link=$admin->link; 
						$linkmod=URL_DOMAIN.$menu."/theme";
						?>
						<div class="card text-center mb-5">
							<div class="card-body">
								<h4>Free</h4>
								<hr class="sidebar-divider m-3">
								SubDomain <b class="text-danger">.PilihanKu.Net</b>
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> Data Profil / CV
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> 10 Post Blog / Artikel
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> 10 Post Image Gallery
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> Tanya Jawab / Aspirasi
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> Kontak & Social Media
								<hr class="sidebar-divider m-2">
								<i class="fa fa-x mr-1 text-danger">X</i> <b>Fitur</b> Pendaftaran Relawan
								<hr class="sidebar-divider m-2">
								<i class="fa fa-x mr-1 text-danger">X</i> <b>Fitur</b> Donasi Online
								<hr class="sidebar-divider m-2">
								<i class="fa fa-x mr-1 text-danger">X</i> <b>Fitur</b> Ganti Theme Design
								<hr class="sidebar-divider m-2">
								<i class="fa fa-x mr-1 text-danger">X</i> <b>FREE</b> Konsultasi
							</div>
							<?php if ($package=='free') { ?><div class="card-header"><b>Paket Anda saat ini</b></div><?php } ?>
						</div>
						<?php
					}
					
					function package_pro ($package) {
						$admin=new admin();
						$admin->get_variable(); 
						$admin->koneksi();
						$menu=$admin->menu; 
						$link=$admin->link; 
						$linkmod=URL_DOMAIN.$menu."/theme";
						?>
						<div class="card text-center mb-5">							
							<div class="card-body">
								<h4>Pro</h4>
								<hr class="sidebar-divider m-3">
								Domain <b class="text-success">.Com / .ID / .Net</b>
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> Data Profil / CV
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> <b>UNLIMITED</b> Post Blog / Artikel
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> <b>UNLIMITED</b> Post Image Gallery
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> Tanya Jawab / Aspirasi
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> Kontak & Social Media
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> <b>Fitur</b> Pendaftaran Relawan
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> <b>Fitur</b> Donasi Online
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> <b>Fitur</b> Ganti Theme Design
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> <b>FREE</b> Konsultasi
							</div>
							<?php 
							if ($package=='pro') { ?><div class="card-header"><b>Paket Anda saat ini</b></div><?php } 
							else {
								?>								
								<div class="card-header">
									<h6><b>Rp. 1.000.000 / tahun</b></h6>
									<form method="post">
										<input type="hidden" name="process" value="upgrade">
										<input type="hidden" name="package" value="pro">
										<input type="hidden" name="value" value="1000000">
										<button class="btn btn-primary btn-user btn-block btn-lg" type="submit" onclick="return confirm ('Yakin Akan Upgrade Paket Pro?')">UPGRADE</button>
									</form>
								</div>
								<?php
							} 	
							?>
						</div>
						<?php
					}
					
					function package_custom ($package) {
						$admin=new admin();
						$admin->get_variable(); 
						$admin->koneksi();
						$menu=$admin->menu; 
						$link=$admin->link; 
						$linkmod=URL_DOMAIN.$menu."/theme";
						?>
						<div class="card text-center mb-5">							
							<div class="card-body">
								<h4>Custom</h4>								
								<hr class="sidebar-divider m-3">
								Domain <b class="text-success">.Com / .ID / .Net</b>
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> Data Profil / CV
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> <b>UNLIMITED</b> Post Blog / Artikel
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> <b>UNLIMITED</b> Post Image Gallery
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> Tanya Jawab / Aspirasi
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> Kontak & Social Media
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> <b>Fitur</b> Pendaftaran Relawan
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> <b>Fitur</b> Donasi Online
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> <b>Fitur</b> Ganti Theme Design
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> <b>FREE</b> Konsultasi
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> <b>FREE</b> Desain Slider
								<hr class="sidebar-divider m-2">
								<i class="fa fa-check mr-1 text-success"></i> <b>FREE</b> Custom Theme
							</div>
							<?php 
							if ($package=='custom') { ?><div class="card-header"><b>Paket Anda saat ini</b></div><?php } 
							else {
								?>
								<div class="card-header">
									<h6><b>Rp. 3.000.000 / tahun</b></h6>
									<form method="post">
										<input type="hidden" name="process" value="upgrade">
										<input type="hidden" name="package" value="custom">
										<input type="hidden" name="value" value="3000000">
										<button class="btn btn-success btn-user btn-block btn-lg" type="submit" onclick="return confirm ('Yakin Akan Upgrade Paket Pro?')">UPGRADE</button>
									</form>
								</div>
								<?php
							} 	
							?>
						</div>
						<?php
					}
					if (empty($_POST['process'])) { }
					elseif ($_POST['process']=='upgrade') { 
						$upgrade_package = strip_tags($_POST['package']);
						$upgrade_value = strip_tags($_POST['value']);
						mysqli_query($model->koneksi,"UPDATE setting SET upgrade='$upgrade_package', upgrade_value='$upgrade_value' WHERE subdomain='".SUBDOMAIN."'"); 
						?>
						<div class="alert alert-success" role="alert">
							Permintaan Upgrade telah masuk ke sistem kami.
							<br/>
							Kami akan segera melakukan pengecekkan dan melakukan verifikasi.
						</div>
						<?php
					}
					
					
					if ($package == 'free') {
						?>
						<h3>Upgrade Paket</h3>
						<div class="row">
							<div class="col-md-4">
								<?php package_free($package);?>
							</div>
							<div class="col-md-4">
								<?php package_pro($package);?>
							</div>
							<div class="col-md-4">
								<?php package_custom($package);?>
							</div>
						</div>
						<?php
					}
					elseif ($package=='pro') {	 
						?>
						<h3>Upgrade Paket</h3>
						<div class="row">
							<div class="col-md-6">
								<?php package_pro($package);?>
							</div>
							<div class="col-md-6">
								<?php package_custom($package);?>
							</div>
						</div>
						<?php
					} 
					else {
						?>
						<h3>Upgrade Paket</h3>
						<?php
					}
					?>
                    
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>

</html>