<!DOCTYPE html>
<html dir="ltr" lang="id-ID">
    
<head><?php
    include (DIR_THEME.THEME.'/app/asset-head.php');
    include (DIR_THEME.THEME.'/app/metatag.php'); 
    include (DIR_THEME.THEME.'/app/opengraph.php');
    include (DIR_THEME.THEME.'/app/favicon.php');
    include (DIR_THEME.THEME.'/app/title.php');
    ?>
</head>

<body>
	<?php
	include (DIR_THEME.THEME.'/app/loader.php');
	include (DIR_THEME.THEME.'/app/header.php');
    ?>

    <div id="main-wrapper">
        <div class="site-wrapper-reveal">
            <div class="feature-large-images-wrapper section-space--ptb_60">
                <div class="container">
                    <p>
                    PilihanKu.net adalah salah satu program layanan kampanye digital dari PT. Bumi Tekno Indonesia. Kami menyediakan layanan paling mudah kepada kontestan dan pelaku politik dalam melakukan kampanye secara cerdas menggunakan media digital untuk mendukung budaya politik yang sehat di Indonesia. Misi PilihanKu.net adalah menyediakan layanan pembuatan website kampanye yang profesional, terpercaya, tercepat, dan termudah meskipun pengguna belum pernah memiliki pengalaman membuat website sama sekali. Selain itu PilihanKu.net juga menyediakan fitur yang mempermudah penggunanya dalam membuat media kampanye visual berupa flyer yang bisa dibuat sendiri dengan proses yang sangat sederhana, cepat, dan dapat dilakukan oleh siapapun meskipun tidak memiliki background sebagai grafis desainer.
                    </p>
                    <br/>
                    <h5 class="mb-10">Sejarah Singkat PilihanKu.net</h5>
                    <p>
                    Berawal dari kegelisaan kami melihat budaya politik yang tidak sehat dengan menyalahgunakan isu SARA sebagai bahan kampanye yang dapat menimbulkan perpecahan antar golongan. Selain itu praktik politik yang transaksional yang menghiasai hampir setiap event politik dari mulai tingkat desa, pilkada kabupaten-kota, pilkada gubernur, maupun pemilu partai politik, dan pemilihan presiden. Tentunya kami sebagai putra bangsa mengharapkan budaya dan praktik politik semakin lebih baik kedepannya. Sehingga muncullah platform PilihanKu.net sebagai upaya sederhana untuk membangun budaya politik yang sehat agar terlahir pemimpin-pemimpin yang akan membawa bangsa dan negera ini ke arah yang lebih baik.
                    </p>
                    <br/>
                    <h5 class="mb-10">Layanan PilihanKu.net</h5>
                    <h6>Pembuatan Website Kampanye</h6>
                    <p>
                    Layanan utama PilihanKu.net adalah pembuatan website kampanye. Website kampanye PilihanKu.net dirancang untuk memudahkan pengguna mendesain website kampanye sendiri meskipun belum berpengalaman membuat website. Website kampanye PilihanKu.net didesain secara modern menggunakan teknologi terkini sehingga tidak ketinggalan zaman. 
                    Anda dapat membuat website kampanye sendiri. Cukup dengan memasukkan data-data pada formulir yang tersedia. Kurang dari 1 menit website kampanye Anda, langsung jadi, langsung online. 
                    </p>
                    <h6>Pembuatan Flyer Digital</h6>
                    <p>
                    Flyer Digital adalah media visual berupa gambar yang dapat digunakan oleh user untuk mensosialisasikan kampanye politiknya. Pembuatan Flyer Digital bisa dilakukan oleh siapa saja meskipun tidak memiliki kemampuan sebagai desainer grafis. Disini Anda bisa membuatnya sendiri dengan hanya memilih pola / template desain yang telah tersedia, kemudian memilih foto terbaik Anda. Hanya beberapa detik, Flyer Digital untuk kampanye Anda sudah jadi. Dan siap untuk dishare / dibagikan melalui platform sosial media Anda.
                    </p>
                    <h6>Pengelolaan Sosial Media</h6>
                    <p>
                    Bagi Anda yang ingin memaksimalkan kampanye melalui berbagai platform sosial media seperti facebook, instagram, tiktok, youtube, dsb. Kami siap membantu Anda mengelola akun sosial media Anda untuk mensukseskan kampanye politik Anda. Kami menyediakan berbagai layanan mulai dari riset hastag, perencanaan konten, pembuatan desain gambar, pembuatan narasi / deskripsi, sampai dengan distribusi materi ke berbagai platform sosial media baik secara organik, maupun melalui media iklan berbayar. Dengan dibantu pengelolaan sosial media yang profesional dan berpengalaman, Anda dapat menjangkau audience / target pemilih Anda lebih cepat, lebih akurat, dan lebih luas. 
                    </p>
                    <h6>Aplikasi Pemenangan Caleg</h6>
                    <p>
                    Aplikasi Pemenangan Caleg adalah sebuah software / aplikasi yang dibuat khusus untuk memudahkan calon legislatif untuk mengelola database relawan, menggerakkan relawan, dan mengevaluasi kinerja relawan. Aplikasi Pemenangan Caleg juga dapat mendata calon pemilih yang berhasil direkrut oleh para relawan politik Anda. Anda dapat memonitor progress kerja-kerja relawan berdasarkan sebaran wilayahnya melalui dashboard yang bisa Anda akses dimana saja dan kapan saja. Menyajikan data real time yang dihimpun oleh tim sukses dan relawan politik Anda. Aplikasi Pemenangan Caleg akan memudahkan Anda dalam mengukur tingkat keberhasilan program kampanye Anda.
                    </p>
                    <br/>
                    <h5 class="mb-10">Legalitas Perusahaan</h5>
                    
                    <div class="row">
                        <div class="col-lg-3">
                            <img src="https://www.bumitekno.com/cms_web/upload/picture/office.jpg" alt="kantor pt bumi tekno" width="100%">
                        </div>
                        <div class="col-lg-9">
                            <p>
                            Legalitas perusahaan atau badan usaha merupakan unsur terpenting. Legalitas merupakan jati diri yang melegalkan atau mengesahkan suatu badan usaha sehingga diakui oleh masyarakat. Dengan adanya legalitas, suatu perusahaan dianggap sah menurut undang-undang dan peraturan yang berlaku, dimana perusahaan tersebut dilindungi dengan berbagai dokumen yang sah di mata hukum.
                            </p>
                            <table width="100%" style="vertical-align:top">
                                <tbody>
                                    <tr>
                                        <td width="150">Nama Perusahaan</td><td width="10">:</td>
                                        <td>PT. Bumi Tekno Indonesia</td>
                                    </tr>
                                    <tr>
                                        <td>Bentuk Usaha</td><td>:</td>
                                        <td>Perseroan Terbatas (PT)</td>
                                    </tr>
                                    <tr>
                                        <td>Bidang Perusahaan</td><td>:</td>
                                        <td>Teknologi Informasi dan Komunikasi, pengembangan perangkat lunak, dan konsultasi di bidang TI</td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td><td>:</td>
                                        <td>Kebumen, Kecamatan Pringsurat, Kabupaten Temanggung, Jawa Tengah</td>
                                    </tr>
                                    <tr>
                                        <td>Akta Notaris</td><td>:</td>
                                        <td>No. 71 Tahun 2019, Tanggal 26 Desember 2019</td>
                                    </tr>
                                    <tr>
                                        <td>Surat Ijin Usaha</td><td>:</td>
                                        <td>No. 0220106112111, Tanggal 21 Januari 2020</td>
                                    </tr>
                                    <tr>
                                        <td>NPWP</td><td>:</td>
                                        <td>No. 93.894.341.2-533.000, Tanggal 3 Januari 2020</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="contact-us-area infotechno-contact-us-bg section-space--ptb_60">
                <div class="container">
                    <h3 class="heading mb-20 text-center">Core Value</h3>
                    <div class="row text-center">
                        <div class="col-lg-2">
                            <h3>S</h3>
                            <p>Solid in Solving Problem</p>
                        </div>
                        <div class="col-lg-2">
                            <h3>I</h3>
                            <p>Initiative to Innovate</p>
                        </div>
                        <div class="col-lg-2">
                            <h3>M</h3>
                            <p>Make Progress</p>
                        </div>
                        <div class="col-lg-2">
                            <h3>P</h3>
                            <p>Professional</p>
                        </div>
                        <div class="col-lg-2">
                            <h3>L</h3>
                            <p>Learn Continuously</p>
                        </div>
                        <div class="col-lg-2">
                            <h3>E</h3>
                            <p>End User Focus</p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
    <?php
	include (DIR_THEME.THEME.'/app/footer.php');
	include (DIR_THEME.THEME.'/app/asset-body.php');
	?>
</body>

</html>