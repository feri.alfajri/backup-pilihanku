<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_SUPER.'/app/asset-head.php';?>
</head>

<body class="nav-fixed">
   
	<?php include DIR_THEME.THEME_SUPER.'/app/topbar.php'; ?>
	
	<div id="layoutSidenav">
	
        <div id="layoutSidenav_nav">
			<?php include DIR_THEME.THEME_SUPER.'/app/sidebar.php'; ?>			
		</div>
		
		<div id="layoutSidenav_content">
			<main>
				
				<?php include DIR_THEME.THEME_SUPER.'/app/header.php'; ?>

				<div class="container-xl px-4 mt-4">
					
					<?php                   
					$admin = new admin();
					
					$title = 'User';  
					$menu_label = 'Tambah';
					$menu_action = 'create';
					
					$table_name = 'user';
					$table_label = 'nama,telepon,username,last login';
					$table_column = 'name,phone,username,last_login';
					$table_width = '30,15,15,20'; 
					$table_button = 'update,read,account';  
					$table_condition = " ";
					
					$create_type = '';
					$create_label = 'nama,telepon,email,username,password';
					$create_form = 'name,phone,email,username,password';
					$create_condition = '';
					
					$read_label = 'nama,alamat,kota,provinsi,telepon,email,username,kategori,login terkahir,ip address terkahir,foto';
					$read_content = 'name,address,city,province,phone,email,username,category,last_login,last_ipaddress,picture';
					$read_condition = "WHERE no='$no'";
					
					$update_type = 'picture';
					$update_label = 'nama,alamat,kota,provinsi,telepon,email,foto';
					$update_form = 'name,address,city,province,phone,email,picture';
					$update_condition = "WHERE no='$no'";
					
					$delete_condition = "WHERE no='$no'";
					
					if (empty ($act)) {
						
						$admin->title($title);
						$admin->menu($menu_label,$menu_action);
						$admin->tablepro($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 		
					elseif ($act=='create') {
	
						$admin->title('Tambah '.$title);
						$admin->create($table_name,$create_type,$create_label,$create_form,$create_condition);
						
					}
					elseif ($act=='read') {						
						
						$admin->title($title);
						$admin->read($table_name,$read_label,$read_content,$read_condition); 
						$admin->button ('back','Kembali','','btn-primary');
						
					} 
					elseif ($act=='update') {
						
						$admin->title('Ubah '.$title);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act=='account') {
						
						$admin->title('Ubah Password '.$title);
						$update_type = '';
						$update_label = 'password';
						$update_form = 'password';
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
					} 	
					elseif ($act=='delete') {	
										
						//$admin->delete($table_name,$delete_condition);
						$admin->title($title);
						$admin->menu($menu_label,$menu_action);
						echo '<div class="alert alert-danger" role="alert">Demi Keamanan, Fitur Hapus Dinonaktifkan</div>';
						$admin->tablepro($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
						
					} 
					else {						
						
						$admin->title($title);
						$admin->menu($menu_label,$menu_action);
						$admin->tablepro($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					}
					?>	
					
				</div>
				
			</main>
		</div>
		
	</div>	
	
	<?php include DIR_THEME.THEME_SUPER.'/app/asset-body.php'; ?>
	
</body>
 
</html>