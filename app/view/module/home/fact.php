<div class="feature-icon-wrapper section-space--ptb_60 bg-white">
	<div class="container "> 
		<h3 class="heading text-center">TAHUKAH ANDA</h3>
		<div class="feature-list__three">
			<div class="row text-center">
				<div class="col-md-3 col-sm-6">
					<div class="ht-box-icon style-01 single-svg-icon-box">
						<div class="icon-box-wrap">
							<i class="fal fa-users fa-3x"></i>
							<h5>205 Juta</h5>
							<p>Saat ini, lebih dari 205 juta pengguna internet di Indonesia.</p>	
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="ht-box-icon style-01 single-svg-icon-box">
						<div class="icon-box-wrap">
							<i class="social-icon fab fa-instagram fa-3x"></i>
							<h5>190 Juta</h5>
							<p>190 juta orang terkoneksi dan berinteraksi di sosial media.</p>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="ht-box-icon style-01 single-svg-icon-box">
						<div class="icon-box-wrap">
							<i class="fal fa-clock fa-3x"></i>
							<h5>10 Jam / hari</h5>
							<p>Mereka menghabiskan 10 jam perhari untuk mengakses internet.</p>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="ht-box-icon style-01 single-svg-icon-box">
						<div class="icon-box-wrap">
							<i class="fal fa-signal fa-3x"></i>
							<h5>84 %</h5>
							<p>84% pengguna internet adalah usia produktif, yaitu 19-54 tahun.</p>	
						</div>
					</div>
				</div>
			</div>			
		</div>
		
		<div class="text-center">
			<h5 class="mt-20 mb-10">APA ARTINYA ?</h5>
			<p>
				Era Digital bukan lagi masa depan. Sekaranglah Era Digital.<br/>
				Dimana segala yang berbentuk konvensional, analog, dan manual mulai ditinggalkan.<br/>
				Beralih menjadi era digital yang serba otomatis, cepat, dan terukur.
			</p>
			<h5 class="mt-10 mb-10">LALU BAGAIMANA DENGAN POLITIK ?</h5>
			<p>
				Ya. Begitu juga dengan dunia politik. Karakter pemilih sekarang berbeda dengan sebelumnya.<br/>
				Pemilih saat ini lebih rasional, memilih karena visi, misi, dan program. Mereka tidak mudah terpengaruh dengan isu-isu SARA.<br/>
				Pemilih saat ini juga lebih menyukai yang calon yang cerdas, santun, dan berkarakter.<br/>
				Selain itu secara sosialogis, pemilih saat ini menyukai calon yang lebih dekat.<br/>
				Bukan dekat secara fisik, melainkan dekat karena mudah berkomunikasi dan mendengar setiap keluhan dari pemilihnya.<br/>
				Dan, Kampanye Digital adalah program yang paling tepat dan harus digunakan oleh setiap calon anggota legislatif.
			</p>
		</div>
		
		<h4 class="heading text-center mt-30 mb-10">KELEBIHAN KAMPANYE DIGITAL</h4>
		<div class="feature-list__three">
            <div class="row">
			
				<div class="col-lg-4">
					<div class="grid-item animate">
						<div class="ht-box-icon style-03">
							<div class="icon-box-wrap">
								<div class="content-header">
									<div class="icon">
										<i class="fal fa-compass"></i>
									</div>
									<h5 class="heading">
										Target Lebih Jelas
									</h5>
								</div>
								<div class="content">
									<div class="text">Dengan kampanye digital, Anda dapat menentukan dengan jelas target kampanye. Baik secara lokasi, demografi, dan psikografi.</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="grid-item animate">
						<div class="ht-box-icon style-03">
							<div class="icon-box-wrap">
								<div class="content-header">
									<div class="icon">
										<i class="fal fa-clock"></i>
									</div>
									<h5 class="heading">
										Proses Lebih Cepat
									</h5>
								</div>
								<div class="content">
									<div class="text">Kampanye konvensional memakan waktu untuk produksi dan distribusi. Kampanye digital dapat dengan cepat diterima oleh target.</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="grid-item animate">
						<div class="ht-box-icon style-03">
							<div class="icon-box-wrap">
								<div class="content-header">
									<div class="icon">
										<i class="fal fa-wallet"></i>
									</div>
									<h5 class="heading">
										Biaya Lebih Hemat
									</h5>
								</div>
								<div class="content">
									<div class="text">Anda dapat mengontrol biaya kampanye digital, tentunya Kampanye Digital jauh lebih hemat dibandingkan kampanye konvensional.</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>