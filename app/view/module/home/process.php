<div class="feature-icon-wrapper section-space--ptb_60 bg-white">
	<div class="container "> 
		<h3 class="heading text-center">MULAI DARI MANA</h3>		
		<div class="row ">
			<div class="col-md-4 col-sm-4">
				<div class="ht-box-icon style-01 single-svg-icon-box">
					<div class="icon-box-wrap">
						<h3>1</h3>
						<h5>Register & Login</h5>
						<p>Lakukan pendaftaran / login ke akun dengan mengisi usernam & password.</p>	
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="ht-box-icon style-01 single-svg-icon-box">
					<div class="icon-box-wrap">
						<h3>2</h3>
						<h5>Isi Formulir Data</h5>
						<p>Isi data-data website dan profil dengan mengisi formulir yang ada .</p>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="ht-box-icon style-01 single-svg-icon-box">
					<div class="icon-box-wrap">
						<h3>3</h3>
						<h5>Website Langsung Jadi</h5>
						<p>Dalam waktu 1 menit, website Anda langsung jadi dan langsung online.</p>
					</div>
				</div>
			</div>
		</div>			
	</div>
</div>