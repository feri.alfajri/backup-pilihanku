<!DOCTYPE html> 
<html lang="en">
    
<head>

    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
	
</head>

<body>
    
	<div id="wrapper"> 
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>
		
		<div id="content-wrapper"> 
		    
			<div id="content">
			
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
				    
					<?php
					
					function menu_admin ($menu,$link,$act) {
					    ?>
					    <div style="display:table;width:100%;margin-top:10px; margin-bottom:-8px;">
    						<!-- <?php  if ($act=='banner' or $act=='') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?> -->
    						<!-- <a href="<?php echo URL_DOMAIN.$menu."/".$link;?>/0/banner" class="btn <?php echo $btnstyle;?> ml-3 mr-2 pb-3 text-white" title="Banner">Banner</a> -->
    						<?php  if ($act=='welcome') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
    						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>/0/welcome" class="btn <?php echo $btnstyle;?> mr-2 pb-3 text-white" title="Selamat Datang">Selamat Datang</a>
    						<?php  if ($act=='cta') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
    						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>/0/cta" class="btn <?php echo $btnstyle;?> mr-2 pb-3 text-white" title="Call To Action">Call To Action</a>
    					</div>
					    <?php
					}
					
					if (empty ($act)) {
					    
					    $admin->title('Banner');
						menu_admin ($menu,$link,$act);
						$admin->read('setting','Gambar Banner','banner'," WHERE subdomain='".SUBDOMAIN."' "); 
						$admin->update('setting','picture','Gambar Banner','banner'," WHERE subdomain='".SUBDOMAIN."'");
						
					} 
					elseif ($act=='welcome') {
						
						$admin->title('Ubah Selamat Datang');
						menu_admin ($menu,$link,$act);
						$admin->update('setting','','Ucapan Selamat Datang','content_welcome'," WHERE subdomain='".SUBDOMAIN."'");
						
					}
					elseif ($act=='cta') {
						
						$admin->title('Ubah Call To Action');
						menu_admin ($menu,$link,$act);
						$admin->update('setting','','Ucapan CTA','content_cta'," WHERE subdomain='".SUBDOMAIN."'");
						
					}
					else {
	                    
	                    $admin->title('Banner');
						menu_admin ($menu,$link,$act);
						$admin->read('setting','Gambar Banner','banner'," WHERE subdomain='".SUBDOMAIN."' "); 
						$admin->update('setting','picture','Gambar Banner','banner'," WHERE subdomain='".SUBDOMAIN."' ");
						
					}
					?>
			
                    
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>

</html>