<div class="pricing-table-area section-space--ptb_60 bg-gray">
	<div class="pricing-table-title-area position-relative">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title-wrapper text-center section-space--mb_40 wow move-up">
						<h3 class="section-title">PILIHAN PAKET</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="pricing-table-content-area">
		<div class="container">

			<div class="tab-content ht-tab__content wow move-up">
				<div class="tab-pane fade show active">
					<div class="row pricing-table-one">
						<div class="col-12 col-md-6 col-lg-4 col-xl-4 pricing-table">
							<div class="pricing-table__inner">
								<div class="pricing-table__header">									
									<div class="pricing-table__image">
										<img src="<?php echo URL_IMAGE;?>free.png" class="img-fluid" alt="free">
									</div>
									<h6 class="sub-title">Simple</h6>
									<h5>FREE</h5>
								</div>
								<div class="pricing-table__body">
									<div class="pricing-table__footer">
										<a href="<?php echo URL_DOMAIN;?>create" class="ht-btn ht-btn-md ht-btn--outline" title="Buat Sekarang">Buat Sekarang</a>
									</div>
									<ul class="pricing-table__list text-left">
										<li>Data Profil / CV</li>
										<li>10 Post Blog / Artikel</li>
										<li>10 Post Image Gallery</li>
										<li>Tanya Jawab / Aspirasi</li>
										<li>Kontak & Social Media</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-12 col-md-6 col-lg-4 col-xl-4 pricing-table pricing-table--popular">
							<div class="pricing-table__inner">
								<div class="pricing-table__feature-mark">
									<span>Popular Choice</span>
								</div>
								<div class="pricing-table__header">
									<div class="pricing-table__image">
										<img src="<?php echo URL_IMAGE;?>pro.png" class="img-fluid" alt="pro">
									</div>
									<h6 class="sub-title">PROFESIONAL</h6>
									<h5>Rp. 1 juta</h5>
								</div>
								<div class="pricing-table__body">
									<div class="pricing-table__footer">
										<a href="<?php echo URL_DOMAIN;?>create" class="ht-btn ht-btn-md" title="Buat Sekarang">Buat Sekarang</a>
									</div>
									<ul class="pricing-table__list text-left">
										<li>Data Profil / CV</li>
										<li>UNLIMITED Post Blog / Artikel</li>
										<li>UNLIMITED Post Image Gallery</li>
										<li>Tanya Jawab / Aspirasi</li>
										<li>Kontak & Social Media</li>
										<li>Pendaftaran Relawan</li>
										<li>Donasi Online</li>
										<li>Ganti Theme Design</li>
										<li>Konsultasi</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-12 col-md-6 col-lg-4 col-xl-4 pricing-table">
							<div class="pricing-table__inner">
								<div class="pricing-table__header">
									<div class="pricing-table__image">
										<img src="<?php echo URL_IMAGE;?>custom.png" class="img-fluid" alt="custom">
									</div>
									<h6 class="sub-title">CUSTOM</h6>
									<h5>Rp. 3 juta</h5>
								</div>
								<div class="pricing-table__body">
									<div class="pricing-table__footer">
										<a href="<?php echo URL_DOMAIN;?>create" class="ht-btn ht-btn-md ht-btn--outline" title="Buat Sekarang">Buat Sekarang</a>
									</div>
									<ul class="pricing-table__list text-left">
										<li>Data Profil / CV</li>
										<li>UNLIMITED Post Blog / Artikel</li>
										<li>UNLIMITED Post Image Gallery</li>
										<li>Tanya Jawab / Aspirasi</li>
										<li>Kontak & Social Media</li>
										<li>Pendaftaran Relawan</li>
										<li>Donasi Online</li>
										<li>Ganti Theme Design</li>
										<li>FREE Konsultasi</li>
										<li>FREE Desain Slider</li>
										<li>FREE Custom Theme</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>