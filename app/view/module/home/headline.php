<div class="processing-hero processing-hero-bg">
	<div class="container">
		<div class="row align-items-center" style="padding:80px 0"> 
			<div class="col-lg-7 col-md-7">
				<div class="processing-hero-text wow move-up">
					<h2 class="font-weight--reguler mb-15" style="color:#FFFFFF">Inilah Cara Kampanye Politik Paling Mudah !</h2>
					<p>
						Sekarang bukan jamannya lagi menggunakan flyer, baliho, ataupun stiker untuk kampanye Anda. Saatnya kampanye politik melalui digital.
						Rencanakan kemenangan politik Anda, mulailah dengan membuat website kampanye.
						Disini, Anda bisa membuatnya sendiri, dalam waktu 1 menit, website kampanye Anda langsung jadi.
						Klik <b>Buat Sekarang</b> untuk mulai sekarang juga, GRATIS...!
					</p>
					<div class="hero-button mt-20">
						<a href="<?php echo URL_DOMAIN;?>create" class="btn btn--primary" title="Buat Sekarang">Buat Sekarang</a>
					</div>
				</div>
			</div>
			<div class="col-lg-5 col-md-5">
				<div class="processing-hero-images-wrap wow move-up">
					<div class="video-popup infotech-video-box">
						<img src="<?php echo URL_IMAGE;?>headline.png" alt="headline" width="100%">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>