<div class="feature-images-wrapper section-space--ptb_60 bg-gray">
	<div class="container "> 
		<h3 class="heading text-center">FITUR-FITUR</h3>		
		<div class="feature-images__two small-mt__10">
            <div class="modern-grid-image-box row row--30">

				<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_40  small-mt__40">
					<a href="<?php echo URL_DOMAIN;?>twibbon" class="ht-box-images style-02">
						<div class="image-box-wrap" style="background-color: #faeca2;">
							<div class="box-image">
								<img class="img-fulid" src="<?php echo URL_IMAGE;?>flyer.png" alt="blog">
							</div>
							<div class="content">
								<h6 class="heading">Template Desain Flyer Digital</h6>
							</div>
						</div>
					</a>
				</div>
				
				<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_40  small-mt__40">
					<a class="ht-box-images style-02">
						<div class="image-box-wrap">
							<div class="box-image">
								<img class="img-fulid" src="<?php echo URL_IMAGE;?>website.png" alt="website">
							</div>
							<div class="content">
								<h6 class="heading">Website Kampanye Caleg</h6>
							</div>
						</div>
					</a>
				</div>
				<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_40  small-mt__40">
					<a class="ht-box-images style-02">
						<div class="image-box-wrap">
							<div class="box-image">
								<img class="img-fulid" src="<?php echo URL_IMAGE;?>cv.png" alt="cv">
							</div>
							<div class="content">
								<h6 class="heading">Pusat Profil / Curriculum Vitae</h6>
							</div>
						</div>
					</a>
				</div>
				<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_40  small-mt__40">
					<a class="ht-box-images style-02">
						<div class="image-box-wrap">
							<div class="box-image">
								<img class="img-fulid" src="<?php echo URL_IMAGE;?>gallery.png" alt="gallery">
							</div>
							<div class="content">
								<h6 class="heading">Galeri Foto & Video Kegiatan</h6>
							</div>
						</div>
					</a>
				</div>
				
				<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_40  small-mt__40">
					<a class="ht-box-images style-02">
						<div class="image-box-wrap">
							<div class="box-image">
								<img class="img-fulid" src="<?php echo URL_IMAGE;?>blog.png" alt="blog">
							</div>
							<div class="content">
								<h6 class="heading">Tulisan Artikel / Blog Post</h6>
							</div>
						</div>
					</a>
				</div>
				<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_40  small-mt__40">
					<a class="ht-box-images style-02">
						<div class="image-box-wrap">
							<div class="box-image">
								<img class="img-fulid" src="<?php echo URL_IMAGE;?>discussion.png" alt="blog">
							</div>
							<div class="content">
								<h6 class="heading">Tanya Jawab / Jaring Aspirasi</h6>
							</div>
						</div>
					</a>
				</div>
				<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_40  small-mt__40">
					<a class="ht-box-images style-02">
						<div class="image-box-wrap">
							<div class="box-image">
								<img class="img-fulid" src="<?php echo URL_IMAGE;?>volunteer.png" alt="volunteer">
							</div>
							<div class="content">
								<h6 class="heading">Pendaftaran Tim Sukses / Relawan</h6>
							</div>
						</div>
					</a>
				</div>
				<!-- <div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_40  small-mt__40">
					<a class="ht-box-images style-02">
						<div class="image-box-wrap">
							<div class="box-image">
								<img class="img-fulid" src="<?php echo URL_IMAGE;?>donation.png" alt="donation">
							</div>
							<div class="content">
								<h6 class="heading">Penggalangan Donasi Publik</h6>
							</div>
						</div>
					</a>
				</div> -->
				<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_40  small-mt__40">
					<a class="ht-box-images style-02">
						<div class="image-box-wrap">
							<div class="box-image">
								<img class="img-fulid" src="<?php echo URL_IMAGE;?>socialmedia.png" alt="donation">
							</div>
							<div class="content">
								<h6 class="heading">Pusat Integrasi Social Media</h6>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>