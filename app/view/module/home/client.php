<div class="feature-icon-wrapper section-space--ptb_60 bg-white">
	<div class="container "> 
		<h3 class="heading text-center mb-20">DARI MANA SAJA</h3>		
		<div class="brand-logo-slider__container-area">
			<div class="swiper-container brand-logo-slider__container">
				<div class="swiper-wrapper brand-logo-slider__one">

					

					<?php 
						include DIR_MODEL.'partai.php';
						$partaiModel = new partai();
						$partaiData = $partaiModel->getDataAll();
						

						foreach($partaiData as $data){
					?>

						<div class="swiper-slide brand-logo brand-logo--slider">
							<a href="#">
								<div class="brand-logo__image">
									<img src="<?php echo URL_PICTURE . $data['picture']?>" class="img-fluid" alt="">
								</div>
								<div class="brand-logo__image-hover">
									<img src="<?php echo URL_PICTURE . $data['picture']?>" class="img-fluid" alt="">
								</div>
							</a>
						</div>

					<?php } ?>
				
				</div>
			</div>
		</div>
		
	</div>
</div>