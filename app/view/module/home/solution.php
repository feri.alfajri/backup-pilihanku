<div class="contact-us-area infotechno-contact-us-bg section-space--ptb_60"> 
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-12 col-xs-12">
				<img src="<?php echo URL_IMAGE;?>bingung.png" class="img-responsive mb-20" width="100%" alt="Bingung">
			</div>
			<div class="col-md-9 col-sm-12 col-xs-12 text-left">
				<h3 class="mb-20">MASALAHNYA ADALAH ANDA</h3>
				<ul style="list-style:square;margin-left:20px;font-size:18px;">
					<li>Tidak punya kemampuan membuat website.</li>
					<li>Tidak bisa membuat konten secara rutin.</li>
					<li>Tidak sempat update status dan mengikuti tren saat ini.</li>
					<li>Tidak tau bagaimana mendistribusikan konten secara maksimal.</li>
					<li>Tidak mengetahui cara setting iklan facebook & google.</li>
					<li>Apalagi harus membuat gimmick yang menarik perhatian audience.</li>
				</ul>
			</div>
		</div>
		<h5 class="text-center mt-20">
		Jika Anda merasakan hal-hal seperti yang disebutkan di atas,<br/>
		Anda beruntung telah mengunjungi website ini.
		<br/>
		<div class="mt-20 mb-20">MEMPERKENALKAN</div>
		</h5>
		<center><img src="<?php echo URL_IMAGE;?>logo.png" class="img-responsive" alt="logo"></center>
	</div>
</div>