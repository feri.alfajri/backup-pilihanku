<div class="contact-us-area infotechno-contact-us-bg section-space--ptb_60"> 
	<div class="container">
		<h4 class="mb-10">Galeri Foto</h4>
		<div class="row mesonry-list">
			<?php
			if ($galleryCount!=0) {
				foreach($galleryData as $data){
					$galleryNo = $data['no'];
					$galleryName = $data['name'];
					$galleryLink = $data['link'];
					$galleryPicture = $data['picture'];
					?>
					<div class="col-lg-3 col-md-6">
						<a href="<?php echo URL_PICTURE.$galleryPicture;?>" target="blank" class="projects-wrap style-2" title="<?php echo $galleryName;?>">
							<div class="projects-image-box">
								<div class="projects-image">
									<img class="img-fluid" alt="<?php echo $galleryLink;?>" src="<?php echo URL_UPLOAD;?>image.php/<?php echo $galleryPicture;?>?width=480&cropratio=16:9&nocache&quality=100&image=<?php echo URL_PICTURE.$galleryPicture;?>" width="100%">
								</div>
								<div class="content">
									<h6 class="heading"><?php echo $galleryName;?></h6>
								</div>
							</div>
						</a>
					</div>
					<?php
				}				
			}
			else {
				?>
				<div class="col-lg-3 col-md-6">
					<a href="<?php echo URL_IMAGE;?>gal_1.jpg" target="blank" class="projects-wrap style-2">
						<div class="projects-image-box">
							<div class="projects-image"><img class="img-fluid" alt="galeri-1"  width="100%" src="<?php echo URL_IMAGE;?>gal_1.jpg"></div>
							<div class="content"><h6 class="heading">Contoh Foto 1</h6></div>
						</div>
					</a>
				</div>
				<div class="col-lg-3 col-md-6">
					<a href="<?php echo URL_IMAGE;?>gal_2.jpg" target="blank" class="projects-wrap style-2">
						<div class="projects-image-box">
							<div class="projects-image"><img class="img-fluid" alt="galeri-2"  width="100%" src="<?php echo URL_IMAGE;?>gal_2.jpg"></div>
							<div class="content"><h6 class="heading">Contoh Foto 2</h6></div>
						</div>
					</a>
				</div>
				<div class="col-lg-3 col-md-6">
					<a href="<?php echo URL_IMAGE;?>gal_3.jpg" target="blank" class="projects-wrap style-2">
						<div class="projects-image-box">
							<div class="projects-image"><img class="img-fluid" alt="galeri-3"  width="100%" src="<?php echo URL_IMAGE;?>gal_3.jpg"></div>
							<div class="content"><h6 class="heading">Contoh Foto 3</h6></div>
						</div>
					</a>
				</div>
				<div class="col-lg-3 col-md-6">
					<a href="<?php echo URL_IMAGE;?>gal_4.jpg" target="blank" class="projects-wrap style-2">
						<div class="projects-image-box">
							<div class="projects-image"><img class="img-fluid" alt="galeri-4"  width="100%" src="<?php echo URL_IMAGE;?>gal_4.jpg"></div>
							<div class="content"><h6 class="heading">Contoh Foto 4</h6></div>
						</div>
					</a>
				</div>
				<?php
			}
			?>
		</div>
		<div class="text-center"><a href="<?php echo URL_DOMAIN;?>gallery" class="btn btn--white btn-one">Selengkapnya</a></div>
	</div>
</div>