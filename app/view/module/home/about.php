<div class="feature-icon-wrapper section-space--ptb_60 bg-white">
	<div class="container "> 
		<h3 class="heading text-center">APA ITU</h3>		
		<div class="text-center">
			<img src="<?php echo URL_IMAGE;?>logo.png" class="img-responsive" alt="<?php echo $name;?>">
			<p>
				PilihanKu.net merupakan platform digital untuk mendukung event politik di Indonesia. 
				PilihanKu.net ingin menghadirkan budaya politik baru yaitu dengan meningkatkan partisipasi masyarakat melalui digitalisasi politik agar terlahir pemimpin-pemimpin yang akan membawa Indonesia lebih baik.
			</p>
			<p>
			Sebagai Seorang Pemimpin, Politisi, Pejabat atau Aktivis Sosial-Politik, Anda harus membangun profil dan citra positif serta memublikasikan kegiatan dan pencapaian anda dengan baik kepada publik. 
			Mulailah membangun profil digital sosial-politik Anda, kampanyekan hal-hal positif dan raih dukungan besar dari masyarakat dengan mendaftar di Platform Kampanye Digital PilihanKu.net.
			</p>
		</div>
	</div>
</div>