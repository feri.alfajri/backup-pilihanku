<div class="cta-image-area_one section-space--ptb_60 cta-bg-image_one"> 
	<div class="container">
		<div class="row align-items-center">
			<div class="col-xl-8 col-lg-8">
				<div class="cta-content md-text-center">
					<h4 class="heading text-white"><?php echo $content_cta;?></h4>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4">
				<div class="cta-button-group--one text-center">
					<a href="<?php echo URL_DOMAIN;?>volunteer" class="btn btn--white btn-one" title="Daftar Relawan">
						<span class="btn-icon mr-2"><i class="far fa-users"></i></span>Daftar Relawan
					</a>
				</div>
				<div class="cta-button-group--one text-center">
					<a href="<?php echo URL_DOMAIN;?>contact" class="btn btn--white btn-one" title="Hubungi Kami">
						<span class="btn-icon mr-2"><i class="far fa-comment-alt-dots"></i></span>Hubungi Kami
					</a>
				</div>
			</div>
		</div>
	</div>
</div>