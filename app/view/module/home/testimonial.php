<div class="testimonial-slider-area section-space--ptb_60 bg-gray-2">
	<div class="container "> 
		<h3 class="heading text-center">TESTIMONI</h3>		
		<div class="testimonial-slider">
			<div class="swiper-container testimonial-slider__container">
				<div class="swiper-wrapper testimonial-slider__wrapper">
					
					<div class="swiper-slide">
						<div class="testimonial-slider__one wow move-up">
							<div class="testimonial-slider--info">
								<div class="testimonial-slider__media">
									<img src="<?php echo URL_IMAGE;?>triastuti.jpg" width="64" class="img-fluid" alt="triastuti">
								</div>
								<div class="testimonial-slider__author">
									<div class="testimonial-rating">
										<span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span>
									</div>
									<div class="author-info">
										<h6 class="name">Tri Astuti</h6>
										<span class="designation">Caleg DPRD Kabupaten Temanggung</span>
									</div>
								</div>
							</div>
							<div class="testimonial-slider__text">
							I’ve been working with over 35 IT companies on more than 200 projects of our company, but @Mitech is one of the most impressive to me.
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="testimonial-slider__one wow move-up">
							<div class="testimonial-slider--info">
								<div class="testimonial-slider__media">
									<img src="<?php echo URL_IMAGE;?>trisulo.jpg" width="64" class="img-fluid" alt="trisulo">
								</div>
								<div class="testimonial-slider__author">
									<div class="testimonial-rating">
										<span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span>
									</div>
									<div class="author-info">
										<h6 class="name">Tri Sulo</h6>
										<span class="designation">Caleg DPRD Kabupaten Boyolali</span>
									</div>
								</div>
							</div>
							<div class="testimonial-slider__text">
							I’ve been working with over 35 IT companies on more than 200 projects of our company, but @Mitech is one of the most impressive to me.
							</div>
						</div>
					</div>
					
				</div>
				<div class="swiper-pagination swiper-pagination-t01 section-space--mt_30"></div>
			</div>
		</div>
	</div>
</div>