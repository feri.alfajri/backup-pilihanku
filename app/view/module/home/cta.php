<div class="cta-image-area_one section-space--ptb_80 cta-bg-image_one"> 
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-8 col-lg-8">
                <div class="cta-content md-text-center">
                    <h4 class="heading text-white">Ayo, Saatnya Raih Kemenangan dengan Kampanye Digital dan Buat Website Kampanye Anda Sekarang Juga, GRATIS...!</h4>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="cta-button-group--one text-center">
                    <a href="<?php echo URL_DOMAIN;?>create" class="btn btn--primary btn-one  text-white" title="Buat Sekarang">
						<span class="btn-icon mr-2"><i class="far fa-arrow-right"></i></span>Buat Sekarang
					</a>
                </div>
            </div>
        </div>
    </div>
</div>