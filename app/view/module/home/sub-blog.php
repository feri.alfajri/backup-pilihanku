<div class="projects-wrapper section-space--ptb_60 projects-masonary-wrapper">
	<div class="container">			
		<h4 class="mb-10">Artikel</h4>
		<div class="row mesonry-list mb-20">
			<?php
			if ($blogCount!=0) {
				foreach($blogData as $data){
					$blogNo = $data['no'];
					$blogJudul = $data['title'];
					$blogLink = $data['link'];
					$blogPicture = $data['picture'];
					$blogTanggal = date('d F Y', strtotime($data['date'])); 
					$blogJam = date('H:i',strtotime($data['date'])); 
					$blogTanggal = $blogTanggal.', pukul '.$blogJam;
					$blogUrlDetail = URL_DOMAIN.'blog/detail/'.$blogNo.'/'.$blogLink;
					?>
					<div class="col-lg-4 col-md-6">
						<a href="<?php echo $blogUrlDetail;?>" class="projects-wrap">
							<div class="projects-image-box" style="border:1px solid #F0F0F0;">
								<div class="projects-image">
									<img class="img-fluid" alt="<?php echo $blogLink;?>" src="<?php echo URL_UPLOAD;?>image.php/<?php echo $blogPicture;?>?width=480&cropratio=16:9&nocache&quality=100&image=<?php echo URL_PICTURE.$blogPicture;?>" width="100%">
								</div>
								<div class="content text-left" style="padding:15px 20px;">
									<h6 class="heading text-left"><?php echo $blogJudul;?></h6>
									<small>Ditulis tanggal <?php echo $blogTanggal;?></small>
								</div>
							</div>
						</a>
					</div>
					<?php
				}
			}
			else {
				?>
				<div class="col-lg-4 col-md-6">
					<a href="#" class="projects-wrap">
						<div class="projects-image-box" style="border:1px solid #F0F0F0;">
							<div class="projects-image"><img class="img-fluid" alt="blog-1" src="<?php echo URL_IMAGE;?>blog_1.jpg" width="100%"></div>
							<div class="content text-left" style="padding:15px 20px;">
								<h6 class="heading text-left">Inilah Provinsi dengan Kuota Kursi DPR Terbanyak Periode 2024</h6>
								<small>Ditulis tanggal 11 Januari 2023 Pukul 08:00</small>
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-6">
					<a href="#" class="projects-wrap">
						<div class="projects-image-box" style="border:1px solid #F0F0F0;">
							<div class="projects-image"><img class="img-fluid" alt="blog-2" src="<?php echo URL_IMAGE;?>blog_2.jpg" width="100%"></div>
							<div class="content text-left" style="padding:15px 20px;">
								<h6 class="heading text-left">Inilah Daftar Nomor Urut Partai Politik Peserta Pemilu 2024</h6>
								<small>Ditulis tanggal 5 Januari 2023 Pukul 12:30</small>
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-6">
					<a href="#" class="projects-wrap">
						<div class="projects-image-box" style="border:1px solid #F0F0F0;">
							<div class="projects-image"><img class="img-fluid" alt="blog-3" src="<?php echo URL_IMAGE;?>blog_3.jpg" width="100%"></div>
							<div class="content text-left" style="padding:15px 20px;">
								<h6 class="heading text-left">Kampanye di Media Sosial Mesti Masif dan Sistematis, Kenapa ?</h6>
								<small>Ditulis tanggal 1 Januari 2023 Pukul 15:45</small>
							</div>
						</div>
					</a>
				</div>
				<?php
				
			}
			?>						
		</div>
		<div class="text-center"><a href="<?php echo URL_DOMAIN;?>blog" class="btn btn--white btn-one">Selengkapnya</a></div>
	</div>
</div>