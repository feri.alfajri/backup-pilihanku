<div class="flexible-image-slider-wrapper section-space--ptb_60 border-bottom">
	<div class="container">
		<h3 class="mb-30 text-center">PORTOFOLIO</h3>
		<div class="flexible-image-slider-wrap">
			<div class="swiper-container three-flexible__container">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="single-flexible-slider">
							<img class="img-fluid" src="<?php echo URL_IMAGE;?>portofolio.png" alt="Slider 01">
						</div>
					</div>
					<div class="swiper-slide">
						<div class="single-flexible-slider">
							<img class="img-fluid" src="<?php echo URL_IMAGE;?>portofolio.png" alt="Slider 02">
						</div>
					</div>
					<div class="swiper-slide">
						<div class="single-flexible-slider">
							<img class="img-fluid" src="<?php echo URL_IMAGE;?>portofolio.png" alt="Slider 03">
						</div>
					</div>
				</div>

				<div class="swiper-nav-button swiper-button-next"><i class="nav-button-icon"></i></div>
				<div class="swiper-nav-button swiper-button-prev"><i class="nav-button-icon"></i></div>
			</div>
			<div class="swiper-pagination swiper-pagination-3 section-space--mt_50"></div>
		</div>
	</div>
</div>