<!DOCTYPE html>
<html dir="ltr" lang="id-ID">
    
<head><?php
    include (DIR_THEME.THEME.'/app/asset-head.php');
    include (DIR_THEME.THEME.'/app/metatag.php'); 
    include (DIR_THEME.THEME.'/app/opengraph.php');
    include (DIR_THEME.THEME.'/app/favicon.php');
    include (DIR_THEME.THEME.'/app/title.php');
    ?>
</head>

<body>
	<?php
	include (DIR_THEME.THEME.'/app/loader.php');
	include (DIR_THEME.THEME.'/app/header.php');
	
	include 'headline.php';
	include 'fact.php';
	include 'solution.php';
	include 'about.php';
	include 'feature.php';
	include 'portofolio.php';
	include 'testimonial.php';
	include 'client.php';
	//include 'price.php';
	include 'process.php';
	include 'cta.php';

	include (DIR_THEME.THEME.'/app/footer.php');
	include (DIR_THEME.THEME.'/app/asset-body.php');
	?>
</body>

</html>