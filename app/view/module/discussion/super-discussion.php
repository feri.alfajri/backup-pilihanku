<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_SUPER.'/app/asset-head.php';?>
</head>

<body class="nav-fixed">
   
	<?php include DIR_THEME.THEME_SUPER.'/app/topbar.php'; ?>
	
	<div id="layoutSidenav">
	
        <div id="layoutSidenav_nav">
			<?php include DIR_THEME.THEME_SUPER.'/app/sidebar.php'; ?>			
		</div>
		
		<div id="layoutSidenav_content">
			<main>
				
				<?php include DIR_THEME.THEME_SUPER.'/app/header.php'; ?>

				<div class="container-xl px-4 mt-4">
					
					<?php                   
					$admin = new admin();
					
					$title = 'Diskusi';  
					
					$table_name = 'discussion';
					$table_label = 'subdomain,nama,pertanyaan,tanggal';
					$table_column = 'subdomain,name,question,date';
					$table_width = '5,15,35,10'; 
					$table_button = 'update,read,delete';  
					$table_condition = " ";
										
					$read_label = 'subdomain,nama,telepon,email,pertanyaan,jawaban,tanggal';
					$read_content = 'subdomain,name,phone,email,question,answer,date';
					$read_condition = "WHERE no='$no'";
					
					$update_type = '';
					$update_label = 'subdomain,nama,telepon,email,pertanyaan,jawaban';
					$update_form = 'subdomain,name,phone,email,question,answer';
					$update_condition = "WHERE no='$no'";
					
					$delete_condition = "WHERE no='$no'";
					
					if (empty ($act)) {
						
						$admin->title($title);
						$admin->tablepro($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 	
					elseif ($act=='read') {						
						
						$admin->title($title);
						$admin->read($table_name,$read_label,$read_content,$read_condition); 
						$admin->button ('back','Kembali','','btn-primary');
						
					} 
					elseif ($act=='update') {
						
						$admin->title('Ubah '.$title);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 					
					elseif ($act=='delete') {	
										
						//$admin->delete($table_name,$delete_condition);
						$admin->title($title);
						echo '<div class="alert alert-danger" role="alert">Demi Keamanan, Fitur Hapus Dinonaktifkan</div>';
						$admin->tablepro($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
						
					} 
					else {						
						
						$admin->title($title);
						$admin->tablepro($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					}
					?>		
					
				</div>
				
			</main>
		</div>
		
	</div>	
	
	<?php include DIR_THEME.THEME_SUPER.'/app/asset-body.php'; ?>
	
</body>
 
</html>