<!DOCTYPE html>
<html dir="ltr" lang="id-ID">
    
<head><?php
    include (DIR_THEME.THEME.'/app/asset-head-sub.php');
    include (DIR_THEME.THEME.'/app/metatag.php'); 
    include (DIR_THEME.THEME.'/app/opengraph.php');
    include (DIR_THEME.THEME.'/app/favicon.php');
    include (DIR_THEME.THEME.'/app/title.php');
    ?>
</head>

<body>
	<?php
	include (DIR_THEME.THEME.'/app/loader.php');
	include (DIR_THEME.THEME.'/app/header-top.php');
	include (DIR_THEME.THEME.'/app/header-sub.php');
	?>
	<div id="main-wrapper">
        <div class="site-wrapper-reveal">		
			<div class="blog-pages-wrapper section-space--ptb_60">
                <div class="container">
					<?php
					if ($count == 0) {	 }
					else {
						?>
						<h3 class="mb-15">Tanya Jawab</h3>
						<ol class="comment-list">
							<?php
							foreach($discussionData as $data){
								$discussionName = $data['name'];
								$discussionQuestion = $data['question'];
								$discussionAnswer = $data['answer'];
								$discussionDate = date('d F Y', strtotime($data['date'])); 
								$discussionClock = date('h:i',strtotime($data['date'])); 
								$discussionTime = $discussionDate.' Pukul '.$discussionClock;
								?>
								<li class="comment">
									<div class="comment-author vcard">
										<img alt="comment" src="<?php echo URL_IMAGE;?>member.png">
									</div>
									<div class="comment-content">
										<div class="meta">
											<b class="h5"><?php echo $discussionName;?></b>
											<p class="comment-datetime"> <?php echo $discussionTime;?></p>
										</div>
										<div class="comment-text">
											<p><?php echo $discussionQuestion;?></p>
										</div>										
									</div>
									<?php
									if ($discussionAnswer!='') {
										?>
										<ol class="children">
											<li class="comment ">
													<div class="comment-author vcard">
														<img alt="comment" src="<?php echo URL_UPLOAD;?>image.php/<?php echo $logo;?>?width=86&cropratio=1:1&nocache&quality=100&image=<?php echo URL_PICTURE.$logo;?>">
													</div>
													<div class="comment-content">
														<div class="comment-text">
															<b>Jawab :</b>
															<p><?php echo $discussionAnswer;?></p>
														</div>
													</div>
											</li>
										</ol><?php
									}
									?>
								</li>
								<hr/>
								<?php
							}
							?>
						</ol><?php
					}
					?>
					
					<div class="row">
						<div class="col-lg-12">
							<h4 class="widget-title mb-20">Kirim Pertanyaan </h4>
							<p>Email Anda tidak akan kami publish. Form bertanda * harus diisi</p>
						</div>
						<div class="col-lg-12">
							<div class="contact-from-wrapper section-space--mt_30">
								<form action="#" method="post">
									<input name='proses' value="save" hidden>
									<div class="contact-page-form">
										<div class="contact-inner">
											<input name="name" type="text" placeholder="Nama Anda *" required>
										</div>
										<div class="contact-input">												
											<div class="contact-inner">
												<input name="phone" type="text" placeholder="Nomor Telepon / WA *" required>
											</div>
											<div class="contact-inner">
												<input name="email" type="email" placeholder="Email Anda *" required>
											</div>
										</div>
										<div class="contact-inner contact-message">
											<textarea name="question" placeholder="Pertanyaan" required></textarea>
										</div>
										<div class="comment-submit-btn">
											<button class="ht-btn ht-btn-md" type="submit">Kirim</button>
										</div>
									</div>
								</form>									
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<?php
	include (DIR_THEME.THEME.'/app/footer-sub.php');
	include (DIR_THEME.THEME.'/app/asset-body-sub.php');
	?>
</body>

</html>