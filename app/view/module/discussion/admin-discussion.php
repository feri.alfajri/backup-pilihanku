<!DOCTYPE html> 
<html lang="en">
    
<head>

    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
	
</head>

<body>
    
	<div id="wrapper"> 
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>
		
		<div id="content-wrapper"> 
		    
			<div id="content">
			
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
				    
					<?php
					
					function menu_admin ($menu,$link,$act) {
					    ?>
					    <div style="display:table;width:100%;margin-top:10px; margin-bottom:-8px;">
    						<?php  if ($act=='' or $act=='unreplied') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
    						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>/0/unreplied" class="btn <?php echo $btnstyle;?> ml-3 mr-2 pb-3 text-white" title="Belum Dibalas">Belum Dibalas</a>
    						<?php  if ($act=='replied') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
    						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>/0/replied" class="btn <?php echo $btnstyle;?> mr-2 pb-3 text-white" title="Sudah Dibalas">Sudah Dibalas</a>
    					</div>
					    <?php
					}
					
					$admin = new admin();
					
					$title = 'Tanya Jawab';  
										
					$table_name = 'discussion';
					$table_label = 'nama,telepon,pertanyaan,tanggal';
					$table_column = 'name,phone,question,date';
					$table_width = '30,10,45,10'; 
					$table_button = 'update,read,delete';  
					$table_condition = " WHERE subdomain ='".SUBDOMAIN."' ";
					
					$read_label = 'nama,alamat,telepon,email,pertanyaan,jawaban,tanggal';
					$read_content = 'name,address,phone,email,question,answer,date';
					$read_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
					
					$update_type = '';
					$update_label = 'pertanyaan,jawaban,tampilkan';
					$update_form = 'question,answer,publish';
					$update_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
					
					$delete_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
					
					if (empty ($act)) {
						
						$table_condition = " WHERE subdomain ='".SUBDOMAIN."' AND answer='' ";
						$admin->title('Tanya Jawab (Belum Dibalas)');
						menu_admin ($menu,$link,$act);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					elseif ($act=='unreplied') {
						
						$table_condition = " WHERE subdomain ='".SUBDOMAIN."'  AND answer='' ";
						$admin->title('Tanya Jawab (Belum Dibalas)');
						menu_admin ($menu,$link,$act);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
					} 	
					elseif ($act=='replied') {
						
						$table_label = 'nama,pertanyaan,jawaban,tanggal';
						$table_column = 'name,question,answer,date';
						$table_width = '20,25,30,10'; 
						$table_condition = " WHERE subdomain ='".SUBDOMAIN."'  AND answer!='' ";
						$admin->title('Tanya Jawab (Sudah Dibalas)');
						menu_admin ($menu,$link,$act);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
					} 					
					elseif ($act=='read') {						
						
						$admin->title($title);
						$admin->read($table_name,$read_label,$read_content,$read_condition); 
						
					} 
					elseif ($act=='update') {
						
						$admin->title('Balas '.$title);
						$read_label = 'nama,alamat,telepon,email';
						$read_content = 'name,address,phone,email';
						$admin->read($table_name,$read_label,$read_content,$read_condition); 
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act=='delete') {	
										
						//$admin->delete($table_name,$delete_condition);		
						echo '<div class="alert alert-danger" role="alert">Demi Keamanan, Fitur Hapus Dinonaktifkan</div>';
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
						
					} 
					else {						
						
						$table_condition = " WHERE subdomain ='".SUBDOMAIN."'  AND answer='' ";
						$admin->title('Tanya Jawab (Belum Dibalas)');
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					}
					?>					
                    
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>

</html>