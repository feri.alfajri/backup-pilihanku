<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<h2 class="text-center mb-4">Bantuan</h2>
					<div class="row"> 
						<?php
						$helpUrl = URL_DOMAIN.$menu.'/'.$link;
						if ($act == '') {
							$query = "SELECT * FROM help WHERE publish='1' ORDER BY no ASC";
							$sqlquery = mysqli_query($model->koneksi,$query);
							$jumlah = mysqli_num_rows($sqlquery);
							while ($data = mysqli_fetch_array($sqlquery)) { 
								$helpNo = $data['no'];
								$helpName = $data['name'];
								$helpLink = $data['link'];
								$helpContent = substr($data['content'],0,150);
								$helpUrlDetail = URL_DOMAIN.$menu.'/'.$link.'/'.$helpNo.'/'.$helpLink;
								?>
								<div class="col-md-6"> 
									<div class="card mb-4">
										<div class="card-body">
											<h4><a href="<?php echo $helpUrlDetail;?>" title="<?php echo $helpName;?>"><?php echo $helpName;?></a></h4>
											<?php echo $helpContent;?>
											<br/>
											<a href="<?php echo $helpUrlDetail;?>" title="<?php echo $helpName;?>">selengkapnya</a>
										</div>
									</div>
								</div>
								<?php
							} 
						} 
						else {
							$query = "SELECT * FROM help WHERE publish = '1' AND no = '$no' LIMIT 1";
							$sqlquery = mysqli_query($model->koneksi,$query);
							$jumlah = mysqli_num_rows($sqlquery);
							if ($jumlah == 0) {
								?>
								<div class="alert alert-warning" role="alert">
									Maaf, Data yang Anda cari tidak tersedia.
								</div>
								<?php
							}
							else {
								$data = mysqli_fetch_array($sqlquery);
								$helpNo = $data['no'];
								$helpName = $data['name'];
								$helpLink = $data['link'];
								$helpContent = $data['content'];
								?>
								<div class="card">
									<div class="card-body">
										<h3><?php echo $helpName;?></h3>
										<p>
											<?php echo $helpContent;?>
										</p>
										<a href="<?php echo $helpUrl;?>" class="btn btn-primary" title="Kembali">Kembali</a>
									</div>
								</div>
								<?php
							} 
						} 
						?>
					</div>
					
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>
 
</html>