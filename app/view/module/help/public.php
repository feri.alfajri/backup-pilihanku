<!DOCTYPE html>
<html dir="ltr" lang="id-ID">
    
<head><?php
    include (DIR_THEME.THEME.'/app/asset-head.php');
    include (DIR_THEME.THEME.'/app/metatag.php'); 
    include (DIR_THEME.THEME.'/app/opengraph.php');
    include (DIR_THEME.THEME.'/app/favicon.php');
    include (DIR_THEME.THEME.'/app/title.php');
    ?>
</head>

<body>
	<?php
	include (DIR_THEME.THEME.'/app/loader.php');
	include (DIR_THEME.THEME.'/app/header.php');
	include (DIR_THEME.THEME.'/app/pagetitle.php');
	?>
	<div id="main-wrapper">
        <div class="site-wrapper-reveal">
		
			<div class="blog-pages-wrapper section-space--ptb_60">
                <div class="container">	
                    <div class="row">
                                   
                        <div class="col-lg-8 order-lg-2 order-2">
                            <div class="main-blog-wrap">
                                
                                <?php
								if($no == ''){
									if ($count == 0) {		
										?>
										<div class="alert alert-warning" role="alert">
											Mohon maaf, saat ini tidak ada artikel yang dapat ditampilkan.
										</div>
										<?php
									}
									else {
										foreach($helpData as $data){
											$helpNo = $data['no'];
											$helpName = $data['name'];
											$helpLink = $data['link'];
											$helpContent = $data['content'];											
											$helpPicture = $data['picture'];
											$helpPreview = substr($helpContent,0,200);
											$helpUrlDetail = URL_DOMAIN.$menu.'/detail/'.$helpNo.'/'.$helpLink;
											$helpUrlPicture = $helpPicture;
											?>
											<div class="single-blog-item lg-blog-item border-bottom wow move-up">    
												<div class="post-feature blog-thumbnail">
													<?php
													if(!empty($helpPicture)){ 
														?>
														<a href="<?php echo $helpUrlDetail;?>" title="<?php echo $helpName;?>">
															<img src="<?php echo URL_UPLOAD;?>image.php/<?php echo $helpUrlPicture;?>?width=800&cropratio=16:9&nocache&quality=100&image=<?php echo URL_PICTURE.$helpUrlPicture;?>" width="100%" alt="<?php echo $helpName;?>">
														</a>
														<?php
													} 
													?>
												</div>    
												<div class="post-info lg-blog-post-info">
													<h3 class="post-title">
														<a href="<?php echo $helpUrlDetail;?>" title="<?php echo $helpName;?>"><?php echo $helpName;?></a>
													</h3>  
													<div class="post-excerpt mt-10">
														<p><?php echo $helpPreview;?>…</p>
														<a href="<?php echo $helpUrlDetail;?>">Selengkapnya</a>
													</div>
												</div>											
											</div>
											<?php
										}
										?>											
										<div class="ht-pagination mt-30 pagination justify-content-center">
											<div class="pagination-wrapper">
												<ul class="page-pagination">
													<?php
													$jumhal=ceil($count/$limit); 
													$linkhal="#";
													if ($page>1) { $prev=$page-1; ?><li><a class="prev page-numbers" href="<?php echo URL_DOMAIN.$menu;?>" title="First">First</a></li><li><a class="prev page-numbers" href="<?php echo URL_DOMAIN.$menu."/page/".$prev;?>" title="Prev">Prev</li><?php } 
													else { ?><li><a class="prev page-numbers" href="#" title="First">First</a></li><li><a class="prev page-numbers" href="#" title="Prev">Prev</a></li><?php } 
													$sebakhir=$jumhal-1;
													if ($jumhal<=3) { $awal=1; $akhir=$jumhal; }
													else {
														if ($page==1) { $awal=1; $akhir=$page+2; }
														elseif ($page==2) { $awal=1; $akhir=$page+2; }
														elseif ($page==$jumhal) { $awal=$page-2; $akhir=$jumhal; }
														elseif ($page==$sebakhir) { $awal=$page-2; $akhir=$jumhal; }
														else { $awal=$page-2; $akhir=$page+2; }
													}
													for ($i=$awal; $i<=$akhir; $i++) {
														if ($i!=$page) { ?><li><a class="page-numbers " href="<?php echo URL_DOMAIN.$menu."/page/".$i;?>" title="<?php echo $i;?>"><?php echo $i;?></a></li><?php }  
														else { ?><li><a class="page-numbers current" href="#"><?php echo $i;?></a></li><?php } 
													}
													if ($page<$jumhal){ $next=$page+1; ?><li><a class="next page-numbers" href="<?php echo URL_DOMAIN.$menu."/page/".$next;?>" title="Next">Next</a></li><li><a class="next page-numbers" href="<?php echo URL_DOMAIN.$menu."/page/".$jumhal;?>" title="Last">Last</a></li><?php } 
													else { ?><li><a class="next page-numbers" href="#" title="Next">Next</a></li><li><a class="next page-numbers" href="#" title="Last">Last</a></li><?php } 
													?>
												</ul>
											</div>
										</div>
										<?php
									}
									
								}
								else {
									
									?>
									<div class="single-blog-item">
										<div class="post-feature blog-thumbnail  wow move-up">
											<?php 
											if(!empty($helpPicture)){ 
												?>
												<img src="<?php echo URL_PICTURE.$helpPicture;?>" alt="<?php echo $helpName;?>" width="100%">
												<?php  
											}
											?>
										</div>
										<div class="post-info lg-blog-post-info  wow move-up">											
											<h2 class="post-title"><?php echo $helpName;?></h2>
											<div class="post-excerpt mt-10">
												<p><?php  echo $helpContent; ?></p>
											</div>
										</div>
									</div>
									<?php

								}
								?>
								
                            </div>
                        </div>
						
						<div class="col-lg-4 order-lg-1 order-1"> 
							<?php
							if ($count != 0) {	
								?>
								<div class="page-sidebar-content-wrapper page-sidebar-left  small-mt__40 tablet-mt__40">
									
									<div class="sidebar-widget search-post wow move-up">
										<form method="get" action="<?php echo URL_DOMAIN.$menu?>">
											<div class="widget-search">
												<input type="text" name='key' placeholder="Cari Bantuan…">
												<button type="submit" class="search-submit">
													<span class="search-btn-icon fa fa-search"></span>
												</button>
											</div>
										</form>
									</div>

									<div class="sidebar-widget widget-blog-recent-post wow move-up">
										<h5 class="sidebar-widget-title">Daftar Isi</h5>
										<ul>
											<?php 
											foreach($helpDataLast as $data){
												$helpNo = $data['no'];
												$helpName = $data['name'];
												$helpLink = $data['link'];
												$helpUrlDetail = URL_DOMAIN.$menu.'/detail/'.$helpNo.'/'.$helpLink;?>
												<li><a href="<?php echo $helpUrlDetail;?>" title="<?php echo $helpName;?>"><?php echo $helpName;?></a></li><?php 
											}
											?>
										</ul>
									</div>                                

								</div>
								<?php
							}
							?>
                        </div>						
						
                    </div>
                </div>
            </div>


		</div>
	</div>
	<?php
	include (DIR_THEME.THEME.'/app/footer.php');
	include (DIR_THEME.THEME.'/app/asset-body.php');
	?>
</body>

</html>