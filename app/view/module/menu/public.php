<!DOCTYPE html>
<html dir="ltr" lang="id-ID">
    
<head><?php
    include (DIR_THEME.THEME.'/app/asset-head.php');
    include (DIR_THEME.THEME.'/app/metatag.php'); 
    include (DIR_THEME.THEME.'/app/opengraph.php');
    include (DIR_THEME.THEME.'/app/favicon.php');
    include (DIR_THEME.THEME.'/app/title.php');
    ?>
</head>

<body>
	<?php
	include (DIR_THEME.THEME.'/app/header.php');
	include (DIR_THEME.THEME.'/app/pagetitle.php');	
	?>

    <?= $menuData['content'] ?>


    <?php
    include (DIR_THEME.THEME.'/app/footer.php');
    include (DIR_THEME.THEME.'/app/asset-body.php');
    ?>
</body>

</html>