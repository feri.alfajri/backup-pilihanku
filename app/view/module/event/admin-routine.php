<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<?php
					$title = 'Event';  
					
					$menu_label = 'Tambah';
					$menu_action = 'create';
					
					$table_name = 'event_routine';
					$table_label = 'hari,jam,nama';
					$table_column = 'hari,jam,name';
					$table_width = '15,10,10'; 
					$table_button = 'update,read,delete';  
					$table_condition = " WHERE subdomain ='".SUBDOMAIN."' ";
					
					$create_type = '';
					$create_label = 'nama,hari,tanggal,jam,deskripsi';
					$create_form = 'name,hari,tanggal,jam,content';
					$create_condition = "subdomain,".SUBDOMAIN;
					
					$read_label = 'nama,hari,tanggal,jam,deskripsi';
					$read_content = 'name,hari,tanggal,jam,content';
					$read_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
					
					$update_type = '';
					$update_label = 'nama,hari,tanggal,jam,deskripsi';
					$update_form = 'name,hari,tanggal,jam,content';
					$update_condition = "WHERE no='$no'";
					$delete_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
					
					if (empty ($act)) {
					
						$admin->menu($menu_label,$menu_action);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					elseif ($act=='create') {
	
						$admin->title('Tambah '.$title);
						$admin->create($table_name,$create_type,$create_label,$create_form,$create_condition);
						
					} 
					elseif ($act=='read') {
						
						$admin->title('Detail '.$title);
						$admin->read($table_name,$read_label,$read_content,$read_condition); 
						
					} 
					elseif ($act=='update') {
						
						$admin->title('Ubah '.$title);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act=='delete') {
	
						$admin->menu($menu_label,$menu_action);
						//$admin->delete($table_name,$delete_condition);		
						echo '<div class="alert alert-danger" role="alert">Demi Keamanan, Fitur Hapus Dinonaktifkan</div>';
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					else {
	
						$admin->menu($menu_label,$menu_action);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					}
					?>
					
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>
 
</html>