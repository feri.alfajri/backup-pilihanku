<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<?php
					$evideoUrl = URL_DOMAIN.$menu.'/'.$link;
					if ($act == '') {
					    ?>
					    <h2 class="text-center mb-4"><?php echo $titlepage;?></h2>
					    <div class="row">
					        <?php
    						$query = "SELECT * FROM evideo WHERE publish='1' ORDER BY no ASC";
    						$sqlquery = mysqli_query($admin->koneksi,$query);
    						$jumlah = mysqli_num_rows($sqlquery);
    						while ($data = mysqli_fetch_array($sqlquery)) { 
    							$evideoNo = $data['no'];
    							$evideoName = $data['name'];
    							$evideoLink = $data['link'];
    							$evideoPicture = $data['picture'];
    							$evideoUrl = $data['url'];
    							$evideoUrlDetail = URL_DOMAIN.$menu.'/'.$link.'/'.$evideoNo.'/'.$evideoLink;
    							?>
    							<div class="col-md-4"> 
    								<div class="card mb-3">
    									<div class="card-body text-center p-0">
    									    <a href="<?php echo $evideoUrl;?>" title="<?php echo $evideoName;?>" target="_blank">
    									        <img class="img-responsive" width="100%" src="<?php echo URL_UPLOAD;?>image.php/<?php echo $evideoPicture;?>?width=400&cropratio=16:9&nocache&quality=100&image=<?php echo URL_PICTURE.$evideoPicture;?>"/>
    										    <div  class="p-2"><b><?php echo $evideoName;?></b></div>
    										</a>
    									</div>
    								</div>
    							</div>
    							<?php
    						} 
    						?>
						</div>
						<?php
					} 
					else {
						$query = "SELECT * FROM evideo WHERE publish = '1' AND no = '$no' LIMIT 1";
						$sqlquery = mysqli_query($admin->koneksi,$query);
						$jumlah = mysqli_num_rows($sqlquery);
						if ($jumlah == 0) {
							?>
							<div class="alert alert-warning" role="alert">
								Maaf, Data yang Anda cari tidak tersedia.
							</div>
							<?php
						}
						else {
							$data = mysqli_fetch_array($sqlquery);
							$evideoNo = $data['no'];
							$evideoName = $data['name'];
							$evideoLink = $data['link'];
							$evideoContent = $data['content'];
							$evideoUrl = $data['url'];
							$evideoPicture = $data['picture'];
							?>
							<div class="card">
								<div class="card-body">
								    <div class="row">
								        <div class="col-md-3">
								            <img src="<?php echo URL_PICTURE.$evideoPicture;?>" width="100%"/>
								        </div>
								        <div class="col-md-3">
        									<h3><?php echo $evideoName;?></h3>
        									<p>
        										<?php echo $evideoContent;?>
        									</p>
        									<a href="<?php echo $evideoUrl;?>" class="btn btn-primary btn-lg mt-2" title="Download" target="_blank">
        									    <i class="fa fa-download"></i> Download
        									</a>
        								</div>
        							</div>
								</div>
							</div>
							<a href="<?php echo URL_DOMAIN.$menu.'/'.$link;?>" class="btn btn-secondary mt-2" title="Kembali">Kembali</a>
							<?php
						} 
					} 
					?>
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>
 
</html>