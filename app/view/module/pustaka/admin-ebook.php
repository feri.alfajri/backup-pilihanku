<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<?php
					$ebookUrl = URL_DOMAIN.$menu.'/'.$link;
					if ($act == '') {
					    ?>
					    <h2 class="text-center mb-4"><?php echo $titlepage;?></h2>
					    <div class="row">
					        <?php
    						$query = "SELECT * FROM ebook WHERE publish='1' ORDER BY no ASC";
    						$sqlquery = mysqli_query($admin->koneksi,$query);
    						$jumlah = mysqli_num_rows($sqlquery);
    						while ($data = mysqli_fetch_array($sqlquery)) { 
    							$ebookNo = $data['no'];
    							$ebookName = $data['name'];
    							$ebookLink = $data['link'];
    							$ebookPicture = $data['picture'];
    							$ebookContent = substr($data['content'],0,150);
    							$ebookUrlDetail = URL_DOMAIN.$menu.'/'.$link.'/'.$ebookNo.'/'.$ebookLink;
    							?>
    							<div class="col-md-4"> 
    								<div class="card mb-3">
    									<div class="card-body">
    									    <img src="<?php echo URL_UPLOAD;?>image.php/<?php echo $ebookPicture;?>?width=80&cropratio=3:4&nocache&quality=100&image=<?php echo URL_PICTURE.$ebookPicture;?>" style="width:75px; margin-right:15px;" align="left"/>
    										<h4><a href="<?php echo $ebookUrlDetail;?>" title="<?php echo $ebookName;?>"><?php echo $ebookName;?></a></h4>
    										<?php echo $ebookContent;?>
    										<br/>
    										<a href="<?php echo $ebookUrlDetail;?>" title="<?php echo $ebookName;?>">selengkapnya</a>
    									</div>
    								</div>
    							</div>
    							<?php
    						} 
    						?>
						</div>
						<?php
					} 
					else {
						$query = "SELECT * FROM ebook WHERE publish = '1' AND no = '$no' LIMIT 1";
						$sqlquery = mysqli_query($admin->koneksi,$query);
						$jumlah = mysqli_num_rows($sqlquery);
						if ($jumlah == 0) {
							?>
							<div class="alert alert-warning" role="alert">
								Maaf, Data yang Anda cari tidak tersedia.
							</div>
							<?php
						}
						else {
							$data = mysqli_fetch_array($sqlquery);
							$ebookNo = $data['no'];
							$ebookName = $data['name'];
							$ebookLink = $data['link'];
							$ebookContent = $data['content'];
							$ebookUrl = $data['url'];
							$ebookPicture = $data['picture'];
							?>
							<div class="card">
								<div class="card-body">
								    <div class="row">
								        <div class="col-md-3">
								            <img src="<?php echo URL_PICTURE.$ebookPicture;?>" width="100%"/>
								        </div>
								        <div class="col-md-3">
        									<h3><?php echo $ebookName;?></h3>
        									<p>
        										<?php echo $ebookContent;?>
        									</p>
        									<a href="<?php echo $ebookUrl;?>" class="btn btn-primary btn-lg mt-2" title="Download" target="_blank">
        									    <i class="fa fa-download"></i> Download
        									</a>
        								</div>
        							</div>
								</div>
							</div>
							<a href="<?php echo URL_DOMAIN.$menu.'/'.$link;?>" class="btn btn-secondary mt-2" title="Kembali">Kembali</a>
							<?php
						} 
					} 
					?>
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>
 
</html>