<!DOCTYPE html>  
<html lang="en"> 
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Member | Lupa Password</title>
        <link rel="shortcut icon" href="<?php echo URL_IMAGE;?>favicon.png" type="image/x-icon">
        <link href="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo URL_THEME.THEME_MEMBER;?>/css/sb-admin-2.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    </head>
    <body class="bg-gradient-secondary"> 
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
							<div class="p-5">
								<div class="text-center">
									<img src="<?php echo URL_IMAGE;?>logo.png" class="mb-4" style="width:100%"/>
									<h1 class="h4 text-gray-900 mb-4">LUPA PASSWORD</h1>
								</div>
								<?php
								if (empty($_SESSION['uname'])) {
									?>
									<form class="user text-center" method="post">
										<input type="hidden" name="losspass" value="1">
										Masukkan alamat email Anda :
										<div class="form-group mt-1">
											<input type="text" class="form-control form-control-user text-center" id="email" name="email" placeholder="Email">
										</div>
										<button class="btn btn-danger btn-user btn-block" type="submit">KIRIM KODE KE EMAIL</button>
									</form>
									<div class="mt-4 text-center">Belum punya akun ? <a href="<?php echo URL_DOMAIN;?>account/register" title="Register">Register</a></div>
								    <div class="mt-1 text-center">Sudah punya akun ? <a href="<?php echo URL_DOMAIN;?>member" title="Login">Login<a/></div>
									<?php
								}
								else {
									?>
									<p class="text-center">
									Anda sudah Login, Silahkan masuk ke Area User.
									<br/>
									<a href="<?php echo URL_DOMAIN;?>member" class="btn btn-primary mt-2" title="Masuk Ke Area User">Masuk Ke Area User<a/>
									</p>
									<?php
								}
								?>						
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/jquery/jquery.min.js"></script>
        <script src="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/jquery-easing/jquery.easing.min.js"></script>
        <script src="<?php echo URL_THEME.THEME_MEMBER;?>/js/sb-admin-2.min.js"></script>
    </body>
</html>
