<!DOCTYPE html> 
<html lang="en"> 
     
<head>

    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
	
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-member.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<h2>Selamat Datang, <?php echo $_SESSION['name'];?></h2>  
					<div class="card mb-4"> 
						  <div class="card-body">
							Silahkan gunakan layanan yang ada pada area ini dengan meng-klik tombol menu pada bagian kiri halaman ini.<br/>
							Biasakan untuk secara rutin <b>Mengganti Password</b> Anda setiap bulan.<br/>
							Jangan lupa untuk selalu <b>Logout</b> setelah Anda selesai menggunakan fasilitas ini. 
						</div>	
					</div>   
					
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>
 
</html>