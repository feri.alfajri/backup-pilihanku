<!DOCTYPE html>  
<html lang="en"> 
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Member | Buat Password</title>
        <link rel="shortcut icon" href="<?php echo URL_IMAGE;?>favicon.png" type="image/x-icon">
        <link href="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo URL_THEME.THEME_MEMBER;?>/css/sb-admin-2.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    </head>
    <body class="bg-gradient-secondary"> 
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
							<div class="p-5">
								<div class="text-center">
									<img src="<?php echo URL_IMAGE;?>logo.png" class="mb-4" style="width:100%"/>
									<h1 class="h4 text-gray-900 mb-4">BUAT PASSWORD BARU</h1>
								</div>
								<?php
								
								if (empty($_SESSION['uname'])) {
									include DIR_LIBRARY.'uri.php';
                                    $uri = new uri();
                                    $token = $uri->get('q');
                                    $isExist = false;
                                    $isValidUrl = false;
                                    if ($token!='') {
                                        $sqlRequest = "SELECT no FROM member_reset WHERE publish = '1' AND token = '$token' AND date_expired > NOW() LIMIT 1";
                                        $qRequest = mysqli_query($model->koneksi, $sqlRequest);
                                        $jRequest = mysqli_num_rows($qRequest);
                                        if ($jRequest > 0) {
                                            $ipAddress = $_SERVER['REMOTE_ADDR'];
                                            $currentTime = date('Y-m-d H:i:s');
                                            $sqlUpdate = "UPDATE member_reset SET ip_address = '$ipAddress', date_accessed = '$currentTime' WHERE publish = '1' AND token = '$token'";
                                            mysqli_query($model->koneksi, $sqlUpdate);
                                            $decodeToken = str_replace(
                                                array('-', '_', ''),
                                                array('+', '/', '='),
                                                $token
                                            );
                                            $rawToken = base64_decode($decodeToken);
                                            $payload = json_decode($rawToken, true);
                                            $username = $payload['member_id'];
                                            $query = "SELECT no, name, username, email FROM member WHERE username = '$username'  LIMIT 1";
                                            $sql = mysqli_query($model->koneksi, $query);
                                            $count = mysqli_num_rows($sql);
                                            if ($count > 0) {
                                                $memberData = mysqli_fetch_assoc($sql);
                                                $memberId = $memberData['no'];
                                                $name = $memberData['name'];
                                                $memberUsername = $memberData['username'];
                                                $email = $memberData['email'];
                                                $isValidUrl = true;
                                            }
                                        }
                                    }
                                    if (empty($token) or !$isValidUrl) {
                                        ?>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        				    Maaf, Link Anda tidak VALID.<br/>
                        				    <a href="<?php echo URL_DOMAIN;?>" title="Kembali">Kembali Ke Beranda</a>
                                        </div>
                                        <?php
                                    } 
                        			else { 
    									if (empty($_POST['createpass'])) {
        									?>
        									<form class="user text-center" method="post">
        										<input type="hidden" name="createpass" value="1">
        										<div class="form-group mt-1">
        											<input type="password" class="form-control form-control-user text-center" id="password" name="password" placeholder="Password Baru">
        										</div>
        										<div class="form-group mt-1">
        											<input type="password" class="form-control form-control-user text-center" id="password2" name="password2" placeholder="Password Baru (Ulangi)">
        										</div>
        										<button class="btn btn-danger btn-user btn-block" type="submit">SIMPAN</button>
        									</form>
        									<?php
    									}
                                        elseif ($_POST['createpass']=='1') {
                                            $token = strip_tags($_POST['token']); $token=str_replace("'","",$token); $token=str_replace('"','',$token);
                                            $password = strip_tags($_POST['password']); $password=str_replace("'","",$password); $password=str_replace('"','',$password);
                                            $password2 = strip_tags($_POST['password2']); $password2=str_replace("'","",$password2); $password2=str_replace('"','',$password2);
                                            $pjgnewpass = strlen($password);
                                            $newpassword=md5($password);
                                    		$newpassword2=md5($password2);
                                            if ($password<6) {
                                    		    ?>
                                    		    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        							<div class="sb-msg"><i class="icon-warning-sign"></i>Maaf, Password Anda kurang dari 6 digit.</div>
                        						</div>
                        						<?php
                                    		}
                                    		else { 
                                                $sql = "UPDATE member SET password='$newpassword' WHERE username='$username' LIMIT 1";
                                                mysqli_query($model->koneksi, $sql); 
                                                $sqlUpdate = "UPDATE member_reset SET publish='0' WHERE publish='1' AND token='$token'";
                                                mysqli_query($model->koneksi, $sqlUpdate);
                                                
                                                //KIRIM EMAIL
                                				include  (DIR_LIBRARY.'class.phpmailer.php');
                                				$body='<html>' .
                                					'<head></head>' .
                                					'<body>' .
                                					'<label><img src="'.URL_IMAGE.$logo.'" alt="logo" width="250"></label><br/><br>				
                                					<p>Salam, '.$name.'</p>
                                					<p>Kami menerima laporan bahwa Anda meminta perubahan password melalui sistem kami.</p>	
                                					<p>Berikut ini adalah data perubahan akses Anda :</p>	
                                					<table width="100%" cellpadding="0" cellspacing="0" border="0">
                                						<tr><td align=left width=140>Username</td><td align=left>: &nbsp; '.$memberUsername.'</td></tr>
                                						<tr><td align=left>Password</td><td align=left >: &nbsp; '.$password.'</td></tr>
                                					</table>
                                					<p>Anda dapat login ke member area dengan menggunakan username dan password baru diatas</p>
                                					<p>Informasi lebih lanjut mengenai layanan kami, silahkan menghubungi Customer Service. Semoga kesuksesan senantiasa menyertai kita semua.</p>
                                					<p>Best Regards</p>
                                					<p>PilihanKu.net</p>
                                					 </body>' .
                                					'</html>'; 
                                				date_default_timezone_set('Asia/Jakarta');
                                				$mail=new PHPMailer();
                                				$mail->Subject="Buat Password Baru PilihanKu.net";
                                				$mail->MsgHTML($body);
                                				$mail->addReplyTo(ASAL_EMAIL,"PilihanKu.net");
                                				$mail->SetFrom(ASAL_EMAIL, "PilihanKu.net");
                                				$mail->AddAddress($email, $name);
                                				unset($_SSESSION['kode']);
                                				if(!$mail->Send()) {  echo "Mailer Error: " . $mail->ErrorInfo; } 
                                                ?>
                                                <div class="style-msg successmsg">
                        							<div class="sb-msg">
                        							    <i class="icon-thumbs-up"></i>
                        							    Selamat, Password Anda telah diganti.<br/>
                        					            <a href="<?php echo URL_DOMAIN;?>member"  title="Kembali">Kembali Ke Form Login</a>
                        							</div>
                        						</div><?php
                                                
                                    		}
                                        }
                        			}
								}
								else {
									?>
									<p class="text-center">
									Anda sudah Login, Silahkan masuk ke Area User.
									<br/>
									<a href="<?php echo URL_DOMAIN;?>member" class="btn btn-primary mt-2" title="Masuk Ke Area User">Masuk Ke Area User<a/>
									</p>
									<?php
								}
								?>						
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/jquery/jquery.min.js"></script>
        <script src="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/jquery-easing/jquery.easing.min.js"></script>
        <script src="<?php echo URL_THEME.THEME_MEMBER;?>/js/sb-admin-2.min.js"></script>
    </body>
</html>
