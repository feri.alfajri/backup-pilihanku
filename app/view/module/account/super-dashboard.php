<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_SUPER.'/app/asset-head.php';?>
</head>

<body class="nav-fixed">
   
	<?php include DIR_THEME.THEME_SUPER.'/app/topbar.php'; ?>
	
	<div id="layoutSidenav">
	
        <div id="layoutSidenav_nav">
			<?php include DIR_THEME.THEME_SUPER.'/app/sidebar.php'; ?>			
		</div>
		
		<div id="layoutSidenav_content">
			<main>
				
				<?php include DIR_THEME.THEME_SUPER.'/app/header.php'; ?>

				<div class="container-xl px-4 mt-4">
					<div class="card">
						<div class="card-header">Selamat Datang, <?php echo $_SESSION['name'];?></div>
						<div class="card-body">
							Silahkan gunakan layanan yang ada pada area ini dengan meng-klik tombol menu pada bagian kiri halaman ini.<br/>
							Biasakan untuk secara rutin <b>Mengganti Password</b> Anda setiap bulan.<br/>
							Jangan lupa untuk selalu <b>Logout</b> setelah Anda selesai menggunakan fasilitas ini. 		
						</div>
					</div>
				</div>
				
			</main>
		</div>
		
	</div>	
	
	<?php include DIR_THEME.THEME_SUPER.'/app/asset-body.php'; ?>
	
</body>
 
</html>