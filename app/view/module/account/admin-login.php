<!DOCTYPE html>  
<html lang="en"> 
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?php echo $titleweb; ?> | Login Admin</title>
        <link rel="shortcut icon" href="<?php echo URL_IMAGE;?>favicon.png" type="image/x-icon">
        <link href="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo URL_THEME.THEME_MEMBER;?>/css/sb-admin-2.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    </head>
    <body class="bg-gradient-secondary"> 
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-6 col-md-12">
                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
							<div class="p-5">
								<div class="text-center">
									<img src="<?php echo URL_IMAGE;?>logo.png" class="mb-2"/>
									<h1 class="h4 text-gray-900 mb-4">LOGIN ADMIN</h1>
								</div>
								<form class="user" method="post">
									<input type="hidden" name="process" value="login">
									<div class="form-group">
										<input type="text" class="form-control form-control-user text-center" id="username" name="username" placeholder="Username">
									</div>
									<div class="form-group">
										<input type="password" class="form-control form-control-user text-center" id="password" name="password" placeholder="Password">
									</div>
									<button class="btn btn-danger btn-user btn-block" type="submit">MASUK</button>
								</form>
								<?php
								if (empty($_POST['process'])) {
									$randkode=rand(111111,999999); 
									$_SESSION['kode']=$randkode;
								}
								elseif ($_POST['process']=='login') { 
									$username = strip_tags($_POST['username']); $username = mysqli_real_escape_string($model->koneksi, $username);
									$password = strip_tags($_POST['password']); $encPass = md5($password); $encPass = mysqli_real_escape_string($model->koneksi, $encPass);  
									$query = "SELECT no, name, username, password, phone, email, picture FROM member WHERE password ='$encPass' AND (username = '$username' OR phone = '$username' OR email = '$username')";
									$sql = mysqli_query($model->koneksi, $query);
									$count = mysqli_num_rows($sql);                                            
									if ($count>0) {
										$memberData = mysqli_fetch_assoc($sql);
										$memberId = $memberData['no'];
										$memberIpAddress = $_SERVER['REMOTE_ADDR'];
										$_SESSION['name'] = $memberData['name'];
										$_SESSION['uname'] = $memberData['username'];
										$_SESSION['pword'] = $memberData['password'];
										$_SESSION['pict'] = $memberData['picture'];
										$_SESSION['cat'] = 'member';
										$ipaddress=$_SERVER['REMOTE_ADDR'];  
										mysqli_query($model->koneksi, "UPDATE member SET last_login=sysdate(), last_ipaddress='$ipaddress' WHERE no='$memberId'");?>
										<meta http-equiv="refresh" content="0" url="<?php echo URL_DOMAIN.$menu;?>"><?php
									} 
									else { 
										?>
										<script type="text/javascript">alert('Username dan Password Anda Salah. Silahkan Coba Lagi !');</script>
										<?php 
									}
								} 
								?>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/jquery/jquery.min.js"></script>
        <script src="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo URL_THEME.THEME_MEMBER;?>/vendor/jquery-easing/jquery.easing.min.js"></script>
        <script src="<?php echo URL_THEME.THEME_MEMBER;?>/js/sb-admin-2.min.js"></script>
    </body>
</html>
