<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_SUPER.'/app/asset-head.php';?>
</head>

<body class="nav-fixed">
   
	<?php include DIR_THEME.THEME_SUPER.'/app/topbar.php'; ?>
	
	<div id="layoutSidenav">
	
        <div id="layoutSidenav_nav">
			<?php include DIR_THEME.THEME_SUPER.'/app/sidebar.php'; ?>			
		</div>
		
		<div id="layoutSidenav_content">
			<main>
				
				<?php include DIR_THEME.THEME_SUPER.'/app/header.php'; ?>

				<div class="container-xl px-4 mt-4">
					<?php
			
					$uname = $_SESSION['uname'];
					$table_name = 'user';							
					$update_type = '';
					$update_label = 'nama,alamat,kota,provinsi,telepon,email';
					$update_form = 'name,address,city,province,phone,email';
					$update_condition = "WHERE username='$uname'";							
					
					$admin->title('Ubah '.$titlepage);
					$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					?>	
				</div>
				
			</main>
		</div>
		
	</div>	
	
	<?php include DIR_THEME.THEME_SUPER.'/app/asset-body.php'; ?>
	
</body>
 
</html>