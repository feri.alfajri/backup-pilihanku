<!DOCTYPE html>  
<html lang="en"> 
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>SuperAdmin | Login</title>
		<link rel="icon" type="image/x-icon" href="<?php echo URL_IMAGE;?>favicon.png" />
		<link href="<?php echo URL_THEME.THEME_SUPER;?>/css/styles.css" rel="stylesheet" />        
        <script data-search-pseudo-elements defer src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-default">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container-xl px-4">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-body">
										<form class="user" method="post">
											<input type="hidden" name="process" value="login">
                                            <div class="mb-3">
                                                <label class="small mb-1" for="username">Username</label>
                                                <input class="form-control"  id="username" name="username" type="text" placeholder="Username"/>
                                            </div>
                                            <div class="mb-3">
                                                <label class="small mb-1" for="password">Password</label>
                                                <input class="form-control" id="password" name="password" type="password" placeholder="Password" />
                                            </div>
                                            <div class="d-flex align-items-center justify-content-between mt-4 mb-0">
                                                <button class="btn btn-google btn-user btn-block" type="submit">LOGIN</button>
                                            </div>
                                        </form>
										<?php
                                        if (empty($_POST['process'])) {
                                            $randkode=rand(111111,999999); 
											$_SESSION['kode']=$randkode;
                                        }
                                        elseif ($_POST['process']=='login') { 
                                            $username = strip_tags($_POST['username']); $username = mysqli_real_escape_string($model->koneksi, $username);
                                            $password = strip_tags($_POST['password']); $encPass = md5($password); $encPass = mysqli_real_escape_string($model->koneksi, $encPass);  
											$query = "SELECT no, name, username, password, picture FROM user WHERE username = '$username' AND password ='$encPass' AND category='super' LIMIT 1";
											$sql = mysqli_query($model->koneksi, $query);
                                            $count = mysqli_num_rows($sql);                                            
                                            if ($count==1) {
                                                $memberData = mysqli_fetch_assoc($sql);
                                                $memberId = $memberData['no'];
                                                $memberIpAddress = $_SERVER['REMOTE_ADDR'];
                                                $_SESSION['name'] = $memberData['name'];
                                                $_SESSION['uname'] = $memberData['username'];
                                                $_SESSION['pword'] = $memberData['password'];
                                                $_SESSION['pict'] = $memberData['picture'];
                                                $_SESSION['cat'] = 'admin';
                                                $ipaddress=$_SERVER['REMOTE_ADDR'];  
                                                mysqli_query($model->koneksi,"UPDATE user SET last_login=sysdate(), last_ipaddress='$ipaddress' WHERE no='$memberId'");?>
                                                <meta http-equiv="refresh" content="0" url="<?php echo URL_DOMAIN.$menu;?>"><?php
                                            } 
                                            else { ?><script type="text/javascript">alert('Username dan Password Anda Salah. Silahkan Coba Lagi !');</script><?php }
                                        } ?>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo URL_THEME.THEME_SUPER;?>/js/scripts.js"></script>		
    </body>
</html>
