<!DOCTYPE html> 
<html lang="en">
    
<head>

    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
	
</head>

<body>
    
	<div id="wrapper"> 
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>
		
		<div id="content-wrapper"> 
		    
			<div id="content">
			
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
				    
					<?php
					
					$uname = $_SESSION['uname'];
					$table_name = 'member';							
					$update_type = '';
					$update_label = 'nama Lengkap,alamat,kota/Kabupaten,no. Telepon/WA,email';
					$update_form = 'name,address,city,phone,email';
					$update_condition = "WHERE username='$uname'";	
					
					$admin->title('Ubah Profil');
					$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					?>
					
                    
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>

</html>