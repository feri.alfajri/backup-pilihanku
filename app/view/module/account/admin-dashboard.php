<!DOCTYPE html> 
<html lang="en"> 
     
<head>

    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
	
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					<div class="card bg-primary mb-3 text-white"> 
						<div class="card-body">
							<img class="mr-3" src="<?php echo URL_UPLOAD;?>image.php/<?php echo $logo;?>?width=86&cropratio=1:1&nocache&quality=100&image=<?php echo URL_PICTURE.$logo;?>" alt="logo" align="left" style="border-radius:50%"/>
							<h4 class="mt-3"><?php echo $nameweb;?></h4>
							<a class="btn btn-secondary mt-2 mr-2" href="<?php echo URL_DOMAIN;?>" title="Lihat Website" target="_blank"><i class="fas fa-desktop mr-2"></i> Lihat Website</a>
							<?php  if ($package != 'custom') { ?><a class="btn btn-success mt-2" href="<?php echo URL_DOMAIN.$menu;?>/upgrade" title="Upgrade"><i class="fas fa-star mr-2"></i> Upgrade</a><?php } ?>

						</div>	
					</div> 
					<div class="card mb-3"> 
						  <div class="card-body">
							Silahkan gunakan layanan yang ada pada area ini dengan meng-klik tombol menu pada bagian kiri halaman ini.<br/>
							Biasakan untuk secara rutin <b>Mengganti Password</b> Anda setiap bulan.<br/>
							Jangan lupa untuk selalu <b>Logout</b> setelah Anda selesai menggunakan fasilitas ini. 
						</div>	
					</div>   

				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>
 
</html>