<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-member.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					    
                    <?php                   
                    $uname = $_SESSION['uname'];
					$admin = new admin();
					
					$title = 'Transaksi';  
										
					$table_name = 'transaction';
					$table_label = 'nama,nilai,status,tanggal';
					$table_column = 'no_product,value,status,date';
					$table_width = '45,10,10,18'; 
					$table_button = 'read';  
					$table_condition = " WHERE username ='$uname' ";
					
					$read_label = 'produk,nilai,status,tanggal pesanan,catatan';
					$read_content = 'no_product,value,status,date,note';
					$read_condition = "WHERE username ='$uname' AND no='$no'";
					
					if (empty ($act)) {
						
						$admin->title($title);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					elseif ($act=='read') {						
						
						$admin->title($title);
						$admin->read($table_name,$read_label,$read_content,$read_condition); 
						
					} 
					else {						
						
						$admin->title($title);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					}
    					
					?>	
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>
 
</html>