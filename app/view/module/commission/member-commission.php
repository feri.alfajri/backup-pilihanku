<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-member.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<h2 class="mb-2">Komisi</h2>
					<?php
				    $uname = $_SESSION['uname'];
				    $qin = mysqli_query($model->koneksi,"SELECT SUM(value) FROM commission WHERE username='$uname' AND publish='1' AND status='debit'");
				    $din = mysqli_fetch_array($qin);
				    $totalin = $din[0];
				    $qout = mysqli_query($model->koneksi,"SELECT SUM(value) FROM commission WHERE username='$uname' AND publish='1' AND status='kredit'");
				    $dout = mysqli_fetch_array($qout);
				    $totalout = $dout[0];
				    $saldo = $totalin-$totalout;
				    ?>
				    <div class="row">
				        <div class="col-md-4">
        					<div class="card bg-primary mb-3">
        						<div class="card-body text-white">
        						    <span>Total Komisi</span>
        						    <h2 class="text-right"><?php echo number_format($totalin,0,',','.'); ?></h2>
        						</div>
        					</div>
    					</div>
    					<div class="col-md-4">
        					<div class="card bg-primary mb-3">
        						<div class="card-body text-white">
        						    <span>Total Transfer</span>
        						    <h2 class="text-right"><?php echo number_format($totalout,0,',','.'); ?></h2>
        						</div>
        					</div>
    					</div>
    					<div class="col-md-4">
        					<div class="card bg-primary mb-3">
        						<div class="card-body text-white">
        						    <span>Saldo Komisi</span>
        						    <h2 class="text-right"><?php echo number_format($saldo,0,',','.'); ?></h2>
        						</div>
        					</div>
    					</div>
					</div>
					
					<h4>Riwayat Transaksi</h4>
					<div class="card mb-4">
						<div class="card-body">
						    <?php
						    $query = mysqli_query($model->koneksi,"SELECT * FROM commission WHERE username='$uname' AND publish='1' ORDER BY no ASC");
    						$jumlah = mysqli_num_rows($query);
    						if ($jumlah==0) { 
    						    ?>
    					        <div class="alert alert-warning" role="alert">
    								Maaf, Anda tidak memiliki riwayat transaksi.
    							</div>
    							<?php
    						}
    						else {
    						    ?>
            					<table class="table table-striped table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
            						<thead>
            							<tr style="text-transform:uppercase;" class="text-center">
            								<th width="30">No</th>
            								<th width="120">Tanggal</th>
            								<th class="text-left">Keterangan</th>											
            								<th width="100">Debit</th>
            								<th width="100">Kredit</th>
            								<th width="120">Saldo</th>
            							</tr>
            						</thead>
            						<tbody>
            						    <?php
            						    $commissionSaldo = 0;
            						    $nomor = 1;
            						    while ($data = mysqli_fetch_array($query)) { 
                							$commisionNo = $data['no'];
                							$commisionStatus = $data['status'];
                							$commisionCode = $data['code'];
                							$commisionName = $data['name'];
                							$commisionValue = $data['value'];
                							if ($commisionStatus=='debit') { $commissionDebit = $commisionValue; $commissionKredit = 0; $commissionSaldo = $commissionSaldo + $commisionValue; }
                							else { $commissionDebit = 0; $commissionKredit = $commisionValue; $commissionSaldo = $commissionSaldo - $commisionValue; }
                							$commisionOrigin = $data['origin'];
                							$commisionNews = $data['news'];
                							$commisionDate = $data['date'];
                						    ?>
                							<tr class="text-center">
                								<td><?php echo $nomor; ?></td>
                								<td><?php echo $commisionDate;?></td>
                								<td class="text-left"><?php echo $commisionName.' ('.$commisionOrigin.')';?></td>
                								<td><?php if ($commissionDebit!=0) { echo number_format($commissionDebit,0,',','.'); } ?></td>
                								<td><?php if ($commissionKredit!=0) { echo number_format($commissionKredit,0,',','.'); } ?></td>
                								<td><?php echo number_format($commissionSaldo,0,',','.'); ?></td>
                							</tr>
                							<?php
                							$nomor++;
            						    }
            						    ?>
            						</tbody>
            					</table>
            					<?php
					    	}
					    	?>
        				</div>
        			</div>
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>
 
</html>