<!DOCTYPE html>
<html dir="ltr" lang="id-ID">
    
<head><?php
    include (DIR_THEME.THEME.'/app/asset-head.php');
    include (DIR_THEME.THEME.'/app/metatag.php'); 
    include (DIR_THEME.THEME.'/app/opengraph.php');
    include (DIR_THEME.THEME.'/app/favicon.php');
    include (DIR_THEME.THEME.'/app/title.php');
    ?>
</head>

<body>
	<?php
	include (DIR_THEME.THEME.'/app/loader.php');
	include (DIR_THEME.THEME.'/app/header.php');
	?>
	<div class="processing-hero processing-hero-bg">
    	<div class="container">
    		<div class="row align-items-center" style="padding:80px 0"> 
    			<div class="col-lg-7 col-md-7">
    				<div class="processing-hero-text wow move-up">
    					<h2 class="font-weight--reguler mb-15" style="color:#FFFFFF">Dapatkan Penghasilan Tambahan Hingga Jutaan Rupiah dengan Mudah dan Cepat.</h2>
    					<p>
    					    Manfaatkan handphone dan sosial media Anda untuk mendapatkan penghasilan tambahan hingga jutaan rupiah.
    					    Bergabung di Program Afiliasi Pilihanku.net dan sebarkan Link Afiliasi melalui social media dan blog Anda.
    						Dapatkan komisi mulai dari 15% untuk setiap transaksi melalui Link Afiliasi Anda. 
    						Klik <b>Daftar</b> untuk mulai sekarang juga, GRATIS...!
    					</p>
    					<div class="hero-button mt-20">
    						<a href="<?php echo URL_DOMAIN;?>create" class="btn btn--primary" title="Buat Sekarang">Buat Sekarang</a>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-5 col-md-5">
    				<div class="processing-hero-images-wrap wow move-up">
    					<div class="video-popup infotech-video-box">
    						<img src="<?php echo URL_IMAGE;?>headline.png" alt="headline" width="100%">
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
	<div class="feature-icon-wrapper section-space--ptb_60 bg-white">
    	<div class="container "> 
    		<h3 class="heading text-center">INGIN PENGHASILAN TAMBAHAN ?</h3>
    		<div class="feature-list__three">
    			<div class="row text-center">
    				<div class="col-md-3 col-sm-6">
    					<div class="ht-box-icon style-01 single-svg-icon-box">
    						<div class="icon-box-wrap">
    							<i class="fal fa-users fa-3x"></i>
    							<h5>205 Juta</h5>
    							<p>Saat ini, lebih dari 205 juta pengguna internet di Indonesia.</p>	
    						</div>
    					</div>
    				</div>
    				<div class="col-md-3 col-sm-6">
    					<div class="ht-box-icon style-01 single-svg-icon-box">
    						<div class="icon-box-wrap">
    							<i class="social-icon fab fa-instagram fa-3x"></i>
    							<h5>190 Juta</h5>
    							<p>190 juta orang terkoneksi dan berinteraksi di sosial media.</p>
    						</div>
    					</div>
    				</div>
    				<div class="col-md-3 col-sm-6">
    					<div class="ht-box-icon style-01 single-svg-icon-box">
    						<div class="icon-box-wrap">
    							<i class="fal fa-clock fa-3x"></i>
    							<h5>10 Jam / hari</h5>
    							<p>Mereka menghabiskan 10 jam perhari untuk mengakses internet.</p>
    						</div>
    					</div>
    				</div>
    				<div class="col-md-3 col-sm-6">
    					<div class="ht-box-icon style-01 single-svg-icon-box">
    						<div class="icon-box-wrap">
    							<i class="fal fa-signal fa-3x"></i>
    							<h5>84 %</h5>
    							<p>84% pengguna internet adalah usia produktif, yaitu 19-54 tahun.</p>	
    						</div>
    					</div>
    				</div>
    			</div>			
    		</div>
    		
    		<h4 class="heading text-center mt-30 mb-10">BENEFIT</h4>
    		<div class="feature-list__three">
                <div class="row">
    			    <div class="col-lg-4">
    					<div class="grid-item animate">
    						<div class="ht-box-icon style-03">
    							<div class="icon-box-wrap">
    								<div class="content-header">
    									<div class="icon">
    										<i class="fal fa-user-plus"></i>
    									</div>
    									<h5 class="heading">Daftar Gratis</h5>
    								</div>
    								<div class="content">
    									<div class="text">Program Afiliasi ini GRATIS. Tidak ada biaya pendaftaran untuk menjadi member afiliasi PilihanKu.net.</div>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="col-lg-4">
    					<div class="grid-item animate">
    						<div class="ht-box-icon style-03">
    							<div class="icon-box-wrap">
    								<div class="content-header">
    									<div class="icon">
    										<i class="fal fa-dollar-sign"></i>
    									</div>
    									<h5 class="heading">Komisi Besar</h5>
    								</div>
    								<div class="content">
    									<div class="text">Komisi mulai dari 15% bisa Anda dapatkan setiap kali berhasil menjual produk dan layanan PilihanKu.net.</div>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="col-lg-4">
    					<div class="grid-item animate">
    						<div class="ht-box-icon style-03">
    							<div class="icon-box-wrap">
    								<div class="content-header">
    									<div class="icon">
    										<i class="fal fa-credit-card"></i>
    									</div>
    									<h5 class="heading">Pembayaran Cepat</h5>
    								</div>
    								<div class="content">
    									<div class="text">Komisi Anda akan ditransfer maksimal 1x24 jam setelah pelunasan dari pelanggan yang Anda referensikan.</div>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    			<div class="row">
    			    <div class="col-lg-4">
    					<div class="grid-item animate">
    						<div class="ht-box-icon style-03">
    							<div class="icon-box-wrap">
    								<div class="content-header">
    									<div class="icon">
    										<i class="fal fa-calculator"></i>
    									</div>
    									<h5 class="heading">Perhitungan Akurat</h5>
    								</div>
    								<div class="content">
    									<div class="text">Anda dapat memeriksa komisi Anda dengan mudah, akurat dan detail melalui dashboard member afiliasi.</div>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="col-lg-4">
    					<div class="grid-item animate">
    						<div class="ht-box-icon style-03">
    							<div class="icon-box-wrap">
    								<div class="content-header">
    									<div class="icon">
    										<i class="fal fa-wallet"></i>
    									</div>
    									<h5 class="heading">Passive Income</h5>
    								</div>
    								<div class="content">
    									<div class="text">Berpotensi mendapatkan pasif income selama client yang Anda referensikan melakukan perpanjangan.</div>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="col-lg-4">
    					<div class="grid-item animate">
    						<div class="ht-box-icon style-03">
    							<div class="icon-box-wrap">
    								<div class="content-header">
    									<div class="icon">
    										<i class="fal fa-video"></i>
    									</div>
    									<h5 class="heading">Training Gratis</h5>
    								</div>
    								<div class="content">
    									<div class="text">Anda juga berhak untuk mengikuti program pelatihan strategi pemasaran digital/ online secara gratis.</div>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
	<div class="contact-us-area infotechno-contact-us-bg section-space--ptb_60"> 
    	<div class="container">
    	    
    		<div class="row">
    			<div class="col-md-5 col-sm-12 col-xs-12">
    				<img src="<?php echo URL_IMAGE;?>step.png" class="img-responsive mb-20" width="100%" alt="step">
    			</div>
    			<div class="col-md-7 col-sm-12 col-xs-12 text-left">
    			    <h3 class="mb-20">4 LANGKAH SUKSES AFILIASI</h3>
                    <h6>Langkah 1 : Daftarkan Diri Anda</h6>
                    <p>Anda dapat mendaftar Program Afiliasi PilihanKu.net melalui form pendaftaran yang tersedia. Pendaftaran ini Gratis. Klik DISINI untuk mendaftar.</p>
    			    <h6>Langkah 2: Promosikan Link Afiliasi</h6>
                    <p>Promosikan link afiliasi anda seluas-luasnya! Anda dapat membuat kode kupon, atau menyebarkan Link Afiliasi Anda melalui sosial media, blog, dsb.</p>
                    <h6>Langkah 3: Konversi Pelanggan</h6>
                    <p>Tunggu hingga calon pelanggan mendaftar membeli produk / layanan PilihanKu.net melalui kode kupon atau link afiliasi yang Anda bagikan. </p>
                    <h6>Langkah 4: Cairkan Komisi Afiliasi Anda</h6>
                    <p>Setelah pelanggan melakukan pelunasan, Anda dapat melihat komisi melalui dashboard member afiliasi dan melakukan pencairan ke rekening bank Anda.</p>

    			</div>
    		</div>
    	</div>
    </div>
    <div class="cta-image-area_one section-space--ptb_80 cta-bg-image_one"> 
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-8 col-lg-8">
                    <div class="cta-content md-text-center">
                        <h4 class="heading text-white">Saatnya bergabung bersama Program Afiliasi PilihanKu.net. Dapatkan komisi Jutaan Rupiah. Daftarkan Sekarang, GRATIS...!</h4>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4">
                    <div class="cta-button-group--one text-center">
                        <a href="<?php echo URL_DOMAIN;?>member" class="btn btn--primary btn-one  text-white" title="DAFTAR">
    						<span class="btn-icon mr-2"><i class="far fa-arrow-right"></i></span> DAFTAR SEKARANG
    					</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<?php
	include DIR_THEME.THEME.'/app/footer.php';
	include DIR_THEME.THEME.'/app/asset-body.php';
	?>
</body>

</html>