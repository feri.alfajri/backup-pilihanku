<!DOCTYPE html> 
<html lang="en">
    
<head>

    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
	
</head>

<body>
    
	<div id="wrapper"> 
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>
		
		<div id="content-wrapper"> 
		    
			<div id="content">
			
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
				    
					<?php                   
					$admin = new admin();
					
					$title = 'Donasi Online';  
										
					$table_name = 'donation';
					$table_label = 'nama,telepon,nilai,status,tanggal';
					$table_column = 'name,phone,value,status,date';
					$table_width = '30,15,10,10,18'; 
					$table_button = 'update,read,delete';  
					$table_condition = " WHERE subdomain ='".SUBDOMAIN."'";
					
					$read_label = 'nama,telepon,email,nilai,status,tanggal donasi,tanggal transfer';
					$read_content = 'name,phone,email,value,status,date,date_transfer';
					$read_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
					
					$update_type = '';
					$update_label = 'nama,telepon,email,nilai,status,tanggal transfer';
					$update_form = 'name,phone,email,value,status,date_transfer';
					$update_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
					
					$delete_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
					
					if ($package=='free') {
					    
					    $admin->title($title);
					    ?>
					    <div class="alert alert-danger" role="alert">
					        <h5>Fitur belum aktif</h5>
							Maaf, Fitur ini hanya dapat digunakan utk paket Pro dan Custom.
						</div>
						<div class="card">
						    <div class="card-body">
						        <i class="fa fa-4x fa-star mb-2 text-primary"></i>
						        <h3>Maksimalkan Kampanye Anda !</h3>
						        <p>Upgrade ke paket website kampanye Anda untuk mengaktifkan fitur ini.</p>
						        <a class="btn btn-primary text-white" href="<?php echo URL_DOMAIN.$menu;?>/upgrade">Upgrade Sekarang</a>
						    </div>
						</div>
					    <?php
					}
					else {
					    
    					if (empty ($act)) {
    						
    						$admin->title($title);
    						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
    						
    					} 
    					elseif ($act=='confirm') {
    						
    						$table_condition = " WHERE subdomain ='".SUBDOMAIN."'  AND status='confirm' ";
    						$admin->title($title.' (Confirm)');
    						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
    						
    					} 	
    					elseif ($act=='prospect') {
    						
    						$table_condition = " WHERE subdomain ='".SUBDOMAIN."'  AND status='unpaid' ";
    						$admin->title($title.' (Unpaid)');
    						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
    						
    					} 
    					elseif ($act=='history') {
    						
    						$table_condition = " WHERE subdomain ='".SUBDOMAIN."'  AND status='paid' ";
    						$admin->title($title.' (Paid)');
    						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
    						
    					} 
    					elseif ($act=='read') {						
    						
    						$admin->title($title);
    						$admin->read($table_name,$read_label,$read_content,$read_condition); 
    						
    					} 
    					elseif ($act=='update') {
    						
    						$admin->title('Ubah Status '.$title);
    						$read_label = 'nama,telepon,email,nilai,tanggal transfer';
    						$read_content = 'name,phone,email,value,date_transfer';
    						$admin->read($table_name,$read_label,$read_content,$read_condition); 
    						$admin->update($table_name,$update_type,'status','status',$update_condition);
    						
    					} 
    					elseif ($act=='description') {
    						
    						echo $noweb;
    						$admin->title('Penjelasan '.$title);
    						$admin->update('setting','','isi penjelasan','content_donation'," WHERE subdomain='".SUBDOMAIN."' ");
    						
    					} 
    					elseif ($act=='delete') {	
    										
    						//$admin->delete($table_name,$delete_condition);
    						$admin->title($title);
    						echo '<div class="alert alert-danger" role="alert">Demi Keamanan, Fitur Hapus Dinonaktifkan</div>';
    						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
    						
    						
    					} 
    					else {						
    						
    						$admin->title($title);
    						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
    						
    					}
    					
					}
					?>	
					
                    
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>

</html>