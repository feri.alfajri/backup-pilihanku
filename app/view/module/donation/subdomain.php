<!DOCTYPE html>
<html dir="ltr" lang="id-ID">
    
<head><?php
    include (DIR_THEME.THEME.'/app/asset-head-sub.php');
    include (DIR_THEME.THEME.'/app/metatag.php'); 
    include (DIR_THEME.THEME.'/app/opengraph.php');
    include (DIR_THEME.THEME.'/app/favicon.php');
    include (DIR_THEME.THEME.'/app/title.php');
    ?>
</head>

<body>
	<?php
	include (DIR_THEME.THEME.'/app/loader.php');
	include (DIR_THEME.THEME.'/app/header-top.php');
	include (DIR_THEME.THEME.'/app/header-sub.php');
	?>
	<div id="main-wrapper">
        <div class="site-wrapper-reveal">		
			<div class="blog-pages-wrapper section-space--ptb_60">
                <div class="container">
					<div class="row">
						<div class="col-md-6 mb-15">								
							<div class="text-block text-justify">
								<h3 class="mb-15">Donasi Online</h3>
								<p><?php echo $content_donation;?></p>
							</div>
						</div>
						<br/>
						<div class="col-md-6">		
							<h5 class="mb-10">Formulir Donasi</h5>
							<?php
							if (empty($_POST['process'])) { }
							elseif ($_POST['process']=='save') { 
								?>
								<div class="alert <?php echo $alert;?>" role="alert">
									<?php echo $notif;?>
								</div>
								<?php
							}
							?>
							<div class="contact-form-wrap">
								 <form method="post">
									<input type="hidden" name="process" value="save">
									<div class="contact-form">
										<div class="contact-inner">
											<input type="text" name="name" id="name" placeholder="Nama Lengkap" required>
										</div>
										<div class="contact-input">
                                            <div class="contact-inner">
												<input type="text" name="phone" id="phone" placeholder="No. Telepon" required>
											</div>
											<div class="contact-inner">
												<input type="email" name="email" id="email"  placeholder="Email" required>
											</div>
                                        </div>										
										<div class="contact-inner">
											<input type="number" name="value" id="value" placeholder="Nilai Donasi" required>
										</div>
										<div class="contact-inner">
											<input type="text" name="note" id="note" placeholder="Catatan"/>
										</div>
										<input type="submit" class="ht-btn ht-btn-md"  value="Kirim Donasi" onclick="return confirm('Apakah Anda yakin memberkan donasi kepada kami ?');" />
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include (DIR_THEME.THEME.'/app/footer-sub.php');
	include (DIR_THEME.THEME.'/app/asset-body-sub.php');
	?>
</body>

</html>