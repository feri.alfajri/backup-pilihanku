<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<h3 class="mb-3">Laporan Keuangan</h3>
					
				    <div class="card mb-4">
						<div class="card-body">
							
							<div class="dataTable_wrapper">
								<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
									<tr class="text-center">
										<th class="text-left">Saldo Awal</th>
										<th width="10">Rp.</th>
										<th class="text-right" width="150">10.000.000</th>											
									</tr>
									<tr class="text-center">
										<td colspan="3"></td>
									</tr>
									<tr class="text-center">
										<td class="text-left">Jumlah Pemasukan</td>
										<td>Rp.</td>
										<td class="text-right">3.000.000</td>											
									</tr>
									<tr class="text-center">
										<td class="text-left">Jumlah Pengeluaran</td>
										<td>Rp.</td>
										<td class="text-right">800.000</td>											
									</tr>
									<tr class="text-center">
										<td class="text-left">Selisih</td>
										<td>Rp.</td>
										<td class="text-right">2.200.000</td>											
									</tr>
									<tr class="text-center">
										<td colspan="3"></td>
									</tr>
									<tr class="text-center">
										<th class="text-left">Saldo Akhir</th>
										<th>Rp.</th>
										<th class="text-right">12.200.000</th>											
									</tr>
								</table>
								<br/>
								<h4>Riwayat Transaksi</h4>
								<table class="table table-striped table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
									<thead>
										<tr style="text-transform:uppercase;" class="text-center">
											<th width="30">No</th>
											<th width="100">Tanggal</th>
											<th width="50">Kode</th>											
											<th class="text-left">Keterangan</th>											
											<th width="100">Debit</th>
											<th width="100">Kredit</th>
											<th width="100">Saldo</th>
										</tr>
									</thead>
									<tbody>
										<tr class="text-center">
											<td>1</td>
											<td>1 Oktober 2022</td>
											<td>SA</td>	
											<td class="text-left">Saldo Awal</td>
											<td>10.000.000</td>
											<td>-</td>
											<td>10.000.000</td>
										</tr>
										<tr class="text-center">
											<td>2</td>
											<td>2 Oktober 2022</td>
											<td>PJ</td>	
											<td class="text-left">Pemasukan Jumat</td>
											<td>500.000</td>
											<td>-</td>
											<td>10.500.000</td>
										</tr>
										<tr class="text-center">
											<td>3</td>
											<td>4 Oktober 2022</td>
											<td>PD</td>	
											<td class="text-left">Pemasukan Donatur</td>
											<td>2.500.000</td>
											<td>-</td>
											<td>13.000.000</td>
										</tr>
										<tr class="text-center">
											<td>4</td>
											<td>9 Oktober 2022</td>
											<td>BR</td>	
											<td class="text-left">Biaya Listrik</td>
											<td>-</td>
											<td>800.000</td>
											<td>12.200.000</td>
										</tr>
									</tbody>
								</table>
							</div>
							
						</div>
					</div>
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>
 
</html>