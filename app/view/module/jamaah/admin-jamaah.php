<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<?php
    					function menu_website ($menu,$link,$act) {
    					    ?>
    					    <div style="display:table;width:100%;margin-top:10px; margin-bottom:-8px;">
        						<?php  if ($act=='') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
        						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>" class="btn <?php echo $btnstyle;?> ml-3 mr-2 pb-3 text-white" title="Data Jamaah">Data Jamaah</a>
        						<?php  if ($act=='create') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
        						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>/0/create" class="btn <?php echo $btnstyle;?> mr-2 pb-3 text-white" title="Tambah">Tambah</a>
        						<?php  if ($act=='create-category') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
        						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>/0/create-category" class="btn <?php echo $btnstyle;?> mr-2 pb-3 text-white" title="Tambah Kategori">Tambah Kategori</a>
        					</div>
    					    <?php
    					}
    					
					    $admin = new admin();
					
    					$title = 'Jamaah';  

    					$table_name = 'jamaah';
    					$table_label = 'gambar,nama,no. hp';
    					$table_column = 'picture,name,phone';
    					$table_width = '5,20,15'; 
    					$table_button = 'update,read,delete';  
    					$table_condition = " WHERE subdomain ='".SUBDOMAIN."' ";
    					
    					$create_type = 'picture';
    					$create_label = 'nama,TTL,jenis kelamin,alamat,telepon,gambar';
    					$create_form = 'name,birth,sex,address,phone,picture';
    					$create_condition = "subdomain,".SUBDOMAIN;
    					
        				$read_label = 'gambar,nama,no. KTP,TTL,jenis kelamin,golongan darah,status marital,pekerjaan,status ekonomi,status jamaah,telepon,alamat,kabupaten';
    					$read_content = 'picture,name,nik,birth,sex,blood,marital,job,economic_status,jamaah_status,phone,address,city';
    					$read_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
    					
    					$update_type = 'picture';
    					$update_label = 'gambar,nama,no. KTP,TTL,jenis kelamin,golongan darah,status marital,pekerjaan,status ekonomi,status jamaah,telepon,alamat,kabupaten';
    					$update_form = 'picture,name,nik,birth,sex,blood,marital,job,economic_status,jamaah_status,phone,address,city';
    					$update_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
    					
    					$delete_condition = "WHERE no='$no'";
    					
    					if (empty ($act)) {
    						
    						$admin->title($title);
    						menu_website ($menu,$link,$act);
    						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
    						
    					} 		
    					elseif ($act=='create') {
    	
    						$admin->title('Tambah '.$title);
    						menu_website ($menu,$link,$act);
    						$admin->create($table_name,$create_type,$create_label,$create_form,$create_condition);
    						
    					}
    					elseif ($act=='create-category') {
    	
    						$admin->title('Tambah Kategori');
    						menu_website ($menu,$link,$act);
    						$admin->create('jamaah_category','','nama kategori','name',"subdomain,".SUBDOMAIN);
    						
    					}
    					elseif ($act=='read') {						
    						
    						$admin->title($title);
    						$admin->read($table_name,$read_label,$read_content,$read_condition); 
    						$admin->button ('back','Kembali','','btn-primary');
    						
    					} 
    					elseif ($act=='update') {
    						
    						$admin->title('Ubah '.$title);
    						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
    						
    					} 					
    					elseif ($act=='delete') {	
    										
    						//$admin->delete($table_name,$delete_condition);
    						$admin->title($title);
    						menu_website ($menu,$link,$act);
    						echo '<div class="alert alert-danger" role="alert">Demi Keamanan, Fitur Hapus Dinonaktifkan</div>';
    						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
    						
    						
    					} 
    					else {						
    						
    						$admin->title($title);
    						menu_website ($menu,$link,$act);
    						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
    						
    					}
    					
					?>
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>
 
</html>