<!DOCTYPE html> 
<html lang="en">
    
<head>

    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
	
</head>

<body>
    
	<div id="wrapper"> 
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>
		
		<div id="content-wrapper"> 
		    
			<div id="content">
			
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
				    
					<?php                   
					$admin = new admin();
					
					$title = 'Blog';  
					
					$menu_label = 'Tambah';
					$menu_action = 'create';
					
					$table_name = 'blog';
					$table_label = 'gambar,nama,penulis,tanggal';
					$table_column = 'picture,title,author,date';
					$table_width = '10,40,15,10'; 
					$table_button = 'update,read,delete';  
					$table_condition = " WHERE subdomain ='".SUBDOMAIN."' ";
					
					$create_type = 'picture';
					$create_label = 'judul,link,penulis,isi,meta title,meta description,gambar';
					$create_form = 'title,link,author,content,meta_title,meta_description,picture';
					$create_condition = "subdomain,".SUBDOMAIN;
					
					$read_label = 'judul,link,penulis,isi,pengunjung,komentar,meta title,meta description,gambar,tanggal';
					$read_content = 'title,link,author,content,visitors,comments,meta_title,meta_description,picture,date';
					$read_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
					
					$update_type = 'picture';
					$update_label = 'judul,link,penulis,isi,meta title,meta description,gambar';
					$update_form = 'title,link,author,content,meta_title,meta_description,picture';
					$update_condition = "WHERE no='$no'";
					
					$delete_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
					
					if (empty ($act)) {
					
						$admin->menu($menu_label,$menu_action);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					elseif ($act=='create') {
	
						$admin->title('Tambah '.$title);
						$admin->create($table_name,$create_type,$create_label,$create_form,$create_condition);
						
					} 
					elseif ($act=='read') {
						
						$admin->title($title);
						$admin->read($table_name,$read_label,$read_content,$read_condition); 
						
					} 
					elseif ($act=='update') {
						
						$admin->title('Ubah '.$title);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act=='delete') {
	
										
						$admin->menu($menu_label,$menu_action);
						//$admin->delete($table_name,$delete_condition);		
						echo '<div class="alert alert-danger" role="alert">Demi Keamanan, Fitur Hapus Dinonaktifkan</div>';
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
						
					} 
					else {
	
						$admin->menu($menu_label,$menu_action);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					}
					?>
					
                    
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>

</html>