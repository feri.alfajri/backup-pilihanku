<!DOCTYPE html>
<html dir="ltr" lang="id-ID">
    
<head><?php
    include (DIR_THEME.THEME.'/app/asset-head.php');
    include (DIR_THEME.THEME.'/app/metatag.php'); 
    include (DIR_THEME.THEME.'/app/opengraph.php');
    include (DIR_THEME.THEME.'/app/favicon.php');
    include (DIR_THEME.THEME.'/app/title.php');
    ?>
</head>

<body>
	<?php
	include (DIR_THEME.THEME.'/app/loader.php');
	include (DIR_THEME.THEME.'/app/header.php');
	include (DIR_THEME.THEME.'/app/pagetitle.php');
	?>
	<div id="main-wrapper">
        <div class="site-wrapper-reveal">
		
			<div class="blog-pages-wrapper section-space--ptb_60">
                <div class="container">	
                    <div class="row">
                                   
                        <div class="col-lg-8 order-lg-1 order-1">
                            <div class="main-blog-wrap">
                                
                                <?php
								if($no == ''){
									if ($count == 0) {		
										?>
										<div class="alert alert-warning" role="alert">
											Mohon maaf, saat ini tidak ada artikel yang dapat ditampilkan.
										</div>
										<?php
									}
									else {
										foreach($blogData as $data){
											$blogNo = $data['no'];
											$blogJudul = $data['title'];
											$blogAuthor = $data['author'];
											$blogIsi = $data['content'];
											$blogVisitor = $data['visitors'];
											$blogLink = $data['link'];
											$blogPicture = $data['picture'];
											$blogTanggal = date('d F Y', strtotime($data['date'])); 
											$blogJam = date('H:i',strtotime($data['date'])); 
											$blogTanggal = $blogTanggal.', pukul '.$blogJam;
											$blogPreviewIsi = substr($data['content'],0,250);
											$blogUrlDetail = URL_DOMAIN.$menu.'/detail/'.$blogNo.'/'.$blogLink;
											$blogUrlPicture = $blogPicture;
											?>
											<h3 class="mb-15">Artikel</h3>
											<div class="single-blog-item lg-blog-item border-bottom wow move-up">    
												<div class="post-feature blog-thumbnail">
													<?php
													if(!empty($blogPicture)){ 
														?>
														<a href="<?php echo $blogUrlDetail;?>" title="<?php echo $blogJudul;?>">
															<img src="<?php echo URL_UPLOAD;?>image.php/<?php echo $blogUrlPicture;?>?width=800&cropratio=16:9&nocache&quality=100&image=<?php echo URL_PICTURE.$blogUrlPicture;?>" width="100%" alt="<?php echo $blogJudul;?>">
														</a>
														<?php
													} 
													?>
												</div>    
												<div class="post-info lg-blog-post-info">
													<h3 class="post-title">
														<a href="<?php echo $blogUrlDetail;?>" title="<?php echo $blogJudul;?>"><?php echo $blogJudul;?></a>
													</h3>                                            
													<div class="post-meta mt-10" style="border-top:1px solid #EAEAEA;border-bottom:1px solid #EAEAEA;">
														<div class="post-date">
															<span class="far fa-calendar meta-icon"></span><?php echo $blogTanggal;?>
														</div>
														<div class="post-view">
															<span class="meta-icon far fa-eye"></span><?php echo $blogVisitor;?> pembaca
														</div>
													</div>
													<div class="post-excerpt mt-10">
														<p><?php echo $blogPreviewIsi;?>…</p>
														<a href="<?php echo $blogUrlDetail;?>">Selengkapnya</a>
													</div>
												</div>											
											</div>
											<?php
										}
										?>											
										<div class="ht-pagination mt-30 pagination justify-content-center">
                        					<div class="pagination-wrapper">
                        						<ul class="page-pagination">
                        							<?php
                        							$jumhal=ceil($count/$limit); 
                        							$linkhal="#";
                        							if ($page>1) { $prev=$page-1; ?><li><a class="prev btn btn--secondary text-white mr-1" href="<?php echo URL_DOMAIN.$menu;?>" title="First"><<</a></li><li><a class="prev btn btn--secondary text-white mr-1" href="<?php echo URL_DOMAIN.$menu."/page/".$prev;?>" title="Prev"><</li><?php } 
                        							else { ?><li><a class="prev btn btn--secondary text-white mr-1" href="#" title="First"><<</a></li><li><a class="prev btn btn--secondary text-white mr-1" href="#" title="Prev"><</a></li><?php } 
                        							$sebakhir=$jumhal-1;
                        							if ($jumhal<=3) { $awal=1; $akhir=$jumhal; }
                        							else {
                        								if ($page==1) { $awal=1; $akhir=$page+2; }
                        								elseif ($page==2) { $awal=1; $akhir=$page+2; }
                        								elseif ($page==$jumhal) { $awal=$page-2; $akhir=$jumhal; }
                        								elseif ($page==$sebakhir) { $awal=$page-2; $akhir=$jumhal; }
                        								else { $awal=$page-2; $akhir=$page+2; }
                        							}
                        							for ($i=$awal; $i<=$akhir; $i++) {
                        								if ($i!=$page) { ?><li><a class="btn btn--secondary text-white mr-1" href="<?php echo URL_DOMAIN.$menu."/page/".$i;?>" title="<?php echo $i;?>"><?php echo $i;?></a></li><?php }  
                        								else { ?><li><a class="btn btn--primary text-white mr-1" href="#"><?php echo $i;?></a></li><?php } 
                        							}
                        							if ($page<$jumhal){ $next=$page+1; ?><li><a class="next btn btn--secondary text-white mr-1" href="<?php echo URL_DOMAIN.$menu."/page/".$next;?>" title="Next">></a></li><li><a class="next btn btn--secondary text-white mr-1" href="<?php echo URL_DOMAIN.$menu."/page/".$jumhal;?>" title="Last">>></a></li><?php } 
                        							else { ?><li><a class="next btn btn--secondary text-white mr-1" href="#" title="Next">></a></li><li><a class="next btn btn--secondary text-white mr-1" href="#" title="Last">>></a></li><?php } 
                        							?>
                        						</ul>
                        					</div>
                        				</div>
										<?php
									}
									
								}
								else {
									
									?>
									<div class="single-blog-item">
										<div class="post-feature blog-thumbnail  wow move-up">
											<?php 
											if(!empty($blogPicture)){ 
												?>
												<img src="<?php echo URL_PICTURE.$blogPicture;?>" alt="<?php echo $blogJudul;?>" width="100%">
												<?php  
											}
											?>
										</div>
										<div class="post-info lg-blog-post-info  wow move-up">											
											<h2 class="post-title"><?php echo $blogTitle;?></h2>
											<div class="post-meta mt-10" style="border-top:1px solid #EAEAEA;border-bottom:1px solid #EAEAEA;">
												<div class="post-date">
													<span class="far fa-calendar meta-icon"></span><?php echo $blogDate;?>
												</div>
												<div class="post-view">
													<span class="meta-icon far fa-eye"></span><?php echo $blogVisitor;?> pembaca
												</div>
											</div>
											<div class="post-excerpt mt-10">
												<p><?php  echo $blogContent; ?></p>
											</div>
										</div>
									</div>
									<?php

								}
								?>
								
                            </div>
                        </div>
						
						<div class="col-lg-4 order-lg-2 order-2"> 
							<?php
							if ($count != 0) {	
								?>
								<div class="page-sidebar-content-wrapper page-sidebar-left  small-mt__40 tablet-mt__40">
									
									<div class="sidebar-widget search-post wow move-up">
										<form method='get' action="<?=URL_DOMAIN.$menu?>">
											<div class="widget-search">
												<input type="text" name='key' placeholder="Cari Artikel…">
												<button type="submit" class="search-submit">
													<span class="search-btn-icon fa fa-search"></span>
												</button>
											</div>
										</form>
									</div>

									<div class="sidebar-widget widget-blog-recent-post wow move-up">
										<h5 class="sidebar-widget-title">Artikel Terbaru</h5>
										<ul>
											<?php 
											foreach($blogDataLast as $data){
												$blogNo = $data['no'];
												$blogJudul = $data['title'];
												$blogLink = $data['link'];
												$blogUrlDetail = URL_DOMAIN.$menu.'/detail/'.$blogNo.'/'.$blogLink;?>
												<li><a href="<?php echo $blogUrlDetail;?>" title="<?php echo $blogJudul;?>"><?php echo $blogJudul;?></a></li><?php 
											}
											?>
										</ul>
									</div>                                

								</div>
								<?php
							}
							?>
                        </div>						
						
                    </div>
                </div>
            </div>


		</div>
	</div>
	<?php
	include (DIR_THEME.THEME.'/app/footer.php');
	include (DIR_THEME.THEME.'/app/asset-body.php');
	?>
</body>

</html>