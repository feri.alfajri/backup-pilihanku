<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<?php
					
					function menu_website ($menu,$link,$act) {
					    ?>
					    <div style="display:table;width:100%;margin-top:10px; margin-bottom:-8px;">
    						<?php  if ($act=='create') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
    						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>/0/create" class="btn <?php echo $btnstyle;?> ml-3 mr-2 pb-3 text-white" title="Formulir Peminjaman">Formulir Peminjaman</a>
    						<?php  if ($act=='') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
    						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>" class="btn <?php echo $btnstyle;?> mr-2 pb-3 text-white" title="Peminjaman">Peminjaman</a>
    						<?php  if ($act=='history') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
    						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>/0/history" class="btn <?php echo $btnstyle;?> mr-2 pb-3 text-white" title="Riwayat">Riwayat</a>
    					</div>
					    <?php
					}
					
					$title = 'Peminjaman Inventaris';  
					
					$table_name = 'circulation';
					$table_label = 'inventaris,jumlah,nama,no. hp,kembali';
					$table_column = 'no_inventory,amount,name,phone,date_return';
					$table_width = '20,5,15,15,10'; 
					$table_button = 'update,read';  
					$table_condition = " WHERE subdomain ='".SUBDOMAIN."' AND status = 'out'";
					
					$create_type = '';
					$create_label = 'inventaris,jumlah,nama peminjam,no. hp peminjam,tanggal kembali';
					$create_form = 'no_inventory,amount,name,phone,date_return';
					$create_condition = "subdomain,".SUBDOMAIN;
					
					$read_label = 'inventaris,jumlah,nama peminjam,no. hp peminjam,tanggal kembali,status,catatan';
					$read_content = 'name,hari,tanggal,jam,content';
					$read_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
					
					$update_type = '';
					$update_label = 'inventaris,jumlah,nama peminjam,no. hp peminjam,tanggal kembali,status,catatan';
					$update_form = 'no_inventory,amount,name,phone,date_return,status,note';
					$update_condition = "WHERE no='$no'";
					$delete_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
					
					if (empty ($act)) {
					
					    $admin->title('Peminjaman Invetaris');
						menu_website ($menu,$link,$act);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					elseif ($act=='history') {
	                    
	                    $admin->title('Riwayat Peminjaman');
						menu_website ($menu,$link,$act);
						$table_button = 'read';  
						$table_condition = " WHERE subdomain ='".SUBDOMAIN."' AND status = 'in'";
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					elseif ($act=='create') {
	
						$admin->title('Tambah '.$title);
						menu_website ($menu,$link,$act);
						$admin->create($table_name,$create_type,$create_label,$create_form,$create_condition);
						
					} 
					elseif ($act=='read') {
						
						$admin->title('Detail '.$title);
						$admin->read($table_name,$read_label,$read_content,$read_condition); 
						
					} 
					elseif ($act=='update') {
						
						$admin->title('Ubah '.$title);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act=='delete') {
	
						$admin->title('Peminjaman Invetaris');
						menu_website ($menu,$link,$act);
						//$admin->delete($table_name,$delete_condition);		
						echo '<div class="alert alert-danger" role="alert">Demi Keamanan, Fitur Hapus Dinonaktifkan</div>';
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					else {
	                    
	                    $admin->title('Peminjaman Invetaris');
						menu_website ($menu,$link,$act);
						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					}
					?>
					
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>
 
</html>