<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<?php
    					function menu_website ($menu,$link,$act) {
    					    ?>
    					    <div style="display:table;width:100%;margin-top:10px; margin-bottom:-8px;">
        						<?php  if ($act=='') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
        						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>" class="btn <?php echo $btnstyle;?> ml-3 mr-2 pb-3 text-white" title="Tabel Takmir">Tabel Takmir</a>
        						<?php  if ($act=='create') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
        						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>/0/create" class="btn <?php echo $btnstyle;?> mr-2 pb-3 text-white" title="Tambah Takmir">Tambah Takmir</a>
        						<?php  if ($act=='create-jabatan') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
        						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>/0/create-jabatan" class="btn <?php echo $btnstyle;?> mr-2 pb-3 text-white" title="Tambah Jabatan">Tambah Jabatan</a>
        					</div>
    					    <?php
    					}
    					
					    $admin = new admin();
					
    					$title = 'Takmir Masjid';  

    					$table_name = 'takmir';
    					$table_label = 'gambar,jabatan,nama,no. hp';
    					$table_column = 'picture,no_takmir_jabatan,name,phone';
    					$table_width = '5,20,20,15'; 
    					$table_button = 'update,read,delete';  
    					$table_condition = " WHERE subdomain ='".SUBDOMAIN."' ";
    					
    					$create_type = 'picture';
    					$create_label = 'jabatan,nama lengkap,alamat,no. telepon / HP,email,foto';
    					$create_form = 'no_takmir_jabatan,name,address,phone,email,picture';
    					$create_condition = "subdomain,".SUBDOMAIN;
    					
    					$read_label = 'jabatan,nama lengkap,alamat,no. teleponn / HP,email,foto';
    					$read_content = 'no_takmir_jabatan,name,address,phone,email,picture';
    					$read_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
    					
    					$update_type = 'picture';
    					$update_label = 'jabatan,nama lengkap,alamat,no. teleponn / HP,email,foto';
    					$update_form = 'no_takmir_jabatan,name,address,phone,email,picture';
    					$update_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
    					
    					$delete_condition = "WHERE no='$no'";
    					
    					if (empty ($act)) {
    						
    						$admin->title($title);
    						menu_website ($menu,$link,$act);
    						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
    						
    					} 		
    					elseif ($act=='create') {
    	
    						$admin->title('Tambah '.$title);
    						menu_website ($menu,$link,$act);
    						$admin->create($table_name,$create_type,$create_label,$create_form,$create_condition);
    						
    					}
    					elseif ($act=='create-jabatan') {
    	
    						$admin->title('Tambah Jabatan');
    						menu_website ($menu,$link,$act);
    						$admin->create('takmir_jabatan','','nama jabatan','name','');
    						
    					}
    					elseif ($act=='read') {						
    						
    						$admin->title($title);
    						$admin->read($table_name,$read_label,$read_content,$read_condition); 
    						$admin->button ('back','Kembali','','btn-primary');
    						
    					} 
    					elseif ($act=='update') {
    						
    						$admin->title('Ubah '.$title);
    						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
    						
    					} 					
    					elseif ($act=='delete') {	
    										
    						//$admin->delete($table_name,$delete_condition);
    						$admin->title($title);
    						menu_website ($menu,$link,$act);
    						echo '<div class="alert alert-danger" role="alert">Demi Keamanan, Fitur Hapus Dinonaktifkan</div>';
    						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
    						
    						
    					} 
    					else {						
    						
    						$admin->title($title);
    						menu_website ($menu,$link,$act);
    						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
    						
    					}
    					
					?>
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>
 
</html>