<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<?php
					function menu_website ($menu,$link,$act) {
					    ?>
					    <div style="display:table;width:100%;margin-top:10px; margin-bottom:-8px;">
    						<?php  if ($act=='data' or $act=='') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
    						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>/0/data" class="btn <?php echo $btnstyle;?> ml-3 mr-2 pb-3 text-white" title="Data Masjid">Data Masjid</a>
    						<?php  if ($act=='socialmedia') { $btnstyle='btn-primary'; } else { $btnstyle='btn-secondary'; } ?>
    						<a href="<?php echo URL_DOMAIN.$menu."/".$link;?>/0/socialmedia" class="btn <?php echo $btnstyle;?> mr-2 pb-3 text-white" title="Social Media">Social Media</a>
    					</div>
					    <?php
					}
					
					$table_name = 'masjid';							
					
					if (empty ($act)) {
						
					    $update_type = 'picture';
						$update_label = 'nama masjid,logo masjid,alamat,kota/kabupaten,koordinat peta,telepon,email,luas tanah,luas bangunan,status tanah,tahun berdiri,legalitas';
						$update_form = 'name,logo,address,city,maps,phone,email,luas_tanah,luas_bangunan,status_tanah,tahun_berdiri,legalitas';
						$update_condition = " WHERE subdomain = '".SUBDOMAIN."' ";							
						$admin->title('Data Masjid');
						menu_website ($menu,$link,$act);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act=='socialmedia') {
						
						$update_type = '';
						$update_label = 'URL Facebook,URL Instagram,URL Youtube,URL Tiktok';
						$update_form = 'url_facebook,url_instagram,url_youtube,url_tiktok';
						$update_condition = " WHERE subdomain = '".SUBDOMAIN."' ";							
						$admin->title('Social Media Masjid');
						menu_website ($menu,$link,$act);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					else {

						$update_type = 'picture';
						$update_label = 'nama masjid,logo masjid,alamat,kota/kabupaten,koordinat peta,telepon,email,luas tanah,luas bangunan,status tanah,tahun berdiri,legalitas';
						$update_form = 'name,logo,address,city,maps,phone,email,luas_tanah,luas_bangunan,status_tanah,tahun_berdiri,legalitas';
						$update_condition = " WHERE subdomain = '".SUBDOMAIN."' ";							
						$admin->title('Data Masjid');
						menu_website ($menu,$link,$act);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
					
					} 							
					?>
					
					
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>
 
</html>