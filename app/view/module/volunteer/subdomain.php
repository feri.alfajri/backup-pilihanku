<!DOCTYPE html>
<html dir="ltr" lang="id-ID">
    
<head><?php
    include (DIR_THEME.THEME.'/app/asset-head-sub.php');
    include (DIR_THEME.THEME.'/app/metatag.php'); 
    include (DIR_THEME.THEME.'/app/opengraph.php');
    include (DIR_THEME.THEME.'/app/favicon.php');
    include (DIR_THEME.THEME.'/app/title.php');
    ?>
</head>

<body>
	<?php
	include (DIR_THEME.THEME.'/app/loader.php');
	include (DIR_THEME.THEME.'/app/header-top.php');
	include (DIR_THEME.THEME.'/app/header-sub.php');
	?>
	<div id="main-wrapper">
        <div class="site-wrapper-reveal">		
			<div class="blog-pages-wrapper section-space--ptb_60">
                <div class="container">
					<div class="row">
						<div class="col-md-5 mb-15">								
							<div class="text-block text-justify">
								<h3 class="mb-15">Jadi Relawan Kami </h3>
								<p><?php echo $content_volunteer;?></p>
							</div>
						</div>
						<div class="col-md-7 pl-md-5 px-4">		
							<h5 class="mb-10">Pendaftaran Relawan</h5>
							<?php
							if (empty($_POST['process'])) { }
							elseif ($_POST['process']=='save') { 
								?>
								<div class="alert <?php echo $alert;?>" role="alert">
									<?php echo $notif;?>
								</div>
								<?php
							}
							?>
							<div class="contact-form-wrap">
								 <form method="post">
									<input type="hidden" name="process" value="save">
									<div class="contact-form">
										<div class="contact-inner">
											<label>Nama</label>
											<input type="text" name="name" id="name" placeholder="Nama Lengkap" required>
										</div>
										
										<div class="contact-input">
                                            <div class="contact-inner">
												<label>Telepon</label>
												<input type="text" name="phone" id="phone" placeholder="No. Telepon" required>
											</div>
											<div class="contact-inner">
												<label>Email</label>
												<input type="text" name="email" id="email"  placeholder="Email"  >
											</div>
                                        </div>
										
										<div class="contact-inner">
											<label>Alamat</label>
											<input type="text" name="address" id="address" placeholder="Alamat" >
										</div>
										
										<div class="form-group">
											<label>Provinsi</label>
											<select class="" name="provinsi" id="province">
												<?php
													foreach($dataProvinsi as $val) {
												?>		
													<option value="<?= $val["id"] ?>"><?= $val["name"] ?></option>
												<?php
													}
												?>
											</select>
										</div>

										<div class="form-group">
											<label>Kota</label>
											<select class="" name="kota" id="city">

											</select>
										</div>

										<div class="form-group">
											<label>Kecamatan</label>
											<select class="" name="kecamatan" id="district">

											</select>
										</div>

										<div class="form-group">
											<label>Desa</label>
											<select class="" name="desa" id="village">

											</select>
										</div>

										<div class="contact-inner">
											<label>Motivasi</label>
											<input type="text" name="motivation" id="motivation" placeholder="Motivasi Menjadi Relawan"/>
										</div>

										<div class="contact-inner">
											<label>Kontribusi</label>
											<input type="text" name="contribution" id="contribution" placeholder="Kontribusi Yang Ingin Diberikan"/>
										</div>
										<input type="submit" class="ht-btn ht-btn-md"  value="Daftar Relawan" onclick="return confirm('Apakah Anda yakin menjadi Relawan kami ?');" />
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include (DIR_THEME.THEME.'/app/footer-sub.php');
	include (DIR_THEME.THEME.'/app/asset-body-sub.php');
	?>

	<script type="text/javascript">
        $(document).ready(function() {

            $('#province').change(function() {
				$('#city')[0].innerHTML = `
                    <option>-- Pilih Kota --</option>
                `;
                var id = $(this).val();
                $.ajax({
                    url: "kota-form",
                    method: "POST",
                    data: {
                        id: id
                    },
                    success: function(data) {
						data.forEach((item)=>{
							$('#city')[0].innerHTML += `
								<option value='${item.id}'>
									${item.name}
								</option>
							`;
						});
                    },
                    error: (error) => {
                        console.log("error");
                    }
                });
            });

			$('#city').change(function() {
				$('#district')[0].innerHTML = `
                    <option>-- Pilih Kecamatan --</option>
                `;
                var id = $(this).val();
                $.ajax({
                    url: "kecamatan-form",
                    method: "POST",
                    data: {
                        id: id
                    },
                    success: function(data) {
						data.forEach((item)=>{
							$('#district')[0].innerHTML += `
								<option value='${item.id}'>
									${item.name}
								</option>
							`;
						});
                    },
                    error: (error) => {
                        console.log("error");
                    }
                });
            });

			$('#district').change(function() {
				$('#village')[0].innerHTML = `
                    <option>-- Pilih Desa --</option>
                `;
                var id = $(this).val();
                $.ajax({
                    url: "village-form",
                    method: "POST",
                    data: {
                        id: id
                    },
                    success: function(data) {
						data.forEach((item)=>{
							$('#village')[0].innerHTML += `
								<option value='${item.id}'>
									${item.name}
								</option>
							`;
						});
                    },
                    error: (error) => {
                        console.log("error");
                    }
                });
            });


        });
    </script>

</body>

</html>