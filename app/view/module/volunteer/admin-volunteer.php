<!DOCTYPE html> 
<html lang="en">
    
<head>

    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
	
</head>

<body>
    
	<div id="wrapper"> 
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>
		
		<div id="content-wrapper"> 
		    
			<div id="content">
			
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
				    
					<?php                   
					$admin = new admin();
					
					$title = 'Relawan';  
										
					$table_name = 'volunteer';
					$table_label = 'nama,telepon,motivasi,tanggal,desa';
					$table_column = 'name,phone,motivation,date,no_desa';
					$table_width = '30,10,40,10'; 
					$table_button = 'update,read,delete';  
					$table_condition = " WHERE subdomain ='".SUBDOMAIN."' AND status = 'terima' ";
					
					$read_label = 'nama,alamat,telepon,email,desa,kecamatan,kota,provinsi,motivasi relawan,kontribusi yang diberikan,tanggal register';
					$read_content = 'name,address,phone,email,no_desa,no_kecamatan,no_kota,no_provinsi,motivation,contribution,date';
					$read_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
					
					$update_type = '';
					$update_label = 'nama,alamat,telepon,email,motivasi relawan,kontribusi yang diberikan,status';
					$update_form = 'name,address,phone,email,motivation,contribution,status';
					$update_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
					
					$delete_condition = "WHERE subdomain ='".SUBDOMAIN."' AND no='$no'";
					
					if ($package=='free') {
					    
					    $admin->title($title);
					    ?>
					    <div class="alert alert-danger" role="alert">
					        <h5>Fitur belum aktif</h5>
							Maaf, Fitur ini hanya dapat digunakan utk paket Pro dan Custom.
						</div>
						<div class="card">
						    <div class="card-body">
						        <i class="fa fa-4x fa-star mb-2 text-primary"></i>
						        <h3>Maksimalkan Kampanye Anda !</h3>
						        <p>Upgrade ke paket website kampanye Anda untuk mengaktifkan fitur ini.</p>
						        <a class="btn btn-primary text-white" href="<?php echo URL_DOMAIN.$menu;?>/upgrade">Upgrade Sekarang</a>
						    </div>
						</div>
					    <?php
					    
					}
					else {
					    
    					if (empty ($act)) {
    						
    						$admin->title($title);
    						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
    						
    					} 
    					elseif ($act=='request') {
    						
    						$table_condition = " WHERE subdomain ='".SUBDOMAIN."'  AND status='' ";
    						$admin->title('Request '.$title);
    						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
    					} 	
    					elseif ($act=='read') {						
    						
    						$admin->title($title);
    						$admin->read($table_name,$read_label,$read_content,$read_condition); 
    						
    					} 
    					elseif ($act=='update') {
    						
    						$admin->title('Ubah '.$title);
    						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
    						
    					} 
    					elseif ($act=='description') {
    						
    						$admin->title('Penjelasan '.$title);
    						$admin->update('setting','','Isi Penjelasan','content_volunteer'," WHERE subdomain ='".SUBDOMAIN."' ");
    						
    					} 
    					elseif ($act=='delete') {	
    										
    						//$admin->delete($table_name,$delete_condition);
    						$admin->title($title);
    						echo '<div class="alert alert-danger" role="alert">Demi Keamanan, Fitur Hapus Dinonaktifkan</div>';
    						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
    						
    						
    					} 
    					else {						
    						
    						$admin->title($title);
    						$admin->table($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
    						
    					}
    					
					}
					?>	
					
                    
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>

</html>