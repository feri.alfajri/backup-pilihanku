<!DOCTYPE html> 
<html lang="en">
    
<head>

    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
	
</head>

<body>
    
	<div id="wrapper"> 
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-member.php'; ?>
		
		<div id="content-wrapper"> 
		    
			<div id="content">
			
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<?php
				function validasi($param){
					$data=str_replace('"','',$param);
					$data=str_replace("'","",$data);
					$data=str_replace('\\\\','',$data);
					$data=str_replace('////','',$data);
					$data=strip_tags($data);
					return $data;
				}
				?>

				<div class="container-fluid">
				    <h3>Buat Flyer Online</h3>
					<?php
					$uname = $_SESSION['uname'];
					$name = $_SESSION['name'];
					
					if ($act=='') {
						?>
						<div class="card mb-4">
							<div class="card-header"><h5 class="mb-0">Step 1 : Pilih Partai</h5></div>
							<div class="card-body text-center">
								<div class="row">
									<?php
									$qpartai = mysqli_query($model->koneksi, "SELECT no,name,link,picture FROM partai WHERE publish = '1' ORDER BY no");
									while($dpartai = mysqli_fetch_array($qpartai)) {
										$partaiNo = $dpartai['no'];
										$partaiName = $dpartai['name'];
										$partaiLink = $dpartai['link'];
										$partaiPicture = $dpartai['picture'];
										?>
										<div class="col-md-2 mb-4">
											<div class="card p-2">
												<a href="<?php echo URL_DOMAIN.$menu.'/'.$link.'/0/frame/'.$partaiNo.'/frame';?>" title="<?php echo $partaiName;?>">
													<img src="<?php echo URL_IMAGE.$partaiPicture;?>" width="100%" alt="<?php echo $partaiName;?>"/>
												</a>
											</div>
										</div>
										<?php														
									}
									?>
								</div>
							</div>
						</div>
						<?php
					}
					elseif ($act=='frame') {
						$partaiNo = $orderby;
						?>
						<div class="card mb-4">
							<div class="card-header"><h5 class="mb-0">Step 2 : Pilih Tema</h5></div>
							<div class="card-body text-center">
								<div class="row">
									<?php
									$qflyertheme = mysqli_query($model->koneksi, "SELECT no,name,link,picture FROM flyer_theme WHERE publish = '1' AND no_partai = '$partaiNo' ORDER BY date DESC");
									while($dflyertheme = mysqli_fetch_array($qflyertheme)) {
										$flyerthemeNo = $dflyertheme['no'];
										$flyerthemeName = $dflyertheme['name'];
										$flyerthemeLink = $dflyertheme['link'];
										$flyerthemePicture = $dflyertheme['picture'];
										?>
										<div class="col-md-4 mb-4">
											<div class="card p-2">
												<a href="<?php echo URL_DOMAIN.$menu.'/'.$link.'/0/photo/'.$partaiNo.'/'.$flyerthemeNo;?>" title="<?php echo $flyerthemeLink;?>">
													<img src="<?php echo URL_IMAGE.$flyerthemePicture;?>" width="100%" alt="<?php echo $flyerthemeLink;?>" class="mb-2"/>												
													<h6><?php echo $flyerthemeName;?></h6>
												</a>
											</div>
										</div>
										<?php
									}
									?>
								</div>
							</div>
						</div>
						<?php
					}
					elseif ($act=='photo') { 
						$partaiNo = $orderby;
						$frameNo = $by;
						$qflyertheme = mysqli_query($model->koneksi, "SELECT name,picture FROM flyer_theme WHERE no = '$frameNo'");
						$dflyertheme = mysqli_fetch_array($qflyertheme);
						$flyerthemeName = $dflyertheme['name'];
						$flyerthemePicture = $dflyertheme['picture'];
						?>
						<div class="card mb-4">
							<div class="card-header"><h5 class="mb-0">Step 3 : Upload Foto Anda</h5></div>
							<div class="card-body text-center">
								<form action="<?php echo URL_DOMAIN.$menu.'/'.$link;?>/0/finish" method="post" enctype="multipart/form-data" >
									<input type="hidden" id="no_partai" name="no_partai" value="<?php echo $partaiNo;?>"/>
									<input type="hidden" id="frame" name="frame" value="<?php echo $flyerthemePicture;?>"/>
									<center>
									<p>Pilih Foto Anda :</p>
									<input type="file" id="foto" name="foto" placeholder="Foto Anda" class="form-control mb-2" style="width:50%;"/>
									<input type="submit"  value="BUAT FLYER" class="btn btn-primary mt-2"/>	
									</center>
								</form>
							</div>
						</div>
						<?php
						$randkode=rand(000000,999999); 
						$_SESSION['kode']=$randkode;
					} 
					elseif ($act=='finish') {
						?>
						<div class="card mb-4">
							<div class="card-header"><h5 class="mb-0">Finish</h5></div>
							<div class="card-body text-center">
							<?php
							if (empty($_SESSION['kode'])) {
								?> 
								<h3>Session Telah Habis</h3>Maaf, Session Telah Habis
								<br/><a href="<?php echo URL_DOMAIN.$menu.'/'.$link;?>" title="Kembali">Kembali</a>
								<?php
							}
							else {	
								if (empty($_SESSION['kode'])) { } else { unset($_SESSION['kode']); }
								if ($_FILES['foto']['tmp_name']=='') {
									?>
									<h3>Foto Masih Kosong</h3>Maaf, Gambar Foto Harus Diisi Terlebih Dahulu
									<br/><a href="<?php echo URL_DOMAIN.$menu.'/'.$link;?>" title="Kembali">Kembali</a>
									<?php
								} 
								else {
									$no_partai=strip_tags($_POST['no_partai']); $no_partai=str_replace('"','',$no_partai); $no_partai=str_replace("'","",$no_partai);
									$frame_name=strip_tags($_POST['frame']); $frame_name=str_replace('"','',$frame_name); $frame_name=str_replace("'","",$frame_name);
									$foto=$_FILES['foto']['tmp_name'];
									$foto_name=$_FILES['foto']['name'];
									$foto_type=$_FILES['foto']['type'];
									$acak=rand(00000000,99999999);
									$foto_newname=$acak.$foto_name;
									$foto_newname=str_replace(" ","",$foto_newname);
									if ($foto_type!='image/jpg' and $foto_type!='image/png' and $foto_type!='image/gif' and $foto_type!='image/jpeg'){ 
										?>
										<h3>Format File Salah</h3>Maaf, File Yang Diijinkan Adalah File Gambar dengan Format .jpg, .png, .gif
										<br/><a href="<?php echo URL_DOMAIN.$menu.'/'.$link;?>" title="Kembali">Kembali</a>
										<?php
									}
									else {
										include DIR_LIBRARY.'image.php';
										include DIR_LIBRARY.'validasiupload.php';
										$validasi = new ValidasiUpload($foto,$foto_newname);
										$validasi -> putGambarType($foto_type);	
										if ($validasi->validGambar()){
											$ukuran=500;
											$back_name='back.png';
											copy($foto,DIR_FLYER.$foto_newname);
											$back=new Image(DIR_IMAGE.$back_name);
											$photo=new Image(DIR_FLYER.$foto_newname);
											$frame=new Image(DIR_IMAGE.$frame_name);
											//Crop photo upload
											$tinggi_asli=$photo->getHeight();
											$lebar_asli=$photo->getWidth();
											if ($lebar_asli > $tinggi_asli){
												$acuan=$photo->getHeight();
												$potong=($lebar_asli-$tinggi_asli)/2;
												$top_x=$potong;
												$bottom_x=$acuan+$potong;
												$top_y=0;
												$bottom_y=$acuan;
												$photo->crop($top_x,$top_y,$bottom_x,$bottom_y);
											} 
											else {
												$acuan=$photo->getWidth();
												$potong=($tinggi_asli-$lebar_asli)/2;
												$top_x=0;
												$bottom_x=$acuan;
												$top_y=$potong;
												$bottom_y=$acuan+$potong;
												$photo->crop($top_x,$top_y,$bottom_x,$bottom_y);
											}
											$photo->resize($ukuran,$ukuran);
											//Tumpuk image
											$back->watermark($photo,'topright');						
											$back->watermark($frame,'bottomleft');
											$back->save(DIR_FLYER.$foto_newname);
											//Input Database
											mysqli_query($model->koneksi,"INSERT INTO flyer SET username='$uname', name='$name', no_partai='$no_partai', theme='$frame_name', picture='$foto_newname', date=sysdate() "); 
											?>
											<h3>Flyer Berhasil Dibuat</h3>Selamat, Flyer Reuni Berhasil Dibuat<br/>
											<img src="<?php echo URL_FLYER.$foto_newname;?>" width="480"/>
											<br/><a href="<?php echo URL_FLYER.$foto_newname;?>" target="_blank" class="btn btn-success mt-2 mb-2" title="Download">Lihat Flyer</a>
											<?php
										} 
										else { 
											?>
											<h3>File Tidak Valid</h3>File Tidak Valid<br/>
											<br/><a href="<?php echo URL_DOMAIN.$menu.'/'.$link;?>" title="Kembali">Kembali</a>
											<?php
										}
									
									}
								}
							}
							?>
							</div>
						</div>
						<?php
					}
					?>
                    
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>

</html>