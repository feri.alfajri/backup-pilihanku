<!DOCTYPE html>
<html dir="ltr" lang="id-ID">
    
<head><?php
    include (DIR_THEME.THEME.'/app/asset-head-sub.php');
    include (DIR_THEME.THEME.'/app/metatag.php'); 
    include (DIR_THEME.THEME.'/app/opengraph.php');
    include (DIR_THEME.THEME.'/app/favicon.php');
    include (DIR_THEME.THEME.'/app/title.php');
    ?>
</head>

<body>
	<?php
	include (DIR_THEME.THEME.'/app/loader.php');
	include (DIR_THEME.THEME.'/app/header-top.php');
	include (DIR_THEME.THEME.'/app/header-sub.php');
	?>
	<div id="main-wrapper">
        <div class="site-wrapper-reveal">		
			<div class="blog-pages-wrapper section-space--ptb_60">
                <div class="container">
					<h4 class="mb-10">Profil <?php echo $name;?></h4>
					<div class="row">						
						<?php
						if ($picture=="") { $col="col-md-12"; }
						else {
							$col="col-md-10"; ?>
							<div class="col-md-2">
								<img src="<?php echo URL_UPLOAD;?>image.php/<?php echo $picture;?>?width=200&cropratio=5:6&nocache&quality=100&image=<?php echo URL_PICTURE.$picture;?>" alt="foto" width="100%"/>					
							</div><?php
						} ?>
						<div class="<?php echo $col;?>">							
							<div id="view">
								<div class="view_label">Tempat Tanggal Lahir</div><div class="view_dot">:</div><div class="view_content"><?php echo $birth;?></div>
							</div>
							<div id="view">
								<div class="view_label">Alamat</div><div class="view_dot">:</div><div class="view_content"><?php echo $address;?></div>
							</div>
							<div id="view">
								<div class="view_label">No. Telepon</div><div class="view_dot">:</div><div class="view_content"><?php echo $phone;?></div>
							</div>
							<div id="view">
								<div class="view_label">Email</div><div class="view_dot">:</div><div class="view_content"><?php echo $email;?></div>
							</div>
						</div>
					</div>
					<br/>
					<h5>Profil</h5>
					<p>
					<?php echo $profile;?>
					</p>
					<div id="view">
						<div class="view_label"><b>Riwayat Pendidikan</b></div><div class="view_dot">:</div><div class="view_content"><?php echo $education;?></div>
					</div>
					<div id="view">
						<div class="view_label"><b>Riwayat Organisasi</b></div><div class="view_dot">:</div><div class="view_content"><?php echo $organization;?></div>
					</div>
					<div id="view">
						<div class="view_label"><b>Riwayat Pelatihan</b></div><div class="view_dot">:</div><div class="view_content"><?php echo $training;?></div>
					</div>
					<div id="view">
						<div class="view_label"><b>Riwayat Pekerjaan</b></div><div class="view_dot">:</div><div class="view_content"><?php echo $work;?></div>
					</div>
					<div id="view">
						<div class="view_label"><b>Riwayat Penghargaan</b></div><div class="view_dot">:</div><div class="view_content"><?php echo $award;?></div>
					</div>
					
	
				</div>
			</div>
		</div>
	</div>
	<?php
	include (DIR_THEME.THEME.'/app/footer-sub.php');
	include (DIR_THEME.THEME.'/app/asset-body-sub.php');
	?>
</body>

</html>