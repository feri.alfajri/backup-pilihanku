<!DOCTYPE html> 
<html lang="en">
    
<head>

    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
	
</head>

<body>
    
	<div id="wrapper"> 
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>
		
		<div id="content-wrapper"> 
		    
			<div id="content">
			
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
				    
					<div style="display:table;width:100%;margin:20px 0px;">
						<a href="<?php echo URL_DOMAIN.$menu."/".$link."/0/editcv/";?>" class="btn btn-primary mr-2">Ubah CV</a>
						<a href="<?php echo URL_DOMAIN.$menu."/".$link."/0/editedu/";?>" class="btn btn-primary mr-2">Ubah Pendidikan</a>
						<a href="<?php echo URL_DOMAIN.$menu."/".$link."/0/editorg/";?>" class="btn btn-primary mr-2">Ubah Organisasi</a>
						<a href="<?php echo URL_DOMAIN.$menu."/".$link."/0/editaward/";?>" class="btn btn-primary mr-2">Ubah Penghargaan</a>
						<a href="<?php echo URL_DOMAIN.$menu."/".$link."/0/editwork/";?>" class="btn btn-primary mr-2">Ubah Pekerjaan</a>
					</div>
					<?php
					$uname = $_SESSION['uname'];
					$table_name = 'cv';							
					$update_type = '';	
					$update_label = 'nama lengkap,TTL,jenis kelamin,alamat,telepon,email,agama,status pernikahan';
					$update_form = 'name,birth,sex,address,phone,email,religion,relationship';					
					$update_condition = "WHERE subdomain='".SUBDOMAIN."'";							
					
					if (empty ($act)) {
						
						$admin->title('Ubah CV');
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);	
					
					}
					elseif ($act=="editedu") {
						$admin->title('Ubah Riwayat Pendidikan');
						$update_label = 'Riwayat Pendidikan';
						$update_form = 'education';		
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);	
					}
					elseif ($act=="editorg") {
						$admin->title('Ubah Riwayat Organisasi');
						$update_label = 'Riwayat Organisasi';
						$update_form = 'organization';		
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);	
					}
					elseif ($act=="editaward") {
						$admin->title('Ubah Riwayat Penghargaan');
						$update_label = 'Riwayat Penghargaan';
						$update_form = 'award';		
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);	
					}
					elseif ($act=="editwork") {
						$admin->title('Ubah Riwayat Pekerjaan');
						$update_label = 'Riwayat Pekerjaan';
						$update_form = 'work';		
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);	
					}
					else {
						
						$admin->title('Ubah CV');
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);	
						
					}
					?>
                    
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>

</html>