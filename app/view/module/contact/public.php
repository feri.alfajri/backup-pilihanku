<!DOCTYPE html>
<html dir="ltr" lang="id-ID">
    
<head><?php
    include (DIR_THEME.THEME.'/app/asset-head-sub.php');
    include (DIR_THEME.THEME.'/app/metatag.php'); 
    include (DIR_THEME.THEME.'/app/opengraph.php');
    include (DIR_THEME.THEME.'/app/favicon.php');
    include (DIR_THEME.THEME.'/app/title.php');
    ?>
</head>

<body>
	<?php
	include (DIR_THEME.THEME.'/app/loader.php');
	include (DIR_THEME.THEME.'/app/header-top.php');
	include (DIR_THEME.THEME.'/app/header-sub.php');
	?>
	
	<div id="main-wrapper">
        <div class="site-wrapper-reveal">		
			<div class="blog-pages-wrapper section-space--ptb_60">
                <div class="container">					
					<div class="row">
                        <div class="col-lg-6">
							<h3>Kontak Kami</h3>
							<address>
								<p></p><strong>Alamat :</strong><br><?php echo $address.', '.$city;?></p>
								<p><strong>No. Telepon / WA :</strong> <?php echo $phone;?></p>
								<p><strong>Email :</strong> <?php echo $email;?></p>								
							</address>
							<br/>
                        </div>
                        <div class="col-lg-6">
							<h5 class="mb-10">Kirim Pesan</h5>
							<p>
							    <?php
							    $phoneWA = '62'.substr($mobile, 1);
							    ?>
								<a href='//api.whatsapp.com/send?phone=<?php echo $phoneWA;?>' target="blank">
									<img src="<?php echo URL_IMAGE;?>wa.png"/>
								</a>
							</p>
							<br/>
                            <h5 class="mb-10">Follow Social Media</h5>
							<ul class="list ht-social-networks extra-large-icon">
								<li class="item">
									<a href="<?php echo $facebook;?>" target="_blank" class="social-link"> <i class="fab fa-facebook social-link-icon"></i> </a>
								</li>
								<li class="item">
									<a href="<?php echo $twitter;?>" target="_blank" class="social-link"> <i class="fab fa-twitter social-link-icon"></i> </a>
								</li>
								<li class="item">
									<a href="<?php echo $instagram;?>" target="_blank" class="social-link"> <i class="fab fa-instagram social-link-icon"></i> </a>
								</li>
								<li class="item">
									<a href="<?php echo $youtube;?>" target="_blank" class="social-link"> <i class="fab fa-youtube social-link-icon"></i> </a>
								</li>
								<li class="item">
									<a href="<?php echo $tiktok;?>" target="_blank" class="social-link"> <i class="fab fa-dribbble social-link-icon"></i> </a>
								</li>
							</ul>
                        </div>
                    </div>	
				</div>
			</div>
		</div>
	</div>
	
	<?php
	include (DIR_THEME.THEME.'/app/footer-sub.php');
	include (DIR_THEME.THEME.'/app/asset-body-sub.php');
	?>
</body>

</html>