<!DOCTYPE html>
<html dir="ltr" lang="id-ID">
    
<head><?php
    include (DIR_THEME.THEME.'/app/asset-head.php');
    include (DIR_THEME.THEME.'/app/metatag.php'); 
    include (DIR_THEME.THEME.'/app/opengraph.php');
    include (DIR_THEME.THEME.'/app/favicon.php');
    include (DIR_THEME.THEME.'/app/title.php');
    ?>
</head>

<body>
	<?php
	include (DIR_THEME.THEME.'/app/loader.php');
	include (DIR_THEME.THEME.'/app/header.php');
	?>
	<div id="main-wrapper">
	    <div class="site-wrapper-reveal section-space--pt_100 section-space--pb_100">
    		<div class="container">
    			<div class="row">
    
    				<div class="col-lg-4">
    					<div class="center" style="font-size:64px">Error 404</div>
    				</div>
    
    				<div class="col-lg-8">
    					<div>
    					    <h2>Halaman Tidak Ada...!</h2>
    						<h5>Maaf, Halaman yang Anda cari tidak ditemukan.</h5>
    						<p>Silahkan cari lagi sesuai dengan kata kunci yang tepat :</p>
    					</div>
    				</div>
     
    			</div>
    		</div>
        </div>
	</div>
	<?php
	include (DIR_THEME.THEME.'/app/footer.php');
	include (DIR_THEME.THEME.'/app/asset-body.php');
	?>
</body>

</html>