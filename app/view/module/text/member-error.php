<!DOCTYPE html>
<html lang="en">
    
<head>
    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
</head>

<body>
    
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-member.php'; ?>
		
		<div id="content-wrapper"> 
		    
			<div id="content">
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid"> 
				    
                    <div class="card">
						<div class="card-header">Halaman Tidak Ada...!</div>
						<div class="card-body">
							Maaf, Halaman yang Anda cari tidak ditemukan. 
						</div>
					</div>
                    
				</div>
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>

</html>