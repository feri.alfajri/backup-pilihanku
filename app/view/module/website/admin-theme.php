<!DOCTYPE html> 
<html lang="en">
    
<head>

    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
	
</head>

<body>
    
	<div id="wrapper"> 
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>
		
		<div id="content-wrapper"> 
		    
			<div id="content">
			
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
				    
					<h2><?php echo $titlepage;?></h2>
					<?php
					$linkmod=URL_DOMAIN.$menu.'/'.$link;
					if ($no == 0) {
						if (empty($_POST['process'])) { }
						elseif ($_POST['process']=='save') { 
							$theme = strip_tags($_POST['theme']);
							$query = mysqli_query($model->koneksi,"SELECT no,theme FROM theme WHERE theme='$theme' LIMIT 0,1");
							$jumlah = mysqli_num_rows($query);
							if ($jumlah == 0)  {  }
							else {
								mysqli_query($model->koneksi,"UPDATE setting SET theme='$theme' WHERE subdomain='".SUBDOMAIN."'"); 
								?>
								<div class="alert alert-success" role="alert">
									Selamat Tema Desain Website Anda Berhasil Diganti
								</div>
								<?php
							}
						}
						
						$qset=mysqli_query($model->koneksi,"SELECT theme FROM setting WHERE subdomain='".SUBDOMAIN."' LIMIT 0,1");
						$dset=mysqli_fetch_array($qset);
						$themepakai=$dset['theme']; 
						$qthemeid=mysqli_query($model->koneksi,"SELECT name FROM theme WHERE theme='$themepakai' LIMIT 0,1");
						$dthemeid=mysqli_fetch_array($qthemeid);
						$namethemeid=$dthemeid['name']; 
						$linkpicttempid=URL_THEME.$themepakai.'/image.jpg'; ?>
						<div class="card text-center">
							<div class="card-header"><h5>Tema Yang Sedang Digunakan</h5></div>
							<div class="card-body">
								<img src="<?php echo $linkpicttempid;?>" alt="<?php echo $namethemeid;?>" class="img-responsive mb-2"/>
								<h4><?php echo $namethemeid;?></h4>
							</div>
						</div>
						<br/>
						<div class="row">
						<?php
						$query=mysqli_query($admin->koneksi,"SELECT no,name,theme FROM theme WHERE publish='1' ORDER BY no ASC");
						$jumlah=mysqli_num_rows($query);
						while ($data=mysqli_fetch_array($query)) { 
							$no=$data['no'];
							$name=$data['name'];
							$theme=$data['theme'];
							?>
							<div class="col-md-4">
								<div class="card text-center">
									<div class="card-body">
										<form method="post">
											<input type="hidden" name="process" value="save">
											<input type="hidden" name="theme" value="<?php echo $theme;?>">
											<img class="img-responsive mb-2" src="<?php echo URL_UPLOAD;?>image.php/image.jpg?width=300&nocache&quality=100&image=<?php echo URL_THEME.$theme."/image.jpg";?>" alt="<?php echo $name;?>"/>
											<h5><?php echo $name;?></h5>
											<button class="btn btn-danger btn-user btn-block" type="submit" onclick="return confirm ('Yakin Akan Menggunakan theme <?php echo $name;?>?')">Terapkan</button>
										</form>
									</div>
								</div>
							</div>
							<?php
						} 
					}
					elseif ($no != 0) {			
						$no=$no/1;
						$query=mysqli_query($model->koneksi,"SELECT no,theme FROM theme WHERE no='$no' LIMIT 0,1");
						$data=mysqli_fetch_array($query);
						$no=$data['no'];
						if ($no=='') { 
							?>
							<div class="alert alert-success" role="alert">
								Data berhasil dihapus
							</div>
							<?php
						}
						else {
							$theme=$data['theme'];
							mysqli_query($model->koneksi,"UPDATE setting SET theme='$theme' WHERE subdomain='".SUBDOMAIN."'"); 
							?>
							<div class="alert alert-success" role="alert">
								<h5>Tema Desain Berhasil Diganti</h5>
								Selamat Tema Desain Website Anda Berhasil Diganti
								<br/><a href="<?php echo $linkmod;?>/" title="Kembali">Kembali</a>
							</div>
							<?php
						}
					}
					?>
					</div>					
                    
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>

</html>