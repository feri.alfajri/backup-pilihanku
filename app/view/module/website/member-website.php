<!DOCTYPE html> 
<html lang="en">
    
<head>

    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
	
</head>

<body>
    
	<div id="wrapper"> 
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-member.php'; ?>
		
		<div id="content-wrapper"> 
		    
			<div id="content">
			
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
				    
					<h2 class="mb-4">Website Anda</h2>
					<div class="row"> 
						<?php
						$name = $_SESSION['name'];
						$uname = $_SESSION['uname'];
						$pword = $_SESSION['pword'];
						$query = "SELECT no,subdomain,domain,name,package,logo FROM setting WHERE username='$uname' AND publish='1' ORDER BY date DESC";
						$sqlquery = mysqli_query($admin->koneksi,$query);
						$jumlah = mysqli_num_rows($sqlquery);
						while ($data = mysqli_fetch_array($sqlquery)) { 
							$websiteNo = $data['no'];
							$websiteName = $data['name'];
							$websiteLogo = $data['logo']; if ($websiteLogo=='') { $websiteLogo = 'user.png';}
							$websitePackage = $data['package'];
							$websiteDomain = $data['domain'];
							$websiteSubdomain = $data['subdomain'];
							$websitePackage = $data['package'];
							if ($websitePackage == 'free') { $websiteURL = 'http://'.$websiteSubdomain.'.pilihanku.net'; } 
							else { $websiteURL = 'https://'.$websiteDomain; } 
							$websiteURLSementara = URL_DOMAIN.'subdomain';
							?>
							<div class="col-md-12"> 
								<div class="card mb-4">
									<div class="card-body">
										<img class="mr-3" src="<?php echo URL_UPLOAD;?>image.php/<?php echo $websiteLogo;?>?width=120&cropratio=1:1&nocache&quality=100&image=<?php echo URL_PICTURE.$websiteLogo;?>" alt="logo" align="left" style="border-radius:50%"/>
										<h5 style="float:right;text-transform:uppercase;"><?php  echo $websitePackage; ?></h5>
										<h4 class="mt-1"><?php echo $websiteName;?></h4>
										<p style="color:#888;">URL : <?php  echo $websiteURL; ?></p>
								        <a class="btn btn-sm btn-primary mr-2 font-white" href="<?php echo $websiteURL;?>" title="Lihat Website" target="_blank"><i class="fas fa-desktop mr-2"></i> Lihat Website</a>
								        <a class="btn btn-sm btn-secondary mr-2 font-white" href="<?php echo $websiteURL.'/adm/autologin?u='.$uname.'&p='.$pword;?>" title="Area Admin" target="_blank"><i class="fas fa-edit mr-2"></i> Area Admin</a>
										<?php /* if ($websitePackage != 'custom') { ?><a class="btn btn-sm btn-primary mr-2" href="<?php echo $websiteURL;?>/adm/upgrade" title="Upgrade" target="_blank">Upgrade</a><?php  } */ ?>
									</div>
								</div>
							</div>
							<?php
						} 
						?>
					</div>	
					
					<div class="card mb-4">
						<div class="card-body" style="background:#EAEAEA;">
						    Klik 
							<a href="<?php echo URL_DOMAIN.$menu.'/'.$link;?>/0/create" title="Lihat Website" class="btn btn-success ml-2 mr-2">
								<i class="fas fa-plus mr-2"></i>Buat Website
							</a>
							Untuk mulai membuat website kampanye Anda.
						</div>
					</div>
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>

</html>