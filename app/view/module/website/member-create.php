<!DOCTYPE html> 
<html lang="en">
    
<head>

    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
	<style type="text/css">
	.widget-search {
	  position: relative;
	}

	.widget-search input {
	  width: 100%;
	  outline: none;
	  border: 1px solid #DADADA;
	  border-radius: 5px;
	  padding: 3px 20px;
	  padding-right: 20px;
	  height: 56px;
	  color: #333333;
	  border-color: #DADADA;
	  background-color: #f8f8f8;
	  font-size: 15px;
	  font-weight: 400;
	  letter-spacing: 0em;
	  padding-right: 72px;
	}

	.widget-search .search-submit {
	  position: absolute;
	  top: 0;
	  right: 0;
	  padding: 0;
	  border: 0;
	  border-radius: 0 5px 5px 0;
	  width: 100px;
	  height: 56px;
	  line-height: 56px;
	  text-align: center;
	  background: #EAEAEA;
	  -webkit-box-shadow: none;
			  box-shadow: none;
	  -webkit-transform: none;
		  -ms-transform: none;
			  transform: none;
	}

	.widget-search .search-submit:hover {
	  color: #ffffff;
	  background: #086AD8;
	}
</style>
</head>

<body>
    
	<div id="wrapper"> 
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-member.php'; ?>
		
		<div id="content-wrapper"> 
		    
			<div id="content">
			
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
				    
					<h2 class="mb-4">Buat Website</h2>
					<?php
					
					$uname = $_SESSION['uname'];					
					
					if (empty($_POST['process'])) {
						?>
						<div class="card mb-4">
							<div class="card-header"><h5 class="mb-0"><b>Step 1 : Domain / Subdomain</b></h5></div>
							<div class="card-body text-center">
								<h4>Silahkan Tentukan Nama Domain / Subdomain</h4>
								<div class="row text-center">
									<div class="col-lg-3 ofset"></div>
									<div class="col-lg-6">
										<form action="" method="post">
											<input name="cek" value="domain" hidden>
											<div class="widget-search mb-20">
												<input name="domain" type="text" placeholder="Silahkan Cek Domain Website…" required>
												<button class="search-submit"style="width:125px;cursor:default">.PilihanKu.Net</button>
											</div>
											<button type="submit" name="submit" class="btn btn-primary btn-lg mt-3 mb-4">CEK DOMAIN</button>
										</form>
										<?php
										if (empty($_POST['cek'])) {  }
										elseif ($_POST['cek']=='domain')  {
											$subdomain=strip_tags($_POST['domain']); $subdomain=strtolower($subdomain);
											$subdomain=str_replace('"','',$subdomain); $subdomain=str_replace("'","",$subdomain); $subdomain=str_replace(" ","",$subdomain);
											$subdomain=str_replace("#","","$subdomain"); $subdomain=str_replace("~","","$subdomain"); $subdomain=str_replace("`","","$subdomain");
											$subdomain=str_replace("!","","$subdomain"); $subdomain=str_replace("@","","$subdomain"); $subdomain=str_replace("#","","$subdomain");
											$subdomain=str_replace("$","","$subdomain"); $subdomain=str_replace("%","","$subdomain"); $subdomain=str_replace("^","","$subdomain");
											$subdomain=str_replace("&","","$subdomain"); $subdomain=str_replace("*","","$subdomain"); $subdomain=str_replace("(","","$subdomain");
											$subdomain=str_replace(")","","$subdomain"); $subdomain=str_replace("_","","$subdomain"); $subdomain=str_replace("+","","$subdomain");
											$subdomain=str_replace("=","","$subdomain"); $subdomain=str_replace("|","","$subdomain"); $subdomain=str_replace("{","","$subdomain");
											$subdomain=str_replace("}","","$subdomain"); $subdomain=str_replace("[","","$subdomain"); $subdomain=str_replace("]","","$subdomain");
											$subdomain=str_replace(":","","$subdomain"); $subdomain=str_replace(";","","$subdomain"); $subdomain=str_replace("'","","$subdomain");
											$subdomain=str_replace(">","","$subdomain"); $subdomain=str_replace("<","","$subdomain"); $subdomain=str_replace("?","","$subdomain");
											$subdomain=str_replace(",","","$subdomain"); $subdomain=str_replace(".","","$subdomain"); $subdomain=str_replace("/","","$subdomain");
											$panjangdomain=strlen($subdomain);
											if ($panjangdomain<5) {
												$respon='warning';
												$domainresmi=$subdomain.'.pilihanku.net';
											}
											else {
												$qceksub=mysqli_query($model->koneksi, "SELECT no FROM setting WHERE subdomain='$subdomain'");
												$jumceksub=mysqli_num_rows($qceksub);
												$domainresmi=$subdomain.'.pilihanku.net';
												if ($jumceksub==0) {
													$respon='success';
												}
												else {
													$respon='failed';
												}
											}

											if ($respon=='warning') {
												?>
												<div class="alert alert-danger" role="alert">
													<h6 class="mb-0">
														Maaf, <b><?php echo $domainresmi; ?></b> terlalu pendek. Minimal 5 karakter.
														<br/>Silahkan coba nama domain yang lain.
													</h6>
												</div>
												<?php
											}
											elseif ($respon=='failed') {
												?>
												<div class="alert alert-danger" role="alert">
													<h6 class="mb-0">
														Maaf, <b><?php echo $domainresmi; ?></b> tidak dapat digunakan.
														<br />Silahkan coba nama domain yang lain.
													</h6>
												</div>
												<?php
											}
											elseif ($respon=='success') {
												?>
												<div class="alert alert-success" role="alert">
													<h6>Selamat, <b><?php echo $domainresmi;?></b> dapat digunakan</h6>
													<form action="" method="post">
														<input name="subdomain" value="<?php echo $subdomain;?>" hidden>
														<input name="process" value="data" hidden>
														<button type="submit" name="submit" class="btn btn-success">
															LANJUTKAN <span class="btn-icon"><i class="fa fa-arrow-right"></i></span>
														</button>
													</form>
												</div>
												<?php
											}
										}
										?>
									</div>
								</div>
							</div>
						</div>
						<?php
					}
					elseif ($_POST['process']=='data') {
						$subdomain = strip_tags($_POST['subdomain']);						
						$qmember = mysqli_query($model->koneksi, "SELECT * FROM member WHERE username = '$uname'");
						$dmember = mysqli_fetch_array($qmember);
					    $memberName = $dmember['name'];
						$memberAddress = $dmember['address'];
						$memberCity = $dmember['city'];
						$memberPhone = $dmember['phone'];
						$memberEmail = $dmember['email'];
						$memberPicture = $dmember['picture'];
						?>
						<div class="card mb-4">
							<div class="card-header"><h5 class="mb-0"><b>Step 2 : Data Website</b></h5></div>
							<div class="card-body text-center">
								<form action="" method="post" enctype="multipart/form-data">
									<input name="subdomain" value="<?php echo $subdomain;?>" hidden>
									<input name="process" value="finish" hidden>
									<div class="row">
										<div class="col-md-6">
											<h5 class="mb-3 text-left">PROFIL CALON</h5>
											<div id="view2">
												<div class="view2_label"><b>Nama Lengkap</b></div><div class="view2_dot">:</div>
												<div class="view2_content"><input type="text" class="form-control" name="name" value="<?php echo $memberName;?>" placeholder="Tulis Nama Lengkap Calon" required/></div>
											</div>										
											<div id="view2">
												<div class="view2_label"><b>Alamat</b></div><div class="view2_dot">:</div>
												<div class="view2_content"><input type="text" class="form-control" name="address" value="<?php echo $memberAddress;?>" placeholder="Tulis Alamat Lengkap" required/></div>
											</div>
											<div id="view2">
												<div class="view2_label"><b>Kota</b></div><div class="view2_dot">:</div>
												<div class="view2_content"><input type="text" class="form-control" name="city" value="<?php echo $memberCity;?>" placeholder="Tulis Kota" required/></div>
											</div>
											<div id="view2">
												<div class="view2_label"><b>Telepon</b></div><div class="view2_dot">:</div>
												<div class="view2_content"><input type="text" class="form-control" name="phone" value="<?php echo $memberPhone;?>" placeholder="Tulis No. Telepon / WA" required/></div>
											</div>
											<div id="view2">
												<div class="view2_label"><b>Email</b></div><div class="view2_dot">:</div>
												<div class="view2_content"><input type="email" class="form-control" name="email" value="<?php echo $memberEmail;?>" placeholder="Tulis Email" required/></div>
											</div>
											<div id="view2">
												<div class="view2_label"><b>Foto Profil</b></div><div class="view2_dot">:</div>
												<div class="view2_content"><input type="file" class="form-control" name="logo" placeholder="Sisipkan Foto Profil" required/></div>
											</div>
										</div>
										<div class="col-md-6">
											<h5 class="mb-3 text-left">PROFIL PARTAI / DAPIL</h5>
											<div id="view2">
												<div class="view2_label"><b>Partai Politik</b></div><div class="view2_dot">:</div>
												<div class="view2_content">
													<select name="no_partai" class="form-control" required>
														<option value="">- Pilih Partai-</option>
														<?php
														$qpartai = mysqli_query($model->koneksi, "SELECT no,name FROM partai WHERE publish = '1' ORDER BY name");
														while($dpartai = mysqli_fetch_array($qpartai)) {
															$partaiNo = $dpartai['no'];
															$partaiName = $dpartai['name'];
															?>
															<option value="<?php echo $partaiNo;?>"><?php echo $partaiName;?></option>
															<?php														
														}
														?>
													</select>
												</div>
											</div>
											<div id="view2">
												<div class="view2_label"><b>DPR/DPD Tingkat</b></div><div class="view2_dot">:</div>
												<div class="view2_content">
													<select name="tingkat" class="form-control" required>
														<option value="">Pilih Tingkat</option>
														<option value="DPR RI">DPR RI</option>
														<option value="DPD RI">DPD RI</option>
														<option value="DPRD Provinsi">DPRD Provinsi</option>
														<option value="DPRD Kabupaten / Kota">DPRD Kabupaten / Kota</option>
													</select>
												</div>
											</div>	
											<div id="view2">
												<div class="view2_label"><b>Daerah Pemilihan</b></div><div class="view2_dot">:</div>
												<div class="view2_content"><input class="form-control" name="dapil" placeholder="Tulis Daerah Pemilihan"/></div>
											</div>
											<div id="view2">
												<div class="view2_label"><b>Slogan</b></div><div class="view2_dot">:</div>
												<div class="view2_content"><input class="form-control" name="slogan" placeholder="Tulis Slogan Anda"/></div>
											</div>
										</div>
									</div>
									<button type="submit" name="submit" class="btn btn-primary btn-lg mt-3">
										SELESAIKAN <span class="btn-icon"><i class="fa fa-arrow-right"></i></span>
									</button>
								</form>
							</div>
						</div>
						<?php
						$randcode=rand(111111,999999); 
						$_SESSION['code']=$randcode;
					}
					elseif ($_POST['process']=='finish') {
						if (empty ($_SESSION['code'])) {  
							?>
							<div class="alert alert-danger" role="alert">
								Maaf, Session Habis. Ulangi Prosesnya dari awal.
								<a href="<?php echo URL_DOMAIN.$menu.'/'.$link.'/0/create';?>" title="Kembali" class="btn btn-primary">Kembali</a>
							</div>
								<?php
						}
						else {
							$subdomain=strip_tags($_POST['subdomain']); $subdomain=str_replace('"','',$subdomain);$subdomain=str_replace("'","",$subdomain);
							$name=strip_tags($_POST['name']); $name=str_replace('"','',$name); $name=str_replace("'","",$name);
							$address=strip_tags($_POST['address']); $address=str_replace('"','',$address); $address=str_replace("'","",$address);
							$city=strip_tags($_POST['city']); $city=str_replace('"','',$city); $city=str_replace("'","",$city);
							$phone=strip_tags($_POST['phone']); $phone=str_replace('"','',$phone); $phone=str_replace("'","",$phone); $phone=str_replace(" ","",$phone); 
							$email=strip_tags($_POST['email']); $email=str_replace('"','',$email); $email=str_replace("'","",$email); $email=str_replace(" ","",$email); 
							$no_partai=strip_tags($_POST['no_partai']); $no_partai=str_replace('"','',$no_partai); $no_partai=str_replace("'","",$no_partai);
							$tingkat=strip_tags($_POST['tingkat']); $tingkat=str_replace('"','',$tingkat); $tingkat=str_replace("'","",$tingkat);  
							$dapil=strip_tags($_POST['dapil']); $dapil=str_replace('"','',$dapil); $dapil=str_replace("'","",$dapil); 
							$slogan=strip_tags($_POST['slogan']); $slogan=str_replace('"','',$slogan); $slogan=str_replace("'","",$slogan); 
							
							//INPUT KONTEN PAGE
							$content_welcome = '
    							<p><b>Selamat Datang di Website '.$name.'</b></p>
                                <p>Terima kasih sudah terhubung dengan saya melalui website pribadi saya ini. Website ini ditujukan sebagai media untuk saling mengenal dan berinteraksi sebagai bagian dari komitmen saya untuk ikut membangun negeri. </p>
                                <p>Semoga jalan perjuangan kita semua sebagai bangsa merdeka, diridhoi oleh Tuhan Yang Maha Esa.</p>
                                <br/>
                                <p>Salam hangat,</p>
                                <p>'.$name.'</p>';
							$content_volunteer = '<p>KAMI BUKAN SIAPA-SIAPA TANPA ANDA! </p>
                                <p>adalah prinsip yang selalu kami junjung tinggi dalam setiap gerakan kami. MENJADI RELAWAN adalah sebuah gerakan solidaritas baru. </p>
                                <p>Ibarat kanvas yang masih bersih, Indonesia yang baru akan kita lukis kembali. Kami mengajak semua dari KITA untuk menorehkan warna kita masing-masing di atas kanvas Indonesia Baru.</p>
                                <p>Tunjukkan bahwa Indonesia tidak hanya dibangun diatas warna-warna yang kusam dan membosankan. Indonesia Baru bisa kita lukis dengan warna-warni yang membentuk mozaik indah di garis khatulistiwa. </p>
                                <p>Setitik kecil yang anda berikan adalah sumbangan besar untuk masa depan Indonesia yang kita cintai.</p>
                                <br/>
                                <p>Mari Gabung Menjadi Relawan Gerakan Kami. OK</p>';
							$content_donation = '<p>Kami menyadari bahwa gerakan kami tidak akan maksimal tanpa bantuan dan partisipasi Anda. 
                                Kami menerima donasi sebagai dukungan Anda terhadap gerakan kami. Anda bisa mengirimkan donasi ke rekening di bawah ini :</p>
                                <p><b>BANK ABC</b><br>No. Rek. 1234567890<br>A.N. Nama Orang</p>
                                <p>Setelah melakukan transfer donasi, silahkan konfirmasi pada formulir dibawah/disamping.</p>
                                <p>Semoga bantuan dan donasi Anda mendapatkan keutamaan yang berlipat dan dibalas oleh Tuhan Yang Maha Esa.</p>';
                            $content_cta = 'Ayo, Dukung dan bergabunglah bersama kami, dan jadikan Indonesia lebih baik !';
                            
							mysqli_query($model->koneksi, "INSERT INTO setting (subdomain, username, no_partai, tingkat, dapil, name, slogan, favicon, footer, theme, address, city, phone, email, content_welcome, content_volunteer, content_donation, content_cta, date, publish)
								VALUES ('$subdomain', '$uname', '$no_partai', '$tingkat', '$dapil', '$name', '$slogan', '$favicon', '2022 - $subdomain', 'mitech', '$address', '$city', '$phone', '$email', '$content_welcome', '$content_volunteer', '$content_donation', '$content_cta', sysdate(), '1')");
							mysqli_query($model->koneksi, "INSERT INTO cv (subdomain, name, address, city, phone, email, publish)
								VALUES ('$subdomain', '$name', '$address', '$city', '$phone', '$email',  '1')");
							if (empty($_FILES['logo']['tmp_name'])) { }
							else {
								$logo=$_FILES['logo']['tmp_name'];
								$logo_name=$_FILES['logo']['name'];
								$random=rand(00000000,99999999);
								$logo_namenew=$random.$logo_name;
								$logo_namenew=str_replace(" ","",$logo_namenew);
								mysqli_query($model->koneksi,"UPDATE setting SET logo='$logo_namenew' WHERE subdomain='$subdomain' AND username='$uname'");
								mysqli_query($model->koneksi,"UPDATE cv SET picture='$logo_namenew' WHERE subdomain='$subdomain'");
								copy ($logo, DIR_PICTURE.$logo_namenew);
							}							
							//EMAIL NOTIFIKASI
							//KIRIM EMAIL
            				include  (DIR_LIBRARY.'class.phpmailer.php');
            				$isi='<html>' .
            					'<head></head>' .
            					'<body>' .
            					'<label><img src="'.URL_IMAGE.$logo.'" alt="logo" width="250"></label><br/><br>				
            					<p>Salam, '.$name.'</p>
            					<p>Terima kasih telah menggunakan layanan website kampanye PilihanKu.Net.</p>			
            					<p>Pembuatan website kampanye Anda telah jadi dan dapat digunakan.</p>
            					<br/>
            				    <p>Berikut ini adalah data-data website Anda :</p>	
            				    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                	<tr><td align=left width=140>Nama Website</td><td align=left>: &nbsp; '.$name.'</td></tr>
                                	<tr><td align=left>URL Website</td><td align=left >: &nbsp; http://'.$subdomain.'.pilihanku.net</td></tr>
                                </table>
            					<p>Untuk melakukan perubahan data dan konten website, Anda dapat mengakses halaman admin.</p>
            					<p>Halaman Admin dapat diakses melalui URL : http://'.$subdomain.'.pilihanku.net/adm</p>
            					<p>Gunakan username dan password yang telah Anda dapatkan sebelumnya.</p>
            					<br/>
            					<p>Informasi lebih lanjut mengenai layanan kami, silahkan menghubungi Customer Service.</p>
            					<p>Semoga kesuksesan senantiasa menyertai daya upaya kita bersama.</p>
            					<br/>				
            					<p>Best Regards</p>
            					<p>PilihanKu.net</p>
            					 </body>' .
            					'</html>'; 
            				date_default_timezone_set('Asia/Jakarta');
            				$mail=new PHPMailer();
            				$body=$isi;
            				$mail->Subject='Aktivasi Website '.$subdomain.'.pilihanku.net';
            				$mail->MsgHTML($body);
            				$mail->addReplyTo(ASAL_EMAIL,'PilihanKu.net');
            				$mail->SetFrom(ASAL_EMAIL, 'PilihanKu.net');
            				$mail->AddAddress($email, $name);
            				unset($_SSESSION['kode']);
            				if(!$mail->Send()) {  echo "Mailer Error: " . $mail->ErrorInfo; } 
							?>
							<div class="card mb-4">
								<div class="card-header"><h5 class="mb-0"><b>Step 3 : Selesai</b></h5></div>
								<div class="card-body text-left">
									<h2>Selamat, Website kampanye Anda telah jadi.</h2>
									<p>Website <b><?php echo $subdomain;?>.pilihanku.net</b> telah jadi.</p>
									<p>Klik <a class="btn btn-primary" href="http://<?php echo $subdomain;?>.pilihanku.net">DISINI</a> untuk melihat tampilan website kampanye Anda.</p>
                                    <p>Untuk melakukan perubahan data dan konten website, Anda dapat mengakses halaman admin.</p>
                					<p>Halaman Admin dapat diakses melalui URL : http://'.$subdomain.'.pilihanku.net/adm</p>
                					<p>Gunakan username dan password yang telah Anda dapatkan sebelumnya.</p>
                					<p>Informasi lebih lanjut mengenai layanan kami, silahkan menghubungi Customer Service.</p>
								</div>
							</div>
							<?php
						}
					}
					?>
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>

</html>z