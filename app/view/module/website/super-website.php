<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_SUPER.'/app/asset-head.php';?>
</head>

<body class="nav-fixed">
   
	<?php include DIR_THEME.THEME_SUPER.'/app/topbar.php'; ?>
	
	<div id="layoutSidenav">
	
        <div id="layoutSidenav_nav">
			<?php include DIR_THEME.THEME_SUPER.'/app/sidebar.php'; ?>			
		</div>
		
		<div id="layoutSidenav_content">
			<main>
				
				<?php include DIR_THEME.THEME_SUPER.'/app/header.php'; ?>

				<div class="container-xl px-4 mt-4">
					
					
					<?php        
					function sub_menu($menu,$link,$no) {
						?>
						<div style="display:table;width:100%;margin:20px 0px;">
							<a href="<?php echo URL_DOMAIN.$menu.'/'.$link.'/'.$no.'/edit-setting/';?>" class="btn btn-primary mr-1">Pengaturan</a>
							<a href="<?php echo URL_DOMAIN.$menu.'/'.$link.'/'.$no.'/edit-package/';?>" class="btn btn-primary mr-1">Paket</a>
							<a href="<?php echo URL_DOMAIN.$menu.'/'.$link.'/'.$no.'/edit-partai/';?>" class="btn btn-primary mr-1">Partai</a>
							<a href="<?php echo URL_DOMAIN.$menu.'/'.$link.'/'.$no.'/edit-contact/';?>" class="btn btn-primary mr-1">Kontak</a>							
							<a href="<?php echo URL_DOMAIN.$menu.'/'.$link.'/'.$no.'/edit-banner/';?>" class="btn btn-primary mr-1">Banner</a>							
							<a href="<?php echo URL_DOMAIN.$menu.'/'.$link.'/'.$no.'/edit-icon/';?>" class="btn btn-primary mr-1">Icon</a>							
							<a href="<?php echo URL_DOMAIN.$menu.'/'.$link.'/'.$no.'/edit-upgrade/';?>" class="btn btn-primary mr-1">Upgrade</a>
							<a href="<?php echo URL_DOMAIN.$menu.'/'.$link.'/'.$no.'/edit-welcome/';?>" class="btn btn-primary mr-1">Welcome</a>
							<a href="<?php echo URL_DOMAIN.$menu.'/'.$link.'/'.$no.'/edit-volunteer/';?>" class="btn btn-primary mr-1">Relawan</a>
							<a href="<?php echo URL_DOMAIN.$menu.'/'.$link.'/'.$no.'/edit-donation/';?>" class="btn btn-primary mr-1">Donasi</a>
							<a href="<?php echo URL_DOMAIN.$menu.'/'.$link.'/'.$no.'/edit-cta/';?>" class="btn btn-primary mr-1">CTA</a>
						</div><?php					
					}
					
					
					$admin = new admin();
					
					$title = 'Website';  
					
					$table_name = 'setting';
					$table_label = 'logo,nama,telepon,partai,tingkat,tanggal';
					$table_column = 'logo,name,phone,no_partai,tingkat,date';
					$table_width = '5,15,12,10,15,10'; 
					$table_button = 'read,account';  
					$table_condition = "";
					
					$read_label = 'subdomain,domain,paket,username,referral,partai,tingkat,dapil,name,slogan 1,slogan 2,meta title,meta description,meta keyword,meta tag,logo,banner,favicon,footer,tema,alamat,kota,telepon,email,URL facebook,RL twitter,RL instagram,RL youtube,RL tiktok,content selamat datang,content relawan,content donasi,content cta,upgrade ke,nilai upgrade,tanggal';
					$read_content = 'subdomain,domain,package,username,referral,no_partai,tingkat,dapil,name,slogan_1,slogan_2,meta_title,meta_description,meta_keyword,meta_tag,logo,banner,favicon,footer,theme,address,city,phone,email,facebook,twitter,instagram,youtube,tiktok,content_welcome,content_volunteer,content_donation,content_cta,upgrade,upgrade_value,date';
					$read_condition = "WHERE no='$no'";
					
					$update_type = '';
					$update_label = 'subdomain,domain,paket,username,referral,partai,tingkat,dapil,name,slogan 1,slogan 2,meta title,meta description,meta keyword,meta tag,logo,banner,favicon,footer,tema,alamat,kota,telepon,email,URL facebook,RL twitter,RL instagram,RL youtube,RL tiktok,content selamat datang,content relawan,content donasi,content cta,upgrade ke,nilai upgrade';
					$update_form = 'subdomain,domain,package,username,referral,no_partai,tingkat,dapil,name,slogan_1,slogan_2,meta_title,meta_description,meta_keyword,meta_tag,logo,banner,favicon,footer,theme,address,city,phone,email,facebook,twitter,instagram,youtube,tiktok,content_welcome,content_volunteer,content_donation,content_cta,upgrade,upgrade_value';
					$update_condition = "WHERE no='$no'";
					
					$delete_condition = "WHERE no='$no'";
					
					if (empty ($act)) {
						
						$admin->title($title);
						$admin->tablepro($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 	
					elseif ($act=='custom') {						
						
						$admin->title($title.' (Custom)');
						$table_condition = "WHERE package = 'custom'";
						$admin->tablepro($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					elseif ($act=='pro') {						
						
						$admin->title($title.' (Pro)');
						$table_condition = "WHERE package = 'pro'";
						$admin->tablepro($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					elseif ($act=='free') {						
						
						$admin->title($title.' (Free)');
						$table_condition = "WHERE package = 'free'";
						$admin->tablepro($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					} 
					elseif ($act=='read') {						
						
						$admin->title($title);
						sub_menu($menu,$link,$no);
						$admin->read($table_name,$read_label,$read_content,$read_condition); 
						$admin->button ('back','Kembali','','btn-primary');
						
					} 
					elseif ($act=='update') {
						
						$admin->title('Ubah '.$title);
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 	
					elseif ($act=='edit-setting') {
						
						$admin->title('Ubah Paket');
						sub_menu($menu,$link,$no);
						$update_type = 'picture';
						$update_label = 'name,slogan 1,slogan 2,meta title,meta description,meta keyword,meta tag,footer,tema,logo';
						$update_form = 'name,slogan_1,slogan_2,meta_title,meta_description,meta_keyword,meta_tag,footer,theme,logo';
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act=='edit-banner') {
						
						$admin->title('Ubah Paket');
						sub_menu($menu,$link,$no);
						$update_type = 'picture';
						$update_label = 'banner,';
						$update_form = 'banner';
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act=='edit-icon') {
						
						$admin->title('Ubah Paket');
						sub_menu($menu,$link,$no);
						$update_type = 'picture';
						$update_label = 'favicon';
						$update_form = 'favicon';
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act=='edit-package') {
						
						$admin->title('Ubah Paket');
						sub_menu($menu,$link,$no);
						$update_label = 'subdomain,domain,paket,username,referral';
						$update_form = 'subdomain,domain,package,username,referral';
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act=='edit-partai') {
						
						$admin->title('Ubah Partai');
						sub_menu($menu,$link,$no);
						$update_label = 'partai,tingkat,dapil';
						$update_form = 'no_partai,tingkat,dapil';
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act=='edit-contact') {
						
						$admin->title('Ubah Kontak & Social Media');
						sub_menu($menu,$link,$no);
						$update_label = 'alamat,kota,telepon,email,URL facebook,RL twitter,RL instagram,RL youtube,RL tiktok';
						$update_form = 'address,city,phone,email,facebook,twitter,instagram,youtube,tiktok';
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act=='edit-upgrade') {
						
						$admin->title('Ubah Upgrade');
						sub_menu($menu,$link,$no);
						$update_label = 'Upgrade Ke,Nilai';
						$update_form = 'upgrade,upgrade_value';
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 
					elseif ($act=='edit-welcome') {
						
						$admin->title('Ubah Ucapan Selamat Datang');
						sub_menu($menu,$link,$no);
						$update_label = 'Ucapan Selamat Datang';
						$update_form = 'content_welcome';
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 					
					elseif ($act=='edit-volunteer') {
						
						$admin->title('Ubah Konten Relawan');
						sub_menu($menu,$link,$no);
						$update_label = 'Konten Relawan';
						$update_form = 'content_volunteer';
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 		
					elseif ($act=='edit-donation') {
						
						$admin->title('Ubah Konten Donasi');
						sub_menu($menu,$link,$no);
						$update_label = 'Konten Donasi';
						$update_form = 'content_donation';
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 	
					elseif ($act=='edit-cta') {
						
						$admin->title('Ubah Konten CTA');
						sub_menu($menu,$link,$no);
						$update_label = 'Konten CTA';
						$update_form = 'content_cta';
						$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);
						
					} 	
					elseif ($act=='delete') {	
										
						//$admin->delete($table_name,$delete_condition);
						$admin->title($title);
						echo '<div class="alert alert-danger" role="alert">Demi Keamanan, Fitur Hapus Dinonaktifkan</div>';
						$admin->tablepro($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
						
					} 
					else {						
						
						$admin->title($title);
						$admin->tablepro($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition);
						
					}
					?>		
					
				</div>
				
			</main>
		</div>
		
	</div>	
	
	<?php include DIR_THEME.THEME_SUPER.'/app/asset-body.php'; ?>
	
</body>
 
</html>