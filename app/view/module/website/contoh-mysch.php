<!DOCTYPE html>
<html dir="ltr" lang="id-ID">

<head>
    <?php
     //error_reporting(0);
    
    //error_reporting(-1);
    $titleweb = 'Buat Website Sekolah';
    include DIR_THEME . THEME . '/app/asset/asset-head.php';
    include DIR_THEME . THEME . '/app/asset/metatag.php';
    include DIR_THEME . THEME . '/app/asset/opengraph.php';
    include DIR_THEME . THEME . '/app/asset/favicon.php';
    include DIR_THEME . THEME . '/app/asset/title.php';
    // Menggunkan library domp pdf yang saya taruh di folder library
    include DIR_LIBRARY . 'dompdf/autoload.inc.php';
   
    
    ?>
    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '772977643242377');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=772977643242377&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->
</head>

<body>
    <?php
    include DIR_THEME . THEME . '/app/header/header.php';
    include DIR_THEME . THEME . '/app/header/pagetitle.php';
    date_default_timezone_set('Asia/Jakarta');
    include DIR_LIBRARY . 'class.phpmailer.php';
    
    require_once "/home/mysch123/public_html/blesta/blesta_sdk-master/api/blesta_api.php";
 
    $user_blesta = "Mysch";
    $key_blesta = "1263f3852b2bf7992d0e9057e1f0b19e";
    $url_blesta = "https://blesta.mysch.id/api/";
    
    $api_blesta = new BlestaApi($url_blesta, $user_blesta, $key_blesta);
    
    include DIR_LIBRARY . 'SMS.php';
    $smsOptions = ['user' => SMS_USERNAME, 'password' => SMS_PASSWORD, 'output' => 'json'];
    $sms = new SMS(SMS_SENDERID, '', '', $smsOptions);
    use WHMCS;
    use Carbon\Carbon;
    use WHMCS\Database\Capsule;
    use Dompdf\Dompdf;
    ?>
    <div id="main-wrapper">
        <div class="site-wrapper-reveal">

            <?php
            if (empty($_GET['id'])) { $_SESSION['referral']="admin"; }
            else {  $id=(int)$_GET['id']; $_SESSION['referral']=$id; }
            if(isset($_SESSION['referral'])){ $idsponsor=$_SESSION['referral']; }
            else { $idsponsor=0; }

            if ($act=='' OR $act=='paket') {

                ?>
            <div class="pricing-table-area section-space--ptb_70 bg-white">
                <div class="pricing-table-title-area position-relative">
                    <div class="container">
                        <div class="section-title-wrapper text-center section-space--mb_60 wow move-up">
                            <h3 class="heading">Step 1 : Pilih Paket Website</h3>
                        </div>
                    </div>
                </div>
                <div class="pricing-table-content-area">
                    <div class="container">
                        <div class="row pricing-table-one"
                            style="-webkit-box-align:top;-webkit-align-items:top;-ms-flex-align:top;align-items:top">

                            <div
                                class="col-12 col-md-6 col-lg-4 col-xl-4 pricing-table pricing-table--popular wow move-up">
                                <div class="pricing-table__inner">

                                    <div class="pricing-table__header">
                                        <h6 class="sub-title">Profesional</h6>
                                        <div class="pricing-table__image">
                                            <img src="https://www.mysch.id/cms_web/upload/image/pricing-2.png"
                                                class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="pricing-table__body">
                                        <ul class="pricing-table__list text-left">
                                            <li>Domain resmi (.sch.id)</li>
                                            <li>Email @namasekolah.sch.id</li>
                                            <li>Unlimited Menu</li>
                                            <li>Unlimited Blog/Berita</li>
                                            <li>Unlimited Galeri/Foto</li>
                                            <li>Unlimited Theme</li>
                                            <li>Fitur Khusus Sekolah</li>
                                        </ul>
                                    </div>
                                    <div class="mt-10 text-center">
                                        <form action="<?php echo URL_DOMAIN . $menu . '/' . $link . '/1/check'; ?>" method="post">
                                            <input name="paket" type="hidden" value="profesional" />
                                            <select name="pid" class="mb-10">
                                                <?php
                    							     $pro =  $api_blesta->get("packages", "getAllPackagesByGroup", array('package_group_id' => 3, 'status' => 'active'));
                                                     $datapro = $pro->response();
                                                    
                                                     foreach($datapro as $data){
                							            ?>
                                                        <option value="<?php echo $data->id; ?>,<?= $data->pricing['0']->id ?>" data-harga="<?php echo number_format($data->pricing['0']->price,0,",","."); ?>"
                                                            <?php if ($data->id == 10) {
                                                                echo 'selected';
                                                            } ?>><?php echo 'Rp ' . number_format($data->pricing['0']->price,0,",",".") . ' / ' . $data->description; ?></option>
                                                        <?php
                    							        }
                							        ?>
                                            </select>
                                            <i class="mb-2" style="float:left;">* Harga belum termasuk pajak </i>
                                            <button type="submit" name="submit" class="ht-btn ht-btn-md" />LANJUTKAN
                                            <span class="btn-icon"><i
                                                    class="far fa-arrow-right"></i></span></button>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-md-6 col-lg-4 col-xl-4 pricing-table wow move-up">
                                <div class="pricing-table__inner">
                                    <div class="pricing-table__feature-mark">
                                        <span>Popular Choice</span>
                                    </div>
                                    <div class="pricing-table__header">
                                        <h6 class="sub-title">Exclusive</h6>
                                        <div class="pricing-table__image">
                                            <img src="https://www.mysch.id/cms_web/upload/image/pricing-3.png"
                                                class="img-fluid" alt="">
                                        </div>

                                    </div>
                                    <div class="pricing-table__body">
                                        <ul class="pricing-table__list text-left">
                                            <li>Domain resmi (.sch.id)</li>
                                            <li>Email @namasekolah.sch.id</li>
                                            <li>Unlimited Menu</li>
                                            <li>Unlimited Blog/Berita</li>
                                            <li>Unlimited Galeri/Foto</li>
                                            <li>Unlimited Theme</li>
                                            <li>Fitur Khusus Sekolah</li>
                                            <li><b>Akses CPanel Space 1GB</b></li>
                                        </ul>
                                    </div>
                                    <div class="mt-10 text-center">
                                        <form action="<?php echo URL_DOMAIN . $menu . '/' . $link . '/1/check'; ?>" method="post">
                                            <input name="paket" type="hidden" value="exclusive" />
                                            <select name="pid" class="mb-10">
                                                <?php
                    							     $exc =  $api_blesta->get("packages", "getAllPackagesByGroup", array('package_group_id' => 4, 'status' => 'active'));
                                                     $dataexc = $exc->response();
                                                    
                                                     foreach($dataexc as $data){
                                                         
                                                         if($data->id == '41'){
                                                             $pricing = number_format($data->pricing['0']->price,0,",",".");
                                                             $pricing_id = $data->pricing['0']->id;
                                                         }elseif($data->id == '38'){
                                                             $pricing = number_format($data->pricing['0']->price,0,",",".");
                                                             $pricing_id = $data->pricing['0']->id;
                                                         }elseif($data->id == 39){
                                                             $pricing = number_format($data->pricing['1']->price,0,",",".");
                                                             $pricing_id = $data->pricing['1']->id;
                                                         }elseif($data->id == 40){
                                                             $pricing = number_format($data->pricing['1']->price,0,",",".");
                                                             $pricing_id = $data->pricing['1']->id;
                                                         }
                                                         
                							            ?>
                                                        <option value="<?php echo $data->id; ?>,<?= $pricing_id ?>" data-harga="<?php echo $pricing; ?>"
                                                            <?php if ($data->id == 38) {
                                                                echo 'selected';
                                                            } ?>><?php echo 'Rp ' . $pricing . ' / ' . $data->description; ?></option>
                                                        <?php
                    							        }
                							        ?>
                                            </select>
                                            <i class="mb-2" style="float:left;">* Harga belum termasuk pajak </i>
                                            <button type="submit" name="submit" class="ht-btn ht-btn-md" />LANJUTKAN
                                            <span class="btn-icon"><i
                                                    class="far fa-arrow-right"></i></span></button>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-md-6 col-lg-4 col-xl-4 pricing-table wow move-up">
                                <div class="pricing-table__inner">
                                    <div class="pricing-table__header">
                                        <h6 class="sub-title">Custom</h6>
                                        <div class="pricing-table__image">
                                            <img src="https://www.mysch.id/cms_web/upload/image/pricing-1.png"
                                                class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="pricing-table__body">
                                        <ul class="pricing-table__list text-left">
                                            <li>Domain resmi (.sch.id)</li>
                                            <li>Email @namasekolah.sch.id</li>
                                            <li>Unlimited Menu</li>
                                            <li>Unlimited Blog/Berita</li>
                                            <li>Unlimited Galeri/Foto</li>
                                            <li>Unlimited Theme</li>
                                            <li>Fitur Khusus Sekolah</li>
                                            <li><b>Akses CPanel 1GB</b></li>
                                            <li><b>Tema Desain Customize</b></li>
                                            <li><b>Input Data, Profil Sekolah</b></li>
                                            <li><b>Desain Banner, Slider, Dll</b></li>
                                        </ul>
                                    </div>
                                    <div class="mt-10 text-center">
                                        <form action="<?php echo URL_DOMAIN . $menu . '/' . $link . '/1/check'; ?>" method="post">
                                            <input name="paket" type="hidden" value="custom" />
                                            <select name="pid" class="mb-10">
                                                <?php
                    							     $exc =  $api_blesta->get("packages", "getAllPackagesByGroup", array('package_group_id' => 5, 'status' => 'active'));
                                                     $dataexc = $exc->response();
                                                    
                                                     foreach($dataexc as $data){
                							            ?>
                                                        <option value="<?php echo $data->id; ?>,<?= $data->pricing['0']->id ?>" data-harga="<?php echo number_format($data->pricing['0']->price,0,",","."); ?>"
                                                            <?php if ($data->id == 42) {
                                                                echo 'selected';
                                                            } ?>><?php echo 'Rp ' . number_format($data->pricing['0']->price,0,",",".") . ' / ' . $data->description; ?></option>
                                                        <?php
                    							        }
                							        ?>
                                            </select>
                                            <i class="mb-2" style="float:left;">* Harga belum termasuk pajak </i>
                                            <button type="submit" name="submit" class="ht-btn ht-btn-md" />LANJUTKAN
                                            <span class="btn-icon"><i
                                                    class="far fa-arrow-right"></i></span></button>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 pricing-table wow move-up">
            				    <div class="pricing-table__inner" style="padding:30px 20px;">
            						<div class="text-left">
            							<form action="<?php echo URL_DOMAIN.$menu.'/'.$link.'/1/check';?>" method="post">
			                            	<input name="paket" type="hidden" value="free"/>
			                            	<h5>Free Trial</h5>
                                            Anda dapat membuat website sekolah dengan domain gratis dari <b>.mysch.id</b> 
            							    <button type="submit" name="submit" class="ht-btn ht-btn-xs ht-btn--outline"/>LANJUTKAN <span class="btn-icon"><i class="far fa-arrow-right"></i></span></button>
                                        </form>
            						</div>
            					</div>
            				</div>

                        </div>
                    </div>
                </div>
            </div>
            <?php

            }
            elseif ($act=='check') {

                if (empty($_POST['paket']) and empty($_POST['pid'])) {
                    echo 'Balikin Ke Depan';
                }
                else {
                    $paket  = $_SESSION['paket']= strip_tags($_POST['paket']);
                    $pid    = $_SESSION['pid']  = strip_tags($_POST['pid']);
                    ?>
            <div class="contact-us-section-wrappaer section-space--pt_70 section-space--pb_70 bg-white">
                <div class="container text-center">
                    <div class="pricing-table-title-area position-relative">
                        <div class="container">
                            <div class="section-title-wrapper text-center section-space--mb_60 wow move-up">
                                <h3 class="heading">Step 2 : Tentukan Nama Domain</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col-lg-3 ofset"></div>
                        <div class="col-lg-6">
                            <form action="<?php echo URL_DOMAIN . $menu . '/' . $link . '/1/check'; ?>" method="post">
                                <input name='cek' value='domain' hidden>
                                <input name='paket' value='<?php echo $paket; ?>' hidden>
                                <input name='pid' value='<?php echo $pid; ?>' hidden>
                                <div class="widget-search mb-20">
                                    <input name="domain" type="text" placeholder="Silahkan Cek Domain Website…"
                                        required>
                                    <?php
                                            if ($paket=='free') { ?> <button class="search-submit"
                                        style="width:100px;cursor:default">.MYSCH.ID</button><?php }
                                            else { ?><button
                                        class="search-submit"
                                        style="width:100px;cursor:default">.SCH.ID</button><?php }
                                            ?>
                                </div>
                                <button type="submit" name="submit" class="ht-btn ht-btn-md btn--secondary">CEK
                                    DOMAIN</button>
                            </form>
                            <?php
                                    if (empty($_POST['cek'])) {  }
                                    elseif ($_POST['cek']=='domain')  {
                                        $subdomain=strip_tags($_POST['domain']);	$subdomain=strtolower($subdomain);
                						$subdomain=str_replace('"','',$subdomain); $subdomain=str_replace("'","",$subdomain); $subdomain=str_replace(" ","",$subdomain);
                						$subdomain=str_replace("#","","$subdomain"); $subdomain=str_replace("~","","$subdomain"); $subdomain=str_replace("`","","$subdomain");
                						$subdomain=str_replace("!","","$subdomain"); $subdomain=str_replace("@","","$subdomain"); $subdomain=str_replace("#","","$subdomain");
                						$subdomain=str_replace("$","","$subdomain"); $subdomain=str_replace("%","","$subdomain"); $subdomain=str_replace("^","","$subdomain");
                						$subdomain=str_replace("&","","$subdomain"); $subdomain=str_replace("*","","$subdomain"); $subdomain=str_replace("(","","$subdomain");
                						$subdomain=str_replace(")","","$subdomain"); $subdomain=str_replace("_","","$subdomain"); $subdomain=str_replace("+","","$subdomain");
                						$subdomain=str_replace("=","","$subdomain"); $subdomain=str_replace("|","","$subdomain"); $subdomain=str_replace("{","","$subdomain");
                						$subdomain=str_replace("}","","$subdomain"); $subdomain=str_replace("[","","$subdomain"); $subdomain=str_replace("]","","$subdomain");
                						$subdomain=str_replace(":","","$subdomain"); $subdomain=str_replace(";","","$subdomain"); $subdomain=str_replace("'","","$subdomain");
                						$subdomain=str_replace(">","","$subdomain"); $subdomain=str_replace("<","","$subdomain"); $subdomain=str_replace("?","","$subdomain");
                						$subdomain=str_replace(",","","$subdomain"); $subdomain=str_replace(".","","$subdomain"); $subdomain=str_replace("/","","$subdomain");
                						$panjangdomain=strlen($subdomain);
                						if ($panjangdomain<5) {
                						    $respon='warning';
                						    $domainresmi=$subdomain.'.mysch.id';
                						}
                						else {
                    						if ($paket=='free') {
                    						    $qceksub=mysqli_query($model->koneksi2, "SELECT no FROM setting WHERE subdomain='$subdomain'");
                        						$jumceksub=mysqli_num_rows($qceksub);
                        						$domainresmi=$subdomain.'.mysch.id';
                        						//$jumceksub=0;
                        						if ($jumceksub==0) {
                    							    $respon='success';
                    							}
                    							else {
                    							    $respon='failed';
                    							}
                    						}
                    						else {
                        						$domainresmi=$subdomain.'.sch.id';
                        						$arrHost=@gethostbynamel($domainresmi);
                    							if (empty($arrHost)) {
                    							    $respon='success';
                    							}
                    							else {
                    							    $respon='failed';
                    							}
                    						}
                						}


                						if ($respon=='warning') {
                						    ?>
                            <div class="ht-message-box style-error mt-30" role="alert">
                                <h6 color="#FF0000">Maaf, <?php echo $domainresmi; ?> Terlalu Pendek. Minimal 5
                                    Karakter.<br />Silahkan coba nama domain yang lain.</h6>
                            </div>
                            <?php
                						}
                						elseif ($respon=='failed') {
                						    ?>
                            <div class="ht-message-box style-error mt-30" role="alert">
                                <h6 color="#FF0000">Maaf, <?php echo $domainresmi; ?> Tidak Dapat Digunakan.<br />Silahkan coba
                                    nama domain yang lain.</h6>
                            </div>
                            <?php
                						}
                						elseif ($respon=='success') {
                						    ?>
                            <div class="ht-message-box style-success mt-30" role="alert">
                                <h6 color="#FF0000">Selamat, <?php echo $domainresmi; ?> Dapat Dipesan</h6>
                                <form action="<?php echo URL_DOMAIN . $menu . '/' . $link . '/1/form'; ?>" method="post">
                                    <input name='subdomain' value='<?php echo $subdomain; ?>' hidden>
                                    <button type="submit" name="submit" class="ht-btn ht-btn-md mt-10">LANJUTKAN <span
                                            class="btn-icon"><i class="far fa-arrow-right"></i></span></button>
                            </div>
                            <?php
                						}


                                    }
                                    ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                }

            }
            elseif ($act=='form') {

                if (empty($_POST['subdomain'])) {
                    echo 'Balikin Ke Depan';
                }
                else {
                    $_SESSION['subdomain'] = strip_tags($_POST['subdomain']);
                    ?>
            <div class="contact-us-section-wrappaer section-space--pt_70 section-space--pb_70 bg-white">
                <div class="container text-center">
                    <div class="pricing-table-title-area position-relative">
                        <div class="container">
                            <div class="section-title-wrapper text-center section-space--mb_60 wow move-up">
                                <h3 class="heading">Step 3 : Isi Data Website</h3>
                            </div>
                        </div>
                    </div>
                    <form action="<?php echo URL_DOMAIN . $menu . '/' . $link . '/1/theme'; ?>" method="post">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="contact-form-wrap">
                                    <h5 class="mb-10 text-left">Data Sekolah</h5>
                                    <div class="contact-form">
                                        <div class="contact-inner">
                                            <select name="school_level" required>
                                                <option value="">- Pilih Tingkat-</option>
                                                <option value="sma">SMA</option>
                                                <option value="smk">SMK</option>
                                                <option value="smp">SMP</option>
                                                <option value="sd">SD</option>
                                                <option value="tk">TK</option>
                                                <option value="paud">PAUD</option>
                                            </select>
                                        </div>
                                        <div class="contact-inner">
                                            <input name="school_name" type="text" placeholder="Nama Sekolah" required>
                                        </div>
                                        <div class="contact-inner">
                                            <input name="school_address" type="text" placeholder="Alamat Sekolah"
                                                required>
                                        </div>
                                        <div class="contact-inner">
                                            <input name="school_city" type="text" placeholder="Kabupaten / Kota"
                                                required>
                                        </div>
                                        <div class="contact-inner">
                                            <input name="school_postcode" type="text" placeholder="Kodepos" required>
                                        </div>
                                        <div class="contact-inner">
                                            <input name="school_phone" type="text" placeholder="No. Telepon Sekolah"
                                                required>
                                        </div>
                                        <div class="contact-inner">
                                            <input name="school_email" type="email" placeholder="Email Sekolah"
                                                required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="contact-form-wrap">
                                    <h5 class="mb-10  text-left">Data Kepala Sekolah</h5>
                                    <div class="contact-form">
                                        <div class="contact-inner">
                                            <input name="kepsek_name" type="text" placeholder="Nama Kepala Sekolah"
                                                required>
                                        </div>
                                        <div class="contact-inner">
                                            <input name="kepsek_phone" type="text" placeholder="No. Handphone" required>
                                        </div>
                                    </div>
                                    <h5 class="mb-10  text-left">Data Admin / Operator</h5>
                                    <div class="contact-form ">
                                        <div class="contact-inner">
                                            <input name="admin_name" type="text" placeholder="Nama Admin / Operator"
                                                required>
                                        </div>
                                        <div class="contact-inner">
                                            <input name="admin_phone" type="text"
                                                placeholder="Gunakan 62 Contoh : 62815********" required>
                                            <small class="text-danger">(*)Aktivasi akan dikirimkan via wa ke nomor
                                                ini</small>
                                        </div>
                                        <div class="contact-inner">
                                            <input name="admin_email" type="email" placeholder="Email" required>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <button type="submit" name="submit" class="ht-btn ht-btn-lg" />LANJUTKAN <span
                            class="btn-icon"><i class="far fa-arrow-right"></i></span></button>
                    </form>
                </div>
            </div>
            <?php
                }

            }
            elseif ($act=='theme') {

                if (empty($_POST['school_level'])) {
                    echo 'Balikin Ke Depan';
                }
                else {
                    $school_level=strip_tags($_POST['school_level']); $school_level=str_replace('"','',$school_level); $school_level=str_replace("'","",$school_level); $_SESSION['school_level']=$school_level;
                    $school_name=strip_tags($_POST['school_name']); $school_name=str_replace('"','',$school_name); $school_name=str_replace("'","",$school_name); $_SESSION['school_name']=$school_name;
                    $school_address=strip_tags($_POST['school_address']); $school_address=str_replace('"','',$school_address); $school_address=str_replace("'","",$school_address); $_SESSION['school_address']=$school_address;
                    $school_city=strip_tags($_POST['school_city']); $school_city=str_replace('"','',$school_city); $school_city=str_replace("'","",$school_city); $_SESSION['school_city']=$school_city;
                    $school_postcode=strip_tags($_POST['school_postcode']); $school_postcode=str_replace('"','',$school_postcode); $school_postcode=str_replace("'","",$school_postcode); $_SESSION['school_postcode']=$school_postcode;
                    $school_phone=strip_tags($_POST['school_phone']); $school_phone=str_replace('"','',$school_phone); $school_phone=str_replace("'","",$school_phone); $_SESSION['school_phone']=$school_phone;
                    $school_email=strip_tags($_POST['school_email']); $school_email=str_replace('"','',$school_email); $school_email=str_replace("'","",$school_email); $_SESSION['school_email']=$school_email;
                    $kepsek_name=strip_tags($_POST['kepsek_name']); $kepsek_name=str_replace('"','',$kepsek_name); $kepsek_name=str_replace("'","",$kepsek_name); $_SESSION['kepsek_name']=$kepsek_name;
                    $kepsek_phone=strip_tags($_POST['kepsek_phone']); $kepsek_phone=str_replace('"','',$kepsek_phone); $kepsek_phone=str_replace("'","",$kepsek_phone); $_SESSION['kepsek_phone']=$kepsek_phone;
                    $admin_name=strip_tags($_POST['admin_name']); $admin_name=str_replace('"','',$admin_name); $admin_name=str_replace("'","",$admin_name); $_SESSION['admin_name']=$admin_name;
                    $admin_phone=strip_tags($_POST['admin_phone']); $admin_phone=str_replace('"','',$admin_phone); $admin_phone=str_replace("'","",$admin_phone); $_SESSION['admin_phone']=$admin_phone;
                    $admin_email=strip_tags($_POST['admin_email']); $admin_email=str_replace('"','',$admin_email); $admin_email=str_replace("'","",$admin_email); $_SESSION['admin_email']=$admin_email;
                    ?>
            <div class="feature-images-wrapper bg-gray section-space--ptb_100">
                <div class="container text-center">
                    <div class="pricing-table-title-area position-relative">
                        <div class="container">
                            <div class="section-title-wrapper text-center section-space--mb_60 wow move-up">
                                <h3 class="heading">Step 4 : Pilih Tema Desain Website</h3>
                            </div>
                        </div>
                    </div>
                    <div class="feature-images__four">
                        <div class="row">
                            <div class="single-item col-lg-6 col-md-6 mt-30 wow move-up">
                                <div class=" ht-box-images style-04">
                                    <div class="image-box-wrap">
                                        <form action="<?php echo URL_DOMAIN . $menu . '/' . $link . '/1/finish'; ?>" method="post">
                                            <input name="tema" type="hidden" value="academy" />
                                            <img src="https://www.mysch.id/cms_baru/theme/academy/Academy.jpg"
                                                width="100%" class="mb-10" />
                                            <button type="submit" name="submit" class="ht-btn ht-btn-sm" />Academy<span
                                                class="btn-icon"><i
                                                    class="far fa-arrow-right"></i></span></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="single-item col-lg-6 col-md-6 mt-30 wow move-up">
                                <div class=" ht-box-images style-04">
                                    <div class="image-box-wrap">
                                        <form action="<?php echo URL_DOMAIN . $menu . '/' . $link . '/1/finish'; ?>" method="post">
                                            <input name="tema" type="hidden" value="universh" />
                                            <img src="https://www.mysch.id/cms_baru/theme/universh/Universh.jpg"
                                                width="100%" class="mb-10" />
                                            <button type="submit" name="submit" class="ht-btn ht-btn-sm" />Universh
                                            <span class="btn-icon"><i
                                                    class="far fa-arrow-right"></i></span></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="single-item col-lg-6 col-md-6 mt-30 wow move-up">
                                <div class=" ht-box-images style-04">
                                    <div class="image-box-wrap">
                                        <form action="<?php echo URL_DOMAIN . $menu . '/' . $link . '/1/finish'; ?>" method="post">
                                            <input name="tema" type="hidden" value="varsity" />
                                            <img src="https://www.mysch.id/cms_baru/theme/varsity/Varsity.jpg"
                                                width="100%" class="mb-10" />
                                            <button type="submit" name="submit" class="ht-btn ht-btn-sm" />Varsity
                                            <span class="btn-icon"><i
                                                    class="far fa-arrow-right"></i></span></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="single-item col-lg-6 col-md-6 mt-30 wow move-up">
                                <div class=" ht-box-images style-04">
                                    <div class="image-box-wrap">
                                        <form action="<?php echo URL_DOMAIN . $menu . '/' . $link . '/1/finish'; ?>" method="post">
                                            <input name="tema" type="hidden" value="bike" />
                                            <img src="https://www.mysch.id/cms_baru/theme/bike/Bike.jpg" width="100%"
                                                class="mb-10" />
                                            <button type="submit" name="submit" class="ht-btn ht-btn-sm" />Bike <span
                                                class="btn-icon"><i
                                                    class="far fa-arrow-right"></i></span></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="single-item col-lg-6 col-md-6 mt-30 wow move-up">
                                <div class=" ht-box-images style-04">
                                    <div class="image-box-wrap">
                                        <form action="<?php echo URL_DOMAIN . $menu . '/' . $link . '/1/finish'; ?>" method="post">
                                            <input name="tema" type="hidden" value="educat" />
                                            <img src="https://www.mysch.id/cms_baru/theme/educat/image.jpg" width="100%"
                                                class="mb-10" />
                                            <button type="submit" name="submit" class="ht-btn ht-btn-sm" />Educat <span
                                                class="btn-icon"><i
                                                    class="far fa-arrow-right"></i></span></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="single-item col-lg-6 col-md-6 mt-30 wow move-up">
                                <div class=" ht-box-images style-04">
                                    <div class="image-box-wrap">
                                        <form action="<?php echo URL_DOMAIN . $menu . '/' . $link . '/1/finish'; ?>" method="post">
                                            <input name="tema" type="hidden" value="yoga" />
                                            <img src="https://www.mysch.id/cms_baru/theme/yoga/Yoga.jpg" width="100%"
                                                class="mb-10" />
                                            <button type="submit" name="submit" class="ht-btn ht-btn-sm" />Yoga <span
                                                class="btn-icon"><i
                                                    class="far fa-arrow-right"></i></span></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                    $randkode=rand(111111,999999);
			        $_SESSION['kode']=$randkode;
                }

            }
            elseif ($act=='finish') {

                if (empty($_SESSION['kode'])) {
                    echo 'Balikin Ke Depan';
                }
				elseif (empty($_POST['tema'])) {
				    echo 'Balikin Ke Depan';
				}
                else {
                    if (empty($_SESSION['kode'])) { } else { unset($_SESSION['kode']); }
                    $tema       = strip_tags($_POST['tema']);
                    $paket      = $_SESSION['paket'];
                    $pid        = $_SESSION['pid'];
                    $subdomain  = $_SESSION['subdomain'];
                    $school_level   = $_SESSION['school_level'];
                    $school_name    = $_SESSION['school_name'];
                    $school_address = $_SESSION['school_address'];
                    $school_city    = $_SESSION['school_city'];
                    $school_postcode= $_SESSION['school_postcode'];
                    $school_phone   = $_SESSION['school_phone'];
                    $school_email   = $_SESSION['school_email'];
                    $kepsek_name    = $_SESSION['kepsek_name'];
                    $kepsek_phone   = $_SESSION['kepsek_phone'];
                    $admin_name     = $_SESSION['admin_name']; $ex=explode(' ',$admin_name); $first_name=$ex[0]; $last_name=end($ex);
                    $admin_phone    = $_SESSION['admin_phone'];
                    $admin_email    = $_SESSION['admin_email'];
                    $karakter   = 'ABCDEF1234567890';
					$pword      = '';   for ($i=0; $i<6; $i++) { $pos=rand(0,strlen($karakter)-1); $pword.= $karakter{$pos}; }
					$password   = md5($pword);


                    // SETTING
					mysqli_query($model->koneksi2, "INSERT INTO setting (idsponsor, subdomain, paket, username, judul, deskripsi, kata_kunci, tipe_header, footer, theme, theme_new, kolom, alamat, kota, telepon, email, logo_text, logo_text2, version_web, tgl)
						VALUES ('$idsponsor', '$subdomain', '$paket', '$school_email', '$school_name', 'Website $school_name', '$school_name', 'logo', '2021 - $school_name', '$tema', '$tema', '2', '$school_address', '$school_city', '$school_phone', '$school_email', '$school_name', '$school_city', '1', sysdate())");
					$qsetlast=mysqli_query($model->koneksi2, "SELECT no FROM setting WHERE subdomain='$subdomain' ORDER BY no DESC");
					$dsetlast=mysqli_fetch_array($qsetlast);
					$setting_id = $dsetlast['no'];

					// KEPALA SEKOLAH
					mysqli_query($model->koneksi2, "INSERT INTO kepala_sekolah (subdomain, nama, telepon, tgl) VALUES ('$subdomain', '$kepsek_name', '$kepsek_phone', sysdate())");

					// USER
					mysqli_query($model->koneksi2, "INSERT INTO user(subdomain, nama, username, password, email, telepon, kategori, tgl, publish)
					    VALUES ('$subdomain', '$admin_name', '$admin_email', '$password', '$admin_email', '$admin_phone', 'admin', sysdate(), '1')");

					//MODULE
					//Main
					mysqli_query($model->koneksi2, "INSERT INTO module (subdomain, judul, link, posisi, tipe, tampilkan_judul, hal, urutan, tgl) VALUES ('$subdomain', 'Konten', 'konten', 'main', 'konten', '1', 'semua', '1', sysdate())");
					mysqli_query($model->koneksi2, "INSERT INTO module (subdomain, judul, link, posisi, tipe, tampilkan_judul, hal, urutan, tgl) VALUES ('$subdomain', 'Sambutan Kepala Sekolah', 'sambutan', 'main', 'sambutan', '1', 'home', '2', sysdate())");
					mysqli_query($model->koneksi2, "INSERT INTO module (subdomain, judul, link, posisi, tipe, tampilkan_judul, hal, urutan, tgl) VALUES ('$subdomain', 'Berita', 'berita', 'main', 'berita_terbaru',  '1', 'home', '3', sysdate())");
					mysqli_query($model->koneksi2, "INSERT INTO module (subdomain, judul, link, posisi, tipe, tampilkan_judul, hal, urutan, tgl) VALUES ('$subdomain', 'Galeri', 'galeri', 'main', 'galeri', '1', 'home', '4', sysdate())");
					//Side
					mysqli_query($model->koneksi2, "INSERT INTO module (subdomain, judul, link, posisi, tipe, tampilkan_judul, hal, urutan, tgl) VALUES ('$subdomain', 'Pencarian', 'pencarian', 'side', 'cariform', '1', 'semua', '1', sysdate())");
					mysqli_query($model->koneksi2, "INSERT INTO module (subdomain, judul, link, posisi, tipe, tampilkan_judul, hal, urutan, tgl) VALUES ('$subdomain', 'Kontak', 'kontak', 'side', 'kontak', '1', 'semua', '2', sysdate())");
					mysqli_query($model->koneksi2, "INSERT INTO module (subdomain, judul, link, posisi, tipe, tampilkan_judul, hal, urutan, tgl) VALUES ('$subdomain', 'Berita Terbaru', 'berita-terbaru', 'side', 'berita_terbaru', '1', 'semua', '3', sysdate())");
					mysqli_query($model->koneksi2, "INSERT INTO module (subdomain, judul, link, posisi, tipe, tampilkan_judul, hal, urutan, tgl) VALUES ('$subdomain', 'Banner', 'banner', 'side', 'banner', '1', 'semua', '4', sysdate())");
					mysqli_query($model->koneksi2, "INSERT INTO module (subdomain, judul, link, posisi, tipe, tampilkan_judul, hal, urutan, tgl) VALUES ('$subdomain', 'Kalender', 'kalender', 'side', 'kalender', '1', 'semua', '5', sysdate())");


					if ($paket!='free') {

					    $checkclient = $api_blesta->get("clients", "getList", array('status' => 'active', 'page' => '1', 'filters' => array('contact_email' => $admin_email)));
					    $clientdata = $checkclient->response();
					    $client_id = $clientdata['0']->id;
				        
				        if(!$client_id){
                            // Register New Client To WHMCS
                            $data_client = array(
                                'vars' => array(
                                    'username' => $admin_email,
                                    'new_password' => $pword,
                                    'confirm_password' => $pword,
                                    'client_group_id' => '1',
                                    'status' => 'active',
                                    'first_name' => $first_name,
                                    'last_name' => $last_name,
                                    'email' => $admin_email,
                                    'address1' => $school_address,
                                    'city' => $school_city,
                                    'zip' => $school_postcode,
                                    'country' => 'ID',
                                    'numbers' => array(
                                        'number' => $admin_phone
                                        )
                                    )
                            );
                            
                            $addclient = $api_blesta->post("clients", "create", $data_client);
                            $clientdata=$addclient->response();
                            $client_id = $clientdata->id;
                            
                        }

                        $domain=$subdomain.".sch.id";
    					
    					$datapid = explode(",", strip_tags($pid));
                        $packageid = $datapid['0'];
                        $pricingid = $datapid['1'];

    					$addservice = $api_blesta->post('services', 'add', array('vars' => array('client_id' => $client_id, 'pricing_id' => $pricingid, 'status' => 'pending'), 'packages' => array($packageid), 'notify' => 'false'));
                        $serviceid = $addservice->response();
                        
                        $date = date("Y-m-d");
                        $addinvoice = $api_blesta->post("invoices", "createFromServices", array('client_id' => $client_id, 'service_ids' => array($serviceid), 'currency' => 'IDR', 'due_date' => $date));
                        $invoice_id = $addinvoice->response();
                        
                        $searchinvoice = $api_blesta->get("invoices", "search", array('query' => array('invoice_id' => $invoice_id)));
                        $inv = $searchinvoice->response();
                        $hargaFormated = number_format($inv['0']->total,0,",",".");

                        //UPDATE PESANAN WEB
                        mysqli_query($model->koneksi2,"UPDATE setting SET whmcs='1', pid='$pid', clientid='$client_id' WHERE no='$setting_id'");
                        mysqli_query($model->koneksi,"INSERT INTO pesanan (idsponsor, name, domain, paket, nilai, judul, tema, telepon, email, teleponadmin, emailadmin, tgl)
							VALUES ('$idsponsor','$admin_name', '$domain', '$paket', '$hargaformat', '$school_name', '$tema', '$school_phone', '$school_email', '$admin_phone', '$admin_email', sysdate())");

					    // KIRIM SMS 1
                        $isipesan="MySCH.id: Pesanan Website $domain telah tersimpan di database kami. Segera lakukan pembayaran sebesar Rp. $hargaFormated,-.";
                        $kirim_sms_1 = $sms->setTo($admin_phone)
                            ->setMessageText($isipesan)
                            ->createNusaSMSOptions()
                            ->sendNusaSMS();

					    // KIRIM SMS 2
                        $isipesan2="MySCH.id: Rekening MySCH.id BCA No. 8030112343 a.n. Tri Astuti, MANDIRI No. 1360010201660 a.n. Tri Astuti, BNI No. 0850844796 a.n. Tri Astuti, BRI No. 144701001148505 a.n. Tri Astuti, JENIUS No. 90010668308 a.n. Khoerul Umam";
                        $kirim_sms_2 = $sms->setMessageText($isipesan2)
                            ->setTo($admin_phone)
                            ->createNusaSMSOptions()
                            ->sendNusaSMS();

                        $isipesan3="MySCH.id: Selamat $domain telah aktif. Hal admin : $domain/adm, Username : $admin_email, Pass : $pword";
                        $kirim_sms_3 = $sms->setMessageText($isipesan3)
                            ->setTo($admin_phone)
                            ->AddOption('otp', 'Y')
                            ->createNusaSMSOptions()
                            ->sendNusaSMS();
					}
                    //sampe sini
					//EMAIL
				    $isi='<html>
						<body>
						<label><img src=https://mysch.id/cms/image/logo.png alt=logo></label>
						<br/>
						<br/>
						Salam, '.$school_name.'
						<br/>
						Terima kasih telah melakukan pemesanan website di Mysch.id ,&nbsp;Website&nbsp;'.$school_name.' telah masuk ke database kami
						<br/>
						Berikut ini adalah data-data pendaftaran Anda :<br/>
						<br/>
						<table width="300" cellpadding="0" cellspacing="0" border="0">
							<tr><th colspan="2" align=left>DATA SEKOLAH</th></tr>
							<tr><td align=left width="120">Nama Sekolah</td><td align=left>:&nbsp;'.$school_name.'</td></tr>
							<tr><td align=left>Alamat Sekolah</td><td align=left>:&nbsp;'.$school_address.'</td></tr>
							<tr><td align=left>Kota</td><td align=left>:&nbsp;'.$school_city.'</td></tr>
							<tr><td align=left>Telepon</td><td align=left>:&nbsp;'.$school_phone.'</td></tr>
							<tr><td align=left>Email</td><td align=left>:&nbsp;'.$school_email.'</td>
							<tr><th colspan="2" align=left>DATA ADMIN</th></tr>
							<tr><td align=left>Nama Admin</td><td align=left>:&nbsp;'.$admin_name.'</td>
							<tr><td align=left>No. Handphone Admin</td><td align=left >:&nbsp;'.$admin_phone.'</td></tr>
							<tr><td align=left>Username</td><td align=left >:&nbsp;'.$admin_email.'</td></tr>
							<tr><td align=left>Password</td><td align=left>:&nbsp;'.$pword.'</td></tr>
							</tr>
						</table>
						(Data username dan password berikut juga digunakan untuk login ke halaman CLIENT ZONE sebagai media konsultasi kendala website Anda)
						<br/><br/>';

					if ($paket=='free') {
					    $isi2='Untuk login , klik <a href=http://'.$subdomain.'.mysch.id/adm/>http://'.$subdomain.'.mysch.id/adm</a><br/>
							Dengan Username&nbsp; :&nbsp;'.$admin_email.' &nbsp;&nbsp;Password&nbsp; :&nbsp;'.$pword.'<br/>';
					}
					else {
    					$isi2='Untuk mengaktifkan website Anda, Silahkan Lakukan Pembayaran Sebesar Rp. '. $hargaFormated .',-<br/>
    						Pembayaran Dapat Dilakukan Melalui ATM Transfer, Internet Banking, SMS Banking, Setoran Tunai ke Rekening Dibawah Ini :<br/>
    						- BANK MANDIRI No. Rek. 1360010201660 A.N. Tri Astuti<br/>
    						- BANK BCA No. Rek. 8030112343 A.N. Tri Astuti<br/>
    						- BANK BRI No. Rek. 144701001148505 A.N. Tri Astuti<br/>
    						- BANK BNI No. Rek. 0850844796 A.N. Tri Astuti<br/>
    						Setelah Melakukan Pembayaran, Segera Melakukan Konfirmasi Pada Halaman <b><a href="https://wa.me/6281228237219?text=Hai%20MySCH!%20Saya%20mau%20konfirmasi%20pembayaran" value="Konfirmasi">KLIK DISINI</a></b>
    						<br/><i>*Harga belum termasuk pajak</i>
    						<br/><br/>';
					}

					$isi3='Demikian informasi mengenai pendaftaran Anda.<br/>
						Informasi lebih lanjut mengenai layanan MySCH.id, silahkan menghubungi Customer Service kami.<br/>
						Semoga kesuksesan senantiasa menyertai daya upaya kita bersama.<br/>
						Best Regards,<br/>
						MySCH.id
						</body>
						</html>';

					$mail = new PHPMailer();
					$body = $isi.$isi2.$isi3;
					$mail->isHTML(true);
					$mail->SetFrom('info@mysch.id', 'Mysch.id');
					$mail->Subject= 'mysch';
					$mail->MsgHTML($body);

					$address    = $school_email;
					$address1   = $admin_email;
					$address2   ='myschid@gmail.com';
					if ($address==$address1){
						$mail->AddAddress($address1, 'Admin');
					}
					else {
						$mail->AddAddress($address, 'Klien');
						$mail->AddAddress($address1, 'Admin');
					}
					$mail->AddAddress($address2, 'Sistem');
					$mail->addReplyTo($address2, 'Mysch');

					if(!$mail->Send()) {
					  echo 'Mailer Error: '.$mail->ErrorInfo;
					}

					//kirim sms free
					$isipesan3="MySCH.id: Selamat $subdomain.mysch.id telah aktif. Hal admin : $domain/adm, Username : $admin_email, Pass : $pword";
					mysqli_query($model->koneksi2, "INSERT INTO outbox (no,tujuan, pesan, tgl) VALUES ('$setting_id', '$admin_phone', '$isipesan3', sysdate())");

					$salam="
Salam, $school_name
Terima kasih telah melakukan pemesanan website di Mysch.id. Website $school_name telah masuk ke database kami
Berikut ini adalah data-data pendaftaran Anda :
";

                    $penutup="
Demikian informasi mengenai pendaftaran Anda.
Hubungi kami:
https://wa.me/6281228237219 untuk informasi produk
https://wa.me/6285740000146 untuk konsultasi pengelolaan

Semoga kesuksesan senantiasa menyertai daya upaya kita bersama.
Best Regards,
MySCH.id
";

                    $wafree="
$salam

Lihat website sekolah Anda klik http://$subdomain.mysch.id
Masuk ke halaman admin klik http://$subdomain.mysch.id/adm
Username  : $admin_email
Password  : $pword

$penutup
";

					$wapremium="
$salam

    DATA SEKOLAH
    Nama Sekolah : $subdomain
    Alamat Sekolah : $school_address
    Kota : $school_city
    Telepon : $school_phone
    Email : $school_email

    DATA ADMIN
    Nama Admin : $admin_name
    No. Handphone Admin : $admin_phone
    Username : $admin_email
    Password : $pword

(Data username dan password berikut juga digunakan untuk login ke halaman CLIENT ZONE sebagai media konsultasi kendala website Anda)

Untuk mengaktifkan website Anda, Silahkan Lakukan Pembayaran Sebesar Rp. '. $hargaFormated .',-*
Pembayaran Dapat Dilakukan Melalui ATM Transfer, Internet Banking, SMS Banking, Setoran Tunai ke Rekening Dibawah Ini :
	- BANK MANDIRI No. Rek. 1360010201660 A.N. Tri Astuti
	- BANK BCA No. Rek. 8030112343 A.N. Tri Astuti
	- BANK BRI No. Rek. 144701001148505 A.N. Tri Astuti
	- BANK BNI No. Rek. 0850844796 A.N. Tri Astuti
Setelah Melakukan Pembayaran, Segera Melakukan Konfirmasi dengan mengirimkan bukti transfer melalui email myschid@gmail.com atau whatsapp wa.me/85740000146
_*harga belum termasuk pajak_

$penutup
";
        function tanggal_indo($tanggal, $cetak_hari = false)
        {
        	$hari = array ( 1 =>    'Senin',
        				'Selasa',
        				'Rabu',
        				'Kamis',
        				'Jumat',
        				'Sabtu',
        				'Minggu'
        			);
        			
        	$bulan = array (1 =>   'Januari',
        				'Februari',
        				'Maret',
        				'April',
        				'Mei',
        				'Juni',
        				'Juli',
        				'Agustus',
        				'September',
        				'Oktober',
        				'November',
        				'Desember'
        			);
        	$split 	  = explode('-', $tanggal);
        	$tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
        	
        	if ($cetak_hari) {
        		$num = date('N', strtotime($tanggal));
        		return $hari[$num] . ', ' . $tgl_indo;
        	}
        	return $tgl_indo;
        }

                        // fungsi ke wa_api bikinan dev
				        
				        function wa_notif($url, $body = null){
				            $curl = curl_init();
                            curl_setopt_array($curl, array(
                                CURLOPT_URL => $url,
                                  CURLOPT_RETURNTRANSFER => true,
                                  CURLOPT_ENCODING => "",
                                  CURLOPT_MAXREDIRS => 10,
                                  CURLOPT_TIMEOUT => 0,
                                  CURLOPT_FOLLOWLOCATION => true,
                                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                  CURLOPT_CUSTOMREQUEST => "POST",
                                  CURLOPT_POSTFIELDS => $body
                            ));
    
                            $response = curl_exec($curl);
                            curl_close($curl);
                            return json_decode($response, true);
				        }
				        
                        // Send Notif Whatsapp Pesan gambar biasa
                        
                        $data_array = array(
                            'api_key' => KEY_WA,
                            'sender'  => SENDER_WA,
                            'number' => $admin_phone,
                            'message' => $paket == 'free' ?  $wafree : $wapremium,
                            'url' => IMAGE_WA
                        );
                        $send_notif = wa_notif(URLWA_MEDIA, json_encode($data_array));
                        
                        // Bila milih paket berbayar, kirim invoice custom
                        
                        if($paket != 'free'){
                            
                             // Connect ke DB WHMCS untuk dimasukan ke notif wa perpanjangan
//                             $mysqliDB1 = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE3);
//                             $id_hosting = $inv['items']['item'][0]['relid'];
//                             $hosting = $mysqliDB1->query("SELECT * FROM tblhosting WHERE id = $id_hosting")->fetch_array();
//                             $pesan = 'Salam '.$admin_name.',

// Proforma Invoice '.$inv['invoiceid'].' telah dicetak pada tanggal '.tanggal_indo($hosting['nextinvoicedate'], true).'

// Proforma Invoice: '.$inv['invoiceid'].'
// Nilai Invoice: Rp '.str_replace(',', '.', number_format($hosting['amount'])) .'IDR
// Tanggal Jatuh Tempo: '.tanggal_indo($hosting['nextinvoicedate'], true).' 

// Pembayaran bisa dilakukan via transfer  ke:
// rekening bank sesuai di invoice
// sertakan berita: PINV '.$inv['invoiceid'].'

// Rincian Invoice';
// foreach($inv['items']['item'] as $it){
//     $pesan .= '
    
//     '.$it['description'] .'Rp '.str_replace(',', '.', number_format($it['amount'])).'IDR';
// }
// $pesan .= 'Untuk melihat detil invoice, silahkan akses di https://www.console.mysch.id/viewinvoice.php?id='.$inv['invoiceid'].'

// Konfirmasi Pembayaran dilakukan dengan mengirimkan bukti transfer melalui email myschid@gmail.com atau whatsapp 085740000146
// ';
                            // $arr_jadwal = array(
                            //     'nomor' => $admin_phone,
                            //     'msg' => $pesan,
                            //     'jadwal'=> $hosting['nextinvoicedate'],
                            // );
                            // $jadwal = $hosting['nextinvoicedate'];
                            
                            // mysqli_query($model->koneksi3, "INSERT INTO pesan (sender, nomor, pesan, media, jadwal, make_by) VALUES ('SENDER_WA', '$admin_phone', '$pesan', 'IMAGE_WA', '$jadwal', 1");
                            // $result_jadwal = wa_notif(URLWA_JADWAL, json_encode($arr_jadwal));
                            ob_start();
                            // Membuat template invoice sendiri karena saya nggak bisa ngambil dari whmcs, banyak function yang udah di enkrpsi dan gak bisa dipanggil
                            require DIR_VIEW . 'module/buat/template_invoice.php';
                            $dompdf = new Dompdf();
                            $options = $dompdf->getOptions(); 
                            $options->set(array('isRemoteEnabled' => true));
                            $dompdf->setOptions($options);
                            $dompdf->load_html(ob_get_clean());    
                            $dompdf->render();
                          
                            $pdf = $dompdf->output();
                            $file_location = "/home/mysch123/public_html/cms_web/invoice/invoice_". $invoice_id.".pdf";
                            file_put_contents($file_location,$pdf); 
                            $data_arr = array(
                                'api_key' => KEY_WA,
                                'sender'  => SENDER_WA,
                                'number' => $admin_phone,
                                'url' => 'https://'.$_SERVER['HTTP_HOST'].'/cms_web/invoice/invoice_'. $invoice_id.'.pdf',
                                'filetype'=>'pdf',
                                'filename' => 'invoice-'.$invoice_id
                            );
                            $response = wa_notif(URLWA_MEDIA, json_encode($data_arr));
                            if($response['status'] == true){
                                // Bila sudah terkirim, hapus file invoice agar tidak membebani project
                                unlink($file_location);
                            }
                        }
                        
					?>
            <div class="contact-us-section-wrappaer section-space--pt_70 section-space--pb_70 bg-white">
                <div class="container text-center">
                    <div class="pricing-table-title-area position-relative">
                        <div class="container">
                            <div class="section-title-wrapper text-center section-space--mb_60 wow move-up">
                                <h3 class="heading">Selesai</h3>
                            </div>
                        </div>
                    </div>
                    <div class="text-left">
                        <?php
                                if ($paket=='free') {
                                    ?>
                        <center>
                            <img src="<?php echo URL_IMAGE; ?>success.png" />
                            <h5>Selamat, Website <?php echo $subdomain; ?>.mysch.id Telah Aktif</h5>
                            <h5>Dapatkan bonus menarik berupa gratis input data awal khusus aktivasi website baru minggu
                                ini saja!</h5>
                            Klik <b><a href="http://<?php echo $subdomain; ?>.mysch.id" target="_blank"
                                    title="Detail">DISINI</a></b> untuk melihat tampilan website
                            <?php echo $subdomain; ?>.mysch.id<br>
                            Informasi Lebih Detail, Silahkan Cek Email Anda.<br />
                            <a class="btn btn-primary" href="http://<?php echo $subdomain; ?>.mysch.id" target="_blank"
                                title="LIHAT WEBSITE">LIHAT WEBSITE</a>
                        </center>
                        <?php
                                }
                                else {
                                    ?>
                        <img src="<?php echo URL_IMAGE; ?>success.png" />
                        <h5>Pesanan Website Sekolah <?php echo $domain; ?> Telah Dikirim</h5>
                        <h5>Dapatkan bonus menarik berupa gratis input data awal khusus aktivasi website baru minggu ini
                            saja!</h5>
                        Silahkan Lakukan Pembayaran Sebesar Rp. <?php echo $hargaFormated; ?>,- untuk mengaktifkan Website
                        <?php echo $domain; ?><br><br>
                        Pembayaran Dapat Dilakukan Melalui ATM Transfer, Internet Banking, SMS Banking, Setoran Tunai ke
                        Rekening Dibawah Ini :<br>
                        - BANK MANDIRI No. Rek. 1360010201660 A.N. Tri Astuti<br>
                        - BANK BCA No. Rek. 8030112343 A.N. Tri Astuti<br>
                        - BANK BRI No. Rek. 144701001148505 A.N. Tri Astuti<br>
                        - BANK BNI No. Rek. 0850844796 A.N. Tri Astuti<br>
                        - BANK BNI No. Rek. 0905698564 A.N. PT. Bumi Tekno Indonesia<br><br>
                        Setelah Melakukan Pembayaran, Segera Melakukan Konfirmasi Pada Halaman <b><a
                                href="https://wa.me/6281228237219?text=Hai%20MySCH!%20Saya%20mau%20konfirmasi%20pembayaran/" value="Pembayaran">KLIK DISINI</a></b>
                                <br>
                                <i>*harga belum termasuk pajak</i>
                                <br><br>
                        Untuk sementara, Anda dapat melihat tampilan demo website Anda pada URL <b><a
                                class="btn btn-primary" href="http://<?php echo $subdomain . '.mysch.id'; ?>"
                                target="_blank"><?php echo $subdomain; ?>.mysch.id</a></b><br><br>
                        Informasi Lebih Detail, Silahkan Cek Email Anda.<br /> Anda akan menerima 3 email terdiri dari
                        email order confirmation, email konfirmasi pembuatan website, customer invoice jika email tidak
                        masuk inbox silahkan cek di folder spam atau promosi
                        <?php
                                }
                                ?>
                    </div>
                </div>
            </div>
            <?php
                }

            }
            ?>


        </div>
    </div>
    <?php
    include DIR_THEME . THEME . '/app/footer/footer.php';
    include DIR_THEME . THEME . '/app/asset/asset-body.php';
    ?>
</body>

</html>
