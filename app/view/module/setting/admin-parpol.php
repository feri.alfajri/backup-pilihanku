<!DOCTYPE html> 
<html lang="en">
    
<head>

    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
	
</head>

<body>
    
	<div id="wrapper"> 
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-admin.php'; ?>
		
		<div id="content-wrapper"> 
		    
			<div id="content">
			
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
				    
					<?php					
					$uname = $_SESSION['uname'];
					$table_name = 'setting';							
					$update_type = '';
					$update_label = 'Partai,DPR Tingkat,Daerah Pemilihan';
					$update_form = 'no_partai,tingkat,dapil';
					$update_condition = "WHERE subdomain='".SUBDOMAIN."'";							
					
					$admin->title('Ubah Partai Politik');
					$admin->update($table_name,$update_type,$update_label,$update_form,$update_condition);						
					?>						
                    
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>

</html>