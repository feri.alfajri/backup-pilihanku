<!DOCTYPE html>
<html dir="ltr" lang="id-ID">
    
<head><?php
    include (DIR_THEME.THEME.'/app/asset-head.php');
    include (DIR_THEME.THEME.'/app/metatag.php'); 
    include (DIR_THEME.THEME.'/app/opengraph.php');
    include (DIR_THEME.THEME.'/app/favicon.php');
    include (DIR_THEME.THEME.'/app/title.php');
    ?>
</head>

<body>
	<?php
	include (DIR_THEME.THEME.'/app/loader.php');
	include (DIR_THEME.THEME.'/app/header.php');
	include (DIR_THEME.THEME.'/app/pagetitle.php');
	?>
	<div id="main-wrapper">
        <div class="site-wrapper-reveal">
		
			<div class="blog-pages-wrapper section-space--ptb_60">
                <div class="container">	
                    <div class="row">
                                   
                        <div class="col-lg-6 order-lg-2 order-2 mx-auto">
                            <div class="card card-body shadow-sm mb-4">
                                <?php
                                if (isset($_POST['submit']))
                                {
                                    $namafile       = $_FILES["file"]["name"];
                                    $filegambar     = substr($namafile, 0, strripos($namafile, '.'));
                                    $ekstensifile   = substr($namafile, strripos($namafile, '.'));
                                    $ukuranfile     = $_FILES["file"]["size"];
                                    $jenisfile      = array('.jpg','.jpeg','.png','.bmp', '.JPG','.JPEG','.PNG','.BMP');

                                    if (!empty($filegambar))
                                    {
                                        if ($ukuranfile <= 500000)
                                        {
                                            if (in_array($ekstensifile, $jenisfile) && ($ukuranfile <= 500000))
                                            {
                                                $namabaru = time().'_'.uniqid().'_'.date("Ymdhis").'_n'.$ekstensifile;
                                                if (file_exists( DIR_FLYER_UPLOAD . $namabaru))
                                                {
                                                    echo '<script>';
                                                    echo 'swal("Error!", "Terjadi kesalahan, silahkan coba lagi", "error");';
                                                    echo '</script>';
                                                }
                                                else
                                                {       
                                                    //move_uploaded_file($_FILES["file"]["tmp_name"], DIR_FLYER_UPLOAD . $namabaru);
                                                    copy ($_FILES["file"]["tmp_name"], DIR_FLYER_UPLOAD . $namabaru);
                                                    $gambar = URL_FLYER_UPLOAD . $namabaru;

                                                    echo '<img src="'.$gambar.'" id="img1" width="100px" height="100px" hidden="true" class="img-fluid">';
                                                    echo '<img src="'. $_POST['twibbon'].'" id="img2" width="500px" hidden="true" height="500px" hidden="true" class="img-fluid">';
                                                    echo '<h2><canvas id="canvas" class="img-fluid border"></canvas></h2>';
                                                    echo '<a id="download" class="btn btn-primary mb-3 text-white">Download gambar</a>';
                                                }
                                            }
                                            else
                                            {
                                                echo '<script>';
                                                echo 'swal("Error!", "File yang diupload harus gambar", "error");';
                                                echo '</script>';
                                                unlink($_FILES["file"]["tmp_name"]);
                                            } 
                                        }
                                        else
                                        {
                                            echo '<script>';
                                            echo 'swal("Error!", "Ukuran file tidak boleh lebih dari 5MB", "error");';
                                            echo '</script>';
                                        }
                                    }
                                    else
                                    {
                                        echo '<script>';
                                        echo 'swal("Error!", "Gambar tidak boleh kosong", "error");';
                                        echo '</script>';
                                    }
                                }else{
                                    echo '
                                    <div class="">
                                        <img src="'. URL_FLYER .'hari-guru-pks.png" id="img2" class="img-fluid w-100" hidden="false">
                                    </div>
                                    <div class="canvas-container"></div>
                                    <a id="download" hidden></a>
                                    ';
                                }
                                ?>
                                
                                <div class="mt-3 mb-2 font-weight-light">
                                    <h5 class="font-weight-light">Tutorial membuat Twibbon</h5>
                                    <ol>
                                        <li>Pilih template twibbon yang Anda inginkan</li>
                                        <li>Upload foto Anda</li>
                                        <li>Atur foto Anda sesuai keiginan Anda</li>
                                        <li>Tambahkan text bila perlu</li>
                                        <li>Download gambar</li>
                                    </ol>
                                </div>


                                <form method="post" enctype="multipart/form-data">
                                    <label class="font-weight-light"><b>Upload foto Anda</b></label>
                                    <input type="file" name="file" id="image" accept="image/*" class="p-1 img-thumbnail btn-block" onchange="preview()">
                                    <input type="text" id="twibbon-template" name='twibbon' hidden>
                                    <div class="mb-2 font-italic">
                                        <small>Tips: gunakan ukuran foto 1:1 untuk hasil terbaik</small>
                                    </div>
                                    <button type="submit" name="submit" class="btn btn-primary btn-sm">
                                        Buat Twibbon!
                                    </button>
                                </form>

                            </div>
                        </div>
						
						<div class="col-lg-6 order-lg-1 order-1 mb-4"> 
                            <h5 class="mt-20 mb-10"><?= $titleweb ?></h5>
                            <div class="mb-3">
                                <?php
                                    // $full_url = $_SERVER['REQUEST_URI'];
                                    // $x = pathinfo($full_url);
                                    
                                    
                                    $url_components = parse_url($_SERVER["REQUEST_URI"]);
 
                                    // Use parse_str() function to parse the
                                    // string passed via URL

                                    if(isset($url_components['query'])){
                                        parse_str($url_components['query'], $params);

                                        if(isset($params['key'])){
                                            $param = $params['key'];
                                        }else{
                                            $param = '';
                                        }

                                        $key_params = '?key='.$param.'&';

                                    }else{
                                        $key_params = '?key=';
                                    }


                                    foreach($partaiData as $data){ ?>
                                        <span class="badge badge-pill <?= $params['partai'] == $data['no'] ? 'badge-info' : 'badge-light' ?>  py-1 px-3 border">
                                            <a href="<?= URL_DOMAIN.$menu.$key_params ?>partai=<?=$data['no']?>">
                                                <?= $data['name'] ?>
                                            </a>
                                        </span>
                                <?php } ?>
                            </div>

                            <form method='get' action="<?php echo URL_DOMAIN.$menu;?>">
								<div class="widget-search">   
									<input type="text" name='key' placeholder="Cari twibbon" class="border">
									<button type="submit" class="search-submit">
										<span class="search-btn-icon fa fa-search"></span>
									</button>
								</div>
							</form>

                            <div class="mt-3">
                                <div class="row text-center">

                                <?php
                                    if ($count!=0) {
                                        foreach($twibbonData as $data){ ?>
                                            <div class="col-3 select-twibbon" style="cursor:pointer" data-src="<?= URL_PICTURE . $data['picture'] ?>">
                                                <img class="img-responsive w-100" src="<?= URL_PICTURE . $data['picture']?>" alt="">
                                            </div>
                                
                                <?php   }
                                    }else {
                                ?>
                                        <div class="col-md-12 mt-30 mb-30">
                                            <div class="alert alert-warning" role="alert">
                                                Mohon maaf, data yang Anda cari tidak ditemukan.
                                            </div>
                                        </div>
                                <?php 
                                    }
                                ?>
                                </div>
                            </div>
                        </div>						
						
                    </div>
                </div>
            </div>


		</div>
	</div>
	<?php
	include (DIR_THEME.THEME.'/app/footer.php');
	include (DIR_THEME.THEME.'/app/asset-body.php');
	?>



    <script>

        $(document).on('click', '.select-twibbon', function() {
            console.log($(this).data('src'));
            let images = $(this).data('src');
            $('#twibbon-template').val(images);
            $("#img2").attr("src", images);
            $("#img2").removeAttr("hidden");
            $(".canvas-container").attr("hidden","true");
            $("#download").attr("hidden","true");
        });
		
		window.onload = function () {
			var canvas = new fabric.Canvas("canvas");
			canvas.setWidth(500);
			canvas.setHeight(500);

            // ADD FUNCTIONAL OBJECT
            
            // var deleteIcon = "data:image/svg+xml,%3C%3Fxml version='1.0' encoding='utf-8'%3F%3E%3C!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'%3E%3Csvg version='1.1' id='Ebene_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' width='595.275px' height='595.275px' viewBox='200 215 230 470' xml:space='preserve'%3E%3Ccircle style='fill:%23F44336;' cx='299.76' cy='439.067' r='218.516'/%3E%3Cg%3E%3Crect x='267.162' y='307.978' transform='matrix(0.7071 -0.7071 0.7071 0.7071 -222.6202 340.6915)' style='fill:white;' width='65.545' height='262.18'/%3E%3Crect x='266.988' y='308.153' transform='matrix(0.7071 0.7071 -0.7071 0.7071 398.3889 -83.3116)' style='fill:white;' width='65.544' height='262.179'/%3E%3C/g%3E%3C/svg%3E";
            // var img = document.createElement('img');
            // img.src = deleteIcon;

            // fabric.Object.prototype.transparentCorners = false;
            // fabric.Object.prototype.cornerColor = 'blue';
            // fabric.Object.prototype.cornerStyle = 'circle';

            // fabric.Object.prototype.controls.deleteControl = new fabric.Control({
            //     x: 0.5,
            //     y: -0.5,
            //     offsetY: 16,
            //     cursorStyle: 'pointer',
            //     mouseUpHandler: deleteObject,
            //     render: renderIcon,
            //     cornerSize: 24
            // });

            // function deleteObject(eventData, transform) {
            //     var target = transform.target;
            //     var canvas = target.canvas;
            //         canvas.remove(target);
            //     canvas.requestRenderAll();
            // }

            // function renderIcon(ctx, left, top, styleOverride, fabricObject) {
            //     var size = this.cornerSize;
            //     ctx.save();
            //     ctx.translate(left, top);
            //     ctx.rotate(fabric.util.degreesToRadians(fabricObject.angle));
            //     ctx.drawImage(img, -size/2, -size/2, size, size);
            //     ctx.restore();
            // }  

			// GET ID ELEMENT
			var image = document.getElementById('img1');	//gambar
			var image2 = document.getElementById('img2');	//twibbon
            image2.setAttribute("hidden", "true");
            

            //SET GAMBAR USER

			var fabricImage = new fabric.Image(image,
				{
					left: 0,
					top: 0,
                    opacity: 0.7,
					selectable: true,
				}
			);

			fabricImage.scaleToWidth(400, false);
			fabricImage.scaleToHeight(400, false);
			
			var fabricImage2 = new fabric.Image(image2,	//twibbon
				{
					left: 0,
					top: 0,
					selectable: false,
				}
			);


            // SET GAMBAR TWIBBON
			fabricImage2.scaleToWidth(500, false);
			fabricImage2.scaleToHeight(500, false);
			
			canvas.add(fabricImage2);

			canvas.on('object:added', function(e) {
				fabricImage2.bringToFront();
			});

			canvas.add(fabricImage);


            // SET TEXTBOX
            var text = new fabric.Textbox("Add sample text here");

            // Set the properties
            text.set("top", 70);
            text.set("left", 65);
            text.set("fontWeight", "bold");
            canvas.add(text);
            text.bringToFront();



			// DOWNLOAD BUTTON
			document.getElementById("download").addEventListener(
				"click",
				function(e) {
					canvas.discardActiveObject();
					canvas.renderAll();
					this.href = canvas.toDataURL({
					format: "png"
					});

					this.download = "canvas.png";
				},
				false
			);
			
		};

		function downloadCanvas(link, canvasId, filename) {
			link.href = document.getElementById(canvasId).toDataURL();
			link.download = filename;
		}

		// document.getElementById('download').addEventListener('click', function() {
		// 	downloadCanvas(this, 'canvas', 'wfi-twibbon.png');
		// }, false);

	</script>	

    <script src="https://unpkg.com/fabric@5.3.0/dist/fabric.min.js" crossorigin="anonymous"></script>

</body>

</html>