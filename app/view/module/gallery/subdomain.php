<!DOCTYPE html>
<html dir="ltr" lang="id-ID">
    
<head><?php
    include (DIR_THEME.THEME.'/app/asset-head-sub.php');
    include (DIR_THEME.THEME.'/app/metatag.php'); 
    include (DIR_THEME.THEME.'/app/opengraph.php');
    include (DIR_THEME.THEME.'/app/favicon.php');
    include (DIR_THEME.THEME.'/app/title.php');
    ?>
</head>

<body>
	<?php
	include (DIR_THEME.THEME.'/app/loader.php');
	include (DIR_THEME.THEME.'/app/header-top.php');
	include (DIR_THEME.THEME.'/app/header-sub.php');
	?>
	<div id="main-wrapper">
        <div class="site-wrapper-reveal">
			<div class="projects-wrapper section-space--ptb_60 projects-masonary-wrapper">
                <div class="container">					
					<?php
					if ($count == 0) {		
						?>
						<div class="alert alert-warning" role="alert">
							Mohon maaf, saat ini tidak ada foto yang dapat ditampilkan.
						</div>
						<?php
					}
					else {
						?>
						<h3 class="mb-15">Galeri Foto</h3>
						<div class="projects-case-wrap">
							<div class="row mesonry-list">
								<?php
								foreach($galleryData as $data){
									$galleryNo = $data['no'];
									$galleryName = $data['name'];
									$galleryLink = $data['link'];
									$galleryPicture = $data['picture'];
									?>
									<div class="col-lg-4 col-md-6">
										<a href="<?php echo URL_PICTURE.$galleryPicture;?>" target="blank" class="projects-wrap style-2">
											<div class="projects-image-box">
												<div class="projects-image">
													<img class="img-fluid" alt="<?php echo $galleryLink;?>" src="<?php echo URL_UPLOAD;?>image.php/<?php echo $galleryPicture;?>?width=480&cropratio=16:9&nocache&quality=100&image=<?php echo URL_PICTURE.$galleryPicture;?>" width="100%" alt="<?php echo $blogJudul;?>">
												</div>
												<div class="content">
													<h6 class="heading"><?php echo $galleryName;?></h6>
												</div>
											</div>
										</a>
									</div>
									<?php
								}
								?>
							</div>
						</div>
						<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
	<?php
	include (DIR_THEME.THEME.'/app/footer-sub.php');
	include (DIR_THEME.THEME.'/app/asset-body-sub.php');
	?>
</body>

</html>