<!DOCTYPE html>
<html lang="en"> 
     
<head>
    <?php include DIR_THEME.THEME_MEMBER.'/app/asset-head.php';?>
</head>

<body>
   
	<div id="wrapper">
	    
		<?php include DIR_THEME.THEME_MEMBER.'/app/sidebar-member.php'; ?>  
		
		<div id="content-wrapper">
		    
			<div id="content"> 
				<?php include DIR_THEME.THEME_MEMBER.'/app/topbar.php'; ?>
				
				<div class="container-fluid">
					
					<?php
					$uname = $_SESSION['uname'];
					
					if ($no=='') {
						
						$query = mysqli_query($model->koneksi,"SELECT *,DATE_FORMAT(date,'%d %M %Y, Pukul %H:%i') as date FROM notification WHERE username='$uname' AND publish='1' ORDER BY date DESC");
						$jumlah = mysqli_num_rows($query);
						if ($jumlah==0) { 
						    ?>
						    <h2 class="mb-4">Nofitikasi</h2>
					        <div class="alert alert-warning" role="alert">
								Maaf, Saat ini tidak ada notifikasi.
							</div>
							<?php
						}
						else {
						    ?>
						    <h2 class="mb-4">Nofitikasi</h2>
						    <?php
    						while ($data = mysqli_fetch_array($query)) { 
    							$notificationNo = $data['no'];
    							$notificationName = $data['name'];
    							$notificationDate = $data['date'];
    							$statusURL = $data['status']; if ($statusURL=='unread') { $notificationBackground = 'alert-warning'; } else { $notificationBackground = ''; }
    							$notificationPicture = $data['picture']; if ($notificationPicture=='') { $notificationPicture = 'notification.png';}
    							?>
								<div class="mb-3 card <?php echo $notificationBackground;?>">
								    <div class="card-body">
    								    <a href="<?php echo URL_DOMAIN.$menu.'/'.$link.'/'.$notificationNo.'/detail';?>" title="<?php echo $notificationName;?>">
    										<img class="mr-2 mt-1" src="<?php echo URL_UPLOAD;?>image.php/<?php echo $notificationPicture;?>?width=48&cropratio=1:1&nocache&quality=100&image=<?php echo URL_PICTURE.$notificationPicture;?>" alt="notif" align="left"/>
    										<h4><?php echo $notificationName;?></h4>
    									</a>
    									<small><?php echo $notificationDate;?></small>
									</div>
								</div>
    							<?php
    						} 
						}
					
					}
					else {
					    
					    $query = mysqli_query($model->koneksi,"SELECT *,DATE_FORMAT(date,'%d %M %Y, Pukul %H:%i') as date FROM notification WHERE username='$uname' AND no='$no' LIMIT 1");
					    $jumlah = mysqli_num_rows($query);
						if ($jumlah == 0) {
							?>
							<h2 class="mb-4">Nofitikasi</h2>
							<div class="alert alert-danger" role="alert">
								Maaf, Data yang Anda cari tidak tersedia.
							</div>
							<?php
						}
						else {
							$data = mysqli_fetch_array($query);
							$notificationNo = $data['no'];
							$notificationName = $data['name'];
							$notificationContent = $data['content'];
							$notificationDate = $data['date'];
							$notificationPicture = $data['picture']; 
							mysqli_query($model->koneksi,"UPDATE notification SET status = 'read' WHERE username='$uname' AND no='$no' LIMIT 1");
							?>
							<h2 class="mb-4"><?php echo $notificationName;?></h2>
							<div class="card mb-3">
								<div class="card-body">
								    <small class="text-right text-grey">Ditulis tanggal <?php echo $notificationDate;?></small>
								    <?php
								    if ($notificationPicture!='') { ?><img src="<?php echo URL_PICTURE.$notificationPicture;?>" alt="foto" class="mb-4"/><?php }
								    ?>
								    <p><?php echo $notificationContent;?></p>
								</div>
							</div>
							<a href="<?php echo URL_DOMAIN.$menu.'/'.$link;?>" class="btn btn-primary mt-1" title="Kembali">Kembali</a>
							<?php
						} 
						
					}
					?>
						
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php include DIR_THEME.THEME_MEMBER.'/app/asset-body.php'; ?>
	
</body>
 
</html>