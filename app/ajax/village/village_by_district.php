<?php

    include DIR_MODEL. 'village.php';

    $modelVillage = new village;
    $dataVillage = $modelVillage->getByDistrict($_POST['id']);
    
    header("Content-type:text/html; charset=UTF-8");
    header("Content-type:application/json");
    echo json_encode($dataVillage, JSON_PRETTY_PRINT);


?>