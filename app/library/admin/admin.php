<?php  
class admin extends model {  	
	
	function title ($menu_title) {
		echo '<h3>'.$menu_title.'</h3>';
	}
	
	
	function menu ($menu_label,$menu_action) {
		$menu=$this->menu;
		$link=$this->link;		
		$linkmod=URL_DOMAIN.$menu.'/'.$link.'/0/';
		echo '<a href="'.$linkmod.$menu_action.'" class="btn btn-primary btn-sm mb-2 text-white" title="'.$menu_label.'">'.$menu_label.'</a>';
	}
	
	function button ($button_type,$button_label,$button_url,$button_style) {		
		if ($button_type=='back') { $button_onclick='self.history.back()'; } else { $button_onclick=''; } 
		?>
		<input type="button" value="<?php echo $button_label;?>" onclick="<?php echo $button_onclick;?>" class="btn <?php echo $button_style;?>" style="margin:8px 0px;"/>
		<?php
	}
		
	function table ($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition) {
		$menu=$this->menu;
		$link=$this->link;		
		$linkmod=URL_DOMAIN.$menu.'/'.$link;
		$lab=explode(",",$table_label);
		$col=explode(",",$table_column);
		$wid=explode(",",$table_width);
		$jumcolom=count($col);
		$qjum=mysqli_query($this->koneksi,"SELECT * FROM $table_name $table_condition");
		$jumlah=mysqli_num_rows($qjum);
		if ($jumlah==0) { 
			$this->notify('empty'); 
		}
		else { 
			?>
			<div class="card mb-4">
				<div class="card-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
							<thead>
								<tr style="text-transform:uppercase;">
									<th width="30" class="text-center">No</th>
									<?php							
									for($i=0;$i<$jumcolom;$i++){
										$align='text-center'; 
										if ($lab[$i]=='nama') { $width=''; $align='text-left';  } //jika label nama maka alight left
										else { $width=$wid[$i];  }								  //menentukan lebar dinamis kolom
										echo '<th width="'.$width.'%" class="'.$align.'">'.$lab[$i].'</th>';
									}
									if ($table_button!='') {
										echo '<th width="100" class="text-center">Action</th>';
									}	
									?>
								</tr>
							</thead>
							<tbody>
							<?php
							$nomor=1;
							
							$query=mysqli_query($this->koneksi,"SELECT $table_column,date,no,publish FROM $table_name $table_condition ORDER BY date DESC");
							while($data=mysqli_fetch_array($query)) {
								$nodata=$data['no'];
								$date=$data['date'];
								?>
								<tr>
									<td class="text-center"><?php echo $nomor;?></td>
									<?php
									for($j=0;$j<$jumcolom;$j++){
										$align='text-center';
										$buttonact=0;
										if ($col[$j]=='title' or $col[$j]=='name' or $col[$j]=='description' or  $col[$j]=='content' or $col[$j]=='no_product') { $align='text-left'; }  ?>
										<td class="<?php echo $align;?>">
											<?php 
											$pisah=explode('_',$col[$j]); 
											$jumpisah=count($pisah);
											if ($jumpisah<=1) {  $namecolom=$col[$j]; } else { list ($namecolom,$tabelcolom)=explode('_',$col[$j]); }									
											
											if ($namecolom=='value' or $namecolom=='old_price' or $namecolom=='price' or $namecolom=='total' or $namecolom=='amount' or $namecolom=='discount') { echo number_format($data[$col[$j]],0,',','.'); }
											elseif ($namecolom=='picture' or $namecolom=='logo' or $namecolom=='thumbnail' ) { 
												if ($data[$col[$j]]!='') { ?><img src="<?php echo URL_UPLOAD;?>image.php/<?php echo $data[$col[$j]];?>?width=64&cropratio=1:1&nocache&quality=100&image=<?php echo URL_PICTURE.$data[$col[$j]];?>"/><?php } 
											}
											elseif ($namecolom=='no') { 
											    $tabelcolom =  substr($col[$j],3);
												$nocolom=$data[$col[$j]];
												$qcolom=mysqli_query($this->koneksi,"SELECT * FROM $tabelcolom WHERE id='$nocolom'  LIMIT 1");
												$dcolom=mysqli_fetch_array($qcolom); 
												$jcolom=mysqli_num_rows($qcolom); 
												if ($jcolom!=0) { echo $dcolom['name']; }
											}
											elseif ($namecolom=='content') { echo substr($data['content'],0,200);  }
											else { echo $data[$col[$j]]; } 
											?>
										</td>								
										<?php
									}
									if ($table_button!='') {
										echo '<td class="text-center">';
											$tomact=explode(',',$table_button); $jumtomact=count($tomact);
											for($k=0;$k<$jumtomact;$k++){ 
												$linkaction=$linkmod."/".$nodata."/".$tomact[$k];
												if ($tomact[$k]=='hapus' or $tomact[$k]=='delete') { $icon='fa-trash-alt'; $color='btn-dark'; $onclick="onclick=\"return confirm('Apakah Anda yakin akan menghapus data ini ?');\""; }
												elseif ($tomact[$k]=='ubah' or $tomact[$k]=='update') { $icon='fa-edit'; $color='btn-dark'; $onclick=''; }
												elseif ($tomact[$k]=='detail' or $tomact[$k]=='read') { $icon='fa-eye'; $color='btn-dark'; $onclick=''; }
												elseif ($tomact[$k]=='komisi' or $tomact[$k]=='bank') { $icon='fa-dollar-sign'; $color='btn-dark'; $onclick=''; }
												elseif ($tomact[$k]=='account') { $icon='fa-user'; $color='btn-dark'; $onclick=''; }
												elseif ($tomact[$k]=='program') { $icon='fa-shopping-cart'; $color='btn-dark'; $onclick=''; }
												elseif ($tomact[$k]=='pendukung') { $icon='fa-users'; $color='btn-dark'; $onclick=''; }
												else { $icon='fa-edit'; $color='btn-dark'; $onclick=''; }
												echo '<a href="'.$linkaction.'" class="'.$color.' mr-2" style="padding:5px 7px"><i class="fa '.$icon.' '.$color.'" '.$onclick.'></i></a>';
											}
										echo '</td>';
									}							
									?>
								</tr>
								<?php
								$nomor++;
							} 
							?>
							</tbody>
						</table>				
					</div>
				</div>
			</div>
			<?php 
		}
	}	
	
	
	function tablepro ($table_name,$table_label,$table_column,$table_width,$table_button,$table_condition) {
		$menu=$this->menu;
		$link=$this->link;		
		$linkmod=URL_DOMAIN.$menu.'/'.$link;
		$lab=explode(",",$table_label);
		$col=explode(",",$table_column);
		$wid=explode(",",$table_width);
		$jumcolom=count($col);
		$qjum=mysqli_query($this->koneksi,"SELECT * FROM $table_name $table_condition");
		$jumlah=mysqli_num_rows($qjum);
		if ($jumlah==0) { 
			$this->notify('empty'); 
		}
		else { 
			?>
			<div class="card mb-4">
				<div class="card-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="datatablesSimple" width="100%" cellspacing="0">
							<thead>
								<tr style="text-transform:uppercase;">
									<th width="30" class="text-center">No</th>
									<?php							
									for($i=0;$i<$jumcolom;$i++){
										$align='text-center'; 
										if ($lab[$i]=='nama') { $width=''; $align='text-left';  } 
										else { $width=$wid[$i];  }
										echo '<th width="'.$width.'%" class="'.$align.'">'.$lab[$i].'</th>';
									}
									if ($table_button!='') {
										echo '<th width="120" class="text-center">Action</th>';
									}	
									?>
								</tr>
							</thead>
							<tbody>
							<?php
							$nomor=1;
							
							$query=mysqli_query($this->koneksi,"SELECT $table_column,date,no,publish FROM $table_name $table_condition ORDER BY date DESC");
							while($data=mysqli_fetch_array($query)) {
								$nodata=$data['no'];
								$date=$data['date'];
								?>
								<tr>
									<td class="text-center"><?php echo $nomor;?></td>
									<?php
									for($j=0;$j<$jumcolom;$j++){
										$align='text-center';
										$buttonact=0;
										if ($col[$j]=='title' or $col[$j]=='name' or $col[$j]=='description' or  $col[$j]=='content') { $align='text-left'; }  ?>
										<td class="<?php echo $align;?>">
											<?php 
											$pisah=explode('_',$col[$j]); 
											$jumpisah=count($pisah);
											if ($jumpisah<=1) {  $namecolom=$col[$j]; } else { list ($namecolom,$tabelcolom)=explode('_',$col[$j]); }									
											
											if ($namecolom=='value' or $namecolom=='old_price' or $namecolom=='price' or $namecolom=='total' or $namecolom=='amount' or $namecolom=='discount') { echo number_format($data[$col[$j]],0,',','.'); }
											elseif ($namecolom=='picture' or $namecolom=='logo' or $namecolom=='thumbnail' ) { 
												if ($data[$col[$j]]!='') { ?><img src="<?php echo URL_UPLOAD;?>image.php/<?php echo $data[$col[$j]];?>?width=64&cropratio=1:1&nocache&quality=100&image=<?php echo URL_PICTURE.$data[$col[$j]];?>"/><?php } 
											}
											elseif ($namecolom=='no') { 
												$nocolom=$data[$col[$j]];
												$qcolom=mysqli_query($this->koneksi,"SELECT * FROM $tabelcolom WHERE no='$nocolom'  LIMIT 1");
												$dcolom=mysqli_fetch_array($qcolom); 
												$jcolom=mysqli_num_rows($qcolom); 
												if ($jcolom!=0) { echo $dcolom['name']; }
											}
											elseif ($namecolom=='content') { echo substr($data['content'],0,200);  }
											elseif($namecolom=='bulan'){
												$this->bulan($data[$col[$j]]);
												echo $this->bulannama;
											}
											else { echo $data[$col[$j]]; } 
											?>
										</td>								
										<?php
									}
									if ($table_button!='') {
										echo '<td class="text-center">';
											$tomact=explode(',',$table_button); $jumtomact=count($tomact);
											for($k=0;$k<$jumtomact;$k++){ 
												$linkaction=$linkmod."/".$nodata."/".$tomact[$k];
												if ($tomact[$k]=='hapus' or $tomact[$k]=='delete') { $icon='fa-trash-alt'; $color='btn-dark'; $onclick="onclick=\"return confirm('Apakah Anda yakin akan menghapus data ini ?');\""; }
												elseif ($tomact[$k]=='ubah' or $tomact[$k]=='update') { $icon='fa-edit'; $color='btn-dark'; $onclick=''; }
												elseif ($tomact[$k]=='detail' or $tomact[$k]=='read') { $icon='fa-eye'; $color='btn-dark'; $onclick=''; }
												elseif ($tomact[$k]=='komisi' or $tomact[$k]=='bank') { $icon='fa-dollar-sign'; $color='btn-dark'; $onclick=''; }
												elseif ($tomact[$k]=='account') { $icon='fa-user'; $color='btn-dark'; $onclick=''; }
												elseif ($tomact[$k]=='program') { $icon='fa-shopping-cart'; $color='btn-dark'; $onclick=''; }
												elseif ($tomact[$k]=='pendukung') { $icon='fa-users'; $color='btn-dark'; $onclick=''; }
												else { $icon='fa-edit'; $color='btn-dark'; $onclick=''; }
												echo '<a href="'.$linkaction.'" class="'.$color.' mr-2" style="padding:5px 7px"><i class="fa '.$icon.' '.$color.'" '.$onclick.'></i></a>';
											}
										echo '</td>';
									}							
									?>
								</tr>
								<?php
								$nomor++;
							} 
							?>
							</tbody>
						</table>				
					</div>
				</div>
			</div>
			<?php 
		}
	}	
	
	
	function create ($table_name,$create_type,$create_label,$create_form,$create_condition) { 
		if ($create_condition=='') { } else { list($namekolom,$isikolom)=explode(",",$create_condition); } 
		if (empty ($_POST['process'])) { 
			?>		
			<div class="card mb-4">
				<div class="card-body">
					<form action="" method="post" enctype="multipart/form-data" name="titleform">
						<input name="process" type="hidden" value="input"/>
						<?php
						$label=explode(",",$create_label);
						$form=explode(",",$create_form);
						$jumkoma=count($form);
						for($i=0;$i<$jumkoma;$i++){ $this->form($label[$i],$form[$i],"input","",""); echo "\n";} 
						?>
						<input type="submit" name="submit" value="SIMPAN" class="btn btn-primary" style="margin:5px 0px;"/>
						<input type="button" name="back" value="KEMBALI" onclick="self.history.back();" class="btn btn-default" style="margin:5px 0px;"/>
					</form>
				</div>
			</div>
			<?php
			$randcode=rand(111111,999999); 
			$_SESSION['code']=$randcode;
		}
		elseif ($_POST['process']=="input") {			
			if (empty ($_SESSION['code'])) { $this->notify("save_ok"); }
			else { 
				if (empty($_SESSION['code'])) { } else { unset($_SESSION['code']); }
				if ($create_type=="") {
					$this->create_action($create_form,"");
					$kolom=$this->actionkolom;
					$contentan=$this->actioncontentan;
					if ($create_condition=="") { mysqli_query($this->koneksi,"INSERT INTO $table_name ($kolom date) VALUES ($contentan sysdate())"); }
					else { mysqli_query($this->koneksi,"INSERT INTO $table_name ($namekolom, $kolom date) VALUES ('$isikolom', $contentan sysdate())"); }
					$this->notify("save_ok");
					
				}
				elseif ($create_type=="picture") {
					if ($_FILES['picture']['tmp_name']=="") { 
						$this->create_action($create_form,"");
						$kolom=$this->actionkolom;
						$contentan=$this->actioncontentan;
						if ($create_condition=="") { mysqli_query($this->koneksi,"INSERT INTO $table_name ($kolom date) VALUES ($contentan sysdate())");  }						
						else { mysqli_query($this->koneksi,"INSERT INTO $table_name ($namekolom, $kolom date) VALUES ('$isikolom', $contentan sysdate())"); }
						$this->notify("save_ok");
					}
					elseif(!empty($_FILES['picture']['tmp_name'])) {
						$picture=$_FILES['picture']['tmp_name'];
						$picture_name=$_FILES['picture']['name'];
						$picture_type=$_FILES['picture']['type'];
						$jumform= explode(".", $picture);
						$jumpict= count($jumform);
						$acak=rand(00000000,99999999);
						$title_baru=$acak.$picture_name;
						$title_baru=str_replace(" ","",$title_baru); $title_baru=str_replace("'","",$title_baru);  $title_baru=str_replace('"','',$title_baru);  $title_baru=str_replace("#","",$title_baru);
						$picture_dimensi=getimagesize($picture);
						if ($picture_type!="image/jpg" and $picture_type!="image/png" and $picture_type!="image/gif" and $picture_type!="image/jpeg"){ $this->notify("img_format"); }
						elseif ($picture_dimensi['0']>"5000" or $picture_dimensi['1']>"5000") { $this->notify("image_dimention"); } 
						else {
							$this->create_action($create_form,$title_baru);
							$kolom=$this->actionkolom;
							$contentan=$this->actioncontentan;
							if ($create_condition=="") { mysqli_query($this->koneksi,"INSERT INTO $table_name ($kolom date) VALUES ($contentan sysdate())");   }						
							else { mysqli_query($this->koneksi,"INSERT INTO $table_name ($namekolom, $kolom date) VALUES ('$isikolom', $contentan sysdate())"); }							
							copy ($picture, DIR_PICTURE.$title_baru);
							$this->notify("save_ok");
						}
					}
				}
				elseif ($create_type=="file") {
					if ($_FILES['file']['tmp_name']=="") { $this->notify("file_empty"); }
					else {
						$file=$_FILES['file']['tmp_name'];
						$file_name=$_FILES['file']['name'];
						$file_size=$_FILES['file']['size'];
						$file_type=$_FILES['file']['type'];
						$acak=rand(00000000,99999999);
						$title_baru=$acak.$file_name;
						$title_baru=str_replace(" ","",$title_baru); $title_baru=str_replace("'","",$title_baru);  $title_baru=str_replace('"','',$title_baru);  $title_baru=str_replace("#","",$title_baru);
						if ($file_size>"100000000") { $this->notify("file_size"); } 
						else {
							$this->create_action($create_form,$title_baru);
							$kolom=$this->actionkolom;
							$contentan=$this->actioncontentan;							
							if ($create_condition=="") { mysqli_query($this->koneksi,"INSERT INTO $table_name ($kolom date) VALUES ($contentan sysdate())");   }						
							else { mysqli_query($this->koneksi,"INSERT INTO $table_name ($namekolom, $kolom date) VALUES ('$isikolom', $contentan sysdate())"); }
							copy ($file, DIR_FILE.$title_baru);
							$this->notify("save_ok");
						}
					}
				}
			}
		}
	}
	
	
	function create_action ($form,$title_baru) {
		$form=explode(",",$form);
		$jumkoma=count($form); 
		$kolom=""; $contentan="";
		for($i=0;$i<$jumkoma;$i++){ 
			$spasi=explode("_",$form[$i]);  
			$jumspasi=count($spasi);
			if ($jumspasi<=1) { $forminput=$form[$i]; } else { list($forminput,$contentform)=explode("_",$form[$i]); }
			if ($forminput=="content") { $kolom=$kolom."".$form[$i].", "; $formcontent=str_replace("'","",$_POST[$form[$i]]);  $formcontent=str_replace('"','',$formcontent); $contentan=$contentan."'".$formcontent."', "; }
			elseif ($forminput=="password") { $kolom=$kolom."".$form[$i].", "; $contentan=$contentan."'".md5($_POST[$form[$i]])."', "; }
			elseif ($forminput=="picture") { $kolom=$kolom."".$form[$i].", "; $contentan=$contentan."'".$title_baru."', "; }
			elseif ($forminput=="file") { $kolom=$kolom."".$form[$i].", "; $contentan=$contentan."'".$title_baru."', "; }
			else { $kolom=$kolom."".$form[$i].", "; $formcontent=str_replace("'","",$_POST[$form[$i]]); $formcontent=str_replace('"','',$formcontent); $contentan=$contentan."'".strip_tags($formcontent)."', "; }
		}
		$this->actionkolom=$kolom;
		$this->actioncontentan=$contentan;
	} 
	
	
	
	function read ($table_name,$read_label,$read_content,$read_condition) {
		$query=mysqli_query($this->koneksi,"SELECT * FROM $table_name $read_condition LIMIT 1");
		$data=mysqli_fetch_array($query);
		$no=$data['no'];
		echo '<div class="card mb-4">';
			echo '<div class="card-body">';
				if ($no=='') { 
					$this->notify('empty'); 
				} 
				else { 
					$label=explode(",",$read_label);
					$content=explode(",",$read_content);
					$jumisi=count($content);
					for($i=0;$i<$jumisi;$i++){				
						echo '<div id="view">';
							echo '<div class="view_label">'.$label[$i].'</div>';
							echo '<div class="view_dot">:</div>';
							echo '<div class="view_content">';
								$pisah=explode('_',$content[$i]); 
								$jumpisah=count($pisah);
								if ($jumpisah<=1) {  $namekolom=$content[$i]; } 
								elseif($jumpisah==2) { list ($namekolom,$tabelkolom)=explode('_',$content[$i]); }	
								else { list ($namekolom,$tabelkolom)=explode('_',$content[$i]); }	

								if ($namekolom=='picture' or $namekolom=='thumbnail' or $namekolom=='logo'  or $namekolom=='banner')  { 
									if ($data[$content[$i]]!='') { 
										echo '<img src="'.URL_PICTURE.$data[$content[$i]].'" style="width:100%; max-width:480px;"/>';	
									}
								}
								elseif ($namekolom=='no') { 
								    $tabelkolom =  substr($content[$i],3);
									$dataisidetail=$data[$content[$i]];
									$qkolom=mysqli_query($this->koneksi,"SELECT * FROM $tabelkolom WHERE id='$dataisidetail' LIMIT 1");
									$jkolom=mysqli_num_rows($qkolom); 
									$dkolom=mysqli_fetch_array($qkolom); 
									if ($jkolom!=0) { echo $dkolom['name']; }
								}
								elseif ($namekolom=='value' or $namekolom=='price' or $namekolom=='total'  or $namekolom=='discount') {
									echo number_format($data[$content[$i]],0,',','.');
								}
								else { echo $data[$content[$i]]; }								
							echo '</div>';
						echo '</div>';							
					} 
				} 
			echo '</div>';
		echo '</div>';
	}
	
	
	function update ($table_name,$update_type,$update_label,$update_form,$update_condition) {
		$query=mysqli_query($this->koneksi,"SELECT * FROM $table_name $update_condition LIMIT 0,1");
		$data=mysqli_fetch_array($query);
		$no=$data['no'];
		if ($no=="") {  $this->notify("empty");  }
		else {
			if (empty ($_POST['process'])) { 
				?>		
				<div class="card mb-4">
					<div class="card-body">			
						<form action="" method="post" enctype="multipart/form-data" name="titleform">
							<input name="process" type="hidden" value="edit">
							<input name="no" type="hidden" value="<?php echo $no;?>"/>
							<?php
							$label=explode(",",$update_label);
							$form=explode(",",$update_form);
							$jumkoma=count($form);
							for($i=0;$i<$jumkoma;$i++){ $this->form($label[$i],$form[$i],"edit",$data[$form[$i]],""); echo "\n"; } 
							?>
							<input type="submit" name="submit" value="SIMPAN" class="btn btn-primary" style="margin:5px 0px;"/>
							<input type="button" name="back" value="KEMBALI" onclick="self.history.back();" class="btn btn-default" style="margin:5px 0px;"/>
						</form>
					</div>
				</div>
				<?php
				$randcode=rand(111111,999999); 
				$_SESSION['code']=$randcode;
			}
			elseif ($_POST['process']=="edit") {
				$no=strip_tags($_POST['no'])/1;
				if (empty ($_SESSION['code'])) {  $this->notify("edit_ok"); }
				else { 
					if (empty($_SESSION['code'])) { } else { unset($_SESSION['code']); }
					if ($update_type=="") {
						$this->update_action($update_form,"");
						$ubah=$this->actionubah;	
						mysqli_query($this->koneksi,"UPDATE $table_name SET $ubah no='$no' $update_condition");
						$this->notify("edit_ok");
					}
					elseif ($update_type=="picture" or $update_type=="logo" or $update_type=="favicon") { 
						if (empty($_FILES[$update_type]['tmp_name'])) { 
							$this->update_action($update_form,$_POST['picturelama']);
							$ubah=$this->actionubah;		
							mysqli_query($this->koneksi,"UPDATE $table_name SET $ubah no='$no' $update_condition");
							$this->notify("edit_ok");
						}
						else { 
							$picture=$_FILES[$update_type]['tmp_name'];
							$picture_name=$_FILES[$update_type]['name'];
							$picture_size=$_FILES[$update_type]['size'];
							$picture_type=$_FILES[$update_type]['type'];
							$acak=rand(00000000,99999999);
							$title_baru=$acak.$picture_name;
							$title_baru=str_replace(" ","",$title_baru);
							$picture_dimensi=getimagesize($picture);
							if ($picture_type!="image/jpg" and $picture_type!="image/png" and $picture_type!="image/gif" and $picture_type!="image/jpeg"){  $this->notify("img_format"); } 
							elseif ($picture_dimensi['0']>"2000" or $picture_dimensi['1']>"2000") {  $this->notify("image_dimention");  } 
							else {
								$this->update_action($update_form,$title_baru);
								$ubah=$this->actionubah;
								mysqli_query($this->koneksi,"UPDATE $table_name SET $ubah no='$no' $update_condition");
								$this->notify("edit_ok");
								copy ($picture, DIR_PICTURE.$title_baru);
							}
						}
					}
					elseif ($update_type=="file") {
						if (empty($_FILES['file']['tmp_name'])) {
							$this->update_action($update_form,$_POST['filelama']);
							$ubah=$this->actionubah;
							mysqli_query($this->koneksi,"UPDATE $table_name SET $ubah no='$no' $update_condition"); 								
							$this->notify("edit_ok");
						}
						else {
							$file=$_FILES['file']['tmp_name'];
							$file_name=$_FILES['file']['name'];
							$file_size=$_FILES['file']['size'];
							$file_type=$_FILES['file']['type'];
							$acak=rand(00000000,99999999);
							$title_baru=$acak.$file_name;
							$title_baru=str_replace(" ","",$title_baru);
							if ($file_size>"100000000") {  $this->notify("image_size");  } 
							else {
								$this->update_action($update_form,$title_baru);
								$ubah=$this->actionubah;
								mysqli_query($this->koneksi,"UPDATE $table_name SET $ubah no='$no' $update_condition"); 									
								$this->notify("edit_ok");
								copy ($file, DIR_FILE.$title_baru);
							}
						}
					}
				}
			}
		}
	}
	
	
	
	function update_action ($form,$title_baru) {
		$form=explode(",",$form);
		$jumkoma=count($form);
		$ubah="";
		for($i=0;$i<$jumkoma;$i++){
			$spasi=explode("_",$form[$i]); 
			$jumspasi=count($spasi);
			if ($jumspasi<=1) { $forminput=$form[$i]; } else { list($forminput,$contentform)=explode("_",$form[$i]); }
			if ($forminput=="content") { $contentan=str_replace("'","",$_POST[$form[$i]]); $contentan=str_replace('"','',$contentan); }
			elseif ($forminput=="meta") { $contentan=str_replace("'","",$_POST[$form[$i]]); }
			elseif ($forminput=="password") { $contentan=md5($_POST[$form[$i]]); }
			elseif ( $forminput=="thumbnail") { $contentan=$title_baru; }
			elseif ($forminput=="picture") { $contentan=$title_baru; }
			elseif ( $forminput=="picture1") { $contentan=$title_baru; }
		    elseif ( $forminput=="picture2") { $contentan=$title_baru; }
		    elseif ( $forminput=="picture3") { $contentan=$title_baru; }
		    elseif ( $forminput=="picture4") { $contentan=$title_baru; }
		    elseif ( $forminput=="picture5") { $contentan=$title_baru; }
			elseif ($forminput=="favicon") { $contentan=$title_baru; }
			elseif ($forminput=="logo") { $contentan=$title_baru; }
			elseif ($forminput=="file") { $contentan=$title_baru; }
			else { $contentan=$_POST[$form[$i]]; $contentan=str_replace("'","",$contentan);   }
			$ubah=$ubah.$form[$i]."='".$contentan."', ";
		}
		$this->actionubah=$ubah;
	}
	
	
	function delete ($table_name,$delete_condition) {
		$no=$this->no;
		if (empty ($no)) { $this->notify('empty'); }
		else {
			$no=$no/1;
			$query=mysqli_query($this->koneksi,"SELECT * FROM $table_name $delete_condition LIMIT 1");
			$data=mysqli_fetch_array($query);
			$no=$data['no'];
			if ($no=='') { $this->notify('empty'); }
			else {
				mysqli_query($this->koneksi,"DELETE FROM $table_name $delete_condition LIMIT 1");
				$this->notify('delete');
			}
		}
	}
	
	
	function delete_file ($table_name,$delete_condition) {
		$no=$this->no;
		if (empty ($no)) { $this->notify('empty'); }
		else {
			$no=$no/1;
			$query=mysqli_query($this->koneksi,"SELECT * FROM $table_name $delete_condition LIMIT 1");
			$data=mysqli_fetch_array($query);
			$no=$data['no'];
			$file=$data['picture'];
			if ($no=='') { $this->notify('empty'); }
			else {
				mysqli_query($this->koneksi,"DELETE FROM $table_name $delete_condition LIMIT 1");
				$path_delete = DIR_PICTURE.$file;
				unlink($path_delete);
				$this->notify('delete');
			}
		}
	}
	
	
	function form($label,$form,$action,$value,$tampilan) {
		$spasi=explode('_',$form); 
		$jumspasi=count($spasi);
		if ($jumspasi<=1) {  $formcontent=$form; $tabelcontent=$form; } else { list ($formcontent,$tabelcontent)=explode('_',$form); $tabelcontent = str_replace($formcontent . '_', '', $form);  }
		if ($formcontent=='form') { $tipe='text'; }
		elseif ($formcontent=="author") { $tipe="author"; }
		elseif ($formcontent=="bulan") { $tipe="bulan"; }
		elseif ($formcontent=="content") { $tipe="texteditor"; }
		elseif ($formcontent=="penjelasan") { $tipe="texteditor"; }
		elseif ($formcontent=="description") { $tipe="texteditor"; }
		elseif ($formcontent=="problem") { $tipe="texteditor"; }
		elseif ($formcontent=="gain") { $tipe="texteditor"; }
		elseif ($formcontent=="solution") { $tipe="texteditor"; }
		elseif ($formcontent=="benefit") { $tipe="texteditor"; }
		elseif ($formcontent=="feature") { $tipe="texteditor"; }
		elseif ($formcontent=="instructor") { $tipe="texteditor"; }
		elseif ($formcontent=="term") { $tipe="texteditor"; }
		elseif ($formcontent=="work") { $tipe="texteditor"; }
		elseif ($formcontent=="organization") { $tipe="texteditor"; }
		elseif ($formcontent=="award") { $tipe="texteditor"; }
		elseif ($formcontent=="education") { $tipe="texteditor"; }
		elseif ($formcontent=="about") { $tipe="texteditor"; }
		
		elseif ($formcontent=="address") { $tipe="textarea"; }
		elseif ($formcontent=="comment") { $tipe="textarea"; }
		elseif ($formcontent=="testimoni") { $tipe="textarea"; }
		elseif ($formcontent=="meta") { $tipe="textarea"; }
		elseif ($formcontent=="note") { $tipe="textarea"; }	
		elseif ($formcontent=="offer") { $tipe="textarea"; }	
		elseif ($formcontent=="detail") { $tipe="textarea"; }
		elseif ($formcontent=="spesification") { $tipe="textarea"; }
		elseif ($formcontent=="answer") { $tipe="textarea"; }
		
		elseif ($formcontent=="picture") { $tipe="picture"; }
		elseif ($formcontent=="thumbnail") { $tipe="picture"; }
		elseif ($formcontent=="picture1") { $tipe="picture"; }
		elseif ($formcontent=="picture2") { $tipe="picture"; }
		elseif ($formcontent=="picture3") { $tipe="picture"; }
		elseif ($formcontent=="picture4") { $tipe="picture"; }
		elseif ($formcontent=="picture5") { $tipe="picture"; }
		elseif ($formcontent=="picture6") { $tipe="picture"; }
		elseif ($formcontent=="picture7") { $tipe="picture"; }
		elseif ($formcontent=="picture8") { $tipe="picture"; }
		elseif ($formcontent=="picture9") { $tipe="picture"; }
		elseif ($formcontent=="logo") { $tipe="picture"; }
		elseif ($formcontent=="cover") { $tipe="picture"; }
		elseif ($formcontent=="favicon") { $tipe="picture"; }
		elseif ($formcontent=="banner") { $tipe="picture"; }
		
		elseif ($formcontent=="file") { $tipe="file"; }
		elseif ($formcontent=="id") { $tipe="id"; }
		elseif ($formcontent=="up") { $tipe="up"; }
		elseif ($formcontent=="no") { $tipe="no"; }
		elseif ($formcontent=="password") { $tipe="password"; }
		elseif ($formcontent=="date") { $tipe="date"; }
		elseif ($formcontent=="show") { $tipe="optiontampil"; }
		elseif ($formcontent=="publish") { $tipe="publish"; }
		elseif ($formcontent=="target") { $tipe="optiontarget"; }
		elseif ($formcontent=="newsletter") { $tipe="newsletter"; }
		elseif ($formcontent=="offers") { $tipe="offers"; }	
        elseif ($formcontent=="toaster") { $tipe="optionadatidak"; }	
		elseif ($formcontent=="pool") { $tipe="optionadatidak"; }
		elseif ($formcontent=="parking") { $tipe="optionadatidak"; }
		elseif ($formcontent=="cabletv") { $tipe="optionadatidak"; }
		elseif ($formcontent=="bedding") { $tipe="optionadatidak"; }
		elseif ($formcontent=="dishwater") { $tipe="optionadatidak"; }
		elseif ($formcontent=="lift") { $tipe="optionadatidak"; }
		elseif ($formcontent=="balcony") { $tipe="optionadatidak"; }
		elseif ($formcontent=="ac") { $tipe="optionadatidak"; }
		elseif ($formcontent=="sub") { $tipe="no"; }
		else { $tipe="text"; }
		if ($form=="no_gallery_category") { $tabelcontent="gallery_category"; } 
		elseif ($form=="no_service_category") { $tabelcontent="service_category"; } 
		elseif ($form=="no_course_bab") { $tabelcontent="course_bab"; } 
		elseif ($form=="no_faq_category") { $tabelcontent="faq_category"; } 
		elseif ($form=="id_customer_kategori") { $tabelcontent="customer_kategori"; }
		elseif ($form=="sub_location") { $tabelcontent="sub_location"; } 
		elseif ($form=="no_location") { $tabelcontent="location"; } 
		elseif ($form=="id_properties") { $tabelcontent="properties"; } 
		elseif ($form=="no_properties_category") { $tabelcontent="properties_category"; } 
		elseif ($form=="no_sertifikat_properties") { $tabelcontent="sertifikat_properties"; } 
		else {  $tabelcontent= $tabelcontent; }
		
		if ($tipe=="text") {  ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content"><input type="text" name="<?php echo $form;?>" id="<?php echo $form;?>" class="form-control" style="width:100%;" value="<?php echo $value;?>"/></div>
			</div><?php
		}
		elseif ($tipe=="author") {  
			if ($value=="") { $valueauthor=$_SESSION['uname']; } else { $valueauthor=$value; } ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content"><input type="text" name="<?php echo $form;?>" id="<?php echo $form;?>" value="<?php echo $valueauthor;?>" class="form-control" style="width:100%;"/></div>
			</div><?php
		}
		elseif ($tipe=="password") {  ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content"><input type="password" name="<?php echo $form;?>" id="<?php echo $form;?>"  class="form-control" style="width:100%;"/></div>
			</div><?php
		}
		elseif ($tipe=="texteditor") {  ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content"><textarea name="<?php echo $form;?>" id="editor" class="form-control" style="width:100%; height:300px;"><?php echo $value;?></textarea></div>
			</div><?php
		}
		elseif ($tipe=="textarea") { ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content"><textarea name="<?php echo $form;?>" id="<?php echo $form;?>" class="form-control" style="width:100%; height:80px; text-transform:none;"><?php echo $value;?></textarea></div>
			</div><?php
		}
		elseif ($tipe=="picture") { ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content"><?php 
					if ($value=="") { ?>
						<input name="picturelama" type="hidden" value="<?php echo $value;?>"/>
						<input type="file" name="<?php echo $form;?>" id="<?php echo $form;?>" class="form-control" style="width:100%; padding:0px;"/><?php 
					} 
					else { ?>
						<img src="<?php echo URL_UPLOAD;?>image.php/<?php echo $value;?>?width=90&nocache&quality=100&image=<?php echo URL_PICTURE.$value;?>" width="90" height="64" align="left" alt="<?php echo $value;?>" style="margin:0px 10px 5px 0px;"/>
						<input name="picturelama" type="hidden" value="<?php echo $value;?>"/>
						<input type="file" name="<?php echo $form;?>" id="<?php echo $form;?>" class="form-control" style="width:100%; margin-top:5px; padding:0px;"/>
						<small>Kosongkon fom ini jika tidak ada perubahan gambar</small><?php 
					} ?>
				</div>
			</div><?php
		}
		elseif ($tipe=="file") {  ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content">
					<input type="file" name="file" id="file" style="width:100%; padding:0px;"/><?php 
					if ($value=="") { ?><input name="filelama" type="hidden"  class="form-control" value="<?php echo $value;?>"/><?php } 
					else { ?>
						<input name="filelama" type="hidden" class="form-control" value="<?php echo $value;?>" />
						<small>Kosongkon fom ini jika tidak ada perubahan file</small><?php 
					} ?>
				</div>
			</div><?php
		}
		elseif ($tipe=="bulan") {
			?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content">
					<select name="<?php echo $form;?>" class="form-control" style="width:100%;">
						<option value="1">Januari</option>
						<option value="2">Februari</option>
						<option value="3">Maret</option>
						<option value="4">April</option>
						<option value="5">Mei</option>
						<option value="6">Juni</option>
						<option value="7">Juli</option>
						<option value="8">Agustus</option>
						<option value="9">September</option>
						<option value="10">Oktober</option>
						<option value="11">November</option>
						<option value="12">Desember</option>
					</select>
				</div>
			</div>
		<?php }
		elseif ($tipe=="no" or $tipe=="id") { 
			if ($tipe=="id") { $colom="no"; $formdata="title"; } else { $colom="no"; $formdata="name"; }?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content">
					<select name="<?php echo $form;?>" class="form-control" style="width:100%;"><?php
						$tabelcat=$tabelcontent;
						if ($value=="") { ?>
							<option value="0">- Tidak Dipilih -</option><?php
							$qid=mysqli_query($this->koneksi,"SELECT * FROM $tabelcat ORDER BY $formdata");
							while($did=mysqli_fetch_array($qid)){ ?><option value="<?php echo $did[$colom];?>"><?php echo $did[$formdata];?></option><?php }
						} 
						else {
							if ($value==0) { ?>
								<option value="0">- Tidak Dipilih -</option><?php
							}
							else {
								$qidid=mysqli_query($this->koneksi,"SELECT * FROM $tabelcat WHERE $colom='$value' LIMIT 0,1");
								$didid=mysqli_fetch_array($qidid); ?>
								<option value="<?php echo $didid[$colom];?>"><?php echo $didid[$formdata];?></option><?php
							}
							$qid=mysqli_query($this->koneksi,"SELECT * FROM $tabelcat ORDER BY $formdata");
							while($did=mysqli_fetch_array($qid)){ ?><option value="<?php echo $did[$colom];?>"><?php echo $did[$formdata];?></option><?php } ?>
							<option value="0">- Tidak Dipilih -</option><?php
						} ?>
					</select>
				</div>
			</div><?php
		}
		elseif ($tipe=="optiontarget") {  ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content">
					<select name="target" class="form-control" style="width:100%;"><?php
						if ($value=="") { ?><option value="_parent">Buka Di Halaman Yang Sama</option><option value="_blank">Buka Di Tab Yang Baru</option><?php }
						elseif ($value=="_parent"){ ?><option value="_parent">Buka Di Halaman Yang Sama</option><option value="_blank">Buka Di Tab Yang Baru</option><?php } 
						else { ?><option value="_blank">Buka Di Tab Yang Baru</option><option value="_parent">Buka Di Halaman Yang Sama</option><?php } ?>
					</select>
				</div>
			</div><?php
		}
		elseif ($tipe=="optiontampil") {?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content">
					<select name="<?php echo $form;?>" class="form-control" style="width:100%;"><?php 
						if ($value==""){ ?><option value="1">Tampilkan</option><option value="0">Sembunyikan</option><?php } 
						elseif ($value==1) { ?><option value="1">Tampilkan</option><option value="0">Sembunyikan</option><?php } 
						elseif ($value==0) { ?><option value="0">Sembunyikan</option><option value="1">Tampilkan</option><?php } ?>
					</select>
				</div>
			</div><?php
		}
		elseif ($tipe=="optionadatidak") {?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content">
					<select name="<?php echo $form;?>" class="form-control" style="width:100%;"><?php 
						if ($value==""){ ?><option value="1">Ada</option><option value="0">Tidak</option><?php } 
						elseif ($value==1) { ?><option value="1">Ada</option><option value="0">Tidak</option><?php } 
						elseif ($value==0) { ?><option value="0">Tidak</option><option value="1">Ada</option><?php } ?>
					</select>
				</div>
			</div><?php
		}
		elseif ($tipe=="publish") {?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content">
					<select name="<?php echo $form;?>" class="form-control" style="width:100%;"><?php 
						if ($value==""){ ?><option value="1">Tampilkan</option><option value="0">Sembunyikan</option><?php } 
						elseif ($value==1) { ?><option value="1">Tampilkan</option><option value="0">Sembunyikan</option><?php } 
						elseif ($value==0) { ?><option value="0">Sembunyikan</option><option value="1">Tampilkan</option><?php } ?>
					</select>
				</div>
			</div><?php
		}
		elseif ($tipe=="newsletter" or $tipe="offers") {?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content">
					<select name="<?php echo $form;?>" class="form-control" style="width:100%;"><?php 
						if ($value==""){ ?><option value="1">Ya</option><option value="0">Tidak</option><?php } 
						elseif ($value==1) { ?><option value="1">Ya</option><option value="0">Tidak</option><?php } 
						elseif ($value==0) { ?><option value="0">Tidak</option><option value="1">Ya</option><?php } ?>
					</select>
				</div>
			</div><?php
		}
		elseif ($tipe=="date") { ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content"><input type="text" name="<?php echo $form;?>" id="<?php echo $form;?>" class="form-control" style="width:100%;" value="<?php echo $value; ?>"/></div>
			</div><?php
		}
		else { ?>
			<div id="view">
				<div class="view_label"><?php echo $label;?></div>
				<div class="view_dot">:</div>
				<div class="view_content"><input type="text" class="form-control" name="<?php echo $form;?>" id="<?php echo $form; ?>" style="width:100%;" value="<?php echo $value; ?>"/></div>				
			</div><?php
		}
	}
	
	
	function notify ($tipe) {
		$link=$this->link;
		$menu=$this->menu;
		$linkmod=URL_DOMAIN.$menu."/".$link;
		if($tipe=='empty'){ /*PAKE*/ ?>
			<div class="alert alert-warning" role="alert">
				Maaf, Data yang Anda cari tidak tersedia.
			</div><?php
		}
		elseif($tipe=='delete'){ /*PAKE*/ ?>
			<div class="alert alert-success" role="alert">
				Data berhasil dihapus
			</div><?php
		}
		elseif($tipe=="img_empty"){ ?>
			<div class="alert alert-warning" role="alert">
				Silahkan Pilih Gambar Yang Anda Masukkan
			</div><?php
		}
		elseif($tipe=="img_format"){ ?>
			<div class="alert alert-warning" role="alert">
				Format Gambar Salah, Format Gambar Yang Diijinkan adalah (JPG, PNG, GIF)
			</div><?php
		}
		elseif($tipe=="image_dimention"){ ?>
			<div class="alert alert-warning" role="alert">
				Resolusi Gambar Terlalu Besar, Resolusi Gambar Yang Diijinkan maksimal 1000 X 1000 px
			</div><?php
		}
		elseif($tipe=="image_size"){ ?>
			<div class="alert alert-warning" role="alert">
				Ukuran Gambar Terlalu Besar, Ukuran Gambar Yang Diijinkan maksimal 1 MB
			</div><?php
		}
		elseif($tipe=="file_empty"){ ?>
			<div class="alert alert-warning" role="alert">
				Silahkan Masukkan File Anda
			</div><?php
		}
		elseif($tipe=="file_size"){ ?>
			<div class="alert alert-warning" role="alert">
				Ukuran File Terlalu Besar, Ukuran File Yang Diijinkan maksimal 10 MB
			</div><?php
		}
		elseif($tipe=="nosession"){ ?>
			<div class="alert alert-success" role="alert">
				Maaf, Session Telah Habis. Silahkan Ulangi Lagi
			</div><?php
		}
		elseif($tipe=="save_ok"){ ?>
			<div class="alert alert-success" role="alert">
				Selamat, Data Berhasil Disimpan
				<br/>
				<a href="<?php echo $linkmod;?>/" title="Kembali">Kembali</a>
			</div><?php
		}
		elseif($tipe=="password_ok"){ ?>
			<div class="alert alert-success" role="alert">
				Selamat, Password Berhasil Disimpan
				<br/>
				<a href="<?php echo $linkmod;?>/" title="Kembali">Kembali</a>
			</div><?php
		}
		elseif($tipe=="save_fail"){ ?>
			<div class="alert alert-danger" role="alert">
				Maaf, Data Gagal Disimpan
				<br/>
				<a href="<?php echo $linkmod;?>/" title="Kembali">Kembali</a>
			</div><?php
		}
		elseif($tipe=="edit_ok"){ ?>
			<div class="alert alert-success" role="alert">
				Selamat, Data Berhasil Disimpan
				<br/>
				<a href="<?php echo $linkmod;?>/" title="Kembali">Kembali</a>
			</div><?php
		}
		
		elseif($tipe=="email_notvalid"){ ?>
			<div class="alert alert-danger" role="alert">
				Maaf, Email Anda Tidak Valid
				<br/>
				<a href="<?php echo $linkmod;?>/" title="Kembali">Kembali</a>
			</div><?php
		}
		else {
		}
	}	
	
	
	function bulan($bulan) { 
		if ($bulan=="1") { $this->bulannama="Januari"; } elseif ($bulan=="2") { $this->bulannama="Februari"; }
		elseif ($bulan=="3") { $this->bulannama="Maret"; } elseif ($bulan=="4") { $this->bulannama="April"; }
		elseif ($bulan=="5") { $this->bulannama="Mei"; } elseif ($bulan=="6") { $this->bulannama="Juni"; }
		elseif ($bulan=="7") { $this->bulannama="Juli"; } elseif ($bulan=="8") { $this->bulannama="Agustus"; }
		elseif ($bulan=="9") { $this->bulannama="September"; } elseif ($bulan=="10") { $this->bulannama="Oktober"; }
		elseif ($bulan=="11") { $this->bulannama="November"; } elseif ($bulan=="12") { $this->bulannama="Desember"; }
		else { $this->bulannama=""; } 
	}
	
	
	
}
?>