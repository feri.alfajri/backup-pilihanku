<?php   
class edit extends model { 
	
	function action_edit ($form,$title_baru) { 
		$form=explode(",",$form);
		$jumkoma=count($form);
		$ubah="";
		for($i=0;$i<$jumkoma;$i++){
			$spasi=explode("_",$form[$i]); 
			$jumspasi=count($spasi);
			if ($jumspasi<=1) { $forminput=$form[$i]; } else { list($forminput,$contentform)=explode("_",$form[$i]); }
			if ($forminput=="content") { $contentan=str_replace("'","",$_POST[$form[$i]]); $contentan=str_replace('"','',$contentan); }
			elseif ($forminput=="meta") { $contentan=str_replace("'","",$_POST[$form[$i]]); }
			elseif ($forminput=="password") { $contentan=md5($_POST[$form[$i]]); }
			elseif ( $forminput=="thumbnail") { $contentan=$title_baru; }
			elseif ($forminput=="picture") { $contentan=$title_baru; }
			elseif ( $forminput=="picture1") { $contentan=$title_baru; }
		    elseif ( $forminput=="picture2") { $contentan=$title_baru; }
		    elseif ( $forminput=="picture3") { $contentan=$title_baru; }
		    elseif ( $forminput=="picture4") { $contentan=$title_baru; }
		    elseif ( $forminput=="picture5") { $contentan=$title_baru; }
			elseif ($forminput=="favicon") { $contentan=$title_baru; }
			elseif ($forminput=="logo") { $contentan=$title_baru; }
			elseif ($forminput=="file") { $contentan=$title_baru; }
			else { $contentan=$_POST[$form[$i]]; $contentan=str_replace("'","",$contentan);   }
			$ubah=$ubah.$form[$i]."='".$contentan."', ";
		}
		$this->actionubah=$ubah;
	}


	function showedit ($titlemod,$tabel,$jenisinput,$onclick,$labelinput,$forminput,$syarat) {
		$admin=new admin();
		$admin->get_variable();
		$admin->koneksi();
		$query=mysqli_query($admin->koneksi,"SELECT * FROM $tabel $syarat LIMIT 0,1");
		$data=mysqli_fetch_array($query);
		$no=$data['no'];
		if ($no=="") {  $admin->notify("empty");  }
		else { ?>
			<h4>Ubah <?php echo $titlemod;?></h4><?php
			if (empty ($_POST['process'])) { ?>					
				<form action="" method="post" enctype="multipart/form-data" name="titleform">
					<input name="process" type="hidden" value="edit">
					<input name="no" type="hidden" value="<?php echo $no;?>"/><?php
						$label=explode(",",$labelinput);
						$form=explode(",",$forminput);
						$jumkoma=count($form);
						for($i=0;$i<$jumkoma;$i++){ $admin->form($label[$i],$form[$i],"edit",$data[$form[$i]],""); echo "\n"; } ?>
						<input type="submit" name="submit" value="SIMPAN" onclick="return <?php echo $onclick;?>();" class="btn btn-primary" style="margin:5px 0px;"/>
						<input type="button" name="back" value="KEMBALI" onclick="self.history.back();" class="btn btn-default" style="margin:5px 0px;"/>
				</form><?php
				$randcode=rand(111111,999999); 
				$_SESSION['code']=$randcode;
			}
			elseif ($_POST['process']=="edit") {
				$no=strip_tags($_POST['no'])/1;
				if (empty ($_SESSION['code'])) {  $admin->notify("edit_ok"); }
				else { 
					if (empty($_SESSION['code'])) { } else { unset($_SESSION['code']); }
					if ($jenisinput=="") {
						$this->action_edit($forminput,"");
						$ubah=$this->actionubah;	
						mysqli_query($admin->koneksi,"UPDATE $tabel SET $ubah no='$no' $syarat");
						$admin->notify("edit_ok");
					}
					elseif ($jenisinput=="picture" or $jenisinput=="logo" or $jenisinput=="favicon") { 
						if (empty($_FILES[$jenisinput]['tmp_name'])) { 
							$this->action_edit($forminput,$_POST['picturelama']);
							$ubah=$this->actionubah;		
							mysqli_query($admin->koneksi,"UPDATE $tabel SET $ubah no='$no' $syarat");
							$admin->notify("edit_ok");
						}
						else { 
							$picture=$_FILES[$jenisinput]['tmp_name'];
							$picture_name=$_FILES[$jenisinput]['name'];
							$picture_size=$_FILES[$jenisinput]['size'];
							$picture_type=$_FILES[$jenisinput]['type'];
							$acak=rand(00000000,99999999);
							$title_baru=$acak.$picture_name;
							$title_baru=str_replace(" ","",$title_baru);
							$picture_dimensi=getimagesize($picture);
							if ($picture_type!="image/jpg" and $picture_type!="image/png" and $picture_type!="image/gif" and $picture_type!="image/jpeg"){  $admin->notify("img_format"); } 
							elseif ($picture_dimensi['0']>"2000" or $picture_dimensi['1']>"2000") {  $admin->notify("image_dimention");  } 
							else {
								$this->action_edit($forminput,$title_baru);
								$ubah=$this->actionubah;
								mysqli_query($admin->koneksi,"UPDATE $tabel SET $ubah no='$no' $syarat");
								$admin->notify("edit_ok");
								copy ($picture, DIR_PICTURE.$title_baru);
							}
						}
					}
					elseif ($jenisinput=="file") {
						if (empty($_FILES['file']['tmp_name'])) {
							$this->action_edit($forminput,$_POST['filelama']);
							$ubah=$this->actionubah;
							mysqli_query($admin->koneksi,"UPDATE $tabel SET $ubah no='$no' $syarat"); 								
							$admin->notify("edit_ok");
						}
						else {
							$file=$_FILES['file']['tmp_name'];
							$file_name=$_FILES['file']['name'];
							$file_size=$_FILES['file']['size'];
							$file_type=$_FILES['file']['type'];
							$acak=rand(00000000,99999999);
							$title_baru=$acak.$file_name;
							$title_baru=str_replace(" ","",$title_baru);
							if ($file_size>"100000000") {  $admin->notify("image_size");  } 
							else {
								$this->action_edit($forminput,$title_baru);
								$ubah=$this->actionubah;
								mysqli_query($admin->koneksi,"UPDATE $tabel SET $ubah no='$no' $syarat"); 									
								$admin->notify("edit_ok");
								copy ($file, DIR_FILE.$title_baru);
							}
						}
					}
				}
			}
		}
	}
	
		
	

}
?>