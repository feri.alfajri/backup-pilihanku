<?php
class Resize  { 
	function ubah($source,$filename, $width, $height) {
		if (!is_file(DIR_UPLOAD.'picture/'.$filename)) {
			return;
		}
		$extension = pathinfo($filename, PATHINFO_EXTENSION);
		if (!is_dir(DIR_UPLOAD."imagecache")){
			mkdir(DIR_UPLOAD."imagecache", 0755);
		}
		$old_image = $filename;
		$new_image ='imagecache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;
		if (!is_file( $new_image) || (filectime(DIR_UPLOAD.'picture/'. $old_image) > filectime( $new_image))) {			
			list($width_orig, $height_orig) = getimagesize(DIR_UPLOAD.'picture/' . $old_image);
			if ($width_orig != $width || $height_orig != $height) {
				$image = new Image(DIR_UPLOAD.'picture/'. $old_image);
				$image->resize($width, $height);
				$image->save(DIR_UPLOAD.$new_image);
			} 
			else {
				copy(DIR_UPLOAD.'picture/'. $old_image,DIR_UPLOAD.$new_image);
			}
		}		
		return $source.$new_image;
	}
}
